export default [
  {
    path: '/user',
    component: '../layouts/UserLayout',
    routes: [
      {
        path: '/user',
        redirect: '/user/login',
      },
      {
        path: '/user/login',
        name: 'login',
        component: './User/Login',
      },
      {
        path: '/user/register',
        name: 'register',
        component: './User/Register',
      },
      {
        path: '/user/register-result',
        name: 'register.result',
        component: './User/RegisterResult',
      },
    ],
  },

  {
    path: '/',
    component: '../layouts/BasicLayout',
    Routes: ['src/pages/Authorized'],
    routes: [
      {
        path: "/",
        redirect: "/trangchu",
        authority: ['USER']
      },
      {
        path: '/trangchu',
        name: 'Trang chủ',
        icon: 'home',
        component: './TrangChu',
        authority: ['USER'],
      },
      {
        name: "Quỹ",
        authority: ['USER', 'ADMIN'],
        path: "/quy",
        icon: "dollar",
        routes: [
          {
            name: "Phiếu thu",
            path: '/quy/thutien',
            component: "./Quy/ThuTien"
          },
          {
            name: "Phiếu chi",
            path: '/quy/chitien',
            component: "./Quy/ChiTien"
          }
        ]
      },
      {
        name: "Bán hàng",
        authority: ['USER', 'ADMIN'],
        path: "/banhang",
        icon: "home",
        routes: [
          {
            name: "Danh sách đơn hàng",
            path: '/banhang/donhang',
            component: "./BanHang/DonHang"
          }
        ]
      },
      {
        name: "Quản lý kho hàng",
        authority: ['USER', 'ADMIN'],
        path: "/kho",
        icon: "home",
        routes: [
          {
            name: "Danh sách đơn mua",
            path: '/kho/donmua',
            component: "./MuaHang"
          },
          {
            name: "Nhập kho",
            path: '/kho/nhapxuat',
            component: "./Kho/NhapXuatKho"
          }
        ]
      },
      {
        name: "Thông tin lương",
        // authority: ['USER', 'ADMIN'],
        path: "/luong",
        icon: "global",
        routes: [
          {
            name: "Bảng chấm công",
            path: '/luong/bangchamcong',
            component: "./Luong/BangChamCong/"
          },
          {
            name: "Thanh toán lương",
            path: '/luong/thanhtoan',
            component: "./Luong/ThanhToanLuong/"
          },
        ]
      },
      {
        name: "Danh mục",
        authority: ['USER', 'ADMIN'],
        path: "/admin",
        icon: "global",
        routes: [
          {
            name: "Danh sách nhân viên",
            path: '/admin/nhanvien',
            component: "./Admin/NhanVien/"
          },
          {
            name: "Danh sách chi nhánh",
            path: '/admin/chinhanh',
            component: "./Admin/ChiNhanh/"
          },
          {
            name: "Danh sách kho hàng",
            path: '/admin/khohang',
            component: "./Kho/KhoHang"
          },
          {
            name: "Danh sách hàng hoá",
            path: '/admin/hanghoa',
            component: "./Kho/HangHoa"
          }
        ]
      },
      // {
      //   name: 'Sức khỏe',
      //   path: '/suckhoe',
      //   icon: 'plus-square',
      //   authority: ['USER'],
      //   routes: [
      //     {
      //       name: 'Khai báo sức khỏe',
      //       icon: 'plus-square',
      //       path: '/suckhoe/khaibaosuckhoe',
      //       component: './KhaiBaoSucKhoe',
      //     }, // {
      //     //   path: '/suckhoe/theongay',
      //     //   name: 'Khai báo sức khỏe theo ngày',
      //     //   icon: 'container',
      //     //   component: './DaKhaiBao',
      //     // },
      //     {
      //       name: 'Khai báo cách ly',
      //       path: '/suckhoe/khaibaocachly',
      //       icon: 'alert',
      //       component: './KhaiBaoCachLy',
      //     },
      //   ],
      // },
      // {
      //   name: 'VP một cửa',
      //   path: '/motcua',
      //   icon: 'plus-square',
      //   authority: ['admin'],
      //   routes: [
      //     {
      //       name: 'Bảng điểm',
      //       path: '/motcua/bangdiem',
      //       component: './MotCua/BangDiem',
      //     },
      //   ],
      // },
      // {
      //   path: '/ThongTinChung',
      //   name: 'Thông tin chung',
      //   icon: 'global',
      //   authority: ['admin'],
      //   routes: [
      //     {
      //       path: '/ThongTinChung/BaiVietChung',
      //       name: 'Bài viết chung',
      //       component: './ThongTinChung/BaiVietChung',
      //     },
      //     {
      //       path: '/ThongTinChung/cocau',
      //       name: 'Cơ cấu tổ chức',
      //       component: './CoCauToChuc',
      //     },
      //   ],
      // },
      // {
      //   path: '/TaiLieu',
      //   name: 'Quản lý tài liệu',
      //   icon: 'paper-clip',
      //   component: './TaiLieu/TaiLieu',
      //   authority: ['teacher'],
      // },
      // {
      //   path: '/DanhSachLop',
      //   name: 'Danh sách lớp giảng dạy',
      //   icon: 'unordered-list',
      //   component: './LopHoc/DanhSachLopGV',
      //   authority: ['teacher'],
      // },
      // {
      //   path: '/DanhSachLop/ThongTinLop',
      //   name: 'Danh sách lớp giảng dạy',
      //   hideInMenu: true,
      //   component: './LopHoc/ThongTinLop',
      //   authority: ['teacher'],
      // },
      // {
      //   path: '/admin/taikhoan',
      //   name: 'Quản lý tài khoản',
      //   icon: 'usergroup-add',
      //   authority: ['admin'],
      //   routes: [
      //     {
      //       path: '/admin/taikhoan/sinhvien',
      //       name: 'Tài khoản sinh viên',
      //       component: './TaiKhoan/SinhVien',
      //     },
      //     {
      //       path: '/admin/taikhoan/giangvien',
      //       name: 'Tài khoản giảng viên',
      //       component: './TaiKhoan/GiangVien',
      //     },
      //   ],
      // },
      // {
      //   path: '/admin/danhmuc',
      //   name: 'Danh mục',
      //   icon: 'container',
      //   authority: ['admin'],
      //   routes: [
      //     {
      //       path: '/admin/danhmuc/donvi',
      //       name: 'Quản lý các đơn vị',
      //       component: './DanhMuc/DonVi',
      //       authority: ['admin'],
      //     },
      //     {
      //       path: '/admin/danhmuc/loaitintuc',
      //       name: 'Quản lý loại tin tức',
      //       component: './DanhMuc/LoaiTinTuc',
      //       authority: ['admin'],
      //     },
      //     {
      //       path: '/admin/danhmuc/monhoc',
      //       name: 'Quản lý môn học',
      //       component: './DanhMuc/MonHoc',
      //       authority: ['admin'],
      //     },
      //     {
      //       path: '/admin/danhmuc/nhomnguoidung',
      //       name: 'Quản lý nhóm sinh viên',
      //       component: './DanhMuc/NhomNguoiDung',
      //       authority: ['admin'],
      //     },
      //   ],
      // },
      // {
      //   path: '/admin/tintuc',
      //   name: 'Quản trị tin tức',
      //   icon: 'notification',
      //   component: './TinTuc',
      //   authority: ['admin'],
      // },
      // {
      //   path: '/admin/vanban',
      //   name: 'Quản trị văn bản',
      //   icon: 'file-pdf',
      //   component: './VanBan',
      //   authority: ['admin'],
      // },
      // {
      //   path: '/admin/vanban/:id',
      //   name: 'Thông tin văn bản',
      //   icon: 'file-pdf',
      //   hideInMenu: true,
      //   component: './VanBan/$id.tsx',
      //   authority: ['admin'],
      // },
      // {
      //   path: '/admin/thongbao',
      //   name: 'Quản lý thông báo',
      //   authority: ['admin'],
      //   icon: 'bell',
      //   component: './ThongBao',
      // },
      // {
      //   path: '/admin/khaosatnew',
      //   name: 'Quản trị khảo sát',
      //   icon: 'bar-chart',
      //   component: './KhaoSatNew',
      //   authority: ['admin'],
      // },
      // {
      //   path: '/admin/phanhoi',
      //   name: 'Phản hồi',
      //   icon: 'question',
      //   authority: ['admin'],
      //   routes: [
      //     {
      //       path: '/admin/phanhoi/hoidap',
      //       name: 'Hỏi đáp',
      //       icon: 'question-circle',
      //       routes: [
      //         {
      //           path: '/admin/phanhoi/hoidap/sinhvien',
      //           name: 'Sinh viên',
      //           component: './PhanHoi/HoiDap/SinhVien',
      //         },
      //         {
      //           path: '/admin/phanhoi/hoidap/giangvien',
      //           name: 'Giảng viên',
      //           component: './PhanHoi/HoiDap/GiangVien',
      //         },
      //       ],
      //     },
      //     {
      //       path: '/admin/phanhoi/hotro',
      //       name: 'Hỗ trợ kĩ thuật',
      //       icon: 'reddit',
      //       component: './PhanHoi/HoTro',
      //     },
      //   ],
      // },

      // {
      //   path: '/admin/import',
      //   name: 'Import dữ liệu',
      //   authority: ['admin'],
      //   icon: 'file-add',
      //   component: './Import',
      // },
      {
        hideInMenu: true,
        name: 'user',
        icon: 'user',
        path: '/account',
        routes: [
          {
            path: '/account/center',
            name: 'profile',
            component: './Account/Center/Center',
            routes: [
              {
                path: '/account/center',
                redirect: '/account/center/editProfile',
              },
              {
                path: '/account/center/articles',
                component: './Account/Center/Articles',
              },
              {
                path: '/account/center/applications',
                component: './Account/Center/Applications',
              },
              {
                path: '/account/center/projects',
                component: './Account/Center/Projects',
              },
              {
                path: '/account/center/changePassword',
                component: './Account/Center/ChangePassword',
              },
              {
                path: '/account/center/editProfile',
                component: './Account/Center/EditProfile',
              },
            ],
          },
          {
            path: '/account/settings',
            name: 'setting',
            component: './Account/Settings/Info',
            routes: [
              {
                path: '/account/settings',
                redirect: '/account/settings/base',
              },
              {
                path: '/account/settings/base',
                component: './Account/Settings/BaseView',
              },
              {
                path: '/account/settings/security',
                component: './Account/Settings/SecurityView',
              },
              {
                path: '/account/settings/binding',
                component: './Account/Settings/BindingView',
              },
              {
                path: '/account/settings/notification',
                component: './Account/Settings/NotificationView',
              },
            ],
          },
        ],
      },
      {
        component: '404',
      },
    ],
  },
];
