import React from 'react';
import { Popover, Button, Divider, Icon, Popconfirm } from 'antd';

interface Props {
    record: object,
    handleEdit: Function,
    handleDel: Function,
    actions?: Array<object>,
    functions?: object
}

interface State {
    visible: boolean
}

class TagPopover extends React.Component<Props, State> {
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
        };

        this.hiden = this.hiden.bind(this);
    }

    handleVisibleChange = visible => {
        this.setState({ visible });
    };

    hiden() {
        this.setState({
            visible: false,
        });
    }

    render() {
        const { record, handleEdit, handleDel, actions, functions } = this.props;

        const list = record => (
            <React.Fragment>
                {actions.map(item => {
                    const { node, click: func } = item;
                    return (
                        <React.Fragment>
                            <node.type
                                {...node.props}
                                onClick={() => {
                                    this.hiden();
                                    return functions[func](record);
                                }}
                            />
                            <Divider type="vertical" />
                        </React.Fragment>
                    );
                })}

                <Button
                    type="primary"
                    shape="circle"
                    icon="edit"
                    onClick={() => {
                        this.hiden();
                        return handleEdit(record);
                    }}
                    title="Chỉnh sửa"
                />

                <Divider type="vertical" />

                <Popconfirm title="Bạn có chắc muốn xóa?"
                    onConfirm={() => {
                        this.hiden();
                        handleDel(record._id)
                    }}
                >
                    <Button type="danger" shape="circle" icon="delete" title="Xóa" />
                </Popconfirm>
            </React.Fragment>
        );

        return (
            <Popover
                content={list(record)}
                visible={this.state.visible}
                onVisibleChange={this.handleVisibleChange}
                title="Thao tác"
            >
                <Button type="primary">
                    <Icon type="edit" />
                </Button>
            </Popover>
        );
    }
}

export default TagPopover;
