import React from 'react';
import { Drawer } from 'antd';
import { connect } from 'dva';

@connect(state => ({
  state,
}))
class SimpleDrawer extends React.Component {
  onClose = () => {
    const field = this.props.field || "showDrawer";
    this.props.dispatch({
      type: `${this.props.name}/changeState`,
      payload: {
        [field]: false,
      },
    });
  };

  render() {
    const { name, state } = this.props; // Name của models. Ex: nghiquyet
    const models = state[name];    
    const field = this.props.field || "showDrawer";
    const showDrawer = models[field];
    return (
      <Drawer
        width="60%"
        onClose={this.onClose}
        visible={!!showDrawer}
        closable={false}
        destroyOnClose
        mask
      >
        {this.props.children}
      </Drawer>
    );
  }
}

export default SimpleDrawer;
