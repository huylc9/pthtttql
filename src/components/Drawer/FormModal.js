import React from 'react';
import { Modal } from 'antd';
import { connect } from 'dva';
import { withRouter } from 'umi';
import router from 'umi/router';
import queryString from 'query-string';

@connect(state => ({
  state
}))
class SimpleModal extends React.Component {
  onClose = () => {
    const field = this.props.field || "showDrawer";
    this.props.dispatch({
      type: `${this.props.name}/changeState`,
      payload: {
        [field]: false
      }
    });
  };

  render() {
    const { name, state } = this.props; // Name của models. Ex: nghiquyet
    const models = state[name];
    const field = this.props.field || "showDrawer";
    const showModal = models[field];
    return (
      <Modal
        width="80%"
        onCancel={() => this.onClose()}
        onOk={() => this.onClose()}
        visible={!!showModal}
        footer={null}
        style={{ marginLeft: "10%", padding: 0 }}
      >
        {this.props.children}
      </Modal>
    );
  }
}

export default SimpleModal;
