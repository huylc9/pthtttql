export default {
  'app.pwa.offline': 'Ban đang ở chế độ Offline',
  'app.pwa.serviceworker.updated': 'Dữ liệu mới đã sẵn sàng',
  'app.pwa.serviceworker.updated.hint': 'Bấm nút để cập nhật lại trang web',
  'app.pwa.serviceworker.updated.ok': 'Cập nhật',
};
