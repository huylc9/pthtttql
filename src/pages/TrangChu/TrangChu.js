import React from 'react';
import { connect } from 'dva';
import { Card, Row, Col, Divider, Input, InputNumber, Button, Form } from 'antd';
import SuKien from '@/pages/SuKien/Sukien';
import ThoiKhoaBieu from '@/pages/ThoiKhoaBieu/ThoiKhoaBieu';
import { getAuthority } from '@/utils/authority';
import Donut from './ThongKeDashBoard';
import Demo from './BieuDoCot';
import BieuDoCotCachLy from './BieuDoCotCachLy';

const { Search } = Input;
@connect(({ loading, trangchu }) => ({
  trangchu,
  loading: loading.models.trangchu,
}))
class TrangChu extends React.Component {
  componentDidMount() {
    
  }

  render() {

    
      return (
        <Row>
          <Col sm={12} md={6}>
            <Card title="Card title" bordered={false} style={{ width: 300 }}>
              <p>Card content</p>
              <p>Card content</p>
              <p>Card content</p>
            </Card>

          </Col>
        </Row>
      );
  }
}

const WrappedNormalLoginForm = Form.create({})(TrangChu);

export default WrappedNormalLoginForm;
