import React from 'react';
import { Row, Col } from 'antd';
import { connect } from 'dva';

import ThoiKhoaBieu from './TrangChuGiangVien/Layout/ThoiKhoaBieu/ThoiKhoaBieu';
import Card1 from './TrangChuGiangVien/Layout/Card1';
import Card2 from './TrangChuGiangVien/Layout/Card2';
import Card3 from './TrangChuGiangVien/Layout/Card3';
import TinTuc from './TrangChuGiangVien/Layout/TinTuc';
import Card4 from './TrangChuGiangVien/Layout/Card4';

@connect(({ loading, dashboardgvsv, lopgv, monhoc }) => ({
  dashboardgvsv,
  loading: loading.models.dashboardgvsv,
  lopgv,
  monhoc,
}))
class TrangChu extends React.Component {
  componentDidMount() {
    this.props.dispatch({
      type: 'dashboardgvsv/tinTuc',
      payload: {
        page: 1,
        limit: 4,
      },
    });
    this.props.dispatch({
      type: 'lopgv/get',
    });
  }

  render() {
    const {
      dashboardgvsv: { tinTuc },
    } = this.props;
    const card = {
      sm: 12,
      lg: 12,
      xl: 6,
      style: {},
    };
    return (
      <>
        <h1 style={{ fontWeight: 'bold' }}>THÔNG TIN CHUNG</h1>
        <Row style={{ marginLeft: '-12px', marginRight: '-12px', marginTop: '-12px' }}>
          <Col {...card}>
            <Card1 />
          </Col>
          <Col {...card}>
            <Card2 />
          </Col>
          <Col {...card}>
            <Card3 />
          </Col>
          <Col {...card}>
            <Card4 />
          </Col>
        </Row>
        <Row style={{ marginRight: -12, marginLeft: -12 }}>
          <Col xs={24} sm={24} md={24} lg={16} xl={16}>
            <ThoiKhoaBieu style={{ marginLeft: 12, marginRight: 12 }} />
          </Col>
          <Col xs={24} sm={24} md={24} lg={8} xl={8}>
            <TinTuc data={tinTuc} />
          </Col>
        </Row>
      </>
    );
  }
}

export default TrangChu;
