// data-set 可以按需引入，除此之外不要引入别的包
import React from 'react';
import { Chart, Axis, Tooltip, Geom, Legend } from 'bizcharts';
import moment from 'moment';
// 下面的代码会被作为 cdn script 注入 注释勿删
// CDN START

const data = {};
const cols = {
  value: { min: 0 },
  ngay: { tickInterval: 5, tickCount: 10 },
};

// legend 默认选中请使用 chart filter

// eslint-disable-next-line react/prefer-stateless-function

class Demo extends React.Component {
  componentDidMount() {}

  render() {
    const data = [];

    const { preData } = this.props;
    preData.map(val => {
      data.push({
        type: 'Không cách ly',
        ngay: moment(val.ngay).format('DD/MM/YYYY'),
        value: Number(`${val.khongCachLy}`),
      });
      data.push({
        type: 'Cách ly tại nhà',
        ngay: moment(val.ngay).format('DD/MM/YYYY'),
        value: Number(`${val.cachLyTaiNha}`),
      });
      data.push({
        type: 'Cách ly tập trung',
        ngay: moment(val.ngay).format('DD/MM/YYYY'),
        value: Number(`${val.cachLyTapTrung}`),
      });
      data.push({
        type: 'Chưa khai báo',
        ngay: moment(val.ngay).format('DD/MM/YYYY'),
        value: Number(`${val.chuaKhaiBao}`),
      });
    });
    data.reverse();
    return (
      <div style={{ marginTop: 20, textAlign: 'center' }}>
        <h1>{this.props.name}</h1>
        <Chart
          filter={[
            [
              'type',
              t => {
                if (t === 'Chưa Khai Báo') return false;
                return true;
              },
            ],
          ]}
          height={500}
          data={data}
          scale={cols}
          forceFit
        >
          <Axis name="ngay" />
          <Axis name="value" />
          <Legend />
          <Tooltip crosshairs={{ type: 'y' }} />
          <Geom type="line" position="ngay*value" size={2} color="type" />
          <Geom
            type="point"
            position="ngay*value"
            size={4}
            shape="circle"
            style={{ stroke: '#fff', lineWidth: 1 }}
            color="type"
          />
        </Chart>
      </div>
    );
  }
}
export default Demo;
// CDN END
