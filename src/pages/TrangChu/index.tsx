import React from 'react';
import { Card, Row, Col, Divider, Input, InputNumber, Button, Form, Statistic, Typography } from 'antd';
import { Line } from '@ant-design/charts';
import { connect } from 'dva';

const month = ["06-2020", "07-2020", "08-2020", "09-2020", "10-2020", "11-2020", "12-2020", "01-2021" , "02-2021" , "03-2021" , "04-2021" , "05-2021"];
@connect(({ loading, hachToan }) => ({
  hachToan,
  loading: loading.models.muaHang,
}))
class TrangChu extends React.Component {
  componentDidMount() {
    this.props.dispatch({
      type: "hachToan/getThongKeTheoThang"
    })
  }
  
  render() {
    const {
      hachToan: {
        thongKe: {
          111: tienMat,
          112: tienGui,
          141: tamUng,
          151: hangMuaDiDuong,
          156: tongHangHoa,
          641: chiPhi,
          511: doanhThu
        },
        thongKeTheoThang
      }
    } = this.props;
    const rowDot = (
      <hr style={{ borderStyle: 'dashed', borderColor: '#ced0d4', borderWidth: "0.5px", marginBottom: "7.5px" }} />
    );

    // const data = [
    //   { year: '1991', value: 3 },
    //   { year: '1992', value: 4 },
    //   { year: '1993', value: 3.5 },
    //   { year: '1994', value: 5 },
    //   { year: '1995', value: 4.9 },
    //   { year: '1996', value: 6 },
    //   { year: '1997', value: 7 },
    //   { year: '1998', value: 9 },
    //   { year: '1999', value: 13 },
    // ];
    const data = thongKeTheoThang.map((item, i) => {
      return {
      "Thời gian": - item["511"] || 0,
      "Doanh thu": month[i],
    }
    })

    const config = {
      data,
      height: 400,
      xField: 'Doanh thu',
      yField: 'Thời gian',
      point: {
        size: 5,
        shape: 'diamond',
      },
    };
    return (
      <Row>
          <Col sm={12} md={12}>
            <Card title="Tình hình tài chính" bordered={false} style={{ padding: "0 10px" }}>
            <Statistic title="Tiền mặt" value={-tienMat || 0} />
            {rowDot}
            <Statistic title="Tiền gửi" value={-tienGui || 0} />
            {rowDot}
            <Statistic title="Tiền tạm ứng NV" value={-tamUng || 0} />
            {rowDot}
            <Statistic title="Hàng mua đi đường" value={-hangMuaDiDuong || 0} />
            {rowDot}
            <Statistic title="Hàng trong kho" value={-tongHangHoa || 0} />
            {rowDot}
            <Statistic title="Tổng chi phí" value={-chiPhi || 0} />
            </Card>

          </Col>
        <Col sm={12} md={12}>
          <Card title="Doanh thu" bordered={false} style={{ margin: "0 10px", height: 555 }}>
            <Line {...config} />
            </Card>
        </Col>
      </Row>
      
    );
  }
}

export default TrangChu;
