import React from 'react';
import { List, Avatar, Typography, Tooltip } from 'antd';
import styles from '@/pages/List/Articles.less';

const TrangChu = props => {
  const {
    data: { tieuDe, _id, anhDaiDien },
  } = props;
  return (
    <List.Item key={_id} extra={<div className={styles.listItemExtra} />}>
      <List.Item.Meta
        avatar={<Avatar src={anhDaiDien} shape="square" style={{ height: 100, width: 100 }} />}
        style={{ marginBottom: 20, marginTop: 20 }}
        title={
          <Tooltip title={tieuDe}>
            <Typography.Paragraph ellipsis={{ rows: 3, expandable: false }}>
              <a className={styles.listItemMetaTitle} href="#" style={{ fontSize: 14 }}>
                {tieuDe}
              </a>
            </Typography.Paragraph>
          </Tooltip>
        }
      />
    </List.Item>
  );
};

export default TrangChu;
