import React from 'react';
import { Card, List } from 'antd';
import Item from './TinTucCard';
import styles from './style.css';

type Props = {
  data: Array<object>;
};

// eslint-disable-next-line react/prefer-stateless-function
export default class TinTuc extends React.Component<Props> {
  render() {
    const data = this.props.data || [];
    const tmp = [];
    for (let i = 0; i < data.length; i += 1) {
      if (i < 4) tmp.push(data[i]);
    }
    return (
      <div className={styles.dau}>
        <Card
          style={{ height: 705 }}
          bordered={false}
          title={
            <div className="cardTitle" style={{ textAlign: 'center' }}>
              Tin Tức
            </div>
          }
        >
          <List
            style={{ marginRight: -30, marginTop: -20 }}
            size="small"
            loading={tmp.length === 0}
            rowKey="id"
            itemLayout="vertical"
            dataSource={tmp}
            renderItem={item => <Item data={item} />}
          />
        </Card>
      </div>
    );
  }
}
