/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-indent */
/* eslint-disable react/sort-comp */
import React from 'react';
import { Card, Row, Col, Button, Modal, Typography, Spin } from 'antd';
import { Calendar, momentLocalizer, Views } from 'react-big-calendar';
import moment from 'moment';
import { connect } from 'dva';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import _ from 'lodash';
import { toHexa, tinhNgayTheoTuan } from '@/utils/utils';
import router from 'umi/router';
import DiemDanh from './DiemDanh';
import styles from '../style.css';

const messages = {
  allDay: 'Cả ngày',
  previous: 'Trước',
  next: 'Sau',
  today: 'Hôm nay',
  month: 'Tháng',
  week: 'Tuần',
  day: 'Ngày',
  agenda: 'Chung',
  date: 'Ngày',
  time: 'Thời gian',
  event: 'Sự kiện',
  showMore: total => `+ Xem thêm (${total})`,
};

@connect(({ loading, thoikhoabieu }) => ({
  thoikhoabieu,
  loading: loading.models.thoikhoabieu,
}))
class SuKien extends React.Component {
  componentDidMount() {
    this.props.dispatch({
      type: 'thoikhoabieu/get',
    });
  }

  handleOk = () => {
    this.props.dispatch({
      type: 'thoikhoabieu/changeState',
      payload: {
        visible: false,
      },
    });
  };

  onDiemDanh = () => {
    const { record } = this.props.thoikhoabieu;
    this.props.dispatch({
      type: 'thoikhoabieu/getLop',
      payload: {
        maLopHoc: record.maLopHoc,
        tuanHoc: record.tuanHoc,
        thuHoc: record.thuHoc,
        tietBatDau: record.tietBatDau,
      },
    });
    this.props.dispatch({
      type: 'thoikhoabieu/changeState',
      payload: {
        visibleDiemDanh: true,
      },
    });
  };

  eventPropGetter = event => ({
    style: { backgroundColor: toHexa(event.title) },
  });

  onSelectEvent = event => {
    this.props.dispatch({
      type: 'thoikhoabieu/changeState',
      payload: {
        visible: true,
        record: {
          ...event,
        },
      },
    });
  };

  onHistory = () => {
    const { record } = this.props.thoikhoabieu;
    this.props.dispatch({
      type: 'thoikhoabieu/changeState',
      payload: {
        visible: false,
      },
    });
    router.push({
      pathname: '/DanhSachLop/ThongTinLop',
      query: {
        idLop: record._id,
        maLopHoc: record.maLopHoc,
      },
    });
  };

  render() {
    const { thoikhoabieu, loading } = this.props;
    const localizer = momentLocalizer(moment);
    const { danhSach } = thoikhoabieu;
    const { record, visible } = thoikhoabieu;
    const data = [];
    const setMonHoc = new Set();
    danhSach.map(
      ({ tenLopHoc, monHoc, lichHoc, kyHoc: { ngayBatDau }, sinhVien, maLopHoc, _id }) => {
        const tenMonHoc = _.get(monHoc, 'tenMonHoc', '');
        setMonHoc.add(tenMonHoc);
        lichHoc.map(
          ({
            tuanHoc,
            thuHoc,
            thoiGianBatDau,
            thoiGianKetThuc,
            phongHoc,
            tietBatDau,
            tietKetThuc,
            // eslint-disable-next-line array-callback-return
          }) => {
            // eslint-disable-next-line array-callback-return
            tuanHoc.map(i => {
              const ngayHoc = tinhNgayTheoTuan(i + 1, thuHoc, ngayBatDau); // 00:00 của ngày học
              const startTime = moment(thoiGianBatDau, 'HH:mm');
              const endTime = moment(thoiGianKetThuc, 'HH:mm');
              const start = moment(ngayHoc)
                .add(startTime.hours(), 'hours')
                .add(startTime.minutes(), 'minutes')
                .toDate();
              const end = moment(ngayHoc)
                .add(endTime.hours(), 'hours')
                .add(endTime.minutes(), 'minutes')
                .toDate();
              data.push({
                _id,
                maLopHoc,
                sinhVien,
                tuanHoc: i,
                thuHoc,
                tietBatDau,
                tietKetThuc,
                start,
                end,
                tenMonHoc,
                phongHoc,
                tenLopHoc,
                title: `${`${tenMonHoc}; \nPhòng: ${
                  phongHoc !== 'NaN' ? phongHoc : ''
                };\nLớp: ${tenLopHoc};`}`,
              });
            });
          }
        );
      }
    );
    return (
      <>
        <Card
          bordered={false}
          title={
            <div className="cardTitle" style={{ textAlign: 'center' }}>
              Thời Khóa Biểu
            </div>
          }
          className={styles.dau}
        >
          <Row gutter={24}>
            <Col xs={24}>
              {/* <div style={{ marginBottom: 24 }}>
                <List
                  header={<div>Danh sách các môn giảng dạy</div>}
                  bordered
                  dataSource={Array.from(setMonHoc)}
                  renderItem={item => (
                    <List.Item>
                      <Typography.Text mark>[*]</Typography.Text> {item}
                    </List.Item>
                  )}
                />
              </div> */}
              <Spin spinning={loading}>
                <Calendar
                  localizer={localizer}
                  events={data}
                  defaultView={Views.MONTH}
                  scrollToTime={new Date(1970, 1, 1)}
                  defaultDate={new Date()}
                  messages={messages}
                  views={['month', 'week']}
                  style={{ height: 600 }}
                  min={moment('0600', 'HHmm').toDate()}
                  max={moment('2000', 'HHmm').toDate()}
                  eventPropGetter={this.eventPropGetter}
                  onSelectEvent={event => this.onSelectEvent(event)}
                  popup
                />
              </Spin>
            </Col>
          </Row>
        </Card>
        <Modal
          title="Thông tin lớp học"
          visible={visible}
          onOk={this.handleOk}
          onCancel={this.handleOk}
          footer={[
            <Button style={{ backgroundColor: '#6699FF', color: 'white' }} onClick={this.onHistory}>
              Xem lịch sử điểm danh
            </Button>,
            <Button
              style={{ backgroundColor: '#00BB00', color: 'white' }}
              onClick={this.onDiemDanh}
            >
              Điểm danh
            </Button>,
            <Button type="primary" onClick={this.handleOk}>
              OK
            </Button>,
          ]}
        >
          {_.isEmpty(record) === false ? (
            <div>
              <Typography.Paragraph>Môn học: {record.tenMonHoc}</Typography.Paragraph>
              <Typography.Paragraph>Lớp học: {record.tenLopHoc}</Typography.Paragraph>
              <Typography.Paragraph>Phòng học: {record.phongHoc}</Typography.Paragraph>
              <Typography.Paragraph>
                Ngày: {moment(record.start).format('DD/MM/YYYY')}
              </Typography.Paragraph>
              <Typography.Paragraph>
                Tuần {record.tuanHoc}, Thứ {record.thuHoc + 1}
              </Typography.Paragraph>
              <Typography.Paragraph>
                Tiết: {record.tietBatDau} - {record.tietKetThuc}
              </Typography.Paragraph>
              {/* <Typography.Paragraph>Số sinh viên: {_.get(record, 'sinhVien', []).length}</Typography.Paragraph> */}
            </div>
          ) : null}
        </Modal>
        <DiemDanh />
      </>
    );
  }
}
export default SuKien;
