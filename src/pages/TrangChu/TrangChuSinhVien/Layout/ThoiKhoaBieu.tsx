/* eslint-disable react/sort-comp */
import React from 'react';
import { Card, Button, Modal, Typography } from 'antd';
import { Calendar, momentLocalizer, Views } from 'react-big-calendar';
import moment from 'moment';
import _ from 'lodash';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import { tinhNgayTheoTuan } from '@/utils/utils';
import styles from './style.css';

const messages = {
  allDay: 'Cả ngày',
  previous: 'Trước',
  next: 'Sau',
  today: 'Hôm nay',
  month: 'Tháng',
  week: 'Tuần',
  day: 'Ngày',
  agenda: 'Chung',
  date: 'Ngày',
  time: 'Thời gian',
  event: 'Sự kiện',
  showMore: total => `+ Xem thêm (${total})`,
};

type Props = {
  detail: object;
};

class ThoiKhoaBieu extends React.Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      record: {},
    };
  }

  componentDidMount() {}

  handleOk = () => {
    this.setState({ visible: false });
  };

  handleShow = record => {
    this.setState({
      visible: true,
      record,
    });
  };

  // eventPropGetter = event => ({
  //   style: { backgroundColor: toHexa(event.maSuKien) }
  // });

  render() {
    const localizer = momentLocalizer(moment);
    const danhSach = [];
    const { detail } = this.props;
    const { record, visible } = this.state;
    detail.map(item => {
      const idLop = _.get(item, 'id', undefined);
      item.lichHoc.map(
        ({
          tuanHoc,
          thuHoc,
          thoiGianBatDau,
          thoiGianKetThuc,
          phongHoc,
          tietBatDau,
          tietKetThuc,
        }) => {
          tuanHoc.map(i => {
            const ngayHoc = tinhNgayTheoTuan(
              i + 1,
              thuHoc,
              _.get(item, 'kyHoc.ngayBatDau', undefined)
            ); // 00:00 của ngày học
            const startTime = moment(thoiGianBatDau, 'HH:mm');
            const endTime = moment(thoiGianKetThuc, 'HH:mm');
            const start = moment(ngayHoc)
              .add(startTime.hours(), 'hours')
              .add(startTime.minutes(), 'minutes')
              .toDate();
            const end = moment(ngayHoc)
              .add(endTime.hours(), 'hours')
              .add(endTime.minutes(), 'minutes')
              .toDate();
            danhSach.push({
              idLop,
              phongHoc,
              tuanHoc: i,
              thuHoc,
              tietBatDau,
              tietKetThuc,
              start,
              end,
              tenMonHoc: _.get(item, 'monHoc.tenMonHoc', ''),
              tenLopHoc: _.get(item, 'tenLopHoc', ''),
              title: `${`Môn: ${item.monHoc.tenMonHoc};\nPhòng: ${phongHoc || 'Chưa cập nhật'};`}`,
            });
          });
        }
      );
    });

    return (
      <div className={styles.dau}>
        <Card
          bordered={false}
          title={
            <div className="cardTitle" style={{ textAlign: 'center' }}>
              Thời Khóa Biểu
            </div>
          }
        >
          <Calendar
            localizer={localizer}
            // selectable
            events={danhSach}
            defaultView={Views.WEEK}
            scrollToTime={new Date(1970, 1, 1, 6)}
            defaultDate={new Date()}
            onSelectEvent={event => this.handleShow(event)}
            // onSelectSlot={this.handleSelect}
            messages={messages}
            views={['week']}
            style={{ height: 600 }}
            min={moment('0600', 'HHmm').toDate()}
            max={moment('2000', 'HHmm').toDate()}
            // eventPropGetter={this.eventPropGetter}
            popup
          />
        </Card>
        <Modal
          title="Thông tin lớp học"
          visible={visible}
          onOk={this.handleOk}
          onCancel={this.handleOk}
          footer={
            <Button type="primary" onClick={this.handleOk}>
              OK
            </Button>
          }
        >
          {_.isEmpty(record) === false ? (
            <div>
              <Typography.Paragraph>Môn học: {record.tenMonHoc}</Typography.Paragraph>
              <Typography.Paragraph>Lớp học: {record.tenLopHoc}</Typography.Paragraph>
              <Typography.Paragraph>Phòng học: {record.phongHoc}</Typography.Paragraph>
              <Typography.Paragraph>
                Ngày: {moment(record.start).format('DD/MM/YYYY')}
              </Typography.Paragraph>
              <Typography.Paragraph>
                Tuần {record.tuanHoc}, Thứ {record.thuHoc + 1}
              </Typography.Paragraph>
              <Typography.Paragraph>
                Tiết: {record.tietBatDau} - {record.tietKetThuc}
              </Typography.Paragraph>
              {/* <Typography.Paragraph>Số sinh viên: {_.get(record, 'sinhVien', []).length}</Typography.Paragraph> */}
            </div>
          ) : null}
        </Modal>
      </div>
    );
  }
}
export default ThoiKhoaBieu;
