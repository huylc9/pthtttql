import React from 'react';
import { Badge } from 'antd';

import { ChartCard } from 'ant-design-pro/lib/Charts';
import moment from 'moment';
import styles from './style.css';

// eslint-disable-next-line react/prefer-stateless-function
export default class GPA extends React.Component {
  render() {
    const visitData = [];
    const beginDay = new Date().getTime();
    for (let i = 0; i < 20; i += 1) {
      visitData.push({
        x: moment(new Date(beginDay + 1000 * 60 * 60 * 24 * i)).format('YYYY-MM-DD'),
        y: Math.floor(Math.random() * 100) + 10,
      });
    }
    return (
      <ChartCard
        title="Số Lượng Yêu Cầu"
        className={styles.dau}
        bordered={false}
        total={<p style={{ fontSize: 20 }}>Chưa có dữ liệu</p>}
        contentHeight={70}
      >
        <Badge color="green" text="Chưa có dữ liệu" /> <br />
        <Badge color="red" text="Chua có dữ liệu" />
      </ChartCard>
    );
  }
}
