import React from 'react';
import { Tag } from 'antd';

import { ChartCard, MiniProgress } from 'ant-design-pro/lib/Charts';
import moment from 'moment';
import styles from './style.css';

// eslint-disable-next-line react/prefer-stateless-function
export default class GPA extends React.Component {
  render() {
    const visitData = [];
    const beginDay = new Date().getTime();
    for (let i = 0; i < 20; i += 1) {
      visitData.push({
        x: moment(new Date(beginDay + 1000 * 60 * 60 * 24 * i)).format('YYYY-MM-DD'),
        y: Math.floor(Math.random() * 100) + 10,
      });
    }
    return (
      <ChartCard
        title="Số Môn Đã Đăng Ký"
        className={styles.dau}
        bordered={false}
        total={<p style={{ fontSize: 20 }}>Chưa có dữ liệu</p>}
        footer={<Tag color="green">Tong : Chưa có dữ liệu</Tag>}
        contentHeight={40}
      >
        <MiniProgress percent={78} strokeWidth={8} target={80} />
      </ChartCard>
    );
  }
}
