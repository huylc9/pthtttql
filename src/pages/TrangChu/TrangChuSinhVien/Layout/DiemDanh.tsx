import React from 'react';
import { Card, Table, Row, Col, DatePicker, Typography } from 'antd';
import styles from './style.css';

export default class DiemDanh extends React.Component {
  state = {
    chonNganh: false,
    noiDung: {},
  };

  columns = [
    {
      title: 'STT',
      dataIndex: 'index',
      align: 'center',
      width: 50,
      render(text, record) {
        return {
          props: {
            style: { background: record.color },
          },
          children: <div>{text}</div>,
        };
      },
    },
    {
      title: 'Tên Lớp',
      dataIndex: 'tenLop',
      align: 'center',
      render(text, record) {
        return {
          props: {
            style: { background: record.color },
          },
          children: <div>{text}</div>,
        };
      },
    },
    {
      title: 'Mã Lớp',
      dataIndex: 'maLop',
      align: 'center',
      width: 120,
      render(text, record) {
        return {
          props: {
            style: { background: record.color },
          },
          children: <div style={{ fontSize: 11 }}>{text}</div>,
        };
      },
    },
    {
      title: 'Nghỉ',
      dataIndex: 'buoiNghi',
      align: 'center',
      width: 100,
      render(text, record) {
        return {
          props: {
            style: { background: record.color },
          },
          children: <div>{text}</div>,
        };
      },
    },
    {
      title: 'Tổng',
      dataIndex: 'tong',
      align: 'center',
      width: 100,
      render(text, record) {
        return {
          props: {
            style: { background: record.color },
          },
          children: <div>{text}</div>,
        };
      },
    },
  ];

  public handleChonLop = (record: any) => {
    this.setState({ chonNganh: true, noiDung: record });
    console.log('hhh', record);
  };

  render() {
    const data = this.props.data.map((item, index) => {
      const tmp = {};
      tmp.index = index + 1;
      tmp.coPhep = item?.buoiVangCoPhep?.length ?? '0';
      tmp.khongPhep = item?.buoiVangKhongPhep?.length ?? '0';
      tmp.buoiNghi = tmp.coPhep + tmp.khongPhep ?? '0';
      tmp.buoiDi = item?.buoiDi?.length ?? '0';
      tmp.tong = tmp.buoiNghi + tmp.buoiDi;
      tmp.maLop = item.maLopHoc;
      tmp.tenLop = item.tenLopHoc;
      index % 2 === 0 ? (tmp.color = 'rgb(246,236,240)') : (tmp.color = '#fff');
      tmp.buoiNghi / tmp.tong > 0.2 ? (tmp.color = 'red') : null;
      return tmp;
    });
    return (
      <>
        <Row style={{ marginLeft: -12, marginRight: -12 }}>
          <Col xs={24} sm={24} md={24} lg={12} xl={12}>
            <Card
              className={styles.dau}
              bordered={false}
              title={
                <div className="cardTitle" style={{ textAlign: 'center' }}>
                  Điểm Danh
                </div>
              }
            >
              <Table
                columns={this.columns}
                dataSource={data}
                bordered
                size="small"
                style={{ margin: -24 }}
                onRow={record => ({
                  onClick: () => {
                    this.handleChonLop(record);
                  },
                  style: { cursor: 'pointer' },
                })}
              />
            </Card>
          </Col>
          <Col xs={24} sm={24} md={24} lg={12} xl={12}>
            <Card
              className={styles.dau}
              bordered={false}
              title={
                <div className="cardTitle" style={{ textAlign: 'center' }}>
                  Bài Tập Sắp Tới
                </div>
              }
            >
              <center>
                <DatePicker.RangePicker />
              </center>
              {this.state.chonNganh === true ? (
                <center>
                  <Typography.Text code strong style={{ fontSize: 20 }}>
                    Lớp: {this.state.noiDung.tenLop}
                  </Typography.Text>
                </center>
              ) : (
                <div></div>
              )}
            </Card>
          </Col>
        </Row>
      </>
    );
  }
}
