import axios from '@/utils/axios.js';
import { ip3 } from '@/services/ip';
import { api } from './api';

export const diemDanh = async () => axios.get(`${ip3}/${api.diemDanh}/`);

export const lichHoc = async () => axios.get(`${ip3}/${api.lichHoc}/`);

export const tinTuc = async () => axios.get(`${ip3}/${api.tinTuc}/`);
