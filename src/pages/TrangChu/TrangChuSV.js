import React from 'react';
import { Row, Col } from 'antd';
import { connect } from 'dva';

import GPA from './TrangChuSinhVien/Layout/GPA';
import DiemDanh from './TrangChuSinhVien/Layout/DiemDanh';
import SoMonDK from './TrangChuSinhVien/Layout/SoMonDK';
import ThoiKhoaBieu from './TrangChuSinhVien/Layout/ThoiKhoaBieu';
import TienTrinhHoc from './TrangChuSinhVien/Layout/TienTrinhHoc';
import TinTuc from './TrangChuSinhVien/Layout/TinTuc';
import YeuCau from './TrangChuSinhVien/Layout/YeuCau';

@connect(({ loading, dashboardgvsv }) => ({
  dashboardgvsv,
  loading: loading.models.dashboardgvsv,
}))
class TrangChu extends React.Component {
  componentDidMount() {
    this.props.dispatch({
      type: 'dashboardgvsv/lichHoc',
    });
    this.props.dispatch({
      type: 'dashboardgvsv/diemDanh',
    });
    this.props.dispatch({
      type: 'dashboardgvsv/tinTuc',
    });
  }

  render() {
    const {
      dashboardgvsv: { tinTuc, diemDanh, lichHoc },
    } = this.props;
    const card = {
      sm: 12,
      lg: 12,
      xl: 6,
      style: {},
    };
    return (
      <>
        <h1 style={{ fontWeight: 'bold' }}>THÔNG TIN CHUNG</h1>
        <Row style={{ marginLeft: '-12px', marginRight: '-12px', marginTop: '-12px' }}>
          <Col {...card}>
            <GPA />
          </Col>
          <Col {...card}>
            <TienTrinhHoc />
          </Col>
          <Col {...card}>
            <SoMonDK />
          </Col>
          <Col {...card}>
            <YeuCau />
          </Col>
        </Row>
        <Row style={{ marginRight: -12, marginLeft: -12 }}>
          <Col xs={24} sm={24} md={24} lg={16} xl={16}>
            <ThoiKhoaBieu detail={lichHoc} />
          </Col>
          <Col xs={24} sm={24} md={24} lg={8} xl={8}>
            <TinTuc data={tinTuc} />
          </Col>
        </Row>
        <DiemDanh data={diemDanh} />
      </>
    );
  }
}

export default TrangChu;
