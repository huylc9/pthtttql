import React from 'react';
import {
  G2,
  Pie,
  Chart,
  Geom,
  Axis,
  Tooltip,
  Coord,
  Label,
  Legend,
  View,
  Guide,
  Shape,
  Facet,
  Util,
  Title
} from 'bizcharts';
import DataSet from '@antv/data-set';

class Donut extends React.Component {
  render() {
    const { DataView } = DataSet;
    const { Html } = Guide;
    const data = this.props.data;
    let total = 0;
    data.map((item) => {
      total += item.count;
    })
    // const dv = new DataView();
    // dv.source(data).transform({
    //   type: 'percent',
    //   field: 'count',
    //   dimension: 'item',
    //   as: 'percent',
    // });
    // const cols = {
    //   percent: {
    //     formatter: val => {
    //       console.log(val, "valChart")
    //       val = `${val.toFixed(4) * 100}%`;
    //       return val;
    //     },
    //   },
    // };
    let string = '<div style="color:#8c8c8c;font-size:1.16em;text-align: center;width: 10em;">Tổng số<br><span style="color:#262626;font-size:2.5em">';
    string += total.toString() + '</span>người</div>';
    return (
      <div style={{marginTop: 20, textAlign: 'center'}}>
        <h1>{ this.props.name }</h1>
        <Chart
          height={window.innerHeight-300}
          data={data}
          // scale={cols}
          padding={[40, 80, 40, 80]}
          forceFit
        > 
          <Coord type="theta" radius={0.75} innerRadius={0.6} />
          <Axis name="count" />
          {window.innerWidth <= 1400? null : <Legend position="right" offsetX={-150}/> }
          <Tooltip
            showTitle={false}
            itemTpl='<li><span style="background-color:{color};" class="g2-tooltip-marker"></span>{name}: {value}</li>'
          />
          <Guide>
            <Html
              position={['50%', '50%']}
              html={ string }
              alignX="middle"
              alignY="middle"
            />
          </Guide>
          <Geom
            type="intervalStack"
            position="count"
            color="item"
            tooltip={[
              'item*count',
              (item, count) => {
                // percent = `${percent * 100}%`;
                return {
                  name: item,
                  value: count,
                };
              },
            ]}
            style={{
              lineWidth: 1,
              stroke: '#fff',
            }}
          >
            <Label content="count" formatter={(val, item) => `${item.point.item}: ${val}`} />
          </Geom>
        </Chart>
      </div>
    );
  }
}

export default Donut;
