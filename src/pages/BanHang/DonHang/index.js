
/* eslint-disable react/destructuring-assignment */
import { Button, Card, Col, Icon, Input, Row, Table, Divider, Popconfirm} from 'antd';
import _ from 'lodash';
import { connect } from 'dva';
import React from 'react';
import uuidv1 from 'uuid/v1';
import FormDrawer from '@/components/Drawer/FormDrawer';
import Form from './components/Form';
import moment from 'moment';
import { taiKhoanNoCo } from '@/utils/taikhoan';
import FormHachToan from '@/pages/Quy/HachToan/Form';

@connect(({ loading, donHang }) => ({
  donHang,
  loading: loading.models.donHang,
}))
class TableBase extends React.Component {
  componentDidMount() {
    this.props.dispatch({
      type: `donHang/getAll`,
      payload: {
        page: 1,
        size: 10,
      },
    });
  }

  getCond = () => {
    // lấy thông tin cond đang search
    const { model } = this.props;
    return _.get(model, 'paging.cond', {});
  };

  handleThem = () => {
    this.props.dispatch({
      type: `donHang/changeState`,
      payload: {
        showDrawer: true,
        edit: false,
        record: {},
        key: uuidv1(),
        isTouched: false,
      },
    });
  };

  handleDel = _id => {
    this.props.dispatch({
      type: `donHang/del`,
      payload: {
        _id,
      },
    });
  };

  handleEdit = record => {
    this.props.dispatch({
      type: `donHang/changeState`,
      payload: {
        showDrawer: true,
        edit: true,
        record,
      },
    });
  };

  handleHachToan = (giaoDich, view) => {
    const type = 'banHang';
    if (view) {
      this.props.dispatch({
        type: 'hachToan/changeState',
        payload: {
          giaoDich: { maGiaoDich: `MH${giaoDich.maDonHang}` },
          view,
          showDrawer: true,
        },
      });
      return;
    }
    this.props.dispatch({
      type: 'hachToan/changeState',
      payload: {
        view,
        showDrawer: true,
        record: {
          taiKhoanNo: type ? taiKhoanNoCo[type].no : 0,
          taiKhoanCo: type ? taiKhoanNoCo[type].co : 0,
          soTien: giaoDich.donGia,
          maGiaoDich: `MH${giaoDich.maDonHang}`,
        },
        onComplete: () => {
          this.props.dispatch({
            type: 'donHang/update',
            payload: {
              maDonHang: giaoDich.maDonHang,
              thoiGianHachToan: moment().toISOString(),
              hangHoa: _.get(giaoDich, "hangHoa.maHangHoa"),
              nhanVienBan: _.get(giaoDich, "nhanVienBan.maNhanVien"),
              khoHang: _.get(giaoDich, "khoHang.maKho"),
            },
          })
        },
      },
    })
  }

  render() {
    let {
      donHang,
      loading
    } = this.props;
    let { danhSach, total, paging } = donHang;
    const { page, size } = paging;
    danhSach = danhSach.map((item, index) => ({
      ...item,
      index: index + 1 + (page - 1) * size,
      key: index,
    }));

    const columns = [{
      title: 'STT',
      dataIndex: 'index',
      key: 'index',
    },
    {
      title: 'Mã đơn hàng',
      dataIndex: 'maDonHang',
      key: 'maKho',
    },
    {
      title: 'Tên khách hàng',
      dataIndex: 'tenKhachHang',
    },
    {
      title: 'Thời gian thực hiện',
      dataIndex: 'ngayXuatDon',
      render: val => moment(val).format("DD/MM/YYYY HH:mm")
    },
    {
      title: 'Tên mặt hàng',
      dataIndex: 'hangHoa.tenHangHoa',
    },
    {
      title: 'Số lượng',
      dataIndex: 'soLuong',
    },
    {
      title: 'Đơn giá',
      dataIndex: 'donGia',
    },
    {
      title: 'Nhân viên bán',
      dataIndex: 'nhanVienBan.tenNhanVien',
    },
    // {
    //   title: 'Thời gian hạch toán',
    //   dataIndex: 'thoiGianHachToan',
    //   render: (val, record) => (record.isHachToan ? moment(val).format('DD/MM/YYYY HH:mm') : 'Chưa hạch toán'),
    // },
    ];

    const list = (text, record) => (
      <React.Fragment>
          <React.Fragment>
            <Button
              type="primary"
              // shape="circle"
              icon={record.isHachToan ? 'eye' : 'edit'}
              onClick={() => this.handleHachToan(record, record.isHachToan)}
              title="Hạch toán"
            >
              {record.isHachToan ? 'Xem hạch toán' : 'Hạch toán'}
            </Button>
          </React.Fragment>
      </React.Fragment>
    );

    const last = {
      // phần tử cuối của columns
      title: 'Tùy chọn',
      width: '120px',
      align: 'center',
      fixed: 'right',
      render: (text, record) => list(text, record)
    };

    // columns.push(last);

    return (
      <div className="box">
        <Card bordered title={<div className="cardTitle">Quản lý đơn hàng</div>}>
          <Row>
            <Col xs={24}>
                <Button
                  style={{ marginRight: '20%' }}
                  type="primary"
                  // shape="round"
                  icon="plus"
                  // disabled
                  onClick={() => this.handleThem()}
                >
                  Thêm mới
                </Button>
              <h3 style={{ display: 'inline-block', margin: '0 10px 10px 50px', float: 'right' }}>
                {`Tổng số : ${total}`}
              </h3>

              <Table
                style={{ marginTop: 10 }}
                loading={!!loading}
                columns={columns}
                dataSource={danhSach}
                onChange={this.onChange}
                pagination={{
                  current: page,
                  position: 'bottom',
                  total,
                }}
              />
            </Col>
          </Row>
        </Card>
        <FormDrawer name={`donHang`}>
          <Form />
        </FormDrawer>
        <FormDrawer name="hachToan">
          <FormHachToan />
        </FormDrawer>
      </div>
    );
  }
}

export default TableBase;
