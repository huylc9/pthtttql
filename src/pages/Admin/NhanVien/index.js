/* eslint-disable react/destructuring-assignment */
import { Button, Card, Col, Icon, Input, Row, Table } from 'antd';
import _ from 'lodash';
import { connect } from 'dva';
import React from 'react';
import uuidv1 from 'uuid/v1';
import { toRegex } from '@/utils/utils';
import data from '@/utils/data';
import FormDrawer from '@/components/Drawer/FormDrawer';
import FormModal from '@/components/Drawer/FormModal';
import Form from './components/Form';
import View from './components/ViewNhanVien';

@connect(({ loading, nhanVien }) => ({
  nhanVien,
  loading: loading.models.nhanVien,
}))
class TableBase extends React.Component {
  constructor() {
    super();
    this.state = {
      item: null,
    };
  }

  componentDidMount() {
    this.props.dispatch({
      type: 'nhanVien/getAll',
      payload: {
        page: 1,
        size: 10,
      },
    });
  }

  getCond = () => {
    // lấy thông tin cond đang search
    const { model } = this.props;
    return _.get(model, 'paging.cond', {});
  };

  handleThem = () => {
    this.props.dispatch({
      type: 'nhanVien/changeState',
      payload: {
        showDrawer: true,
        edit: false,
        record: {},
        key: uuidv1(),
        isTouched: false,
      },
    });
  };

  viewNhanVien = record => {
    // this.props.dispatch({
    //   type: 'nhanVien/getOne',
    //   payload: {
    //     id: maNhanVien,
    //   },
    // });
    this.props.dispatch({
      type: 'nhanVien/changeState',
      payload: {
        viewNhanVien: true,
        record,
      },
    });
  };

  handleSua = record => {
    this.props.dispatch({
      type: 'nhanVien/changeState',
      payload: {
        showDrawer: true,
        edit: false,
        record: { ...record },
        key: uuidv1(),
        isTouched: false,
      },
    });
    this.setState({ item: record });
  };

  render() {
    const { nhanVien, loading } = this.props;
    let { danhSach, total, paging } = nhanVien;
    const { page, size } = paging;
    danhSach = danhSach.map((item, index) => ({
      ...item,
      index: index + 1 + (page - 1) * size,
      key: index,
    }));

    const columns = [
      {
        title: 'STT',
        dataIndex: 'index',
        key: 'index',
      },
      {
        title: 'Mã NV',
        dataIndex: 'maNhanVien',
      },
      {
        title: 'Họ tên',
        dataIndex: 'tenNhanVien',
      },
      {
        title: 'Ngày sinh',
        dataIndex: 'ngaySinh',
      },
      {
        title: 'Tài khoản',
        dataIndex: 'soTaiKhoanNganHang',
      },
      {
        title: 'Ngày công',
        dataIndex: 'tienLuongTheoNgay',
      },
      {
        title: 'Chi nhánh',
        dataIndex: 'chiNhanh.tenChiNhanh',
      },
      {
        title: 'Thao tác',
        render: (val, record) => (
          <div
            style={{
              display: 'flex',
              gap: '10px',
            }}
          >
            <Button onClick={() => this.viewNhanVien(record)}>Xem</Button>
            <Button onClick={() => this.handleSua(record)}>Sửa</Button>
          </div>
        ),
      },
    ];

    return (
      <div className="box">
        <Card bordered title={<div className="cardTitle">Quản lý nhân viên</div>}>
          <Row>
            <Col xs={24}>
              <Button
                style={{ marginRight: '20%' }}
                type="primary"
                // shape="round"
                icon="plus"
                // disabled
                onClick={() => this.handleThem()}
              >
                Thêm mới
              </Button>
              <h3 style={{ display: 'inline-block', margin: '0 10px 10px 50px', float: 'right' }}>
                {`Tổng số : ${total}`}
              </h3>

              <Table
                style={{ marginTop: 10 }}
                loading={loading}
                columns={columns}
                dataSource={danhSach}
                pagination={{
                  current: page,
                  position: 'bottom',
                  total,
                }}
              />
            </Col>
          </Row>
        </Card>
        <FormDrawer name="nhanVien">
          <Form item={this.state.item} />
        </FormDrawer>
        <FormModal name="nhanVien" field="viewNhanVien">
          <View />
        </FormModal>
      </div>
    );
  }
}

export default TableBase;
