/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Form, Input, Card, Row, Col, Button, DatePicker, Select, Spin, Badge, Table } from 'antd';
import _ from 'lodash';
import { getRecordValue } from '@/utils/utils';
import rules from '@/utils/rules';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import { formItemLayout, tailFormItemLayout, getSubmitText } from '@/utils/form';
import { connect } from 'dva';
import moment from 'moment';

@connect(({ loading, nhanVien }) => ({
  nhanVien,
  loading: loading.models.khoHang,
}))
class DonVi extends React.Component {
  componentDidMount() {}

  render() {
    const { nhanVien: model, loading } = this.props;
    const { record, thongTinNhanVien } = model;

    const columns = [
      {
        title: 'Mã NV',
        dataIndex: 'maNhanVien',
      },
      {
        title: 'Họ tên',
        dataIndex: 'tenNhanVien',
      },
      {
        title: 'Ngày sinh',
        dataIndex: 'ngaySinh',
      },
      {
        title: 'Tài khoản',
        dataIndex: 'soTaiKhoanNganHang',
      },
      {
        title: 'Ngày công',
        dataIndex: 'tienLuongTheoNgay',
      },
      {
        title: 'Chi nhánh',
        dataIndex: 'chiNhanh.tenChiNhanh',
      },
    ];

    return (
      <div className="box">
        <Card bordered title={<div className="cardTitle">{record.ghiChu}</div>}>
          <GridContent>
            <Spin spinning={!!loading}>
              <Table
                style={{ marginTop: 10 }}
                loading={!!loading}
                columns={columns}
                dataSource={[record]}
              />
            </Spin>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedForm = Form.create({ name: 'nhanVien' })(DonVi);

export default WrappedForm;
