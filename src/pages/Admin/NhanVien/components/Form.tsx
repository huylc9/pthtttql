/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Form, Input, Card, Row, Col, Button, DatePicker, Select, Spin } from 'antd';
import _ from 'lodash';
import { getRecordValue } from '@/utils/utils';
import rules from '@/utils/rules';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import { formItemLayout, tailFormItemLayout, getSubmitText } from '@/utils/form';
import moment from 'moment';

import { connect } from 'dva';

@connect(({ loading, nhanVien }) => ({
  nhanVien,
  loading: loading.models.nhanVien,
}))
class DonVi extends React.Component {
  componentDidMount() {}

  handleSubmit = (e: any) => {
    const { form, item } = this.props;
    e.preventDefault();
    form.validateFieldsAndScroll((err, values) => {
      if (err) return;

      if (!item) {
        this.props.dispatch({
          type: 'nhanVien/create',
          payload: {
            ...values,
          },
        });
      } else {
        this.props.dispatch({
          type: 'nhanVien/update',
          payload: {
            ...values,
          },
        });
      }
    });
  };

  render() {
    const {
      nhanVien: model,
      form: { getFieldDecorator },
      loading,
      item,
    } = this.props;
    const {record} = model;
    return (
      <div className="box">
        <Card
          bordered
          // title={<div className="cardTitle">{!model.edit ? 'Thêm mới' : 'Chỉnh sửa'}</div>}
          title={<div className="cardTitle">{!item ? 'Thêm mới' : 'Chỉnh sửa'}</div>}
        >
          <GridContent>
            <Row>
              <Col span={18}>
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                  <Form.Item label="Mã chi nhánh">
                    {getFieldDecorator('maChiNhanh', {
                      initialValue: (item && item.chiNhanh && item.chiNhanh.maChiNhanh) || '',
                      rules: [...rules.text, ...rules.required],
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Mã nhân viên">
                    {getFieldDecorator('maNhanVien', {
                      initialValue: (item && item.maNhanVien) || '',
                      rules: [...rules.text, ...rules.required],
                    })(<Input disabled={item} />)}
                  </Form.Item>
                  <Form.Item label="Tên NV">
                    {getFieldDecorator('tenNhanVien', {
                      initialValue: (item && item.tenNhanVien) || '',
                      rules: [...rules.text, ...rules.required],
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Ngày sinh">
                    {getFieldDecorator('ngaySinh', {
                      initialValue:
                        (item && item.ngaySinh && moment(new Date(item.ngaySinh))) || '',
                      rules: [...rules.ngaySinh],
                    })(<DatePicker format={['DD/MM/YYYY']} />)}
                  </Form.Item>
                  <Form.Item label="Ngày công">
                    {getFieldDecorator('tienLuongTheoNgay', {
                      initialValue: _.get(record, "tienLuongTheoNgay", 0),
                      rules: [...rules.number(100000000, 0), ...rules.required],
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Số TK">
                    {getFieldDecorator('soTaiKhoanNganHang', {
                      initialValue: (item && item.soTaiKhoanNganHang) || '',
                      rules: [...rules.text, ...rules.required],
                    })(<Input />)}
                  </Form.Item>
                  <Spin spinning={!!loading}>
                    <Form.Item {...tailFormItemLayout}>
                      <Button.Group>
                        <Button type="primary" icon="plus" htmlType="submit">
                          {/* {getSubmitText(model)} */}
                          {!item ? 'Thêm mới' : 'Chỉnh sửa'}
                        </Button>
                      </Button.Group>
                    </Form.Item>
                  </Spin>
                </Form>
              </Col>
            </Row>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedForm = Form.create({ name: 'nhanVien' })(DonVi);

export default WrappedForm;
