/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Form, Input, Card, Row, Col, Button, DatePicker, Select, Spin } from 'antd';
import _ from 'lodash';
import { getRecordValue } from '@/utils/utils';
import rules from '@/utils/rules';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import { formItemLayout, tailFormItemLayout, getSubmitText } from '@/utils/form';

import { connect } from 'dva';

@connect(({ loading, chiNhanh }) => ({
  chiNhanh,
  loading: loading.models.chiNhanh,
}))
class DonVi extends React.Component {
  componentDidMount() {}

  handleSubmit = e => {
    const { form, item } = this.props;
    e.preventDefault();
    form.validateFieldsAndScroll((err, values) => {
      if (err) return;

      if (!item) {
        this.props.dispatch({
          type: 'chiNhanh/create',
          payload: {
            ...values,
          },
        });
      } else {
        this.props.dispatch({
          type: 'chiNhanh/update',
          payload: {
            ...values,
          },
        });
      }
    });
  };

  render() {
    const {
      chiNhanh: model,
      form: { getFieldDecorator },
      loading,
      item,
    } = this.props;
    const { record } = model;
    return (
      <div className="box">
        <Card
          bordered
          title={
            <div className="cardTitle">
              {!item ? 'Thêm mới' : 'Chỉnh sửa'}
              {/* {!model.edit ? 'Thêm mới' : 'Chỉnh sửa'} */}
            </div>
          }
        >
          <GridContent>
            <Row>
              <Col span={18}>
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                  <Form.Item label="Mã chi nhánh">
                    {getFieldDecorator('maChiNhanh', {
                      initialValue: _.get(record, "maChiNhanh", ""),
                      rules: [...rules.text, ...rules.required],
                    })(<Input disabled={!!record.maChiNhanh} />)}
                  </Form.Item>
                  <Form.Item label="Tên chi nhánh">
                    {getFieldDecorator('tenChiNhanh', {
                      initialValue: _.get(record, "tenChiNhanh", ""),
                      rules: [...rules.text, ...rules.required],
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Địa chỉ">
                    {getFieldDecorator('diaChi', {
                      initialValue: _.get(record, "diaChi", ""),
                      rules: [...rules.text, ...rules.required],
                    })(<Input />)}
                  </Form.Item>
                  <Spin spinning={!!loading}>
                    <Form.Item {...tailFormItemLayout}>
                      <Button.Group>
                        <Button type="primary" icon="plus" htmlType="submit">
                          {/* {getSubmitText(model)} */}
                          {!item ? 'Thêm mới' : 'Chỉnh sửa'}
                        </Button>
                      </Button.Group>
                    </Form.Item>
                  </Spin>
                </Form>
              </Col>
            </Row>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedForm = Form.create({ name: 'chiNhanh' })(DonVi);

export default WrappedForm;
