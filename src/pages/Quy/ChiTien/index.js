
/* eslint-disable react/destructuring-assignment */
import { Button, Card, Col, Icon, Input, Row, Table, Divider, Popconfirm} from 'antd';
import _ from 'lodash';
import { connect } from 'dva';
import React from 'react';
import uuidv1 from 'uuid/v1';
import FormDrawer from '@/components/Drawer/FormDrawer';
import Form from './components/Form';
import moment from 'moment';
import { currencyFormat } from '@/utils/utils';
import FormHachToan from "@/pages/Quy/HachToan/Form";
import { getTypeGiaoDich, lyDoGiaoDich, taiKhoanNoCo } from '@/utils/taikhoan';


@connect(({ loading, chiTien }) => ({
  chiTien,
  loading: loading.models.chiTien,
}))
class TableBase extends React.Component {
  componentDidMount() {
    this.props.dispatch({
      type: `chiTien/getAll`,
      payload: {
        page: 1,
        size: 10,
      },
    });
  }

  getCond = () => {
    // lấy thông tin cond đang search
    const { model } = this.props;
    return _.get(model, 'paging.cond', {});
  };

  handleThem = lyDo => {
    this.props.dispatch({
      type: `chiTien/changeState`,
      payload: {
        showDrawer: true,
        edit: false,
        record: {lyDo},
        key: uuidv1(),
        isTouched: false,
      },
    });
  };

  handleDel = _id => {
    this.props.dispatch({
      type: `chiTien/del`,
      payload: {
        _id,
      },
    });
  };

  handleEdit = record => {
    this.props.dispatch({
      type: `chiTien/changeState`,
      payload: {
        showDrawer: true,
        edit: true,
        record,
      },
    });
  };

  handleHachToan = (giaoDich, view) => {
    const type = getTypeGiaoDich(giaoDich.lyDo);
    if (view) {
      this.props.dispatch({
        type: "hachToan/changeState",
        payload: {
          giaoDich,
          view,
          showDrawer: true
        }
      });
      return;
    }
    this.props.dispatch({
      type: "hachToan/changeState",
      payload: {
        giaoDich,
        view,
        showDrawer: true,
        record: {
          taiKhoanNo: type ? taiKhoanNoCo[type].no : 0,
          taiKhoanCo: type ? taiKhoanNoCo[type].co : 0,
        },
        onComplete: () => {
          this.props.dispatch({
            type: "chiTien/update",
            payload: {
              maGiaoDich: giaoDich.maGiaoDich,
              thoiGianHachToan: moment().toISOString(),
            }
          })
        }
      }
    })
  }

  render() {
    let {
      chiTien,
      loading
    } = this.props;
    let { danhSach, total, paging } = chiTien;
    const { page, size } = paging;
    danhSach = danhSach.map((item, index) => ({
      ...item,
      index: index + 1 + (page - 1) * size,
      key: index,
      isHachToan: !!item.thoiGianHachToan && moment(item.thoiGianGiaoDich).isBefore(moment(item.thoiGianHachToan)),
    }));

    const columns = [{
      title: 'STT',
      dataIndex: 'index',
    },
    {
      title: 'Mã giao dịch',
      dataIndex: 'maGiaoDich',
    },
    {
      title: 'Đối tượng',
      dataIndex: 'doiTuongGiaoDich',
    },
    {
      title: 'Thời gian giao dịch',
      dataIndex: 'thoiGianGiaoDich',
      render: (val) => moment(val).format("DD/MM/YYYY HH:mm")
    },
    {
      title: "Số tiền",
      dataIndex: "tongTien",
      render: val => (!!val ? currencyFormat(val) : 0) + " đ",
    },
    {
      title: 'Thời gian hạch toán',
      dataIndex: 'thoiGianHachToan',
      render: (val, record) => (record.isHachToan ? moment(val).format("DD/MM/YYYY HH:mm") : "Chưa hạch toán"),
    },
    {
      title: "Lý do",
      dataIndex: 'lyDo',
    },
    {
      title: 'Nhân viên thực hiện',
      dataIndex: 'userCreated.username',
    },
    ];

    const list = (text, record) => (
      <React.Fragment>
          <React.Fragment>
            <Button
              type={"primary"}
              // shape="circle"
              icon={record.isHachToan ? "eye" : "edit"}
              onClick={() => this.handleHachToan(record, record.isHachToan)}
              title="Hạch toán"
            >
              {record.isHachToan ? "Xem hạch toán" : "Hạch toán"}
            </Button>
          </React.Fragment>
      </React.Fragment>
    );

    const last = {
      // phần tử cuối của columns
      title: 'Tùy chọn',
      width: '120px',
      align: 'center',
      fixed: 'right',
      render: (text, record) => list(text, record),
    };

    columns.push(last);

    return (
      <div className="box">
        <Card bordered title={<div className="cardTitle">Quản lý thu tiền</div>}>
          <Row>
            <div style={{margin: "0 20px 0 0", display: "inline-block"}}>
                <Button
                  type="primary"
                  shape="round"
                  icon="plus"
                  // disabled
                  onClick={() => this.handleThem(lyDoGiaoDich.guiTienNganHang)}
                >
                  Gửi tiền ngân hàng
                </Button>
            </div>
            <div style={{margin: "0 20px 0 0", display: "inline-block"}}>
                <Button
                  type="primary"
                  shape="round"
                  icon="plus"
                  // disabled
                  onClick={() => this.handleThem(lyDoGiaoDich.tamUngNV)}
                >
                  Tạm ứng cho nhân viên
                </Button>
            </div>
            <div style={{margin: "0 20px 0 0", display: "inline-block"}}>
                <Button
                  type="primary"
                  shape="round"
                  icon="plus"
                  // disabled
                  onClick={() => this.handleThem(lyDoGiaoDich.traTienHangNCC)}
                >
                  Trả trước tiền hàng NCC
                </Button>
            </div>
            <div style={{margin: "0 20px 0 0", display: "inline-block"}}>
                <Button
                  type="primary"
                  shape="round"
                  icon="plus"
                  // disabled
                  onClick={() => this.handleThem(lyDoGiaoDich.thanhToanChiPhiKhac)}
                >
                  Thanh toán chí phí khác
                </Button>
            </div>
          </Row>
          <Row>
            <h3 style={{ display: 'inline-block', margin: '0 10px 10px 50px', float: 'right' }}>
                {`Tổng số : ${total}`}
              </h3>
            <Table
                style={{ marginTop: 10 }}
                loading={loading}
                columns={columns}
                dataSource={danhSach}
                onChange={this.onChange}
                pagination={{
                  current: page,
                  position: 'bottom',
                  total,
                }}
              />
          </Row>
        </Card>
        <FormDrawer name={`chiTien`}>
          <Form />
        </FormDrawer>
        <FormDrawer name={`hachToan`}>
          <FormHachToan />
        </FormDrawer>
      </div>
    );
  }
}

export default TableBase;
