/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Form, Input, Card, Row, Col, Button, DatePicker, Select, Spin, TimePicker } from 'antd';
import _ from 'lodash';
import { getRecordValue } from '@/utils/utils';
import rules from '@/utils/rules';
import moment from 'moment';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import {
  formItemLayout,
  tailFormItemLayout,
  getSubmitText,
} from '@/utils/form';

import {connect} from 'dva';


// {giaoDich, view}

@connect(({ loading, hachToan }) => ({
  hachToan,
  loading: loading.models.hachToan,
}))
class DonVi extends React.Component {
  componentDidMount() {
    const { hachToan: { view, giaoDich: {maGiaoDich} } } = this.props;
    if (view && maGiaoDich) {
      this.props.dispatch({
        type: "hachToan/getByMaGiaoDich",
        payload: {
          maGiaoDich
        }
      })
    }
  }

  handleSubmit = e => {
    const {
      form,
      hachToan: { edit, onComplete, view },
    } = this.props;
      if (view) {
        this.props.dispatch({
          type: "hachToan/changeState",
          payload: {
            showDrawer: false,
            record: {}
          }
        })
        return;
      }
    e.preventDefault();
    form.validateFieldsAndScroll((err, values) => {
      if (err) return;
      this.props.dispatch({
        type: "hachToan/create",
        payload: {
          ...values,
          ngayHachToan: moment().toISOString(),
        },
        onComplete
      })
    });
  };

  render() {
    const {
      hachToan: model,
      form: { getFieldDecorator },
      loading,
    } = this.props;
  const { view, record, giaoDich: {maGiaoDich, doiTuongGiaoDich: doiTuong, tongTien: soTien} } = model;
    return (
      <div className="box">
        <Card
          bordered
          title={<div className="cardTitle">{!view ? 'Hạch toán giao dịch' : 'Xem thông tin'}</div>}
        >
          <GridContent>
            <Row>
              <Col span={18}>
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                  <Form.Item label="Mã giao dịch">
                    {getFieldDecorator('maGiaoDich', {
                      initialValue: maGiaoDich || _.get(record, "maGiaoDich", ""),
                      rules: [...rules.text, ...rules.required],
                    })(<Input disabled={!!view} />)}
                  </Form.Item>
                  <Form.Item label="Diễn giải">
                    {getFieldDecorator('dienGiai', {
                      initialValue: _.get(record, "dienGiai", ""),
                      rules: [...rules.text, ...rules.required],
                    })(<Input disabled={!!view} />)}
                  </Form.Item>
                  <Form.Item label="Đối tượng">
                    {getFieldDecorator('doiTuong', {
                      initialValue: doiTuong || _.get(record, "doiTuong", ""),
                      rules: [...rules.text, ...rules.required],
                    })(<Input disabled={!!view} />)}
                  </Form.Item>
                  <Form.Item label="Tài khoản nợ">
                    {getFieldDecorator('taiKhoanNo', {
                      initialValue: _.get(record, "taiKhoanNo", ""),
                      rules: [...rules.number(), ...rules.required],
                    })(<Input disabled={!!view} />)}
                  </Form.Item>
                  <Form.Item label="Tài khoản có">
                    {getFieldDecorator('taiKhoanCo', {
                      initialValue: _.get(record, "taiKhoanCo", ""),
                      rules: [...rules.number(), ...rules.required],
                    })(<Input disabled={!!view} />)}
                  </Form.Item>
                  <Form.Item label="Số tiền">
                    {getFieldDecorator('soTien', {
                      initialValue: soTien ? String(soTien) : String(_.get(record, "soTien", "")),
                      rules: [...rules.text, ...rules.required],
                    })(<Input disabled={!!view} />)}
                  </Form.Item>
                  <Spin spinning={!!loading}>
                    <Form.Item {...tailFormItemLayout}>
                      <Button.Group>
                        <Button type="primary" icon="plus" htmlType="submit">
                          {view ? "Xong" : "Thêm mới"}
                        </Button>
                      </Button.Group>
                    </Form.Item>
                  </Spin>
                </Form>
              </Col>
            </Row>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedForm = Form.create({ name: 'hachToan' })(DonVi);

export default WrappedForm;
