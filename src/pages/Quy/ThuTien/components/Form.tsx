/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Form, Input, Card, Row, Col, Button, DatePicker, Select, Spin, TimePicker } from 'antd';
import _ from 'lodash';
import { getRecordValue } from '@/utils/utils';
import rules from '@/utils/rules';
import moment from 'moment';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import {
  formItemLayout,
  tailFormItemLayout,
  handleValuesChange,
  handleCommonSubmit,
  getSubmitText,
} from '@/utils/form';

import {connect} from 'dva';

@connect(({ loading, thuTien }) => ({
  thuTien,
  loading: loading.models.thuTien,
}))
class DonVi extends React.Component {
  componentDidMount() {}

  handleSubmit = e => {
    const {
      form,
      thuTien: {edit},
    } = this.props;
    e.preventDefault();
    form.validateFieldsAndScroll((err, values) => {
      if (err) return;
      this.props.dispatch({
        type: "thuTien/create",
        payload: {
          ...values,
          thoiGianHachToan: values.thoiGianGiaoDich
        },
      })
    });
  };

  render() {
    const {
      thuTien: model,
      form: { getFieldDecorator },
      loading,
    } = this.props;
    const { edit, record } = model;
    return (
      <div className="box">
        <Card
          bordered
          title={<div className="cardTitle">{!model.edit ? 'Thêm mới' : 'Chỉnh sửa'}</div>}
        >
          <GridContent>
            <Row>
              <Col span={18}>
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                  <Form.Item label="Mã giao dịch">
                    {getFieldDecorator('maGiaoDich', {
                      initialValue: _.get(record, "maGiaoDich", ""),
                      rules: [...rules.text, ...rules.required],
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Đối tượng">
                    {getFieldDecorator('doiTuongGiaoDich', {
                      initialValue: _.get(record, "doiTuongGiaoDich", ""),
                      rules: [...rules.text, ...rules.required],
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Thời gian giao dịch">
                    {getFieldDecorator('thoiGianGiaoDich', {
                      initialValue: moment(),
                      rules: [...rules.ngaySinh],
                    })(<DatePicker showTime={{ format: "HH:mm"}} placeholder="DD/MM/YYYY HH:mm" format="DD/MM/YYYY HH:mm" />)}
                  </Form.Item>
                  <Form.Item label="Số tiền">
                    {getFieldDecorator('tongTien', {
                      initialValue: _.get(record, "tongTien", ""),
                      rules: [...rules.number(), ...rules.required],
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Lý do">
                    {getFieldDecorator('lyDo', {
                      initialValue: _.get(record, "lyDo", ""),
                      rules: [...rules.text, ...rules.required],
                    })(<Input disabled />)}
                  </Form.Item>
                  <Spin spinning={!!loading}>
                    <Form.Item {...tailFormItemLayout}>
                      <Button.Group>
                        <Button type="primary" icon="plus" htmlType="submit">
                          {getSubmitText(model)}
                        </Button>
                      </Button.Group>
                    </Form.Item>
                  </Spin>
                </Form>
              </Col>
            </Row>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedForm = Form.create({ name: 'thuTien' })(DonVi);

export default WrappedForm;
