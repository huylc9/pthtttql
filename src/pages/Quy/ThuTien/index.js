
/* eslint-disable react/destructuring-assignment */
import { Button, Card, Col, Icon, Input, Row, Table, Divider, Popconfirm} from 'antd';
import _ from 'lodash';
import { connect } from 'dva';
import React from 'react';
import uuidv1 from 'uuid/v1';
import FormDrawer from '@/components/Drawer/FormDrawer';
import Form from './components/Form';
import moment from 'moment';
import { currencyFormat } from '@/utils/utils';
import FormHachToan from "@/pages/Quy/HachToan/Form";
import { getTypeGiaoDich, taiKhoanNoCo, lyDoGiaoDich } from '@/utils/taikhoan';

@connect(({ loading, thuTien }) => ({
  thuTien,
  loading: loading.models.thuTien,
}))
class TableBase extends React.Component {
  componentDidMount() {
    this.props.dispatch({
      type: `thuTien/getAll`,
      payload: {
        page: 1,
        size: 10,
      },
    });
  }

  getCond = () => {
    // lấy thông tin cond đang search
    const { model } = this.props;
    return _.get(model, 'paging.cond', {});
  };

  handleThem = lyDo => {
    this.props.dispatch({
      type: `thuTien/changeState`,
      payload: {
        showDrawer: true,
        edit: false,
        record: {lyDo},
        key: uuidv1(),
        isTouched: false,
      },
    });
  };

  handleDel = _id => {
    this.props.dispatch({
      type: `thuTien/del`,
      payload: {
        _id,
      },
    });
  };

  handleEdit = record => {
    this.props.dispatch({
      type: `thuTien/changeState`,
      payload: {
        showDrawer: true,
        edit: true,
        record,
      },
    });
  };

  handleHachToan = (giaoDich, view) => {
    const type = getTypeGiaoDich(giaoDich.lyDo);
    if (view) {
      this.props.dispatch({
        type: "hachToan/changeState",
        payload: {
          giaoDich,
          view,
          showDrawer: true
        }
      });
      return;
    }
    this.props.dispatch({
      type: "hachToan/changeState",
      payload: {
        giaoDich,
        view,
        showDrawer: true,
        record: {
          taiKhoanNo: type ? taiKhoanNoCo[type].no : 0,
          taiKhoanCo: type ? taiKhoanNoCo[type].co : 0,
        },
        onComplete: () => {
          this.props.dispatch({
            type: "thuTien/update",
            payload: {
              maGiaoDich: giaoDich.maGiaoDich,
              thoiGianHachToan: moment(),
            }
          })
        }
      }
    })
  }

  render() {
    let {
      thuTien,
      loading
    } = this.props;
    let { danhSach, total, paging } = thuTien;
    const { page, size } = paging;
    danhSach = danhSach.map((item, index) => ({
      ...item,
      index: index + 1 + (page - 1) * size,
      key: index,
      isHachToan: !!item.thoiGianHachToan && moment(item.thoiGianGiaoDich).isBefore(moment(item.thoiGianHachToan)),
    }));

    const columns = [{
      title: 'STT',
      dataIndex: 'index',
    },
    {
      title: 'Mã giao dịch',
      dataIndex: 'maGiaoDich',
    },
    {
      title: 'Đối tượng',
      dataIndex: 'doiTuongGiaoDich',
    },
    {
      title: 'Thời gian giao dịch',
      dataIndex: 'thoiGianGiaoDich',
      render: (val) => moment(val).format("DD/MM/YYYY HH:mm")
    },
    {
      title: "Số tiền",
      dataIndex: "tongTien",
      render: val => (!!val ? currencyFormat(val) : 0) + " đ",
    },
    {
      title: 'Thời gian hạch toán',
      dataIndex: 'thoiGianHachToan',
      render: (val, record) => (record.isHachToan ? moment(val).format("DD/MM/YYYY HH:mm") : "Chưa hạch toán"),
    },
    {
      title: "Lý do",
      dataIndex: 'lyDo',
    },
    {
      title: 'Nhân viên thực hiện',
      dataIndex: 'userCreated.username',
    },
    ];

    const list = (text, record) => (
      <React.Fragment>
          <React.Fragment>
            <Button
              type={"primary"}
              // shape="circle"
              icon={record.isHachToan ? "eye" : "edit"}
              onClick={() => this.handleHachToan(record, record.isHachToan)}
              title="Hạch toán"
            >
              {record.isHachToan ? "Xem hạch toán" : "Hạch toán"}
            </Button>
          </React.Fragment>
      </React.Fragment>
    );

    const last = {
      // phần tử cuối của columns
      title: 'Tùy chọn',
      width: '120px',
      align: 'center',
      fixed: 'right',
      render: (text, record) => list(text, record),
    };

    columns.push(last);

    return (
      <div className="box">
        <Card bordered title={<div className="cardTitle">Quản lý thu tiền</div>}>
          <Row>
                <Button
                  style={{ marginRight: '20px' }}
                  type="primary"
                  shape="round"
                  icon="plus"
                  // disabled
                  onClick={() => this.handleThem(lyDoGiaoDich.rutTienNganHang)}
                >
                  Rút tiền ngân hàng
                </Button>
                <Button
                  style={{ marginRight: '20px' }}
                  type="primary"
                  shape="round"
                  icon="plus"
                  // disabled
                  onClick={() => this.handleThem(lyDoGiaoDich.rutTienNganHang)}
                >
                  Thu tiền tạm ứng của nhân viên
                </Button>
                <Button
                  style={{ marginRight: '20px' }}
                  type="primary"
                  shape="round"
                  icon="plus"
                  // disabled
                  onClick={() => this.handleThem(lyDoGiaoDich.banHang)}
                >
                  Thu tiền từ đơn mua hàng
                </Button>
                <Button
                  style={{ marginRight: '20px' }}
                  type="primary"
                  shape="round"
                  icon="plus"
                  // disabled
                  onClick={() => this.handleThem(lyDoGiaoDich.dauTu)}
                >
                  Nhận tiền đầu tư
                </Button>
          </Row>
          <Row>
            <h3 style={{ display: 'inline-block', margin: '0 10px 10px 50px', float: 'right' }}>
                {`Tổng số : ${total}`}
              </h3>
            <Table
                style={{ marginTop: 10 }}
                loading={loading}
                columns={columns}
                dataSource={danhSach}
                onChange={this.onChange}
                pagination={{
                  current: page,
                  position: 'bottom',
                  total,
                }}
              />
          </Row>
        </Card>
        <FormDrawer name={`thuTien`}>
          <Form />
        </FormDrawer>
        <FormDrawer name={`hachToan`}>
          <FormHachToan />
        </FormDrawer>
      </div>
    );
  }
}

export default TableBase;
