
/* eslint-disable react/destructuring-assignment */
import { Button, Card, Col, Icon, Input, Row, Table, Divider, Popconfirm} from 'antd';
import _ from 'lodash';
import { connect } from 'dva';
import React from 'react';
import uuidv1 from 'uuid/v1';
import FormModal from '@/components/Drawer/FormModal';
import FormDrawer from '@/components/Drawer/FormDrawer';
import Form from './components/Form';
import { taiKhoanNoCo } from '@/utils/taikhoan';
import FormHachToan from "@/pages/Quy/HachToan/Form";
import moment from 'moment';
import FormNhanVien from "./components/FormNhanVien";
import View from "./components/ViewBangChamCong";

@connect(({ loading, bangChamCong }) => ({
  bangChamCong,
  loading: loading.models.bangChamCong,
}))
class TableBase extends React.Component {
  componentDidMount() {
    this.props.dispatch({
      type: `bangChamCong/getAll`,
      payload: {
        page: 1,
        size: 10,
      },
    });
  }

  getCond = () => {
    // lấy thông tin cond đang search
    const { model } = this.props;
    return _.get(model, 'paging.cond', {});
  };

  handleThem = () => {
    this.props.dispatch({
      type: `bangChamCong/changeState`,
      payload: {
        showDrawer: true,
        edit: false,
        record: {},
        key: uuidv1(),
        isTouched: false,
      },
    });
  };

  handleDel = _id => {
    this.props.dispatch({
      type: `bangChamCong/del`,
      payload: {
        _id,
      },
    });
  };

  handleEdit = record => {
    this.props.dispatch({
      type: `bangChamCong/changeState`,
      payload: {
        showDrawer: true,
        edit: true,
        record,
      },
    });
  };

  handleHachToan = (giaoDich, view) => {
    const maGiaoDich = "KHO" + giaoDich.idPhieuKho;
    const type = (giaoDich.loaiPhieuKho == "XUATKHO") ? "xuatKho" : "nhapKho";
    if (view) {
      this.props.dispatch({
        type: "hachToan/changeState",
        payload: {
          giaoDich: {maGiaoDich},
          view,
          showDrawer: true
        }
      });
      return;
    }
    this.props.dispatch({
      type: "hachToan/changeState",
      payload: {
        giaoDich,
        view,
        showDrawer: true,
        record: {
          taiKhoanNo: type ? taiKhoanNoCo[type].no : 0,
          taiKhoanCo: type ? taiKhoanNoCo[type].co : 0,
          maGiaoDich
        },
        onComplete: () => {
          this.props.dispatch({
            type: "bangChamCong/update",
            payload: {
              idPhieuKho: giaoDich.idPhieuKho,
              ngayHachToan: moment().toISOString(),
            }
          })
        }
      }
    })
  }

  handleChamCong = record => {
    this.props.dispatch({
      type: "bangChamCong/changeState",
      payload: {
        record, 
        showDrawerNV: true
      }
    })
  }

  viewBangChamCong = record => {
    const { maBangChamCong } = record;
    this.props.dispatch({
      type: "bangChamCong/getDuLieu",
      payload: {
        maBangChamCong
      }
    })
    this.props.dispatch({
      type: "bangChamCong/changeState",
      payload: {
        viewBangChamCong: true,
        record
      }
    })
  }

  handleKhoaBang = record => {
    this.props.dispatch({
      type: "bangChamCong/khoaBang",
      payload: record
    })
  }

  render() {
    let {
      bangChamCong,
      loading
    } = this.props;
    let { danhSach, total, paging } = bangChamCong;
    const { page, size } = paging;
    // danhSach = [{
    //   ngayTao: moment(),
    //   ghiChu: "ghi chu",
    //   chiNhanh: "HN"
    // }]
    danhSach = danhSach.map((item, index) => ({
      ...item,
      index: index + 1 + (page - 1) * size,
      key: index,
      isHachToan: !!item.thoiGianHachToan && moment(item.thoiGianGiaoDich).isBefore(moment(item.thoiGianHachToan)),
    }));

    const columns = [{
      title: 'STT',
      dataIndex: 'index',
    },
    {
      title: 'Ngày tạo',
      dataIndex: 'ngayTao',
      render: val => moment(val).format("DD/MM/YYYY")
    },
    {
      title: 'Chi nhánh',
      dataIndex: 'chiNhanhEntity.tenChiNhanh',
    },
    {
      title: 'Tên bảng',
      dataIndex: 'ghiChu',
    },
    ];

    const list = (text, record) => (
      <React.Fragment>
          <React.Fragment>
            <Button
              type="primary"
              icon={'eye'}
              onClick={() => this.viewBangChamCong(record)}
              title="Xem"
            >
              Xem thông tin
            </Button>
            <React.Fragment>
              <Divider type="vertical" />
              <Button
                type="primary"
                icon={'edit'}
                onClick={() => this.handleChamCong(record)}
                title="Chấm công"
              >
                Chấm công
              </Button>
            </React.Fragment>
          </React.Fragment>
      </React.Fragment>
    );

    const last = {
      // phần tử cuối của columns
      title: 'Tùy chọn',
      // width: '120px',
      align: 'center',
      fixed: 'right',
      render: (text, record) => list(text, record),
    };

    columns.push(last);

    return (
      <div className="box">
        <Card bordered title={<div className="cardTitle">Quản lý bảng chấm công</div>}>
          <Row>
            <Col xs={24}>
                <Button
                  style={{ marginRight: '20%' }}
                  type="primary"
                  // shape="round"
                  icon="plus"
                  // disabled
                  onClick={() => this.handleThem()}
                >
                  Thêm mới
                </Button>
              <h3 style={{ display: 'inline-block', margin: '0 10px 10px 50px', float: 'right' }}>
                {`Tổng số : ${total}`}
              </h3>

              <Table
                style={{ marginTop: 10 }}
                loading={loading}
                columns={columns}
                dataSource={danhSach}
                onChange={this.onChange}
                pagination={{
                  current: page,
                  position: 'bottom',
                  total,
                }}
              />
            </Col>
          </Row>
        </Card>
        <FormDrawer name={`bangChamCong`}>
          <Form />
        </FormDrawer>
        <FormDrawer name={`hachToan`}>
          <FormHachToan />
        </FormDrawer>
        <FormDrawer name={`bangChamCong`} field="showDrawerNV">
          <FormNhanVien />
        </FormDrawer>
        <FormModal name={`bangChamCong`} field="viewBangChamCong">
            <View />
        </FormModal>
      </div>
    );
  }
}

export default TableBase;
