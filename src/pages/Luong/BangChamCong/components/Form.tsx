/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Form, Input, Card, Row, Col, Button, DatePicker, Select, Spin } from 'antd';
import _ from 'lodash';
import { getRecordValue } from '@/utils/utils';
import rules from '@/utils/rules';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import {
  formItemLayout,
  tailFormItemLayout,
  handleValuesChange,
  handleCommonSubmit,
  getSubmitText,
} from '@/utils/form';

import {connect} from 'dva';
import moment from 'moment';

@connect(({ loading, bangChamCong, nhanVien, chiNhanh}) => ({
  bangChamCong,
  nhanVien,
  chiNhanh,
  loading: loading.models.bangChamCong,
}))
class DonVi extends React.Component {
  componentDidMount() { }

  handleSubmit = e => {
    const {
      form,
    } = this.props;
    e.preventDefault();
    form.validateFieldsAndScroll((err, values) => {
      if (err) return;
      this.props.dispatch({
        type: "bangChamCong/create",
        payload: {
          ...values,
          ngayTao: moment().toISOString(),
        },
        onComplete: () => {}
      })
    });
  };

  render() {
    const {
      bangChamCong: model,
      nhanVien: { danhSach: danhSachNhanVien },
      chiNhanh: {danhSach: danhSachChiNhanh},
      form: { getFieldDecorator },
      loading,
    } = this.props;
    const { edit, record } = model;
    return (
      <div className="box">
        <Card
          bordered
          title={<div className="cardTitle">{!model.edit ? 'Thêm mới' : 'Chỉnh sửa'}</div>}
        >
          <GridContent>
            <Row>
              <Col span={18}>
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                  <Form.Item label="Tên bảng chấm công">
                    {getFieldDecorator('ghiChu', {
                      initialValue: _.get(record, "ghiChu", ""),
                      rules: [...rules.text, ...rules.required],
                    })(<Input />)}
                  </Form.Item>
                  {/* <Form.Item label="Thời gian bắt đầu">
                    {getFieldDecorator('thoiGianBatDau', {
                      initialValue: moment().startOf("month"),
                    })(<DatePicker format="DD/MM/YYYY"/>)}
                  </Form.Item> */}
                  {/* <Form.Item label="Tên bảng chấm công">
                    {getFieldDecorator('ghiChu', {
                      initialValue: _.get(record, "ghiChu", ""),
                      rules: [...rules.text, ...rules.required],
                    })(<Input />)}
                  </Form.Item> */}


                  <Form.Item label="Chi nhánh">
                    {getFieldDecorator('chiNhanhEntity', {
                      initialValue: _.get(record, 'chiNhanhEntity', undefined),
                      rules: [...rules.required],
                    })(
                      <Select placeholder="" style={{ width: '100%' }}>
                        {danhSachChiNhanh.map(item => (
                          <Option value={item.maChiNhanh}>{item.tenChiNhanh}</Option>
                        ))}
                      </Select>,
                  )}
                  </Form.Item>
{/* 
                   <Form.Item label="Loại thuốc">
                    {getFieldDecorator('maHangHoa', {
                      initialValue: _.get(record, 'maHangHoa', undefined),
                      rules: [...rules.required],
                    })(
                      <Select placeholder="" style={{ width: '100%' }}>
                        {danhSachHangHoa.map(item => (
                          <Option value={item.maHangHoa}>{item.tenHangHoa + ": " + item.donGia}</Option>
                        ))}
                      </Select>,
                  )}
                  </Form.Item>
                  <Form.Item label="Số lượng">
                    {getFieldDecorator('soLuong', {
                      initialValue: _.get(record, "soLuong", ""),
                      rules: [...rules.number(1000000, 0), ...rules.required],
                    })(<Input />)}
                  </Form.Item> */}
                  <Spin spinning={!!loading}>
                    <Form.Item {...tailFormItemLayout}>
                      <Button.Group>
                        <Button type="primary" icon="plus" htmlType="submit">
                          {getSubmitText(model)}
                        </Button>
                      </Button.Group>
                    </Form.Item>
                  </Spin>
                </Form>
              </Col>
            </Row>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedForm = Form.create({ name: 'bangChamCong' })(DonVi);

export default WrappedForm;
