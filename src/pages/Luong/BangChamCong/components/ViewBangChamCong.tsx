/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Form, Input, Card, Row, Col, Button, DatePicker, Select, Spin, Badge, Tag, Table } from 'antd';
import _ from 'lodash';
import { getRecordValue } from '@/utils/utils';
import rules from '@/utils/rules';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import {
  formItemLayout,
  tailFormItemLayout,
  handleValuesChange,
  handleCommonSubmit,
  getSubmitText,
} from '@/utils/form';

import {connect} from 'dva';
import moment from 'moment';

@connect(({ loading, bangChamCong }) => ({
  bangChamCong,
  loading: loading.models.bangChamCong,
}))
class DonVi extends React.Component {
  componentDidMount() {
    
  }

  render() {
    const {
      bangChamCong: model,
      loading,
    } = this.props;
    const { record, thongTinChamCong } = model;
    let data = {};
    thongTinChamCong.map(({nhanVienInBangChamCongId: item}) => {
      const {ngayChamCong} = item;
      const tenNhanVien = _.get(item, "nhanVienEntity.tenNhanVien");
      if (!tenNhanVien) return;
      if (!data[ngayChamCong]) {
        data[ngayChamCong] = [tenNhanVien];
      }
      else data[ngayChamCong].push(tenNhanVien);
    })

    console.log({ data });
    const realData = Object.keys(data).sort((a, b) => (moment(a).isAfter(b) ? 1 : -1)).map(key => ({
      ngayCham: key,
      nhanVien: data[key]
    }))
    console.log({ realData });

    const columns = [{
      title: 'Ngày',
      dataIndex: 'ngayCham',
    },
    {
      title: 'Nhân viên',
      dataIndex: 'nhanVien',
      render: val => (
        <React.Fragment>
          {val.map(item => (<Tag>{ item }</Tag>))}
        </React.Fragment>
      )
    },
    ];
    return (
      <div className="box">
        <Card
          bordered
          title={<div className="cardTitle">{record.ghiChu}</div>}
        >
          <GridContent>

            <Spin spinning={!!loading}>
              <Table
                style={{ marginTop: 10 }}
                loading={!!loading}
                columns={columns}
                dataSource={realData}
              />
            </Spin>

          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedForm = Form.create({ name: 'bangChamCong' })(DonVi);

export default WrappedForm;
