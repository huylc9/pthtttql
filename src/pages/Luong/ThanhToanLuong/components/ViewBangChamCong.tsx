/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Form, Input, Card, Row, Col, Button, DatePicker, Select, Spin, Badge, Tag, Table } from 'antd';
import _ from 'lodash';
import { getRecordValue } from '@/utils/utils';
import rules from '@/utils/rules';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import {
  formItemLayout,
  tailFormItemLayout,
  handleValuesChange,
  handleCommonSubmit,
  getSubmitText,
} from '@/utils/form';

import {connect} from 'dva';
import moment from 'moment';

@connect(({ loading, thanhToanLuong }) => ({
  thanhToanLuong,
  loading: loading.models.thanhToanLuong,
}))
class DonVi extends React.Component {
  componentDidMount() {
    
  }

  render() {
    const {
      thanhToanLuong: model,
      loading,
    } = this.props;
    let { record, thongTinTienLuong } = model;
    thongTinTienLuong = thongTinTienLuong.map((item, i) => ({ ...item, index: i + 1 }));

    

    const columns = [{
      title: 'STT',
      dataIndex: 'index',
    },
    {
      title: 'Nhân viên',
      dataIndex: 'nhanVienEntity.tenNhanVien',
      },
    {
      title: 'Số tiền',
      dataIndex: 'tienLuong',
    },
    ];
    return (
      <div className="box">
        <Card
          bordered
          title={<div className="cardTitle">Thông tin tiền lương</div>}
        >
          <GridContent>

            <Spin spinning={!!loading}>
              <Table
                style={{ marginTop: 10 }}
                loading={!!loading}
                columns={columns}
                dataSource={thongTinTienLuong}
              />
            </Spin>

          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedForm = Form.create({ name: 'thanhToanLuong' })(DonVi);

export default WrappedForm;
