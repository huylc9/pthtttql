/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Form, Input, Card, Row, Col, Button, DatePicker, Select, Spin } from 'antd';
import _ from 'lodash';
import { getRecordValue } from '@/utils/utils';
import rules from '@/utils/rules';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import {
  formItemLayout,
  tailFormItemLayout,
  handleValuesChange,
  handleCommonSubmit,
  getSubmitText,
} from '@/utils/form';

import {connect} from 'dva';
import moment from 'moment';

@connect(({ loading, thanhToanLuong, nhanVien, chiNhanh, bangChamCong}) => ({
  thanhToanLuong,
  bangChamCong,
  nhanVien,
  chiNhanh,
  loading: loading.models.thanhToanLuong,
}))
class DonVi extends React.Component {
  componentDidMount() { }

  handleSubmit = e => {
    const {
      form,
    } = this.props;
    e.preventDefault();
    form.validateFieldsAndScroll((err, {maBangChamCong, chiNhanhEntity}) => {
      if (err) return;
      this.props.dispatch({
        type: "thanhToanLuong/create",
        payload: {
          chiNhanhEntity
        },
        onComplete: maThanhToanLuong => {
          this.props.dispatch({
            type: "thanhToanLuong/themBangChamCong",
            payload: {
              maThanhToanLuong,
              maBangChamCong
            }
          })
        }
      })
    });
  };

  render() {
    const {
      thanhToanLuong: model,
      chiNhanh: { danhSach: danhSachChiNhanh },
      bangChamCong: { danhSach: danhSachBangChamCong },
      form: { getFieldDecorator },
      loading,
    } = this.props;
    const { edit, record } = model;
    return (
      <div className="box">
        <Card
          bordered
          title={<div className="cardTitle">{!model.edit ? 'Thêm mới' : 'Chỉnh sửa'}</div>}
        >
          <GridContent>
            <Row>
              <Col span={18}>
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>
        


                  <Form.Item label="Chi nhánh">
                    {getFieldDecorator('chiNhanhEntity', {
                      initialValue: _.get(record, 'chiNhanhEntity', undefined),
                      rules: [...rules.required],
                    })(
                      <Select placeholder="" style={{ width: '100%' }}>
                        {danhSachChiNhanh.map(item => (
                          <Option value={item.maChiNhanh}>{item.tenChiNhanh}</Option>
                        ))}
                      </Select>,
                  )}
                  </Form.Item>

                  <Form.Item label="Bảng chấm công">
                    {getFieldDecorator('maBangChamCong', {
                      initialValue: _.get(record, 'maBangChamCong', undefined),
                      rules: [...rules.required],
                    })(
                      <Select placeholder="" style={{ width: '100%' }}>
                        {danhSachBangChamCong.map(item => (
                          <Option value={item.maBangChamCong}>{item.ghiChu}</Option>
                        ))}
                      </Select>,
                  )}
                  </Form.Item>
{/* 
                   <Form.Item label="Loại thuốc">
                    {getFieldDecorator('maHangHoa', {
                      initialValue: _.get(record, 'maHangHoa', undefined),
                      rules: [...rules.required],
                    })(
                      <Select placeholder="" style={{ width: '100%' }}>
                        {danhSachHangHoa.map(item => (
                          <Option value={item.maHangHoa}>{item.tenHangHoa + ": " + item.donGia}</Option>
                        ))}
                      </Select>,
                  )}
                  </Form.Item>
                  <Form.Item label="Số lượng">
                    {getFieldDecorator('soLuong', {
                      initialValue: _.get(record, "soLuong", ""),
                      rules: [...rules.number(1000000, 0), ...rules.required],
                    })(<Input />)}
                  </Form.Item> */}
                  <Spin spinning={!!loading}>
                    <Form.Item {...tailFormItemLayout}>
                      <Button.Group>
                        <Button type="primary" icon="plus" htmlType="submit">
                          {getSubmitText(model)}
                        </Button>
                      </Button.Group>
                    </Form.Item>
                  </Spin>
                </Form>
              </Col>
            </Row>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedForm = Form.create({ name: 'thanhToanLuong' })(DonVi);

export default WrappedForm;
