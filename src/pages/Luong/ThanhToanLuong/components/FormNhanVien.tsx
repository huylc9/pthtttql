/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Form, Input, Card, Row, Col, Button, DatePicker, Select, Spin } from 'antd';
import _ from 'lodash';
import { getRecordValue } from '@/utils/utils';
import rules from '@/utils/rules';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import {
  formItemLayout,
  tailFormItemLayout,
  handleValuesChange,
  handleCommonSubmit,
  getSubmitText,
} from '@/utils/form';

import {connect} from 'dva';
import moment from 'moment';

@connect(({ loading, bangChamCong, nhanVien}) => ({
  bangChamCong,
  nhanVien,
  loading: loading.models.bangChamCong,
}))
class DonVi extends React.Component {
  componentDidMount() { }

  handleSubmit = e => {
    const {
      form,
      bangChamCong: {edit, record },
    } = this.props;
    e.preventDefault();
    form.validateFieldsAndScroll((err, values) => {
      if (err) return;
      this.props.dispatch({
        type: "bangChamCong/chamCong",
        payload: {
          nhanVienInbangChamCongLine: values.nhanVienInbangChamCongLine.map(item => ({
            maNhanVien: item,
            ngayCham: values.ngayCham
          })),
          maBangChamCong: record.maBangChamCong,
        },
      })
    });
  };

  render() {
    const {
      bangChamCong: model,
      nhanVien: { danhSach: danhSachNhanVien },
      form: { getFieldDecorator },
      loading,
    } = this.props;
    const { edit, record } = model;
    return (
      <div className="box">
        <Card
          bordered
          title={<div className="cardTitle">{!model.edit ? 'Thêm mới' : 'Chỉnh sửa'}</div>}
        >
          <GridContent>
            <Row>
              <Col span={18}>
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                  <Form.Item label="Ngày chấm công">
                    {getFieldDecorator('ngayCham', {
                      initialValue: moment(),
                    })(<DatePicker format="DD/MM/YYYY"/>)}
                  </Form.Item>
                  {/* <Form.Item label="Tên bảng chấm công">
                    {getFieldDecorator('ghiChu', {
                      initialValue: _.get(record, "ghiChu", ""),
                      rules: [...rules.text, ...rules.required],
                    })(<Input />)}
                  </Form.Item> */}


                  <Form.Item label="Danh sách nhân viên">
                    {getFieldDecorator('nhanVienInbangChamCongLine', {
                      rules: [...rules.required],
                      initialValue: danhSachNhanVien.map(item => item.maNhanVien)
                    })(
                      <Select mode="multiple" placeholder="" style={{ width: '100%' }}>
                        {danhSachNhanVien.map(item => (
                          <Option value={item.maNhanVien}>{item.tenNhanVien}</Option>
                        ))}
                      </Select>,
                  )}
                  </Form.Item>
{/* 
                   <Form.Item label="Loại thuốc">
                    {getFieldDecorator('maHangHoa', {
                      initialValue: _.get(record, 'maHangHoa', undefined),
                      rules: [...rules.required],
                    })(
                      <Select placeholder="" style={{ width: '100%' }}>
                        {danhSachHangHoa.map(item => (
                          <Option value={item.maHangHoa}>{item.tenHangHoa + ": " + item.donGia}</Option>
                        ))}
                      </Select>,
                  )}
                  </Form.Item>
                  <Form.Item label="Số lượng">
                    {getFieldDecorator('soLuong', {
                      initialValue: _.get(record, "soLuong", ""),
                      rules: [...rules.number(1000000, 0), ...rules.required],
                    })(<Input />)}
                  </Form.Item> */}
                  <Spin spinning={!!loading}>
                    <Form.Item {...tailFormItemLayout}>
                      <Button.Group>
                        <Button type="primary" icon="plus" htmlType="submit">
                          {getSubmitText(model)}
                        </Button>
                      </Button.Group>
                    </Form.Item>
                  </Spin>
                </Form>
              </Col>
            </Row>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedForm = Form.create({ name: 'bangChamCong' })(DonVi);

export default WrappedForm;
