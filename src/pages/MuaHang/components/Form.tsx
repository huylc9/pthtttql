/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Form, Input, Card, Row, Col, Button, DatePicker, Select, Spin } from 'antd';
import _ from 'lodash';
import { getRecordValue } from '@/utils/utils';
import rules from '@/utils/rules';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import {
  formItemLayout,
  tailFormItemLayout,
  getSubmitText,
} from '@/utils/form';

import {connect} from 'dva';
import moment from 'moment';

@connect(({ loading, muaHang, hangHoa, nhanVien }) => ({
  hangHoa,
  nhanVien,
  muaHang,
  hangHoa,
  loading: loading.models.muaHang,
}))
class DonVi extends React.Component {
  componentDidMount() {}

  handleSubmit = e => {
    const {
      form,
      muaHang: {edit},
    } = this.props;
    e.preventDefault();
    form.validateFieldsAndScroll((err, values) => {
      if (err) return;
      this.props.dispatch({
        type: "muaHang/create",
        payload: {
          ...values,
          diaChi: "",
          "soDienThoai": "string",
          thue: 0,
          thanhTien: 0,
        }
      })
    });
  };

  render() {
    const {
      muaHang: model,
      hangHoa: {danhSach: danhSachHangHoa},
      nhanVien: {danhSach: danhSachNhanVien },
      form: { getFieldDecorator, setFieldsValue, getFieldValue },
      loading,
    } = this.props;
    const { edit, record } = model;
    return (
      <div className="box">
        <Card
          bordered
          title={<div className="cardTitle">{!model.edit ? 'Thêm mới' : 'Chỉnh sửa'}</div>}
        >
          <GridContent>
            <Row>
              <Col span={18}>
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                  <Form.Item label="Tên nhà cung cấp">
                    {getFieldDecorator('tenKhachHang', {
                      initialValue: _.get(record, "tenKhachHang", ""),
                      rules: [...rules.text, ...rules.required],
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Nhân viên bán">
                    {getFieldDecorator('nhanVienBan', {
                      initialValue: _.get(record, "nhanVienBan", undefined),
                      rules: [...rules.required],
                    })(
                      <Select placeholder="" style={{ width: '100%' }}>
                        {danhSachNhanVien.map(item => (
                          <Option value={item.maNhanVien}>{item.tenNhanVien}</Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>
                  <Form.Item label="Thời gian giao dịch">
                    {getFieldDecorator('ngayXuatDon', {
                      initialValue: moment(),
                      rules: [...rules.ngaySinh],
                    })(<DatePicker showTime={{ format: "HH:mm"}} placeholder="DD/MM/YYYY HH:mm" format="DD/MM/YYYY HH:mm" />)}
                  </Form.Item>
                  <Form.Item label="Loại thuốc">
                    {getFieldDecorator('hangHoa', {
                      initialValue: _.get(record, "hangHoa", undefined),
                      rules: [...rules.required],
                    })(
                      <Select placeholder="" style={{ width: '100%' }}>
                        {danhSachHangHoa.map(item => (
                          <Option value={item.maHangHoa}>{item.tenHangHoa + ": " + item.donGia}</Option>
                        ))}
                      </Select>
                    )}
                </Form.Item>
                 <Form.Item label="Số lượng">
                    {getFieldDecorator('soLuong', {
                      initialValue: _.get(record, 'soLuong', ''),
                      rules: [...rules.number(10000, 0), ...rules.required],
                    })(<Input />)}
                    </Form.Item>
                  <Form.Item label="Đơn giá">
                    {getFieldDecorator('donGia', {
                      initialValue: _.get(record, 'donGia', ''),
                      rules: [...rules.number(10000000000, 0), ...rules.required],
                    })(<Input />)}
                  </Form.Item>
                  <Spin spinning={!!loading}>
                    <Form.Item {...tailFormItemLayout}>
                      <Button.Group>
                        <Button type="primary" icon="plus" htmlType="submit">
                          {getSubmitText(model)}
                        </Button>
                      </Button.Group>
                    </Form.Item>
                  </Spin>
                </Form>
              </Col>
            </Row>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedForm = Form.create({ name: 'muaHang' })(DonVi);

export default WrappedForm;
