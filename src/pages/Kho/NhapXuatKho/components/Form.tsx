/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Form, Input, Card, Row, Col, Button, DatePicker, Select, Spin } from 'antd';
import _ from 'lodash';
import { getRecordValue } from '@/utils/utils';
import rules from '@/utils/rules';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import {
  formItemLayout,
  tailFormItemLayout,
  handleValuesChange,
  handleCommonSubmit,
  getSubmitText,
} from '@/utils/form';

import {connect} from 'dva';

@connect(({ loading, nhapXuatKho, hangHoa, muaHang }) => ({
  hangHoa,
  nhapXuatKho,
  muaHang,
  loading: loading.models.nhapXuatKho,
}))
class DonVi extends React.Component {
  componentDidMount() { }
  
  state = {
    maDonHang: undefined,
    tenMatHang: undefined,
    soLuong: undefined,
  }

  getMaHangHoa = ten => {
    const {
      hangHoa: {danhSach: danhSachHangHoa}
    } = this.props;
    let res;
    danhSachHangHoa.map(item => {
      // console.log(22222, item, item.ten, ten, item.tenMatHang == ten)
      if (item.tenHangHoa == ten) {
        res = item.maHangHoa;
      }
    })
    return res;
  }

  handleSelectDonMua = val => {
    const {
      muaHang: {danhSach: danhSachMuaHang}
    } = this.props;
    const item = JSON.parse(val);
    
    this.setState({
      maHangHoa: _.get(item, "hangHoa.maHangHoa"),
      soLuong: item.soLuong,
    });
  }

  handleSubmit = e => {
    const {
      form,
      nhapXuatKho: {edit},
    } = this.props;
    const { maHangHoa, soLuong } = this.state;
    e.preventDefault();
    form.validateFieldsAndScroll((err, values) => {
      if (err) return;
      this.props.dispatch({
        type: "nhapXuatKho/createNhapKho",
        payload: {
          ...values,
          "doiTuong": "string",
          "diaChi": "string",
          maHangHoa,
          soLuong
        }
      })
    });
  };

  render() {
    const {
      nhapXuatKho: model,
      hangHoa: { danhSach: danhSachHangHoa },
      muaHang: {danhSach: danhSachMuaHang},
      form: { getFieldDecorator },
      loading,
    } = this.props;
    const { edit, record } = model;
    return (
      <div className="box">
        <Card
          bordered
          title={<div className="cardTitle">{!model.edit ? 'Thêm mới' : 'Chỉnh sửa'}</div>}
        >
          <GridContent>
            <Row>
              <Col span={18}>
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                  <Form.Item label="Diễn giải">
                    {getFieldDecorator('dienGiai', {
                      initialValue: _.get(record, "dienGiai", ""),
                      rules: [...rules.text, ...rules.required],
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Mã kho">
                    {getFieldDecorator('maKhoHang', {
                      initialValue: _.get(record, "maKhoHang", ""),
                      rules: [...rules.text, ...rules.required],
                    })(<Input />)}
                  </Form.Item>

                  <Form.Item label="Chọn đơn mua">
                    {getFieldDecorator('maDonHangMua', {
                      initialValue: undefined,
                      rules: [...rules.required],
                    })(
                      <Select onChange={val => this.handleSelectDonMua(val)} placeholder="" style={{ width: '100%' }}>
                        {danhSachMuaHang.map(item => (
                          <Option value={JSON.stringify(item)}>{item.maDonHang}</Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>
{/* 
                   <Form.Item label="Loại thuốc">
                    {getFieldDecorator('maHangHoa', {
                      initialValue: _.get(record, 'maHangHoa', undefined),
                      rules: [...rules.required],
                    })(
                      <Select placeholder="" style={{ width: '100%' }}>
                        {danhSachHangHoa.map(item => (
                          <Option value={item.maHangHoa}>{item.tenHangHoa + ": " + item.donGia}</Option>
                        ))}
                      </Select>,
                  )}
                  </Form.Item>
                  <Form.Item label="Số lượng">
                    {getFieldDecorator('soLuong', {
                      initialValue: _.get(record, "soLuong", ""),
                      rules: [...rules.number(1000000, 0), ...rules.required],
                    })(<Input />)}
                  </Form.Item> */}
                  <Spin spinning={!!loading}>
                    <Form.Item {...tailFormItemLayout}>
                      <Button.Group>
                        <Button type="primary" icon="plus" htmlType="submit">
                          {getSubmitText(model)}
                        </Button>
                      </Button.Group>
                    </Form.Item>
                  </Spin>
                </Form>
              </Col>
            </Row>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedForm = Form.create({ name: 'nhapXuatKho' })(DonVi);

export default WrappedForm;
