
/* eslint-disable react/destructuring-assignment */
import { Button, Card, Col, Icon, Input, Row, Table, Divider, Popconfirm} from 'antd';
import _ from 'lodash';
import { connect } from 'dva';
import React from 'react';
import uuidv1 from 'uuid/v1';
import FormDrawer from '@/components/Drawer/FormDrawer';
import FormModal from '@/components/Drawer/FormModal';
import Form from './components/Form';
import View from "./components/ViewKho";

@connect(({ loading, khoHang }) => ({
  khoHang,
  loading: loading.models.khoHang,
}))
class TableBase extends React.Component {
  componentDidMount() {
    this.props.dispatch({
      type: `khoHang/getAll`,
      payload: {
        page: 1,
        size: 10,
      },
    });
  }

  getCond = () => {
    // lấy thông tin cond đang search
    const { model } = this.props;
    return _.get(model, 'paging.cond', {});
  };

  handleThem = () => {
    this.props.dispatch({
      type: `khoHang/changeState`,
      payload: {
        showDrawer: true,
        edit: false,
        record: {},
        key: uuidv1(),
        isTouched: false,
      },
    });
  };

  handleDel = _id => {
    this.props.dispatch({
      type: `khoHang/del`,
      payload: {
        _id,
      },
    });
  };

  handleEdit = record => {
    this.props.dispatch({
      type: `khoHang/changeState`,
      payload: {
        showDrawer: true,
        edit: true,
        record,
      },
    });
  };

  viewKho = record => {
    const { maKho } = record;
    this.props.dispatch({
      type: "khoHang/getDuLieu",
      payload: {
        maKhoHang: maKho
      }
    })
    this.props.dispatch({
      type: "khoHang/changeState",
      payload: {
        viewKhoHang: true,
        record
      }
    })
  }

  render() {
    let {
      khoHang,
      loading
    } = this.props;
    let { danhSach, total, paging } = khoHang;
    const { page, size } = paging;
    danhSach = danhSach.map((item, index) => ({
      ...item,
      index: index + 1 + (page - 1) * size,
      key: index,
    }));

    const columns = [{
      title: 'STT',
      dataIndex: 'index',
      key: 'index',
    },
    {
      title: 'Mã kho',
      dataIndex: 'maKho',
      key: 'maKho',
    },
    {
      title: 'Tên kho',
      dataIndex: 'tenKho',
      key: 'tenKho',
    },
    {
      title: 'Địa chỉ',
      dataIndex: 'diaChi',
      key: 'diaChi',
    },
    ];

    const list = (text, record) => (
      <React.Fragment>
          <React.Fragment>
            <Button
              type="primary"
              icon={'eye'}
              onClick={() => this.viewKho(record)}
              title="Xem"
            >
              Xem thông tin
            </Button>
          </React.Fragment>
      </React.Fragment>
    );

    const last = {
      // phần tử cuối của columns
      title: 'Tùy chọn',
      // width: '120px',
      align: 'center',
      fixed: 'right',
      render: (text, record) => list(text, record),
    };

    columns.push(last);

    return (
      <div className="box">
        <Card bordered title={<div className="cardTitle">Quản lý kho hàng</div>}>
          <Row>
            <Col xs={24}>
                <Button
                  style={{ marginRight: '20%' }}
                  type="primary"
                  // shape="round"
                  icon="plus"
                  // disabled
                  onClick={() => this.handleThem()}
                >
                  Thêm mới
                </Button>
              <h3 style={{ display: 'inline-block', margin: '0 10px 10px 50px', float: 'right' }}>
                {`Tổng số : ${total}`}
              </h3>

              <Table
                style={{ marginTop: 10 }}
                loading={loading}
                columns={columns}
                dataSource={danhSach}
                onChange={this.onChange}
                pagination={{
                  current: page,
                  position: 'bottom',
                  total,
                }}
              />
            </Col>
          </Row>
        </Card>
        <FormDrawer name={`khoHang`}>
          <Form />
        </FormDrawer>
        <FormModal name={`khoHang`} field="viewKhoHang">
            <View />
        </FormModal>
      </div>
    );
  }
}

export default TableBase;
