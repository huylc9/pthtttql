/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Form, Input, Card, Row, Col, Button, DatePicker, Select, Spin } from 'antd';
import _ from 'lodash';
import { getRecordValue } from '@/utils/utils';
import rules from '@/utils/rules';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import {
  formItemLayout,
  tailFormItemLayout,
  handleValuesChange,
  handleCommonSubmit,
  getSubmitText,
} from '@/utils/form';

import {connect} from 'dva';

@connect(({ loading, khoHang }) => ({
  khoHang,
  loading: loading.models.khoHang,
}))
class DonVi extends React.Component {
  componentDidMount() {}

  handleSubmit = e => {
    const {
      form,
      khoHang: {edit},
    } = this.props;
    e.preventDefault();
    form.validateFieldsAndScroll((err, values) => {
      if (err) return;
      this.props.dispatch({
        type: "khoHang/create",
        payload: {
          ...values
        }
      })
    });
  };

  render() {
    const {
      khoHang: model,
      form: { getFieldDecorator },
      loading,
    } = this.props;
    const { edit, record } = model;
    return (
      <div className="box">
        <Card
          bordered
          title={<div className="cardTitle">{!model.edit ? 'Thêm mới' : 'Chỉnh sửa'}</div>}
        >
          <GridContent>
            <Row>
              <Col span={18}>
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                  <Form.Item label="Mã kho">
                    {getFieldDecorator('maKho', {
                      initialValue: _.get(record, "maKho", ""),
                      rules: [...rules.text, ...rules.required],
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Tên kho">
                    {getFieldDecorator('tenKho', {
                      initialValue: _.get(record, "tenKho", ""),
                      rules: [...rules.text, ...rules.required],
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Địa chỉ">
                    {getFieldDecorator('diaChi', {
                      initialValue: _.get(record, "diaChi", ""),
                      rules: [...rules.text, ...rules.required],
                    })(<Input />)}
                  </Form.Item>
                  <Spin spinning={!!loading}>
                    <Form.Item {...tailFormItemLayout}>
                      <Button.Group>
                        <Button type="primary" icon="plus" htmlType="submit">
                          {getSubmitText(model)}
                        </Button>
                      </Button.Group>
                    </Form.Item>
                  </Spin>
                </Form>
              </Col>
            </Row>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedForm = Form.create({ name: 'KhoHang' })(DonVi);

export default WrappedForm;
