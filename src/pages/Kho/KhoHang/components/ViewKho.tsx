/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Form, Input, Card, Row, Col, Button, DatePicker, Select, Spin, Badge, Table } from 'antd';
import _ from 'lodash';
import { getRecordValue } from '@/utils/utils';
import rules from '@/utils/rules';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import { formItemLayout, tailFormItemLayout, getSubmitText } from '@/utils/form';

import { connect } from 'dva';
import moment from 'moment';

@connect(({ loading, khoHang }) => ({
  khoHang,
  loading: loading.models.khoHang,
}))
class DonVi extends React.Component {
  componentDidMount() {}

  render() {
    const { khoHang: model, loading } = this.props;
    const { record, thongTinKhoHang } = model;

    const columns = [
      {
        title: 'Mã kho',
        dataIndex: 'khoHang.maKho',
      },
      {
        title: 'Tên kho',
        dataIndex: 'khoHang.tenKho',
      },
      {
        title: 'Địa chỉ',
        dataIndex: 'khoHang.diaChi',
      },
    ];

    return (
      <div className="box">
        <Card bordered title={<div className="cardTitle">{record.ghiChu}</div>}>
          <GridContent>
            <Spin spinning={!!loading}>
              <Table
                style={{ marginTop: 10 }}
                loading={!!loading}
                columns={columns}
                dataSource={thongTinKhoHang}
              />
            </Spin>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedForm = Form.create({ name: 'khoHang' })(DonVi);

export default WrappedForm;
