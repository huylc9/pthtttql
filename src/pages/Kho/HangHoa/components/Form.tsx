/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Form, Input, Card, Row, Col, Button, DatePicker, Select, Spin } from 'antd';
import _ from 'lodash';
import rules from '@/utils/rules';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import {
  formItemLayout,
  tailFormItemLayout,
  getSubmitText,
} from '@/utils/form';

import {connect} from 'dva';

@connect(({ loading, donHang }) => ({
  donHang,
  loading: loading.models.donHang,
}))
class DonVi extends React.Component {
  componentDidMount() {}

  handleSubmit = e => {
    const {
      form,
    } = this.props;
    e.preventDefault();
    form.validateFieldsAndScroll((err, values) => {
      if (err) return;
      this.props.dispatch({
        type: "hangHoa/create",
        payload: {
          ...values
        }
      })
    });
  };

  render() {
    const {
      donHang: model,
      form: { getFieldDecorator },
      loading,
    } = this.props;
    return (
      <div className="box">
        <Card
          bordered
          title={<div className="cardTitle">{!model.edit ? 'Thêm mới' : 'Chỉnh sửa'}</div>}
        >
          <GridContent>
            <Row>
              <Col span={18}>
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                  <Form.Item label="Mã thuốc">
                    {getFieldDecorator('maHangHoa', {
                      initialValue: "",
                      rules: [...rules.text, ...rules.required],
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Tên thuốc">
                    {getFieldDecorator('tenHangHoa', {
                      initialValue: "",
                      rules: [...rules.text, ...rules.required],
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Mô tả">
                    {getFieldDecorator('moTa', {
                      initialValue: "",
                      rules: [...rules.text, ...rules.required],
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Đơn giá">
                    {getFieldDecorator('donGia', {
                      initialValue: "",
                      rules: [...rules.number(), ...rules.required],
                    })(<Input />)}
                  </Form.Item>
                  <Spin spinning={!!loading}>
                    <Form.Item {...tailFormItemLayout}>
                      <Button.Group>
                        <Button type="primary" icon="plus" htmlType="submit">
                          {getSubmitText(model)}
                        </Button>
                      </Button.Group>
                    </Form.Item>
                  </Spin>
                </Form>
              </Col>
            </Row>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedForm = Form.create({ name: 'donHang' })(DonVi);

export default WrappedForm;
