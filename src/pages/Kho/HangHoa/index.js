/* eslint-disable react/destructuring-assignment */
import { Button, Card, Col, Icon, Input, Row, Table, Select } from 'antd';
import _ from 'lodash';
import { connect } from 'dva';
import React from 'react';
import uuidv1 from 'uuid/v1';
import { toRegex } from '@/utils/utils';
import data from '@/utils/data';
import FormDrawer from '@/components/Drawer/FormDrawer';
import Form from './components/Form';

@connect(({ loading, hangHoa, khoHang }) => ({
  hangHoa,
  khoHang,
  loading: loading.models.hangHoa,
}))
class TableBase extends React.Component {
  componentDidMount() {
    this.props.dispatch({
      type: 'hangHoa/getAll',
      payload: {
        page: 1,
        size: 10,
      },
    });
    this.props.dispatch({
      type: 'khoHang/getAll',
      payload: {
        page: 1,
        size: 10,
      },
    });
  }

  getCond = () => {
    // lấy thông tin cond đang search
    const { model } = this.props;
    return _.get(model, 'paging.cond', {});
  };

  handleThem = () => {
    this.props.dispatch({
      type: 'hangHoa/changeState',
      payload: {
        showDrawer: true,
        edit: false,
        record: {},
        key: uuidv1(),
        isTouched: false,
      },
    });
  };

  render() {
    const { hangHoa, loading, khoHang } = this.props;
    let { danhSach, total, paging } = hangHoa;
    let { danhSach: danhSachKhoHang } = khoHang;
    const { page, size } = paging;
    danhSach = danhSach.map((item, index) => ({
      ...item,
      index: index + 1 + (page - 1) * size,
      key: index,
    }));

    danhSachKhoHang = danhSachKhoHang.map((item, index) => ({
      ...item,
      index: index + 1 + (page - 1) * size,
      key: index,
    }));

    const columns = [
      {
        title: 'STT',
        dataIndex: 'index',
      },
      {
        title: 'Mã thuốc',
        dataIndex: 'maHangHoa',
      },
      {
        title: 'Tên thuốc',
        dataIndex: 'tenHangHoa',
      },
      {
        title: 'Mô tả',
        dataIndex: 'moTa',
      },
      {
        title: 'Đơn giá',
        dataIndex: 'donGia',
      },
    ];

    console.log('danhSachKhoHang', danhSachKhoHang);

    return (
      <div className="box">
        <Card bordered title={<div className="cardTitle">Quản lý danh mục thuốc</div>}>
          <Row>
            <Col xs={24}>
              <Button
                style={{ marginRight: '20%' }}
                type="primary"
                // shape="round"
                icon="plus"
                // disabled
                onClick={() => this.handleThem()}
              >
                Thêm mới
              </Button>

              <h4>Tìm theo kho hàng</h4>

              <Select>
                {danhSachKhoHang.map((item, index) => (
                  <Select.Option key={`kho-hang-${index}`} value={item.maKho}>
                    {item.tenKho}
                  </Select.Option>
                ))}
              </Select>

              <h3 style={{ display: 'inline-block', margin: '0 10px 10px 50px', float: 'right' }}>
                {`Tổng số : ${total}`}
              </h3>

              <Table
                style={{ marginTop: 10 }}
                loading={loading}
                columns={columns}
                dataSource={danhSach}
                onChange={this.onChange}
                pagination={{
                  current: page,
                  position: 'bottom',
                  total,
                }}
              />
            </Col>
          </Row>
        </Card>
        <FormDrawer name="hangHoa">
          <Form />
        </FormDrawer>
      </div>
    );
  }
}

export default TableBase;
