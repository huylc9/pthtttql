module.exports = {
  navTheme: 'dark',
  primaryColor: '#1171A3',
  layout: 'sidemenu',
  contentWidth: 'Fluid',
  fixedHeader: false,
  autoHideHeader: false,
  fixSiderbar: false,
  colorWeak: false,
  menu: {
    locale: true,
  },
  title: 'Nhà thuốc Hà Đông',
  pwa: false,
  iconfontUrl: '',
};
