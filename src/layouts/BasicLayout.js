import React, { Suspense } from 'react';
import { Layout, notification } from 'antd';
import DocumentTitle from 'react-document-title';
import { connect } from 'dva';
import { ContainerQuery } from 'react-container-query';
import classNames from 'classnames';
import Media from 'react-media';
import router from 'umi/router';
import PageLoading from '@/components/PageLoading';
import SiderMenu from '@/components/SiderMenu';
import getPageTitle from '@/utils/getPageTitle';

import ErrorBoundary from '@/components/ErrorBoundary/ErrorBoundary';
import notificationAlert from '@/components/Notification/index';
import { getToken } from '@/utils/Authorized';
import styles from './BasicLayout.less';
import Context from './MenuContext';
import Header from './Header';
import Footer from './Footer';
import logo from '../assets/logo.png';

const SettingDrawer = React.lazy(() =>
    import('@/components/SettingDrawer'));

const { Content } = Layout;

const query = {
    'screen-xs': {
        maxWidth: 575,
    },
    'screen-sm': {
        minWidth: 576,
        maxWidth: 767,
    },
    'screen-md': {
        minWidth: 768,
        maxWidth: 991,
    },
    'screen-lg': {
        minWidth: 992,
        maxWidth: 1199,
    },
    'screen-xl': {
        minWidth: 1200,
        maxWidth: 1599,
    },
    'screen-xxl': {
        minWidth: 1600,
    },
};

@connect(({ login }) => ({
    login,
}))
class BasicLayout extends React.Component {
    componentDidMount() {
        const {
            dispatch,
            route: { routes, authority },
            login,
        } = this.props;

        const token = getToken();
        if (!token || token === 'undefined') {
            this.props.dispatch({
                type: 'login/logout',
            });
        }
        dispatch({
            type: 'setting/getSetting',
        });
        dispatch({
            type: 'menu/getMenuData',
            payload: { routes, authority },
        });
        if (!login.TenTaiKhoan) this.props.dispatch({
            type: 'login/fetchCurrent',
        });

        this.props.dispatch({
            type: "global/initData"
        })
    }

    componentWillUnmount() {}


    getContext() {
        const { location, breadcrumbNameMap } = this.props;
        return {
            location,
            breadcrumbNameMap,
        };
    }

    getLayoutStyle = () => {
        const { fixSiderbar, isMobile, collapsed, layout } = this.props;
        if (fixSiderbar && layout !== 'topmenu' && !isMobile) {
            return {
                paddingLeft: collapsed ? '80px' : '256px',
            };
        }
        return null;
    };

    handleMenuCollapse = collapsed => {
        const { dispatch } = this.props;
        dispatch({
            type: 'global/changeLayoutCollapsed',
            payload: collapsed,
        });
    };

    renderSettingDrawer = () => {
        // Do not render SettingDrawer in production
        // unless it is deployed in preview.pro.ant.design as demo
        if (process.env.NODE_ENV === 'production' && APP_TYPE !== 'site') {
            return null;
        }
        return <SettingDrawer />;
    };

    render() {
        // console.log('in basiclayout')
        const {
            navTheme,
            layout: PropsLayout,
            children,
            location,
            isMobile,
            menuData,
            breadcrumbNameMap,
            fixedHeader,
            login,
        } = this.props;
        let pathname;
        if (!login.TenTaiKhoan) return <PageLoading />;
        if (location) pathname = location.pathname;
        const isTop = PropsLayout === 'topmenu';
        const contentStyle = !fixedHeader ? { paddingTop: 0 } : {};

        const layout = (<Layout > {
                isTop && !isMobile ? null : (
                <SiderMenu logo = { logo }
                    theme = { navTheme }
                    onCollapse = { this.handleMenuCollapse }
                    menuData = { menuData }
                    isMobile = { isMobile } {...this.props }
                />
                )
            } <Layout style = {{
                    ...this.getLayoutStyle(),
                        minHeight: '100vh',
                }
            } >
            <Header menuData = { menuData }
            handleMenuCollapse = { this.handleMenuCollapse }
            logo = { logo }
            isMobile = { isMobile } {...this.props }
            />

            <Content
            className = { styles.content }
            style = { contentStyle }>
            { children }
            </Content> </Layout > </Layout>
        );
        return (<ErrorBoundary >
            <DocumentTitle title = { getPageTitle(pathname, breadcrumbNameMap) } >
            <ContainerQuery query = { query } > 
            {
                params => (<Context.Provider value = { this.getContext() } >
                    <div className = { classNames(params) } > { layout } </div>
                    </ Context.Provider >
                )
            } 
            </ContainerQuery>
            </ DocumentTitle >
            </ErrorBoundary>
        );
    }
}

export default connect(({ global, setting, menu: menuModel, login }) => {
    console.log('111 here')
    return {
        collapsed: global.collapsed,
        layout: setting.layout,
        menuData: menuModel.menuData,
        breadcrumbNameMap: menuModel.breadcrumbNameMap,
        login,
        ...setting,
    };
})(props =>
        <BasicLayout
            {...props }
            isMobile = { false }
        />,
);
