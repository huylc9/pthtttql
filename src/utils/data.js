const data = {
  trangThai: ['Đang làm việc', 'Chuyển trường', 'Nghỉ hưu'],
  loaiDonVi: ['Phòng ban', 'Khoa/Bộ môn'],
  quanHe: ['Bố', 'Mẹ', 'Người đỡ đầu'],
  gioiTinh: ['Nam', 'Nữ'],
  inactive: ['Đã kích hoạt', 'Chưa kích hoạt'],
  dangTai: ['Không đăng tải', 'Đăng tải'],
  loaiCauHoi: ['Chọn một đáp án', 'Chọn nhiều đáp án'],
  vaiTro: ['Sinh viên', 'Giáo viên', 'Phụ huynh', 'Admin'],
  role: [['student'], ['teacher'], [], ['admin']],
  path: ['/khaibao', '/TrangChu', '/dashboard', '/dashboard'],
  daDoc: ['Chưa đọc', 'Đã đọc'],
  validated: ['Chưa kích hoạt', 'Đã kích hoạt'],
  trangThaiSucKhoe: ['Sốt', 'Ho', 'Khó Thở', 'Viêm Phổi', 'Đau họng', 'Mệt mỏi', 'Khác'],
  tiepXuc: ['Người bệnh hoặc nghi nhiễm bệnh', 'Người từ nước có bệnh', 'Người có biểu hiện'],
  hinhThucCachLy: ['Tại nhà', 'Tập trung'],
  sot: ['Không', 'Có'],
  loaiBangDiem: ['Toàn khóa', 'Năm học', 'Kỳ học'],
};

export const dataObj = {
  trangThai: {
    CHUA_TIEP_NHAN: 'Chưa tiếp nhận',
    DA_TIEP_NHAN: 'Đã tiếp nhận',
    DA_XU_LY: 'Đã xử lý',
    KHONG_XU_LY: 'Không xử lý',
  },
};

export default data;
