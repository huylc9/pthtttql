export const lyDoGiaoDich = {
  rutTienNganHang: "Rút tiền từ TK ngân hàng",
  guiTienNganHang: "Gửi tiền vào TK ngân hàng",
  tamUngNV: "Tạm ứng cho nhân viên",
  traTienHangNCC: "Trả trước tiền hàng cho nhà cung cấp",
  thanhToanChiPhiKhac: "Thanh toán chi phí khác",
  banHang: "Thu tiền từ đơn hàng",
  dauTu: "Nhận tiền từ nhà đầu tư"
}

export const taiKhoanNoCo = {
  rutTienNganHang: {
      no: 111,
      co: 112,
  },
  guiTienNganHang: {
      no: 112,
      co: 111,
  },
  dauTu: {
    no: 111,
    co: 411
  },
  tamUngNV: {
      no: 141,
      co: 111
  },
  thuTamUngNV: {
      no: 111,
      co: 141,
  },
  traTienHangNCC: {
      no: 151,
      co: 111,
  },
  thanhToanChiPhiKhac: {
      no: 641,
      co: 111,
  },
  nhapKho: {
    co: 151,
    no: 156
  },
  banHang: {
    co: 156,
    no: 111,
  },
}

// get type of giaodich
export const getTypeGiaoDich = val => { 
    let ans;
    Object.keys(lyDoGiaoDich).map(key => {
        console.log(val, lyDoGiaoDich[key]);
        console.log(val === lyDoGiaoDich[key])
        if (lyDoGiaoDich[key] === val) {
            ans = key;
        }
    })
    return ans;
}