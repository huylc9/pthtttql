import _ from 'lodash';
import { formatMessage } from 'umi/locale';
import notificationAlert from '@/components/Notification';

const initialState = {
  danhSach: [],
  edit: false,
  record: {},
  showDrawer: false,
  paging: {
    page: 1,
    size: 10,
  },
  filterInfo: {},
  total: 0,
  isTouched: false,
};

const model = Services => ({
  state: initialState,
  effects: {
    *getAll({ payload }, { call, put, select }) {
      let currentPayload = { ...payload };
      if (!payload) {
        const modelName = Services.name;
        currentPayload = yield select(state => state[modelName].paging);
      }
      const response = yield call(Services.getAll, currentPayload);
      yield put({
        type: 'changeState',
        payload: {
          danhSach: _.get(response, 'data.data', []),
          paging: currentPayload,
          total: _.get(response, 'data.total', 0),
        },
      });
    },

    *create({ payload }, { call, put }) {
      yield call(Services.create, payload);
      notificationAlert('success', formatMessage({ id: 'THEM_THANH_CONG' }));
      yield put({ type: 'get' });
      yield put({ type: 'changeState', payload: { showDrawer: false } });
    },
    *delete({ payload }, { call, put, select }) {
      yield call(Services.delete, payload);
      notificationAlert('success', formatMessage({ id: 'XOA_THANH_CONG' }));
      yield put({ type: 'get'});
    },
    *upd({ payload }, { call, put }) {
      yield call(Services.update, payload);
      notificationAlert('success', formatMessage({ id: 'SUA_THANH_CONG' }));
      yield put({ type: 'get' });
      yield put({ type: 'changeState', payload: { showDrawer: false } });
    },
  },
  reducers: {
    changeState(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },
});

export default model;
