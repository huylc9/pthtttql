import { ColumnType as ICol } from 'rc-table/lib/interface';

export interface IColumn extends ICol {
    search?: 'search' | 'filter' | 'sort' | 'filterTF'
}