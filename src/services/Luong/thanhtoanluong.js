import axios from '@/utils/axios.js';
import { ip } from '../ip';


export async function getAll(params) {
    return axios.get(`${ip}/get-all`, { params });
}

export async function getTienLuong({maThanhToanLuong}) {
    return axios.get(`${ip}/get-tien-luong-by-ma-thanh-toan-luong/${maThanhToanLuong}`);
}

export async function create(payload) {
    return axios.post(`${ip}/create`, payload);
}

export async function tinhTienLuong({maThanhToanLuong}) {
    return axios.put(`${ip}/update-tien-luong/${maThanhToanLuong}`);
}

export async function thanhToanLuong({maThanhToanLuong}) {
    return axios.put(`${ip}/tra-luong/${maThanhToanLuong}`);
}

export async function themBangChamCong({maThanhToanLuong, maBangChamCong}) {
    return axios.put(`${ip}/them-bang-cham-cong/${maThanhToanLuong}/${maBangChamCong}`);
}