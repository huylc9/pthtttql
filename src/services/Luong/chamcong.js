import axios from '@/utils/axios.js';
import { ip } from '../ip';


export async function getAll(params) {
    return axios.get(`${ip}/bang-cham-comg/get-all`, { params });
}

export async function create(payload) {
    return axios.post(`${ip}/bang-cham-comg/create`, payload);
}

export async function update(payload) {
    return axios.put(`${ip}/bang-cham-comg/update/${payload.maBangChamCong}`, payload);
}

export async function chamCong({nhanVienInbangChamCongLine, maBangChamCong}) {
    return axios.put(`${ip}/bang-cham-comg/add-nha-vien/${maBangChamCong}`, {nhanVienInbangChamCongLine});
}

export async function getDuLieu({maBangChamCong}) {
    return axios.get(`${ip}/bang-cham-comg/get-all-du-lieu-cham/${maBangChamCong}`);
}

export async function khoaBang({maBangChamCong}) {
    return axios.put(`${ip}/bang-cham-comg/update/${maBangChamCong}`, {khoaBang: true});
}

