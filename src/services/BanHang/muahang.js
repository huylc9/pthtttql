import axios from '@/utils/axios.js';
import { ip } from '../ip';

export async function getAll(params) {
    return axios.get(`${ip}/don-hang-mua/get-all`, { params });
}

export async function create(payload) {
    return axios.post(`${ip}/don-hang-mua/create`, payload);
}

export async function update(payload) {
    return axios.put(`${ip}/don-hang-mua/update/${payload.maDonHang}`, payload);
}
