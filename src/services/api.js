import { stringify } from 'qs';
import request from '@/utils/request';
// import { async } from 'q';
// import { func } from 'prop-types';
import axios from '@/utils/axios.js';
import { getToken } from '@/utils/Authorized';

import { ip } from './ip';

export async function getSoDoToChuc() {
  return axios
    .post(`${ip}/api/co_so_dang/get_so_do_to_chuc`)
    .then(
      res =>
        // console.log(res);
        res
    )
    .catch(err => {
      // console.log(err);
    });
}

export async function getCacCapCoSoDang() {
  return axios.post(`${ip}/api/co_so_dang/get_Cap_csdang`);
}

export async function getInfoCoSoDang(IdCoSoDang) {
  return axios.post(`${ip}/api/co_so_dang/get_info_csdang`, { IdCoSoDang });
}

export async function getCoSoDangCapTren({ IdCapCoSoDang }) {
  return axios
    .post(`${ip}/api/co_so_dang/get_csdang_cap_tren`, { IdCapCoSoDang })
}

export async function editCoSoDang({
  IdCoSoDang,
  LoaiCoSoDang,
  IdCapCoSoDang,
  DiaChi,
  SoDienThoai,
  TenCoSoDang,
  IdNguoiTao,
  IdCapTren,
  IsRoot,
}) {
  return axios.post(`${ip}/api/co_so_dang/edit_csdang`, {
    IdCoSoDang,
    LoaiCoSoDang,
    IdCapCoSoDang,
    DiaChi,
    SoDienThoai,
    TenCoSoDang,
    IdNguoiTao,
    IdCapTren,
    IsRoot,
  });
}

export async function deleteCoSoDang({ IdCoSoDang }) {
  return axios.post(`${ip}/api/co_so_dang/del_csdang`, { IdCoSoDang });
}

export async function addCoSoDang({
  LoaiCoSoDang,
  IdCapCoSoDang,
  DiaChi,
  SoDienThoai,
  TenCoSoDang,
  IdNguoiTao,
  IdCapTren,
}) {
  // console.log('in addcsd', LoaiCoSoDang, IdCapCoSoDang, DiaChi, SoDienThoai, TenCoSoDang, IdNguoiTao, IdCapTren);
  return axios.post(`${ip}/api/co_so_dang/add_csdang`, {
    LoaiCoSoDang,
    IdCapCoSoDang,
    DiaChi,
    SoDienThoai,
    TenCoSoDang,
    IdNguoiTao,
    IdCapTren,
  });
}

export async function queryProjectNotice() {
  return request('/api/project/notice');
}

export async function queryActivities() {
  return request('/api/activities');
}

export async function queryRule(params) {
  return request(`/api/rule?${stringify(params)}`);
}

export async function removeRule(params) {
  return request('/api/rule', {
    method: 'POST',
    body: {
      ...params,
      method: 'delete',
    },
  });
}

export async function addRule(params) {
  return request('/api/rule', {
    method: 'POST',
    body: {
      ...params,
      method: 'post',
    },
  });
}

export async function updateRule(params = {}) {
  return request(`/api/rule?${stringify(params.query)}`, {
    method: 'POST',
    body: {
      ...params.body,
      method: 'update',
    },
  });
}

export async function fakeSubmitForm(params) {
  return request('/api/forms', {
    method: 'POST',
    body: params,
  });
}

export async function fakeChartData() {
  return request('/api/fake_chart_data');
}

export async function queryTags() {
  return request('/api/tags');
}

export async function queryBasicProfile(id) {
  return request(`/api/profile/basic?id=${id}`);
}

export async function queryAdvancedProfile() {
  return request('/api/profile/advanced');
}

export async function queryFakeList(params) {
  return request(`/api/fake_list?${stringify(params)}`);
}

export async function removeFakeList(params) {
  const { count = 5, ...restParams } = params;
  return request(`/api/fake_list?count=${count}`, {
    method: 'POST',
    body: {
      ...restParams,
      method: 'delete',
    },
  });
}

export async function addFakeList(params) {
  const { count = 5, ...restParams } = params;
  return request(`/api/fake_list?count=${count}`, {
    method: 'POST',
    body: {
      ...restParams,
      method: 'post',
    },
  });
}

export async function updateFakeList(params) {
  const { count = 5, ...restParams } = params;
  return request(`/api/fake_list?count=${count}`, {
    method: 'POST',
    body: {
      ...restParams,
      method: 'update',
    },
  });
}

export async function fakeAccountLogin(params) {
  return request('/api/login/account', {
    method: 'POST',
    body: params,
  });
}

export async function fakeRegister(params) {
  return request('/api/register', {
    method: 'POST',
    body: params,
  });
}

export async function queryNotices(params = {}) {
  return request(`/api/notices?${stringify(params)}`);
}

export async function getFakeCaptcha(mobile) {
  return request(`/api/captcha?mobile=${mobile}`);
}
