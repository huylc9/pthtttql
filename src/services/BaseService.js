import PropTypes from 'prop-types';
import axios from '@/utils/axios.js';
import { ip3 } from './ip';
import { isValue, trim } from '../utils/utils';

class Services {
  /**
   * 
   * @param {*} url {get_all: "hang-hoa/get-all", post, put, delete}
   */
  constructor({url, name}) {
    this.name = name;
    this.url = url;
  }

  /**
   * 
   * @param {*} payload 
   * @returns 
   */
  delete = async payload => {
    const { _id } = payload;
    payload._id = undefined;
    return axios.delete(`${ip3}/${this.url.del}/${_id}`, { data: payload });
  }

  getAll = async (payload) => axios.get(`${ip3}/${this.url.get_all}/`, { params: payload })

  create = async payload => {
    if (this.formData) {
      const form = new FormData();
      Object.keys(payload).map(key => {
        if (isValue(payload[key])) {
          if (Array.isArray(payload[key])) {
            for (let i = 0; i < payload[key].length; i += 1) {
              form.append(key, payload[key][i]);
            }
            return;
          }
          form.set(key, trim(payload[key]));
        }
      });
      return axios.post(`${ip}/${this.url.create}/`, form);
    }
    Object.keys(payload).map(key => {
      if (isValue(payload[key])) payload[key] = trim(payload[key]);
    });
    return axios.post(`${ip}/${this.url.create}/`, payload);
  }

  update = async payload => {
    if (this.formData) {
      const form = new FormData();
      const { _id } = payload;
      payload._id = undefined;
      Object.keys(payload).map(key => {
        if (isValue(payload[key])) {
          if (Array.isArray(payload[key])) {
            for (let i = 0; i < payload[key].length; i += 1) {
              form.append(key, payload[key][i]);
            }
            return;
          }
          form.set(key, trim(payload[key]));
        }
      });
      return axios.put(`${ip}/${this.url.update}/${_id}`, form);
    }
    const { _id } = payload;
    payload._id = undefined;
    Object.keys(payload).map(key => {
      if (isValue(payload[key])) payload[key] = trim(payload[key]);
    });
    return axios.put(`${ip3}/${this.url.update}/${_id}`, payload);
  }
}

export default Services;
