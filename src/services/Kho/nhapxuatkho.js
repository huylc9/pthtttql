import axios from '@/utils/axios.js';
import { ip } from '../ip';

export async function getAll(params) {
    return axios.get(`${ip}/phieu-kho/get-all`, { params });
}

export async function createXuatKho(payload) {
    return axios.post(`${ip}/phieu-kho/create-phieu-xuat-kho`, payload);
}

export async function createNhapKho(payload) {
    return axios.post(`${ip}/phieu-kho/create-phieu-nhap-kho`, payload);
}

export async function update(payload) {
    return axios.put(`${ip}/phieu-kho/update-ngay-hach-toan/${payload.idPhieuKho}/${payload.ngayHachToan}`, payload);
}
