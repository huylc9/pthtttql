import axios from '@/utils/axios.js';
import { ip } from '../ip';

export async function getAll(params) {
    return axios.get(`${ip}/hang-hoa/get-all`, { params });
}

export async function create(payload) {
    return axios.post(`${ip}/hang-hoa/create`, payload);
}

export async function update(payload) {
    return axios.put(`${ip}/hang-hoa/update`, payload);
}

export async function del({_id}) {
    return axios.delete(`${ip}/hang-hoa/delte/${_id}`);
}
