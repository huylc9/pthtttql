import axios from '@/utils/axios.js';
import { ip } from '../ip';

export async function getAll(params) {
    return axios.get(`${ip}/kho-hang/get-all`, { params });
}

export async function create(payload) {
    return axios.post(`${ip}/kho-hang/create`, payload);
}

export async function getDuLieuKho({maKhoHang}) {
    return axios.get(`${ip}/kho-hang/get-all-hang-hoa/${maKhoHang}`);
}
