import axios from '@/utils/axios.js';
import { ip } from '../ip';

export async function getAll(params) {
    return axios.get(`${ip}/giao-dich/chi-tien/get-all`, { params });
}

export async function create(payload) {
    return axios.post(`${ip}/giao-dich/chi-tien/create`, payload);
}

export async function update({ maGiaoDich, thoiGianHachToan }) {
    return axios.put(`${ip}/giao-dich/chi-tien/update/${maGiaoDich}`, { thoiGianHachToan });
}