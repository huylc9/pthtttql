import axios from '@/utils/axios.js';
import { ip } from '../ip';

export async function getAll(params) {
    return axios.get(`${ip}/giao-dich/thu-tien/get-all`, { params });
}

export async function create(payload) {
    return axios.post(`${ip}/giao-dich/thu-tien/create`, payload);
}

export async function update({ maGiaoDich, thoiGianHachToan }) {
    return axios.put(`${ip}/giao-dich/thu-tien/update/${maGiaoDich}`, { thoiGianHachToan });
}
