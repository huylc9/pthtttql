import axios from '@/utils/axios.js';
import { ip } from '../ip';

export async function getByMaGiaoDich(maGiaoDich) {
    return axios.get(`${ip}/giao-dich/hach-toan/get-by-ma-giao-dich/${maGiaoDich}`);
}

export async function create(payload) {
    return axios.post(`${ip}/giao-dich/hach-toan/create`, payload);
}

export async function getThongKe({ngayBatDau, ngayKetThuc}) {
    return axios.get(`${ip}/giao-dich/hach-toan/thong-ke-hach-toan/${ngayBatDau}/${ngayKetThuc}`);
}
