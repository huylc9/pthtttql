import axios from '@/utils/axios.js';
import { ip } from '../ip';

export async function getAll(params) {
  return axios.get(`${ip}/nhan-vien/get-all-by-ma-chi-nhanh`, { params });
}

export async function create(payload) {
  return axios.post(`${ip}/nhan-vien/create`, payload);
}

export async function getOne(payload) {
  return axios.get(`${ip}/nhan-vien/get-by-ma-nhan-vien/${payload.id}`);
}

export async function updateOne(payload) {
  const body = { ...payload };
  delete body.maNhanVien;
  return axios.put(`${ip}/nhan-vien/update-by-ma-nhan-vien/${payload.maNhanVien}`, body);
}
