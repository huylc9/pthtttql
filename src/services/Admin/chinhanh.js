import axios from '@/utils/axios.js';
import { ip } from '../ip';

export async function getAll(params) {
  return axios.get(`${ip}/chi-nhanh/get-all`, { params });
}

export async function create(payload) {
  return axios.post(`${ip}/chi-nhanh/create`, payload);
}

export async function getOne(payload) {
  return axios.get(`${ip}/chi-nhanh/get-chi-nhanh-ma-chi-nhanh/${payload.id}`);
}

export async function updateOne(payload) {
  return axios.put(`${ip}/chi-nhanh/update/${payload.maChiNhanh}`, payload);
}

export async function deleteOne(payload) {
  console.log('payload', payload);
  return axios.delete(`${ip}/chi-nhanh/delete-by-ma-chi-nhanh/${payload.maChiNhanh}`);
}
