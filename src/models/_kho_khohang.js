import _ from 'lodash';
import { formatMessage } from 'umi/locale';
import notificationAlert from '@/components/Notification';
import {create, getAll, getDuLieuKho } from '@/services/Kho/khohang';

const namespace = "khoHang";

const initialState = {
  danhSach: [],
  edit: false,
  record: {},
  showDrawer: false,
  paging: {
    page: 1,
    size: 10,
  },
  filterInfo: {},
  total: 0,
  isTouched: false,
  thongTinKhoHang: [],
};

export default {
  namespace,
  state: initialState,
  effects: {
    *getAll({ payload }, { call, put, select }) {
      let currentPayload = { ...payload };
      if (!payload) {
        currentPayload = yield select(state => state[namespace].paging);
      }
      const response = yield call(getAll, currentPayload);
      yield put({
        type: 'changeState',
        payload: {
          danhSach: _.get(response, 'data.data[0]', []),
          paging: currentPayload,
          total: _.get(response, 'data.total', 0),
        },
      });
    },

    *create({ payload }, { call, put }) {
      yield call(create, payload);
      notificationAlert('success', formatMessage({ id: 'THEM_THANH_CONG' }));
      yield put({ type: 'getAll' });
      yield put({ type: 'changeState', payload: { showDrawer: false } });
    },

    *getDuLieu({ payload }, { call, put }) {
      const response = yield call(getDuLieuKho, payload);
      yield put({ 
        type: 'changeState', 
        payload: {
          thongTinKhoHang: _.get(response, 'data.data', []),
        }
       });
    },
  },
  reducers: {
    changeState(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },
};
