
import router from 'umi/router';
import { routerRedux } from 'dva/router';
import { stringify } from 'qs';
import { formatMessage } from 'umi/locale';
import _ from 'lodash';
import { setAuthority, setToken, getAuthority } from '@/utils/authority';
import { getPageQuery, includes } from '@/utils/utils';
import { reloadAuthorized } from '@/utils/Authorized';
import notificationAlert, { success, error } from '@/components/Notification';
import {
    login as loginRequest,
    logout,
    changePassword,
    updateProfile,
    forgotPassword,
    getId,
} from '@/services/user';

import { Process } from '@/utils/selfFunction';
import data from '@/utils/data';

const handleRedirect = vaiTro => {
    const { pathname } = window.location;
    if (includes(pathname, '/user/login') || pathname === '') {
        router.push(_.get(data, `path[${vaiTro}]`, '/'));
    }
};

const initialState = {
    status: undefined,
    TenTaiKhoan: undefined,
    _id: undefined,
    user: {},
};
export default {
    namespace: 'login',

    state: initialState,

    effects: {
        * login({ payload }, { call, put }) {
            const { userName: username, password } = payload;
            let response;

            try {
                response = yield call(loginRequest, { username, password });
            } catch (error) {
                notificationAlert('error', formatMessage({ id: 'Tên tài khoản hoặc mật khẩu chưa đúng' }));
                return;
            }
            const { data: {access_token} } = response.data;
            console.log({access_token});
            yield put({
                type: 'changeLoginStatus',
                payload: {
                    status: true,
                },
            });

            localStorage.setItem('access_token', access_token);

 

            let url = "/trangchu";

            const urlParams = new URL(window.location.href);
            const params = getPageQuery();
            let { redirect } = params;
            if (redirect) {
                const redirectUrlParams = new URL(redirect);
                if (redirectUrlParams.origin === urlParams.origin) {
                    redirect = redirect.substr(urlParams.origin.length);
                    if (redirect.match(/^\/.*#/)) {
                        redirect = redirect.substr(redirect.indexOf('#') + 1);
                    }
                    url = redirect;
                } else {
                    url = redirect;
                }
            }

            // handleRedirect(vaiTro);
            // yield put(routerRedux.replace(url));
            yield put({
                type: 'fetchCurrent',
                payload:{
                    url
                },
                onComplete: () => {
                    reloadAuthorized();
                }
            });
        },
        * loginFacebook({ payload }, { call, put }) {

        },
        * loginGoogle({ payload }, { call, put }) {

        },
        * fetchCurrent({ onComplete: callback, payload}, { call, put }) {
            const url = _.get(payload, "url");
            // lay thong tin user
            const token = localStorage.getItem('access_token');
            if (!token) {
                yield put({
                    type: 'logout',
                });
                return;
            }
            try {
                yield put({
                    type: 'changeState',
                    payload: {
                        loading: true,
                    },
                });
                const response = yield call(getId, { token });

                const { data: {username, role: {name: _role }} } = response.data;
                const role = getAuthority(_role);


                localStorage.setItem("authority", JSON.stringify(role));


                yield put({
                    type: 'changeState',
                    payload: {
                        TenTaiKhoan: username,
                        status: true,
                    },
                });
            } catch (err) {
                // token hết hạn
                yield put.resolve({
                    type: 'login/logout',
                });
                notificationAlert('error', 'Hết phiên đăng nhập', 'Vui lòng đăng nhập lại');
            }
            finally {
                yield put({
                    type: 'changeState',
                    payload: {
                        loading: false,
                    },
                });

                if (
                    typeof callback === 'function'
                ) {
                    callback();
                }
                if (url) yield put(routerRedux.replace(url));
            }
        },
        * changePassword({ payload }, { call, put, select }) {
            if (payload.oldPassword === payload.newPassword) {
                notificationAlert('error', 'Mật khẩu mới không được trùng với mật khẩu cũ');
                return;
            }
            const response = yield call(changePassword, payload);
            const { success, message } = response.data;
            if (!success) {
                notificationAlert('error', 'Mật khẩu cũ  không chính xác');
                return;
            }
            notificationAlert(
                'success',
                'Đổi mật khẩu thành công',
                formatMessage({ id: 'DANG_NHAP_LAI' })
            );
            yield put({
                type: 'login/logout',
            });
        },
        * updateProfile({ payload, onComplete }, { call, put }) {
            yield call(updateProfile, payload);
            notificationAlert('success', formatMessage({ id: 'SUA_THANH_CONG' }));
            yield put({ type: 'get' });
            if (onComplete) onComplete();
            yield put({ type: 'changeState', payload: { showDrawer: false } });
        },
        * fotgotPassword({ payload }, { call, put }) {
            yield call(forgotPassword, payload);
            notificationAlert('success', 'Đã gửi mật khẩu tạm thời', 'Vui lòng kiểm tra gmail');
        },

        * logout(_, { put, call }) {
            // return;
            /**
             * remove chat token
             */
            try {
                // yield call(logout);
            } catch (e) {
                console.log(e);
            }
            // localStorage.removeItem('access_token');
            yield put({
                type: 'changeLoginStatus',
                payload: {
                    status: false,
                    currentAuthority: 'guest',
                },
            });
            reloadAuthorized();
            yield put(
                routerRedux.push({
                    pathname: '/user/login',
                    search: stringify({
                        redirect: window.location.href,
                    }),
                })
            );
        },
    },

    reducers: {
        changeLoginStatus(state, { payload }) {
            setAuthority(payload.currentAuthority);
            setToken(payload.token);
            return {
                ...state,
                status: payload.status,
            };
        },
        changeState(state, { payload }) {
            return {
                ...state,
                ...payload,
            };
        },
    },
};