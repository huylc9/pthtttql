import _ from 'lodash';
import { formatMessage } from 'umi/locale';
import notificationAlert from '@/components/Notification';
import {create, getAll, update, chamCong, getDuLieu, khoaBang } from '@/services/Luong/chamcong';

const namespace = "bangChamCong";

const initialState = {
  danhSach: [],
  edit: false,
  record: {},
  showDrawer: false,
  paging: {
    page: 1,
    size: 10,
  },
  filterInfo: {},
  total: 0,
  isTouched: false,
  thongTinChamCong:[],
};

export default {
  namespace,
  state: initialState,
  effects: {
    *getAll({ payload }, { call, put, select }) {
      let currentPayload = { ...payload };
      if (!payload) {
        currentPayload = yield select(state => state[namespace].paging);
      }
      const response = yield call(getAll, currentPayload);
      yield put({
        type: 'changeState',
        payload: {
          danhSach: _.get(response, 'data.data[0]', []),
          paging: currentPayload,
          total: _.get(response, 'data.total', 0),
        },
      });
    },

    *create({ payload }, { call, put }) {
      yield call(create, payload);
      notificationAlert('success', formatMessage({ id: 'THEM_THANH_CONG' }));
      yield put({ type: 'getAll' });
      yield put({ type: 'changeState', payload: { showDrawer: false } });
    },

    *update({ payload }, { call, put }) {
      yield call(update, payload);
    //   notificationAlert('success', formatMessage({ id: 'THEM_THANH_CONG' }));
      yield put({ type: 'getAll' });
      yield put({ type: 'changeState', payload: { showDrawer: false } });
    },

    *chamCong({ payload }, { call, put }) {
      yield call(chamCong, payload);
      notificationAlert('success', formatMessage({ id: 'THEM_THANH_CONG' }));
      yield put({ type: 'changeState', payload: { showDrawerNV: false } });
    },
    *khoaBang({ payload }, { call, put }) {
      yield call(khoaBang, payload);
      notificationAlert('success', "Khoá bảng thành công");
      yield put({ type: 'getAll'});
    },
    *getDuLieu({ payload }, { call, put }) {
      const res = yield call(getDuLieu, payload);
      console.log(res);
      yield put({ 
        type: 'changeState', 
        payload: { 
          thongTinChamCong: _.get(res, "data.data.count", [])
        } 
      });
    },
  },
  reducers: {
    changeState(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },
};
