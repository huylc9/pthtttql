import _ from 'lodash';
import { formatMessage } from 'umi/locale';
import notificationAlert from '@/components/Notification';
import {create, getByMaGiaoDich, getThongKe } from '@/services/Quy/hachtoan';
import moment from 'moment';

const namespace = "hachToan";
const month = ["06-2020", "07-2020", "08-2020", "09-2020", "10-2020", "11-2020", "12-2020", "01-2021" , "02-2021" , "03-2021" , "04-2021" , "05-2021"];

const initialState = {
  view: false,
  record: {},
  giaoDich: {},
  showDrawer: false,
  paging: {
    page: 1,
    size: 10,
  },
  filterInfo: {},
  total: 0,
  isTouched: false,
  thongKe: {},
  thongKeTheoThang: [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}]
};

export default {
  namespace,
  state: initialState,
  effects: {
    *getByMaGiaoDich({ payload }, { call, put, select }) {
      
      const response = yield call(getByMaGiaoDich, payload.maGiaoDich);
      yield put({
        type: 'changeState',
        payload: {
          record: _.get(response, 'data.data[0]', []),
          total: _.get(response, 'data.total', 0),
        },
      });
    },

    *getThongKeTheoThang({ payload },  { call, put, select }) {
      const data = [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}];
      for (let i = 0; i < 12; i++) {
         const response = yield call(getThongKe, {
           ngayBatDau: moment(`01-${month[i]}`, "DD-MM-YYYY").toISOString(),
           ngayKetThuc: moment(`28-${month[i]}`, "DD-MM-YYYY").toISOString(),
         });
        //  console.log({response});
         data[i] = _.get(response, "data.data", {});
      }
      yield put({
        type: 'changeState',
        payload: {
          thongKeTheoThang: data
        },
      });
    },

    *getThongKe({ payload }, { call, put, select }) {
      
      const response = yield call(getThongKe, payload);
      yield put({
        type: 'changeState',
        payload: {
          thongKe: _.get(response, 'data.data', {}),
        },
      });
    },

    *create({ payload, onComplete }, { call, put, select }) {
      const {taiKhoanNo, taiKhoanCo} = payload;
      // ban hang
      if (taiKhoanNo == 111 && taiKhoanCo == 156) {
        // console.log("ban hang");
        yield call(create, {...payload, taiKhoanNo: 511, taiKhoanCo: 0});
      }

      yield call(create, payload);
      notificationAlert('success', formatMessage({ id: 'THEM_THANH_CONG' }));
      yield put({ type: 'getAll' });
      yield put({ type: 'changeState', payload: { showDrawer: false } });
      yield put({
        type: "getThongKe",
        payload: {
            ngayBatDau: moment().subtract(1000, "days").toISOString(),
            ngayKetThuc: moment().add(10, "days").toISOString(),
          }
      })
      if (onComplete) onComplete();
    },
  },
  reducers: {
    changeState(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },
};
