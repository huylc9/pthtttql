import _ from 'lodash';
import { formatMessage } from 'umi/locale';
import notificationAlert from '@/components/Notification';
import {create, getAll, tinhTienLuong, getTienLuong, thanhToanLuong, themBangChamCong } from '@/services/Luong/thanhtoanluong';

const namespace = "thanhToanLuong";

const initialState = {
  danhSach: [],
  edit: false,
  record: {},
  showDrawer: false,
  paging: {
    page: 1,
    size: 10,
  },
  filterInfo: {},
  total: 0,
  isTouched: false,
  thongTinTienLuong:[],
};

export default {
  namespace,
  state: initialState,
  effects: {
    *getAll({ payload }, { call, put, select }) {
      let currentPayload = { ...payload };
      if (!payload) {
        currentPayload = yield select(state => state[namespace].paging);
      }
      const response = yield call(getAll, currentPayload);
      yield put({
        type: 'changeState',
        payload: {
          danhSach: _.get(response, 'data.data[0]', []),
          paging: currentPayload,
          total: _.get(response, 'data.total', 0),
        },
      });
    },

    *create({ payload, onComplete }, { call, put }) {
      const response = yield call(create, payload);
      notificationAlert('success', formatMessage({ id: 'THEM_THANH_CONG' }));
      yield put({ type: 'getAll' });
      yield put({ type: 'changeState', payload: { showDrawer: false } });
      if (onComplete) onComplete(_.get(response, "data.data.maThanhToanLuong", 0));
    },

    *tinhTienLuong({ payload }, { call, put }) {
      yield call(tinhTienLuong, payload);
    },

    *getTienLuong({ payload, onComplete }, { call, put }) {
      yield call(tinhTienLuong, payload);
      const response = yield call(getTienLuong, payload);
      const data = _.get(response, 'data.data', []);
      yield put({
            type: 'changeState',
            payload: {
                thongTinTienLuong: data
            }
        });
      let soTien = 0;
      data.map(item => soTien += item.tienLuong);
      if (onComplete) onComplete(soTien)
    },

    *thanhToanLuong({ payload }, { call, put }) {
      yield call(thanhToanLuong, payload);
      yield put({
        type: "getAll"
      })
    },
    *themBangChamCong({ payload }, { call, put }) {
      yield call(themBangChamCong, payload);
    },
  },
  reducers: {
    changeState(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },
};
