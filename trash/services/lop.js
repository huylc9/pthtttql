import Service from './BaseService';
import axios from '@/utils/axios.js';
import { ip3 } from './ip';

class ExtendedService extends Service {
  getLopGv = async (payload) => axios.get(`${ip3}/lop/v1/user/my`, { params: payload })

  getById = async ({ _id }) => axios.get(`${ip3}/lop/getListGiaoVienAndHocSinhTheoLop/${_id}`)

  getDiemDanh = async ({ maLopHoc }) => axios.get(`${ip3}/diemdanh/v1/lop/${maLopHoc}`)

  getLopDeChon = async (payload) => axios.get(`${ip3}/lop/giang-vien/search/available-class/current`, { params: payload })

  add = async ({ maLopHoc }) => axios.put(`${ip3}/lop/giang-vien/join-class/${maLopHoc}`)

  del = async ({ maLopHoc }) => axios.put(`${ip3}/lop/giang-vien/leave/${maLopHoc}`)

  getByAdmin = async (payload) => axios.get(`${ip3}/lop`, { params: payload })
}

export default new ExtendedService({ name: 'lop', formData: false });