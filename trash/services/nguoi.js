import _ from 'lodash';
import axios from '@/utils/axios.js';
import Service from './BaseService';
import { ip3 } from './ip';

class NguoiService extends Service {
  getId = async ({ _id }) => axios.get(`${ip3}/nguoi/${_id}`);
}

export default new NguoiService({ name: 'nguoi', formData: true });
