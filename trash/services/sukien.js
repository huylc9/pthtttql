import _ from 'lodash';
import axios from '@/utils/axios.js';
import { ip3 } from './ip';
import Service from './BaseService';

class ExtendedService extends Service {
}

export default new ExtendedService({ name: 'sukien', formData: false });
