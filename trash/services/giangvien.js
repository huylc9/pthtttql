import _ from 'lodash';
import axios from '@/utils/axios.js';
import { ip3 } from './ip';
import Service from './BaseService';


class ExtendedService extends Service {
  forceActive = payload => axios.post(`${ip3}/users/force/activate`, payload);
}

export default new ExtendedService({ name: 'giangvien', formData: true, url: 'users' });
