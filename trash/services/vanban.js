import _ from 'lodash';
import axios from '@/utils/axios.js';
import { ip3 } from './ip';
import Service from './BaseService';
import { isValue, trim } from '../utils/utils';

class ExtendedService extends Service {
  getById = async id => axios.get(`${ip3}/vanban/${id}`);

  addTaiLieu = async ({ idVanBan, ...payload }) => {
    const form = new FormData();
    Object.keys(payload).map(key => {
      if (isValue(payload[key])) form.set(key, trim(payload[key]));
    });
    return axios.post(`${ip3}/vanban/${idVanBan}`, form);
  };

  updateTaiLieu = async ({ idVanBan, idTaiLieu, ...payload }) => {
    const form = new FormData();
    Object.keys(payload).map(key => {
      if (isValue(payload[key])) form.set(key, trim(payload[key]));
    });
    return axios.put(`${ip3}/vanban/${idVanBan}/tailieu/${idTaiLieu}`, form);
  };

  delTaiLieu = async ({ idVanBan, idTaiLieu }) =>
    axios.delete(`${ip3}/vanban/${idVanBan}/tailieu/${idTaiLieu}`);
}

export default new ExtendedService({ name: 'vanban', formData: false });
