import _ from 'lodash';
import axios from '@/utils/axios.js';
import { ip3 } from './ip';
import Service from './BaseService';


class ExtendedService extends Service {
    get = async () => axios.get(`${ip3}/lop/v1/user/my/`)
    getLop = async (payload) => axios.get(`${ip3}/diemdanh/v1/my/lop-tuan-thu-tiet`, { params: payload });
    putDiemDanh = async (payload) => {
        const { _id } = payload;
        payload._id = undefined;
        return axios.put(`${ip3}/diemdanh/${_id}`, payload);
    }
}

export default new ExtendedService({ name: 'thoikhoabieu', formData: true, url: 'tkb' });
