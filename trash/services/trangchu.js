import _ from 'lodash';
import axios from '@/utils/axios.js';
import { ip3, ip } from './ip';
import Service from './BaseService';

class ExtendedService extends Service {
  getGVTheoKhoa = async () => axios.get(`${ip}/dashboard/thong-ke/giang-vien-theo-khoa`);

  getSLLop = async () => axios.get(`${ip}/sotay/lop`);

  getSLKhaiBaoSucKhoe = async payload => {
    const { value } = payload;

    return axios.get(`${ip}/dashboard/thong-ke/khai-bao-suc-khoe/${value}`);
  };

  getSLKhaiBaoCachLy = async payload => {
    const { value } = payload;
    return axios.get(`${ip}/dashboard/thong-ke/khai-bao-cach-ly/${value}`);
  };
}

export default new ExtendedService({ name: 'trangchu', formData: true });
