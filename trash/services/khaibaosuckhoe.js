import _ from 'lodash';
import axios from '@/utils/axios.js';
import { ip } from './ip';
import Service from './BaseService';

class ExtendedService extends Service {
  get = async payload => axios.get(`${ip}/${this.url}/all/latest`, { params: payload });

  getByMSV = async payload => {
    const { maSv, page, limit } = payload;
    return axios.get(`${ip}/${this.url}/msv/${maSv}`, { params: { page, limit } });
  };

  ;
}

export default new ExtendedService({
  name: 'khaibaosuckhoe',
  formData: false,
  url: 'khai-bao-suc-khoe',
});
