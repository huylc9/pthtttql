import Service from './BaseService';
import axios from '@/utils/axios.js';
import { ip } from './ip';

class ExtendedService extends Service {

}

export default new ExtendedService({ name: 'tintuc', formData: true });

export async function uploadFile(file) {
  const form = new FormData();
  form.append('uploadAnh', file);
  return axios.post(`${ip}/upload-anh/`, form);
}
