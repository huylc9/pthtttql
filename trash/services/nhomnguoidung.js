import _ from 'lodash';
import axios from '@/utils/axios.js';
import Service from './BaseService';
import { ip3 } from './ip';

class NhomNguoiDungService extends Service {}

export default new NhomNguoiDungService({
  name: 'nhomnguoidung',
  url: 'usergroup',
  formData: false,
});
