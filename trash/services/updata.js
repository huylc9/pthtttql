import axios from '@/utils/axios.js';
import { ip } from './ip';

export async function sendData({ data }) {
  const formData = new FormData();
  data.map(item => {
    formData.append('data', item.originFileObj);
  });
  return axios.post(`${ip}/sotay/data/import`, formData);
}

export async function get() {
  return axios.get(`${ip}/sotay/data/download`);
}
