import _ from 'lodash';
import axios from '@/utils/axios.js';
import { ip3 } from './ip';
import Service from './BaseService';
import { isValue, trim } from '../utils/utils';

class ExtendedService extends Service {
    get = async (payload) => axios.get(`${ip3}/${this.url}/my`, { params: payload })

    upd = async payload => {
        if (this.formData) {
            const form = new FormData();
            const { _id } = payload;
            payload._id = undefined;
            Object.keys(payload).map(key => {
                if (isValue(payload[key])) form.set(key, trim(payload[key]));
            });
            return axios.put(`${ip3}/${this.url}/v1/${_id}`, form);
        }
        const { _id } = payload;
        payload._id = undefined;
        Object.keys(payload).map(key => {
            if (isValue(payload[key])) payload[key] = trim(payload[key]);
        });
        return axios.put(`${ip3}/${this.url}/v1/${_id}`, payload);
    }
}

export default new ExtendedService({ name: 'tailieu', formData: true });
