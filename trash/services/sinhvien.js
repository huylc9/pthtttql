import _ from 'lodash';
import axios from '@/utils/axios.js';
import { ip3 } from './ip';
import Service from './BaseService';

class ExtendedService extends Service {
  getByMaSinhVien = maSinhVien => axios.get(`${ip3}/users/masinhvien/${maSinhVien}`);

  guiThongBaoTheoMaSinhVien = payload =>
    axios.post(`${ip3}/notification/admin/sinhvien/masinhvien/`, {
      ...payload,
    });

  forceActive = payload => axios.post(`${ip3}/users/force/activate`, payload);

  changePassword = payload => axios.post(`${ip3}/users/u/password`, payload);
}

export default new ExtendedService({ name: 'sinhvien', formData: true, url: 'users' });

export async function resetPwd(idUser) {
  return axios.post(`${ip3}/users/force/reset-password`, {
    idUser,
  });
}
