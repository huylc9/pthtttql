import Service from './BaseService';
import axios from '@/utils/axios.js';
import { ip3 } from './ip';

class ExtendedService extends Service {
  get = async (payload) => {
    const { page, limit, cond: { loaiPhanHoi, vaiTro, trangThai } } = payload;
    if (loaiPhanHoi === 0) {
      // su dung cho phan hoi cua sinh vien va giang vien
      return axios.get(`${ip3}/${this.url}/ask/vaitro?page=${page}&limit=${limit}&vaiTro=${vaiTro}&trangThai=${trangThai ? 0 : 1}`);
    }
    // su dung cho ho tro ky thuat
    return axios.get(`${ip3}/${this.url}/`, { params: payload });

  }
}

export default new ExtendedService({ name: 'phanhoi', formData: false });
