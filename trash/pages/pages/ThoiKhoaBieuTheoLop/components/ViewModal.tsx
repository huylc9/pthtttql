import React from 'react'
import { Modal, Spin, Row, Col } from 'antd';
import { connect } from 'dva';
import { IBase } from '@/utils/base';
import {
    Calendar,
    momentLocalizer,
} from 'react-big-calendar';
import moment from 'moment';
import { tinhNgayTheoTuan } from '@/utils/utils';

const messages = {
    allDay: 'Cả ngày',
    previous: 'Trước',
    next: 'Sau',
    today: 'Hôm nay',
    month: 'Tháng',
    week: 'Tuần',
    day: 'Ngày',
    agenda: 'Chung',
    date: 'Ngày',
    time: 'Thời gian',
    event: 'Sự kiện',
    showMore: total => `+ Xem thêm (${total})`,
};

let minDate = moment();
let didSet = false;// dùng để kiểm tra xem đã set date mặc định trong hàm checkDate hay chưa

type Props = {
    lop: IBase,
    loading: boolean,
    dispatch: Function,
}

@connect(({ lop, loading }) => ({
    lop,
    loading: loading.models.notification,
}))
class ViewModal extends React.Component<Props> {
    state = {
    }

    componentDidMount() {
    }

    componentDidUpdate() {
        didSet = false;
    }

    handleOk = () => {
        this.props.dispatch({
            type: 'lop/changeState',
            payload: {
                visible: false
            }
        })
    }

    handleCancel = () => {
        this.props.dispatch({
            type: 'lop/changeState',
            payload: {
                visible: false
            }
        })
    }

    checkDate = async (date) => {
        if (didSet === false) {
            didSet = true;
            minDate = date;
        }
    }

    getLichHoc = (lichHoc) => {
        const { record: { kyHoc: { ngayBatDau }, monHoc: { tenMonHoc }, giaoVien } } = this.props.lop;
        const { tuanHoc, thuHoc, thoiGianBatDau, thoiGianKetThuc, tietBatDau, tietKetThuc, phongHoc } = lichHoc;
        const tenGiaoVien = (giaoVien.map(item => item.hoTen)).toString();
        const lich = tuanHoc.map(tuan => {
            const ngayHoc = tinhNgayTheoTuan(tuan, thuHoc, ngayBatDau);
            this.checkDate(ngayHoc);
            const timeStart = moment(thoiGianBatDau, 'HH:mm');
            const timeEnd = moment(thoiGianKetThuc, 'HH:mm');
            const start = moment(ngayHoc).add(timeStart.hours(), 'hours').add(timeStart.minutes(), 'minutes').toDate();
            const end = moment(ngayHoc).add(timeEnd.hours(), 'hours').add(timeEnd.minutes(), 'minutes').toDate();
            const title = <div style={{ lineHeight: 1.25 }}>
                Môn học: {tenMonHoc}.
                <br />
                Giảng viên: {tenGiaoVien}.
                <br />
                Phòng: {phongHoc || 'Chưa cập nhật'}.
                <br />
                Tiết: {tietBatDau} - {tietKetThuc}.
            </div>
            return {
                tenMonHoc,
                phongHoc,
                tietBatDau,
                tietKetThuc,
                tenGiaoVien,
                start,
                end,
                title,
            }
        })
        return lich;
    }

    onSelectEvent = event => {
        Modal.info({
            title: event.tenMonHoc,
            content: (
                <div>
                    Giảng viên: {event.tenGiaoVien}.
                    <br />
                    Ngày học: {moment(event.ngayBatDau).format('DD/MM/YYYY')}.
                    <br />
                    Phòng học : {event.phongHoc || 'Chưa cập nhập'}.
                    <br />
                    Tiết học: {event.tietBatDau} - {event.tietKetThuc}.
                </div>
            )
        })
    }

    render() {
        const { lop } = this.props;
        const { visible, record } = lop;
        const { lichHoc } = record;
        let data: Array<object> = [];
        if (typeof lichHoc !== 'undefined') {
            for (let i = 0; i < lichHoc.length; i++) {
                const lich = this.getLichHoc(lichHoc[i]);
                data = [
                    ...data,
                    ...lich,
                ]
            }
        }
        const localizer = momentLocalizer(moment);
        return (
            <Modal
                key={visible}
                title={`Lịch học lớp: ${record.tenLopHoc} - ${!!record.monHoc ? record.monHoc.tenMonHoc : ''}`}
                visible={visible}
                destroyOnClose
                width="90%"
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                style={{ marginTop: -80 }}
            >
                <Row>
                    <Col xs={24}>
                        <Calendar
                            localizer={localizer}
                            selectable
                            defaultView='week'
                            events={data}
                            scrollToTime={new Date(1970, 1, 1, 6)}
                            defaultDate={minDate.toDate()}
                            messages={messages}
                            views={['month', 'week', 'day']}
                            style={{ height: 800 }}
                            min={moment('0600', 'HHmm').toDate()}
                            max={moment('2000', 'HHmm').toDate()}
                            onSelectEvent={event => this.onSelectEvent(event)}
                        />
                    </Col>
                </Row>
            </Modal>
        )
    }
}

export default ViewModal;