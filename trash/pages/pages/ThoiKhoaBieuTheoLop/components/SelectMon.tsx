import React from 'react'
import { Select } from 'antd';
import { Format } from '@/utils/utils';

function SelectMon(danhSach: Array<object>, onSearch: Function, onSelect: Function, value: string) {
    return (
        <Select
            value={value}
            placeholder="Tìm kiếm theo mã môn học"
            onChange={onSelect}
            onSearch={onSearch}
            style={{ width: 300 }}
            allowClear
            showSearch
            filterOption={(input, option) => Format(option.props.children).indexOf(Format(input)) >= 0}
        >
            {danhSach.map((item, index) => (
                <Select.Option key={index} value={item.maMonHoc}>
                    {item.maMonHoc}-{item.tenMonHoc}
                </Select.Option>
            ))}
        </Select>
    );
}

export default SelectMon;