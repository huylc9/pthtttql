import React from 'react';
import { connect } from 'dva';
import uuidv1 from 'uuid/v1';
import { Button, Divider, Popconfirm, Modal } from 'antd';
import { IColumn } from '@/utils/interfaces';
import Form from './components/Form';
import TableBase from '@/components/Table/Table.tsx';
import { IBase } from '@/utils/base';
import moment from 'moment';
import SelectMon from './components/SelectMon';
import ViewModal from './components/ViewModal';
import _ from 'lodash';
import { toRegex } from '@/utils/utils';

type Props = {
  lop: IBase,
  monhoc: IBase,
  loading: boolean,
  dispatch: Function,
}

@connect(({ lop, monhoc, loading }) => ({
  lop,
  monhoc,
  loading: loading.models.lop,
}))
class lop extends React.Component<Props> {
  state = {
    maMonHoc: undefined,
  };

  debouncedMon = _.debounce((value) => { this.handleSearch(value) }, 500);

  componentDidMount() {
    this.props.dispatch({
      type: 'monhoc/get',
      payload: {
        page: 1,
        limit: 10,
      }
    })
  }

  handleView = (record: object) => {
    this.props.dispatch({
      type: 'lop/changeState',
      payload: {
        record,
        visible: true,
      }
    })
  }

  onSelect = (maMonHoc: string) => {
    this.setState({
      maMonHoc
    })
  }

  handleSearch = (value) => {
    if (!value.length) return;
    const cond = { maMonHoc: toRegex(value) };
    this.props.dispatch({
      type: 'monhoc/get',
      payload: {
        cond,
        page: 1,
        limit: 10,
      }
    })
  }

  render() {
    const { monhoc } = this.props;
    const { danhSach } = monhoc;

    const renderLast = (value, record: object) => (
      <React.Fragment>
        <Button
          type="primary"
          shape="circle"
          icon="eye"
          onClick={() => this.handleView(record)}
          title="Xem thời khóa biểu"
        />
      </React.Fragment>
    )

    const columns: IColumn[] = [
      {
        title: 'STT',
        dataIndex: 'index',
        align: 'center',
        width: '80px',
      },
      {
        title: 'Tên lớp học',
        dataIndex: 'tenLopHoc',
        align: 'center',
        width: '200px',
        search: 'search'
      },
      {
        title: 'Tên môn học',
        dataIndex: 'monHoc',
        align: 'center',
        render: value => value.tenMonHoc,
        search: 'search',
        width: '350px',
      },
      {
        title: 'Kỳ học',
        dataIndex: 'kyHoc',
        align: 'center',
        width: '100px',
        render: value => value.ky,
      },
      {
        title: 'Giảng viên',
        dataIndex: 'giaoVien',
        align: 'center',
        width: '200px',
        render: value => {
          const ten = value.map(item => item.hoTen);
          return ten.toString();
        },
        search: 'search',
      },
      {
        title: 'Số lượng sinh viên',
        dataIndex: 'sinhVien',
        align: 'center',
        width: '150px',
        render: value => value.length
      },
      {
        title: 'Thao tác',
        align: 'center',
        render: (value, record) => renderLast(value, record),
        fixed: 'right',
        width: 110,
      }
    ];

    return (
      <div>
        <TableBase
          key={this.state.maMonHoc}
          model={this.props.lop}
          modelName="lop"
          loading={this.props.loading}
          dispatch={this.props.dispatch}
          columns={columns}
          cond={{ maMonHoc: this.state.maMonHoc }}
          Form={Form}
          title={SelectMon(danhSach, this.debouncedMon, this.onSelect, this.state.maMonHoc)}
          tableProp={{
            scroll: { x: 1000, y: 800 }
          }}
        />
        <ViewModal />
      </div>
    );
  }
}

export default lop;
