/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Form, Input, Card, Row, Col, Button, DatePicker, Select, Spin } from 'antd';
import _ from 'lodash';
import { connect } from 'dva';
import { getRecordValue, includes } from '@/utils/utils';
import rules from '@/utils/rules';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import { renderGroup, formItemLayout, tailFormItemLayout, handleValuesChange, handleCommonSubmit, getSubmitText } from '@/utils/form';
import Upload from '@/components/Upload/Upload';
import moment from 'moment';
import { IBase } from '@/utils/base';


type GiangVien_Props = {
  model: { edit, record },
  khoa: IBase,
  cond: object,
  loading: boolean,
  dispatch: Function,
  form: { validateFieldsAndScroll, getFieldDecorator },
}

@connect(({ khoa }) => ({
  khoa,
}))

class GiangVien extends React.Component<GiangVien_Props> {
  componentDidMount() {
  }

  handleSubmit = e => {
    const { form, model: { edit, record }, cond } = this.props;
    e.preventDefault();
    form.validateFieldsAndScroll((err, values) => {
      if (err) return;
      values.anhDaiDien = _.get(values, 'anhDaiDien.fileList[0].originFileObj', undefined);
      handleCommonSubmit(values, this.props);
    });
  };

  render() {
    const { model, form: { getFieldDecorator }, cond, loading, khoa: { danhSach } } = this.props;
    return (
      <div className="box">
        <Card
          bordered
          title={
            <div className="cardTitle">{!model.edit ? `Thêm mới` : `Chỉnh sửa`}</div>
          }
        >
          <GridContent>
            <Row>
              <Col span={18}>
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                  <Form.Item label="Họ tên">
                    {getFieldDecorator('hoTen', {
                      initialValue: model.edit ? _.get(model.record, 'hoTen', '') : '',
                      rules: [...rules.ten, ...rules.required],
                    })(<Input />)}
                  </Form.Item>
                  {!model.edit && (
                    <Form.Item label="Tài khoản">
                      {getFieldDecorator('username', {
                        initialValue: getRecordValue(model, cond, 'username', ''),
                        rules: [...rules.required, ...rules.username],
                      })(<Input disabled={model.edit} />)}
                    </Form.Item>
                  )}
                  {!model.edit && (
                    <Form.Item label="Mật khẩu">
                      {getFieldDecorator('password', {
                        initialValue: '',
                        rules: [...rules.required, ...rules.password],
                      })(
                        <Input
                          style={{ '-webkit-text-security': 'disc' }}
                          placeholder="Mật khẩu mới"
                        />
                      )}
                    </Form.Item>
                  )}
                  <Form.Item label="Thuộc khoa/ phòng ban">
                    {getFieldDecorator('maKhoa', {
                      initialValue: getRecordValue(model, cond, 'maKhoa', ''),
                    })(
                      <Select
                        showSearch
                        filterOption={(value, option) => includes(option.props.children, value)}
                      >
                        {danhSach.map((item, index) => (
                          <Select.Option key={index} value={item.maKhoa}>
                            {item.tenKhoa}
                          </Select.Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>
                  <Form.Item label="Ngày sinh">
                    {getFieldDecorator('ngaySinh', {
                      initialValue: model.edit ? moment(_.get(model.record, 'ngaySinh', '')) : moment(),
                      rules: [...rules.ngaySinh],
                    })(<DatePicker format="DD/MM/YYYY" />)}
                  </Form.Item>
                  <Form.Item label="Giới tính">
                    {getFieldDecorator('gioiTinh', {
                      initialValue: model.edit ? _.get(model.record, 'gioiTinh', 0) : 0,
                      rules: [],
                    })(renderGroup('gioiTinh'))}
                  </Form.Item>

                  <Form.Item label="Ảnh đại diện">
                    {getFieldDecorator('anhDaiDien', {
                      initialValue:
                        model.edit && model.record.anhDaiDien && model.record.anhDaiDien.length > 0
                          ? {
                            fileList: [
                              {
                                uid: 'zzz',
                                name: 'Ảnh',
                                url: model.record.anhDaiDien,
                                status: 'done',
                              },
                            ],
                          }
                          : { fileList: [] },
                      rules: [],
                    })(<Upload />)}
                  </Form.Item>

                  {!model.edit && <Form.Item label="Email">
                    {getFieldDecorator('email', {
                      initialValue: model.edit ? _.get(model.record, 'email', '') : '',
                      rules: [...rules.email],
                    })(<Input />)}
                  </Form.Item>}
                  <Form.Item label="Số điện thoại">
                    {getFieldDecorator('soDienThoai', {
                      initialValue: model.edit ? _.get(model.record, 'soDienThoai', '') : '',
                      rules: [...rules.soDienThoai],
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Địa chỉ">
                    {getFieldDecorator('diaChiHienNay', {
                      initialValue: model.edit ? _.get(model.record, 'diaChiHienNay', '') : '',
                      rules: [...rules.text],
                    })(<Input />)}
                  </Form.Item>
                  <Spin spinning={loading}>
                    <Form.Item {...tailFormItemLayout}>
                      <Button.Group>
                        <Button type="primary" icon="plus" htmlType="submit">
                          {getSubmitText(model)}
                        </Button>
                      </Button.Group>
                    </Form.Item>
                  </Spin>
                </Form>
              </Col>
            </Row>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedForm = Form.create({ name: 'GiangVien', onValuesChange: handleValuesChange })(GiangVien);

export default WrappedForm;
