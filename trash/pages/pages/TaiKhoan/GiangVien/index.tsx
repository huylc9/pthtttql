import React from 'react';
import { connect } from 'dva';
import uuidv1 from 'uuid/v1';
import {
  Button,
  Divider,
  Popconfirm,
  Modal,
  Card,
  Row,
  Col,
  Avatar,
  Descriptions,
  Typography,
} from 'antd';
import { IColumn } from '@/utils/interfaces';
import Form from './components/Form';
import TableBase from '@/components/Table/Table.tsx';
import { IBase } from '@/utils/base';
import SelectKhoa from '@/components/SelectKhoa';
import TagPopover from '@/components/TagPopover/TagPopover';
import moment from 'moment';
import _ from 'lodash';
import { isValue } from '@/utils/utils';

type Props = {
  giangvien: IBase;
  khoa: IBase;
  loading: boolean;
  dispatch: Function;
};

@connect(({ giangvien, khoa, loading }) => ({
  giangvien,
  khoa,
  loading: loading.models.giangvien,
}))
class GiangVien extends React.Component<Props> {
  state = {
    khoa: undefined,
  };

  componentDidMount() {
    this.props.dispatch({
      type: 'khoa/get',
      payload: {
        page: 1,
        limit: 1000,
      },
    });
  }

  onSelectKhoa = _id => {
    this.setState({
      khoa: _id,
    });
  };

  resetPwd = idUser => {
    this.props.dispatch({
      type: 'giangvien/resetPwd',
      idUser,
    });
  };

  forceActice = idUser => {
    this.props.dispatch({
      type: 'giangvien/forceActive',
      payload: {
        idUser,
      },
    });
  };

  handleView = record => {
    const data = {
      gioiTinh: ['Nam', 'Nữ'],
      vaiTro: ['Sinh viên', 'Giảng viên'],
    };
    Modal.info({
      title: (
        <Typography.Title style={{ marginTop: -6 }} level={3}>
          {' '}
          Thông tin sinh viên
        </Typography.Title>
      ),
      width: 1200,
      content: (
        <Card bordered={false} className="ql-editor" bodyStyle={{ minHeight: 400 }}>
          <div>
            <Row gutter={16}>
              <Col sm={24} lg={6} xl={6}>
                <Avatar
                  shape="square"
                  size={200}
                  src={_.get(
                    record,
                    'anhDaiDien',
                    'https://i.pinimg.com/236x/40/8a/c5/408ac5a5102e42678de2827089cf177b.jpg'
                  )}
                  style={{ marginLeft: 30, justifyContent: 'center', alignItems: 'center' }}
                />
                <Typography.Title
                  level={4}
                  style={{ width: 250, textAlign: 'center', marginBottom: 10 }}
                >
                  Ảnh Hồ Sơ
                </Typography.Title>
              </Col>
              <Col sm={24} lg={18} xl={18}>
                <Descriptions title="" column={{ xs: 1, sm: 1, md: 2, lg: 2, xl: 2 }} bordered>
                  <Descriptions.Item span={2} label="Họ và tên">
                    <h3>{_.get(record, 'hoTen', 'Chưa cập nhật')}</h3>
                  </Descriptions.Item>
                  <Descriptions.Item span={2} label="Khoa">
                    <h3>{_.get(record, 'maKhoa', 'Chưa cập nhật')}</h3>
                  </Descriptions.Item>
                  <Descriptions.Item span={1} label="Ngày sinh">
                    <h3>
                      {' '}
                      {moment(_.get(record, 'ngaySinh', 'Chưa cập nhật')).format('DD/MM/YYYY')}
                    </h3>
                  </Descriptions.Item>
                  <Descriptions.Item span={1} label="Giới tính">
                    <h3>
                      {isValue(record.gioiTinh) ? data.gioiTinh[record.gioiTinh] : 'Chưa cập nhật'}
                    </h3>
                  </Descriptions.Item>
                  <Descriptions.Item span={2} label="Địa chỉ hiện tại">
                    <h3>{_.get(record, 'diaChiHienNay', 'Chưa cập nhật')}</h3>
                  </Descriptions.Item>
                  <Descriptions.Item span={1} label="Số điện thoại">
                    <h3>{_.get(record, 'soDienThoai', 'Chưa cập nhật')}</h3>
                  </Descriptions.Item>
                  <Descriptions.Item span={1} label="Email">
                    <h3>{_.get(record, 'email', 'Chưa cập nhật')}</h3>
                  </Descriptions.Item>
                  <Descriptions.Item span={1} label="Vai trò">
                    <h3>{isValue(record.vaiTro) ? data.vaiTro[record.vaiTro] : 'Chưa cập nhật'}</h3>
                  </Descriptions.Item>
                  <Descriptions.Item span={1} label="Trạng thái">
                    <h3>{isValue(record.activated) ? 'Đã kích hoạt' : 'Chưa kích hoạt'}</h3>
                  </Descriptions.Item>
                </Descriptions>
              </Col>
            </Row>
          </div>
        </Card>
      ),
    });
  };

  handleEdit = record => {
    this.props.dispatch({
      type: 'giangvien/changeState',
      payload: {
        showDrawer: true,
        edit: true,
        record,
        key: uuidv1(),
        isTouched: false,
      },
    });
  };

  handleDel = _id => {
    this.props.dispatch({
      type: 'giangvien/del',
      payload: {
        _id,
        key: uuidv1(),
      },
    });
  };

  render() {
    const { khoa } = this.props;
    const { danhSach } = khoa;

    const actions = [
      {
        node: (
          <Button
            style={{ backgroundColor: '#096dd9', color: '#fff' }}
            type="primary"
            shape="circle"
            icon="eye"
            title="Xem Thông Tin"
          />
        ),
        click: 'handleView',
      },
      {
        node: <Button type="primary" shape="circle" icon="reload" title="Cấp lại mật khẩu" />,
        click: 'resetPwd',
      },
      {
        node: <Button type="primary" shape="circle" icon="check" title="Kích hoạt tài khoản" />,
        click: 'forceActice',
      },
    ];

    const functions = {
      handleView: this.handleView,
      resetPwd: this.resetPwd,
      forceActice: this.forceActice,
    };

    const renderLast = (value, record) => (
      <TagPopover
        record={record}
        handleEdit={this.handleEdit}
        handleDel={this.handleDel}
        actions={actions}
        functions={functions}
      />
    );

    const columns: IColumn[] = [
      {
        title: 'STT',
        dataIndex: 'index',
        align: 'center',
        width: '100px',
      },
      {
        title: 'Họ tên',
        dataIndex: 'hoTen',
        align: 'center',
        width: '200px',
        search: 'search',
      },
      {
        title: 'Ngày sinh',
        dataIndex: 'ngaySinh',
        align: 'center',
        width: '150px',
        render: (time, record) => (
          <p style={{ margin: 0, padding: 0 }}>
            {time ? moment(time).format('DD/MM/YYYY') : 'Chưa cập nhật'}
          </p>
        ),
      },
      {
        title: 'Khoa',
        dataIndex: 'khoa',
        align: 'center',
        width: '200px',
        render: val => _.get(val, 'tenKhoa', ''),
      },

      {
        title: 'Tài khoản',
        dataIndex: 'username',
        align: 'center',
        width: '200px',
        search: 'search',
      },
      {
        title: 'Trạng thái',
        dataIndex: 'validated',
        align: 'center',
        width: '100px',
        search: 'filterTF',
      },
      {
        title: 'Email',
        dataIndex: 'email',
        align: 'center',
        width: '200px',
        search: 'search',
      },
      {
        title: 'Số điện thoại',
        dataIndex: 'soDienThoai',
        align: 'center',
        width: '150px',
        search: 'search',
      },
      {
        title: 'Địa chỉ hiện nay',
        dataIndex: 'diaChiHienNay',
        align: 'center',
        search: 'search',
        // width: '200px',
      },
      {
        title: 'Thao tác',
        align: 'center',
        render: (value, record) => renderLast(value, record),
        fixed: 'right',
        width: 120,
      },
    ];

    return (
      <TableBase
        key={this.state.khoa}
        model={this.props.giangvien}
        modelName="giangvien"
        loading={this.props.loading}
        dispatch={this.props.dispatch}
        columns={columns}
        cond={{ vaiTro: 1, maKhoa: this.state.khoa }}
        Form={Form}
        title={SelectKhoa(danhSach, this.onSelectKhoa, this.state.khoa)}
        hasCreate
        tableProp={{
          scroll: { x: 1800, y: 600 },
        }}
      />
    );
  }
}

export default GiangVien;
