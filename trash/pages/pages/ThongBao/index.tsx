import React from 'react';
import { connect } from 'dva';
import uuidv1 from 'uuid/v1';
import { Button, Divider, Popconfirm } from 'antd';
import { IColumn } from '@/utils/interfaces';
import Form from './components/Form';
import TableBase from '@/components/Table/Table.tsx';
import { IBase } from '@/utils/base';
import moment from 'moment';
import { renderParagraph } from '@/utils/table';
import ViewModal from './components/Modal';
import _ from 'lodash';

type Props = {
  notification: IBase;
  loading: boolean;
  dispatch: Function;
};

@connect(({ notification, loading }) => ({
  notification,
  loading: loading.models.notification,
}))
class ThongBao extends React.Component<Props> {
  state = {};

  componentDidMount() {}

  handleView = record => {
    this.props.dispatch({
      type: 'notification/changeState',
      payload: {
        record,
        visible: true,
      },
    });

    this.props.dispatch({
      type: 'notification/getById',
      payload: {
        _id: record._id,
        page: 1,
        limit: 10,
      },
    });
  };

  render() {
    const renderLast = (value, record) => (
      <React.Fragment>
        <Button
          type="primary"
          shape="circle"
          icon="eye"
          onClick={() => this.handleView(record)}
          title="Xem trước"
        />
      </React.Fragment>
    );

    const columns: IColumn[] = [
      {
        title: 'STT',
        dataIndex: 'index',
        align: 'center',
        width: '80px',
      },
      {
        title: 'Tiêu đề',
        dataIndex: 'title',
        align: 'center',
        width: '500px',
        render: val => renderParagraph(val, 3),
      },
      {
        title: 'Người gửi',
        dataIndex: 'senderID',
        align: 'center',
        width: '200px',
        render: val => _.get(val, 'hoTen', undefined) || val || 'Không có dữ liệu',
      },
      {
        title: 'Nơi nhận',
        dataIndex: 'generalReceiver',
        align: 'center',
        width: '200px',
      },
      {
        title: 'Thời gian gửi',
        dataIndex: 'sentAt',
        align: 'center',
        width: '200px',
        render: value => moment(value).format('DD/MM/YYYY HH:mm'),
      },
      {
        title: 'Đã gửi',
        dataIndex: 'totalReceivers',
        align: 'center',
        // width: '200px',
      },
      {
        title: 'Thao tác',
        align: 'center',
        render: (value, record) => renderLast(value, record),
        fixed: 'right',
        width: 120,
      },
    ];

    return (
      <div>
        <TableBase
          model={this.props.notification}
          modelName="notification"
          loading={this.props.loading}
          dispatch={this.props.dispatch}
          columns={columns}
          cond={{ sentType: 1 }}
          Form={Form}
          title={'Quản lý thông báo'}
          hasCreate
          tableProp={{
            scroll: { x: 1200, y: 600 },
          }}
        />
        <ViewModal />
      </div>
    );
  }
}

export default ThongBao;
