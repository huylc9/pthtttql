import React from 'react';
import { connect } from 'dva';
import { IBase } from '@/utils/base';
import { Modal, Typography, Card, Table } from 'antd';
import _ from 'lodash';
import data from '@/utils/data';

type Props = {
  notification: IBase,
  loading: boolean,
  dispatch: Function,
}

@connect(({ notification, loading }) => ({
  notification,
  loading: loading.models.notification,
}))

class ViewModal extends React.Component<Props> {
  componentDidMount() {
    console.log('zzz');
  }

  handleOk = () => {
    this.props.dispatch({
      type: 'notification/changeState',
      payload: {
        visible: false,
      }
    })
  }

  handleCancel = () => {
    this.props.dispatch({
      type: 'notification/changeState',
      payload: {
        visible: false,
      }
    })
  }

  onChange = pagination => {
    const { current: page, pageSize: limit } = pagination;
    const { record: { _id } } = this.props.notification;
    this.props.dispatch({
      type: 'notification/getById',
      payload: {
        _id,
        page,
        limit
      }
    })
  }

  render() {
    const { notification, loading } = this.props;
    let { record, danhSachGui, pagingModal: { page, limit }, visible, totalModal } = notification;
    const noiNhan = !!record.noiNhan ? record.noiNhan : '';
    const nguoiGui = !!record.nguoiGui ? record.nguoiGui.hoTen : '';

    danhSachGui = danhSachGui.map((item, index) => ({
      ...item,
      index: index + 1 + ((page - 1) * limit),
    }))

    const columns = [
      {
        title: 'STT',
        dataIndex: 'index',
        align: 'center',
      },
      {
        title: 'Họ tên',
        dataIndex: 'receiverID',
        align: 'center',
        render: val => _.get(val, 'hoTen', ''),
      },
      {
        title: 'Trạng thái',
        dataIndex: 'status',
        align: 'center',
        render: val => data.daDoc[+val],
      }
    ]
    return (
      <Modal
        title="Danh sách người nhận"
        visible={visible}
        destroyOnClose
        onOk={this.handleOk}
        onCancel={this.handleCancel}
        width="80%"
      >
        <Card bordered={false} bodyStyle={{ minHeight: 400 }}>
          <Table
            style={{ marginBottom: 10 }}
            loading={loading}
            bordered
            onChange={this.onChange}
            columns={columns}
            dataSource={danhSachGui}
            pagination={{
              position: 'top',
              total: totalModal,
              showSizeChanger: true,
              pageSizeOptions: ['10', '20', '30', '40'],
            }}
          />
        </Card>
      </Modal>
    );
  }

}

export default ViewModal;