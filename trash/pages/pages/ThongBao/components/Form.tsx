
import React from 'react';
import { Form, Input, Card, Row, Col, Button, Spin, Select, Divider } from 'antd';
import _ from 'lodash';
import { connect } from 'dva';
import { Format, toRegex, getRecordValue } from '@/utils/utils';
import rules from '@/utils/rules';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import { formItemLayout, tailFormItemLayout } from '@/utils/form';
import { IBase } from '@/utils/base';

const model = ['notification', 'Thông Báo'];

const choice = [
  {
    generalReceiver: 'Tất cả tài khoản',
    url: 'send-all',
  },
  {
    generalReceiver: 'Gửi theo vai trò',
    url: 'send-role',
  },
  {
    generalReceiver: 'Gửi theo khoa',
    url: 'send-khoa',
  },
  {
    generalReceiver: 'Gửi theo lớp',
    url: 'send-lop',
  },
  {
    generalReceiver: 'Gửi theo nhóm sinh viên',
    url: 'send-usergroup',
  },
  {
    generalReceiver: 'Gửi tới những sinh viên cụ thể',
    url: 'send-sinhvien',
  },
  {
    generalReceiver: 'Gửi tới những giảng viên cụ thể',
    url: 'send-giangvien',
  },
];

type Form_Props = {
  notification: IBase,
  khoa: IBase,
  lop: IBase,
  nhomnguoidung: IBase,
  sinhvien: IBase,
  giangvien: IBase,
  loading: boolean,
  dispatch: Function,
  form: { validateFieldsAndScroll, getFieldDecorator, setFieldsValue, getFieldValue },
  thongTinVeSinhVienDangDuocChonDeGuiThongBao: object
}

type Form_States = {
  value: number
}

@connect(({ nhomnguoidung, notification, khoa, lop, loading, sinhvien, giangvien }) => ({
  notification,
  khoa,
  lop,
  nhomnguoidung,
  loading: loading.models.notification || loading.models.lop || loading.models.sinhvien || loading.models.giangvien,
  sinhvien,
  giangvien,
}))
class ThongBaoForm extends React.Component<Form_Props, Form_States> {
  state = {
    url: '',
    generalReceiver: '',
    value: 0,
  };

  debouncedLop = _.debounce((val) => { this.handleSearchLop(val) }, 500);

  debouncedSinhVien = _.debounce((val) => { this.handleSearchSinhVien(val) }, 500);

  debouncedGiangVien = _.debounce((val) => { this.handleSearchGiangVien(val) }, 500);

  componentDidMount() {
    this.props.dispatch({
      type: 'khoa/get',
      payload: {
        page: 1,
        limit: 1000,
        cond: {},
      },
    });

    this.props.dispatch({
      type: 'nhomnguoidung/get',
      payload: {
        page: 1,
        limit: 1000,
        cond: {},
      },
    });

    this.props.dispatch({
      type: 'lop/get',
      payload: {
        page: 1,
        limit: 10,
        cond: {},
      },
    });

    this.props.dispatch({
      type: 'sinhvien/get',
      payload: {
        page: 1,
        limit: 10,
        cond: {
          vaiTro: 0
        },
      },
    });

    this.props.dispatch({
      type: 'giangvien/get',
      payload: {
        page: 1,
        limit: 10,
        cond: {
          vaiTro: 0
        },
      },
    });
  }

  handleSearchLop = value => {
    if (!value.length) return;
    const cond = { maLopHoc: toRegex(value) };
    this.props.dispatch({
      type: 'lop/get',
      payload: {
        cond,
        page: 1,
        limit: 10,
      },
    });
  };

  handleSearchSinhVien = value => {
    if (!value.length) return;
    const cond = { maSv: toRegex(value), vaiTro: 0 };
    this.props.dispatch({
      type: 'sinhvien/get',
      payload: {
        cond,
        page: 1,
        limit: 10,
      },
    });
  };

  handleSearchGiangVien = value => {
    if (!value.length) return;
    const cond = { maSv: toRegex(value), vaiTro: 1 };
    this.props.dispatch({
      type: 'giangvien/get',
      payload: {
        cond,
        page: 1,
        limit: 10,
      },
    });
  };

  onChange = value => {
    this.setState({
      value,
      ...choice[value],
    });
  };

  handleSubmit = e => {
    const { value, url } = this.state;
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (err) return;
      const { generalReceiver, url } = this.state;
      const { listMaSinhVien, listMaKhoa, listMaLop, listUserGroup, listMaGiangVien } = values;
      values = {
        ...values,
        listMaSinhVien: undefined,
        listMaKhoa: undefined,
        listMaLop: undefined,
        listUserGroup: undefined,
        listMaGiangVien: undefined,
      }
      values.url = url;
      values.generalReceiver = generalReceiver;
      switch (url) {
        case 'send-sinhvien':
          values.url = 'send-ma-sinh-vien';
          values.listMaSinhVien = listMaSinhVien;
          break;

        case 'send-giangvien':
          values.url = 'send-ma-sinh-vien';
          values.listMaSinhVien = listMaGiangVien;
          break;

        case 'send-khoa':
          values.listMaKhoa = listMaKhoa;
          break;
        case 'send-lop':
          values.listMaLop = listMaLop;
          break;
        case 'send-usergroup':
          values.listUserGroup = listUserGroup;
          break;
        default:
          break;
      }
      this.props.dispatch({
        type: `${model[0]}/send`,
        payload: {
          ...values,
        },
      });
    });
  };

  render() {
    const {
      notification: { record, edit },
      khoa: { danhSach: danhSachKhoa },
      lop: { danhSach: danhSachLop },
      loading,
      nhomnguoidung: { danhSach: danhSachNhomNguoiDung },
      sinhvien: { danhSach: danhSachSinhVien },
      giangvien: { danhSach: danhSachGiangVien },
    } = this.props;
    const { getFieldDecorator } = this.props.form;

    return (
      <div className="box">
        <Card
          bordered
          title={
            <div className="cardTitle">{!edit ? `Thêm ${model[1]} mới` : `Sửa ${model[1]}`}</div>
          }
        >
          <GridContent>
            <Row>
              <Col span={18}>
                <Spin spinning={!!loading}>
                  <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                    <Form.Item label="Tiêu đề">
                      {getFieldDecorator('title', {
                        initialValue: getRecordValue(model, {}, 'title', ''),
                        rules: [
                          ...rules.text,
                          ...rules.required
                        ],
                      })(<Input />)}
                    </Form.Item>
                    <Form.Item label="Nội dung">
                      {getFieldDecorator('content', {
                        initialValue: getRecordValue(model, {}, 'content', ''),
                        rules: [
                          ...rules.text,
                          ...rules.required
                        ],
                      })(<Input.TextArea />)}
                    </Form.Item>
                    <Form.Item label="Người nhận">
                      {getFieldDecorator('nguoiNhan', {
                        rules: [...rules.required],
                      })(
                        <Select
                          optionFilterProp="children"
                          showSearch
                          autoClearSearchValue={false}
                          onChange={this.onChange}
                        >
                          {choice.map((item, index) => (
                            <Select.Option value={index}>{`${item.generalReceiver}`}</Select.Option>
                          ))}
                        </Select>
                      )}
                    </Form.Item>
                    {this.state.url === 'send-role' && (
                      <Form.Item label="Vai trò người dùng">
                        {getFieldDecorator('roles', {
                          initialValue: [],
                          rules: [...rules.required],
                        })(
                          <Select
                            mode='multiple'
                          >
                            {['Sinh viên', 'Giáo viên', 'Phụ huynh'].map((item, index) => (
                              <Select.Option value={index}>{`${item}`} </Select.Option>
                            ))}
                          </Select>
                        )}
                      </Form.Item>
                    )}
                    {this.state.url === 'send-khoa' && (
                      <Form.Item label="Khoa">
                        {getFieldDecorator('listMaKhoa', {
                          initialValue: [],
                          rules: [...rules.required],
                        })(
                          <Select
                            optionFilterProp="children"
                            showSearch
                            filterOption={(input, option) =>
                              Format(option.props.children).indexOf(Format(input)) >= 0
                            }
                            mode='multiple'
                            placeholder='Tìm kiếm theo tên khoa'
                          >
                            {danhSachKhoa.map(item => (
                              <Select.Option value={item.maKhoa}>{`${item.tenKhoa}`} </Select.Option>
                            ))}
                          </Select>
                        )}
                      </Form.Item>
                    )}


                    {this.state.url === 'send-usergroup' && (
                      <Form.Item label="Chọn nhóm sinh viên">
                        {getFieldDecorator('listUserGroup', {
                          initialValue: [],
                          rules: [...rules.required],
                        })(
                          <Select
                            showSearch
                            placeholder='Tìm kiếm'
                            filterOption={(input, option) =>
                              Format(option.props.children).indexOf(Format(input)) >= 0
                            }
                            mode='multiple'
                          >
                            {danhSachNhomNguoiDung.map(item => (
                              <Select.Option value={item._id}>{`${item.tenNhom}`} </Select.Option>
                            ))}
                          </Select>
                        )}
                      </Form.Item>
                    )}


                    {this.state.url === 'send-sinhvien' && (
                      <Form.Item label="Danh sách sinh viên">
                        {getFieldDecorator('listMaSinhVien', {
                          initialValue: [],
                          rules: [...rules.required],
                        })(
                          <Select
                            showSearch
                            onSearch={this.debouncedSinhVien}
                            mode='multiple'
                            placeholder='Tìm kiếm theo mã sinh viên'
                          >
                            {danhSachSinhVien.map(item => (
                              <Select.Option value={item.maSv}>{`${item.maSv}-${item.hoTen}`} </Select.Option>
                            ))}
                          </Select>
                        )}
                      </Form.Item>
                    )}

                    {this.state.url === 'send-lop' && (
                      <Form.Item label="Lớp">
                        {getFieldDecorator('listMaLop', {
                          initialValue: [],
                          rules: [...rules.required],
                        })(
                          <Select
                            showSearch
                            onSearch={this.debouncedLop}
                            mode='multiple'
                            placeholder='Tìm kiếm theo mã lớp'
                            filterOption={true}
                            key={this.state.url}
                          >
                            {danhSachLop.map(item => (
                              <Select.Option value={item.maLopHoc}>{`${item.maLopHoc}-${_.get(item, 'monHoc.tenMonHoc', '')}`} </Select.Option>
                            ))}
                          </Select>
                        )}
                      </Form.Item>
                    )}

                    {this.state.url === 'send-giangvien' && (
                      <Form.Item label="Danh sách giảng viên">
                        {getFieldDecorator('listMaGiangVien', {
                          initialValue: [],
                          rules: [...rules.required],
                        })(
                          <Select
                            showSearch
                            mode='multiple'
                            onSearch={this.debouncedGiangVien}
                            placeholder='Tìm kiếm theo mã giảng viên'
                          >
                            {danhSachGiangVien.map(item => (
                              <Select.Option value={item.maSv}>{`${item.maSv}-${item.hoTen}`} </Select.Option>
                            ))}
                          </Select>
                        )}
                      </Form.Item>
                    )}
                    <Form.Item {...tailFormItemLayout}>
                      <Button.Group>
                        <Button type="primary" icon="plus" htmlType="submit">
                          {edit ? 'Cập nhật' : 'Thêm mới'}
                        </Button>
                      </Button.Group>
                    </Form.Item>
                  </Form>
                </Spin>
              </Col>
            </Row>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedQuanTriBaiVietForm = Form.create({ name: 'ThongBaoForm' })(ThongBaoForm);

export default WrappedQuanTriBaiVietForm;

