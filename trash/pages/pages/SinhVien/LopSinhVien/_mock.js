import _ from 'lodash';
import uuidv1 from 'uuid/v1';

let data = [
  {
    tenMonHoc: 'Môn 1',
    tenLopHoc: 'Lớp 1',
    phongHoc: 'Phòng 1',
    tgBatDau: 'Tue, 24 Mar 2020 09:09:43 GMT',
    giangVienDay: 'GV 1',
    _id: 'abcd'
  },
  {
    tenMonHoc: 'Môn 2',
    tenLopHoc: 'Lớp 2',
    phongHoc: 'Phòng 2',
    tgBatDau: 'Tue, 24 Mar 2020 09:09:43 GMT',
    giangVienDay: 'GV 2',
    _id: 'abce'
  },
  {
    tenMonHoc: 'Môn 3',
    tenLopHoc: 'Lớp 3',
    phongHoc: 'Phòng 3',
    tgBatDau: 'Tue, 24 Mar 2020 09:09:43 GMT',
    giangVienDay: 'GV 3',
    _id: 'abcf'
  },
  {
    tenMonHoc: 'Môn 4',
    tenLopHoc: 'Lớp 4',
    phongHoc: 'Phòng 4',
    tgBatDau: 'Tue, 24 Mar 2020 09:09:43 GMT',
    giangVienDay: 'GV 4',
    _id: 'abcg'
  },
]

export default {
  // GET
  'GET /api/lopsinhvien': (req, res) => {
    res.status(200).send({
      data: _.reverse(_.clone(data)),
      total: data.length,
    });
  },
  'DELETE /api/lopsinhvien/:_id': (req, res) => {
    const { _id } = req.params;
    data = data.filter(item => item._id !== _id);
    res.status(200).send({
      message: 'OK'
    });
  },
  'POST /api/lopsinhvien': (req, res) => {
    data.push({
      ...req.body,
      _id: uuidv1()
    })
    res.status(200).send({
      message: 'OK'
    });
  },
  'PUT /api/lopsinhvien/:_id': (req, res) => {
    const { _id } = req.params;
    data = data.map(item => {
      if (item._id !== _id) { return item; }
      return {
        ...item,
        ...req.body,
      }
    })
    res.status(200).send({
      message: 'OK'
    });
  },
}