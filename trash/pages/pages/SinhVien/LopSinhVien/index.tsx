import React from 'react';
import { connect } from 'dva';
import uuidv1 from 'uuid/v1';
import { Button, Divider, Popconfirm } from 'antd';
import { IColumn } from '@/utils/interfaces';
import Form from './components/Form';
import TableBase from '@/components/Table/Table.tsx';
import { IBase } from '@/utils/base';
import moment from 'moment';
import mock from './_mock';

type Props = {
  lopsinhvien: IBase,
  loading: boolean,
  dispatch: Function,
}

@connect(({ lopsinhvien, loading }) => ({
  lopsinhvien,
  loading: loading.models.lopsinhvien,
}))
class LopSinhVien extends React.Component<Props> {
  state = {
  };

  componentDidMount() {
    this.props.dispatch({
      type: 'lopsinhvien/get'
    })
  }

  handleEdit = record => {
    this.props.dispatch({
      type: 'lopsinhvien/changeState',
      payload: {
        showDrawer: true,
        edit: true,
        record,
        key: uuidv1(),
        isTouched: false,
      },
    });
  };

  handleDel = _id => {
    this.props.dispatch({
      type: 'lopsinhvien/del',
      payload: {
        _id,
        key: uuidv1(),
      },
    });
  };

  render() {
    const renderLast = (value, record) => (
      <React.Fragment>
        <Button
          type="primary"
          shape="circle"
          icon="edit"
          onClick={() => this.handleEdit(record)}
          title="Chỉnh sửa"
        />

        <Divider type="vertical" />

        <Popconfirm title="Bạn có chắc muốn xóa?" onConfirm={() => this.handleDel(record._id)}>
          <Button type="danger" shape="circle" icon="delete" title="Xóa" />
        </Popconfirm>

      </React.Fragment>
    )

    const columns: IColumn[] = [
      {
        title: 'STT',
        dataIndex: 'index',
        align: 'center',
        width: '6%',
      },
      {
        title: 'Tên lớp học',
        dataIndex: 'tenLopHoc',
        align: 'center',
        width: '15%',
        search: 'search',
      },
      {
        title: 'Môn học',
        dataIndex: 'tenMonHoc',
        align: 'center',
        width: '15%',
        search: 'search',
      },
      {
        title: 'Phòng học',
        dataIndex: 'phongHoc',
        align: 'center',
        width: '15%',
        search: 'search',
      },
      {
        title: 'Thời gian bắt đầu',
        dataIndex: 'tgBatDau',
        align: 'center',
        width: '15%',
        render: val => <span title={moment(val).format('DD/MM/YYYY HH:mm:ss')}>{moment(val).format('DD/MM/YYYY')}</span>,
        search: 'sort',
      },
      {
        title: 'Giảng viên dạy',
        dataIndex: 'giangVienDay',
        align: 'center',
        // width: '30%',
        search: 'search',
      },
      {
        title: 'Thao tác',
        align: 'center',
        render: (value, record) => renderLast(value, record),
        fixed: 'right',
        width: 200,
      }
    ];

    return (
      <TableBase
        model={this.props.lopsinhvien}
        modelName="lopsinhvien"
        loading={this.props.loading}
        dispatch={this.props.dispatch}
        columns={columns}
        cond={{}}
        Form={Form}
        title={'Danh sách lớp học'}
        hasCreate
        tableProp={{
          scroll: { x: 1000 }
        }}
      />
    );
  }
}

export default LopSinhVien;
