/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { connect } from 'dva';
import { Form, Input, Card, Row, Col, Button, DatePicker, Select, Spin } from 'antd';
import _ from 'lodash';
import { getRecordValue } from '@/utils/utils';
import rules from '@/utils/rules';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import { formItemLayout, tailFormItemLayout, handleValuesChange, handleCommonSubmit, getSubmitText } from '@/utils/form';
import moment from 'moment';

type LopSinhVien_Props = {
  model: { edit, record },
  cond: object,
  loading: boolean,
  dispatch: Function,
  form: { validateFieldsAndScroll, getFieldDecorator },
}

@connect(({ lopsinhvien, loading }) => ({
  lopsinhvien,
  loading: loading.models.lopsinhvien
}))
class LopSinhVien extends React.Component<LopSinhVien_Props> {
  componentDidMount() {
    this.props.dispatch({
      type: 'lopsinhvien/type'
    })
  }

  handleSubmit = e => {
    // const { form, model: { edit, record }, cond } = this.props;    //gốc
    const {
      form,
      model: { edit, record, isTouched },
      cond,
      modelName,
      dispatch
    } = this.props;
    e.preventDefault();
    form.validateFieldsAndScroll((err, values) => {
      if (err) return;
      // handleCommonSubmit(values, this.props);          gốc
      if (edit) {
        if (isTouched) { // cập nhật
          dispatch({
            type: `${modelName}/update`,
            payload: {
              ...values,
              _id: record._id,
            },
          });
        } else { // không cập nhật
          dispatch({
            type: `${modelName}/changeState`,
            payload: {
              showDrawer: false,
            },
          });
        }
      } else {
        dispatch({
          type: `${modelName}/add`,
          payload: {
            ...cond,
            ...values,
          },
        });
      }
    });
  };

  render() {
    const { model, form: { getFieldDecorator }, cond, loading } = this.props;
    return (
      <div className="box">
        <Card
          bordered
          title={
            <div className="cardTitle">{!model.edit ? `Thêm mới` : `Chỉnh sửa`} lớp học</div>
          }
        >
          <GridContent>
            <Row>
              <Col span={18}>
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>

                  <Spin spinning={loading}>
                    <Form.Item {...tailFormItemLayout}>
                      <Form.Item label="Tên môn học">
                        {getFieldDecorator('tenLopHoc', {
                          initialValue: model.edit ? _.get(model.record, 'tenLopHoc', '') : '',
                          // rules: [...rules.text, ...rules.required],
                        })(<Input />)}
                      </Form.Item>
                      <Row>
                      <Form.Item label="Môn học">
                        {getFieldDecorator('tenMonHoc', {
                          initialValue: model.edit ? _.get(model.record, 'tenMonHoc', '') : '',
                          // rules: [...rules.text, ...rules.required],
                        })(<Input />)}
                      </Form.Item>
                      </Row>
                      <Form.Item label="Phòng học">
                        {getFieldDecorator('phongHoc', {
                          initialValue: model.edit ? _.get(model.record, 'phongHoc', '') : '',
                          // rules: [...rules.text, ...rules.required],
                        })(<Input />)}
                      </Form.Item>
                      <Form.Item label="Thời gian bắt đầu">
                        {getFieldDecorator('tgBatDau', {
                          initialValue: model.edit ? moment(_.get(model.record, 'tgBatDau', moment())) : moment(new Date()),
                          // rules: [
                          //   {
                          //     validator: (_, value, callback) => {
                          //       if (moment(value).isAfter(moment(timeEnd).subtract(1, 'seconds'))) callback('');
                          //       callback();
                          //     },
                          //     message: 'Không được sớm hơn thời điểm kết thúc'
                          //   }
                          // ],
                        })(<DatePicker showTime={{ format: 'HH:mm' }} format="DD/MM/YYYY HH:mm" />)}
                      </Form.Item>
                      <Form.Item label="Giảng viên dạy">
                        {getFieldDecorator('giangVienDay', {
                          initialValue: model.edit ? _.get(model.record, 'giangvienDay', '') : '',
                          // rules: [...rules.text, ...rules.required],
                        })(<Input />)}
                      </Form.Item>
                      <Button.Group>
                        <Button type="primary" icon="plus" htmlType="submit">
                          {getSubmitText(model)}
                        </Button>
                      </Button.Group>
                    </Form.Item>
                  </Spin>
                </Form>
              </Col>
            </Row>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedForm = Form.create({ name: 'LopSinhVien', onValuesChange: handleValuesChange })(LopSinhVien);

export default WrappedForm;
