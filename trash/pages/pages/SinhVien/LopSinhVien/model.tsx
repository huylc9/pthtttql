import base from '@/utils/base';
import modelExtend from 'dva-model-extend';
import Services from '@/pages/SinhVien/LopSinhVien/service';
import notificationAlert from '@/components/Notification';
import { formatMessage } from 'umi/locale';

// export default modelExtend(base(Services), {
//     namespace: 'lopsinhvien',
//     effects: {
//         *getMock({ payload }, { call, put }){
//             console.log(1, "mockJS");
//             const response = yield call(mock['GET /api/lopsinhvien'], new Response());
//             console.log(response, "mockJS");
//         }
//     }
// })


const initialState = {
    danhSach: [],
    edit: false,
    record: {},
    showDrawer: false,
    paging: {
      page: 1,
      limit: 10,
      cond: {},
    },
    filterInfo: {},
    total: 0,
    isTouched: false,
};
  

export default {
    namespace: 'lopsinhvien',
    state: initialState,
    effects: {
        *get({ payload }, { call, put }){
            console.log(1, "mockJS");
            const response = yield call(Services.getLop);
            console.log(response.data, "mockJS");
            yield put({
                type: 'changeState',
                payload:{
                    danhSach: response.data.data,
                    total: response.data.total
                }
            })
        },
        *del({ payload }, { call, put, select }) {
          let { paging: { page, limit, cond }, total } = state;
          let currentPage = (total % limit == 1 && page == Math.ceil(total / limit) && page > 1) ? page - 1 : page;
          yield call(Services.del, payload);
          notificationAlert('success', formatMessage({ id: 'XOA_THANH_CONG' }));
          yield put({ type: 'get', payload: { page: currentPage, limit, cond } });
        },
        *update({ payload }, { call, put }){
          yield call(Services.update, payload);
          notificationAlert('success', formatMessage({ id: 'SUA_THANH_CONG' }));
          yield put({ type: 'get' });
          yield put({ type: 'changeState', payload: { showDrawer: false } });
        },
        *add({ payload }, { call, put }){
          yield call(Services.add, payload);
          notificationAlert('success', formatMessage({ id: 'THEM_THANH_CONG' }));
          yield put({ type: 'get' });
          yield put({ type: 'changeState', payload: { showDrawer: false } });
        }
    },
    reducers: {
        changeState(state, { payload }) {
          return {
            ...state,
            ...payload,
          };
        },
      },
}
