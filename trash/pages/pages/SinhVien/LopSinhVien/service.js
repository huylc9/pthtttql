import Service from '@/services/BaseService';
import axios from '@/utils/axios.js';

class ExtendedService extends Service {
    getLop = () => axios.get(`http://localhost:8000/api/lopsinhvien`);
    update = payload => axios.post(`http://localhost:8000/api/lopsinhvien`, payload);
    add = paydload => axios.post(`http://localhost:8000/api/lopsinhvien`, payload);
    del = paydload => axios.delete(`http://localhost:8000/api/lopsinhvien`, payload);
}

export default new ExtendedService({ name: 'lopsinhvien', formData: false });
