import base from '@/utils/base';
import modelExtend from 'dva-model-extend';
import Services from '@/pages/SinhVien/KhaiBao/service';

export default modelExtend(base(Services), {
    namespace: 'khaibao',
})
