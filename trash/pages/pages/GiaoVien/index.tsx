import TableBase from '@/components/Table/Table';
import { connect } from 'dva';
import React from 'react';
import Form from './components/Form';

@connect(({ monhoc, khoa }) => ({
  monhoc,
  khoa,
}))
class GiaoVien extends React.Component {
  public state = {
  };

  public componentDidMount() {
  }

  public render() {
    const columns = [
      {
        title: 'STT',
        dataIndex: 'index',
        align: 'center',
        width: '150px',
      },
    ];

    const actions = record => [];

    return (
      <TableBase
        model="giaovien"
        columns={columns}
        scroll={{ x: 1000, y: 600 }}
        cond={{}}
        actions={actions}
        Form={Form}
        // title={''}
        hasEdit={true}
        hasDelete={true}
        hasCreate={true}
        hasSearch={true}
      />
    );
  }
}

export default GiaoVien;
