import _ from 'lodash.get';
import uuidv1 from 'uuid/v1';

let data = [
  {
    hoTen: 'User 1',
    _id: 'abc'
  },
  {
    hoTen: 'User 2',
    _id: 'abcd'
  },
  {
    hoTen: 'User 3',
    _id: 'abcde'
  },
  {
    hoTen: 'User 4',
    _id: 'abcvca'
  },
]

export default {
  // GET
  'GET /api/giaovien': (req, res) => {
    res.status(200).send({
      data: _.reverse(_.clone(data)),
      total: data.length,
    });
  },
  'DELETE /api/giaovien/:_id': (req, res) => {
    const { _id } = req.params;
    data = data.filter(item => item._id !== _id);
    res.status(200).send({
      message: 'OK'
    });
  },
  'POST /api/giaovien': (req, res) => {
    const { hoTen } = req.body;
    data.push({
      hoTen,
      _id: uuidv1()
    })
    res.status(200).send({
      message: 'OK'
    });
  },
  'PUT /api/giaovien/:_id': (req, res) => {
    const { _id } = req.params;
    const { hoTen } = req.body;
    data = data.map(item => {
      if (item._id !== _id) { return item; }
      return {
        ...item,
        hoTen
      }
    })
    res.status(200).send({
      message: 'OK'
    });
  },
}