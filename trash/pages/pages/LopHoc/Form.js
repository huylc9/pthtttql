import React from 'react';
import { Form, Input, Card, Row, Col, Button, DatePicker, Select, Spin } from 'antd';
import _ from 'lodash';
import { connect } from 'dva';
import moment from 'moment';
import { Format, chuanHoa, includes } from '@/utils/utils';
import rules from '@/utils/rules';
import Hint from '@/components/Hint/Hint';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import { renderSelect, renderGroup, formItemLayout, tailFormItemLayout } from '@/utils/form';
import UploadAvatar from '@/components/Upload/UploadAvatar';

@connect(({ giangvien, khoa, loading, lopgv }) => ({
  giangvien, khoa,
  loading: loading.models.lopgv,
  lopgv
}))
class GiaoVienForm extends React.Component {

  debounced = _.debounce((val) => { this.handleSearch(val) }, 500);

  componentDidMount() {
    this.props.dispatch({
      type: 'lopgv/getLopDeChon',
      payload: {
        keyword: '',
        page: 1,
        limit: 10
      }
    })
  }

  handleSearch = (val) => {
    this.props.dispatch({
      type: 'lopgv/getLopDeChon',
      payload: {
        keyword: val,
        page: 1,
        limit: 10
      }
    })
  }

  render() {
    const { lopgv: { lopDeChon }, form: { getFieldDecorator } } = this.props;



    const handleSubmit = e => {
      e.preventDefault();
      this.props.form.validateFieldsAndScroll((err, values) => {
        if (err) return;
        this.props.dispatch({
          type: 'lopgv/add',
          payload: {
            ...values,
          },
        });
      });
    };



    return (
      <div className="box">
        <Card
          bordered
          title={<div className="cardTitle">{'Thêm lớp giảng dạy'}</div>}
        >
          <GridContent>
            <Row>
              <Col span={18}>
                <Form {...formItemLayout} onSubmit={handleSubmit}>

                  <Form.Item label="Lớp học">
                    {getFieldDecorator('maLopHoc', {
                      rules: [...rules.required]
                    })(
                      <Select showSearch onSearch={this.debounced}>
                        {lopDeChon.map((item, index) => (
                          <Select.Option key={index} value={item.maLopHoc}>
                            {item.maLopHoc + '-' + _.get(item, 'monHoc.tenMonHoc')}
                          </Select.Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>

                  <Spin spinning={this.props.loading}>
                    <Form.Item {...tailFormItemLayout}>
                      <Button.Group>
                        <Button type="primary" icon="plus" htmlType="submit">
                          Thêm mới
                        </Button>
                      </Button.Group>
                    </Form.Item>
                  </Spin>
                </Form>
              </Col>
            </Row>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedQuanTriBaiVietForm = Form.create({ name: 'GiaoVienForm' })(GiaoVienForm);

export default WrappedQuanTriBaiVietForm;
