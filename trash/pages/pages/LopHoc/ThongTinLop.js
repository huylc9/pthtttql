/* eslint-disable react/destructuring-assignment */
import { Component } from 'react';
import {
  Breadcrumb,
  Button,
  Card,
  Spin,
  Typography,
  List,
  Divider,
  Row,
  Col,
  Avatar,
  Upload,
  Icon,
  Popconfirm,
  Table,
  Input,
  Tag,
} from 'antd';
import { connect } from 'dva';
import queryString from 'query-string';
import uuidv1 from 'uuid/v1';
import { router } from 'umi';
import _ from 'lodash';
import moment from 'moment';
import { isValue, Format, includes } from '@/utils/utils';
import InputSearch from '@/components/InputSearch/InputSearch';

const { Text } = Typography;

const Nhi = uuidv1();

@connect(({ loading, lopgv }) => ({
  loading: loading.models.lopgv,
  lopgv,
}))
class DanhSachTaiLieu extends Component {
  state = {
    searchText: '',
  };

  componentDidMount() {
    const { location } = this.props;
    const parsed = queryString.parse(location.search);
    const { idLop, maLopHoc } = parsed;
    if (idLop) {
      this.props.dispatch({
        type: 'lopgv/getById',
        payload: {
          _id: idLop,
        },
      });
      this.props.dispatch({
        type: 'lopgv/getDiemDanh',
        payload: {
          maLopHoc,
        },
      });
    }
    this.goBack = this.goBack.bind(this);
  }

  handleSearch = value => {
    this.setState({
      searchText: value,
    });
  };

  renderTitle = (index, thoiGianDiemDanh) =>
    `Buổi ${index + 1} (${moment(thoiGianDiemDanh).format('DD/MM')})`;

  goBack() {
    router.push('/DanhSachLop');
  }

  getSearch = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder="Tìm kiếm"
          value={selectedKeys[0]} //  || selectedKeys[0]
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => confirm()}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Button
          type="primary"
          onClick={() => {
            confirm();
          }}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Tìm
        </Button>
        <Button
          onClick={() => {
            clearFilters();
          }}
          size="small"
          style={{ width: 90 }}
        >
          Xóa
        </Button>
      </div>
    ),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    onFilter: (value, record) =>
      _.get(record, dataIndex, undefined) &&
      Format(_.get(record, dataIndex, undefined)).includes(Format(value)),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
  });

  render() {
    let {
      loading,
      location,
      lopgv: {
        record: { sinhVien, danhSachGiangVien: giaoVien, tenLopHoc },
        thongTinDiemDanh,
      },
    } = this.props;
    sinhVien = sinhVien || [];
    giaoVien = giaoVien || [];
    tenLopHoc = tenLopHoc || '';
    const dataDiemDanh = {};
    const mangDiemDanh = {};

    thongTinDiemDanh.map(({ danhSachSinhVien, thoiGianDiemDanh }) => {
      danhSachSinhVien.map(({ maSv: _id, trangThai }) => {
        if (!isValue(dataDiemDanh[_id])) {
          dataDiemDanh[_id] = 0;
          mangDiemDanh[_id] = [];
        }

        dataDiemDanh[_id] += !!trangThai;
        mangDiemDanh[_id].push({ trangThai, thoiGianDiemDanh });
      });
    });

    const columns = [
      {
        title: 'STT',
        dataIndex: 'index',
        align: 'center',
        width: '100px',
      },
      {
        title: 'Họ tên',
        dataIndex: 'hoTen',
        align: 'center',
        width: '200px',
      },
      {
        title: 'Số buổi vắng',
        dataIndex: 'vang',
        align: 'center',
        width: '150px',
        render: value => `${value}/${thongTinDiemDanh.length}`,
      },
    ];
    // hocSinh[0].buoiDiemDanh = [1, 1, 1, 1];

    // Đoạn này là tạo 1 key buoiDiemDanh: array các buổi đi
    //     if (hocSinh && hocSinh[0]) hocSinh[0].buoiDiemDanh = [1, 1, 1, 1];
    sinhVien = (sinhVien || []).map((item, index) => ({
      ...item,
      index: index + 1,
      vang: dataDiemDanh[item.maSv] || 0,
      buoiDiemDanh: mangDiemDanh[item.maSv],
    }));
    // Tạo cột ứng với các buổi
    if (sinhVien && sinhVien[0] && sinhVien[0].buoiDiemDanh) {
      sinhVien[0].buoiDiemDanh.map(({ thoiGianDiemDanh }, index) => {
        columns.push({
          // eslint-disable-next-line no-template-curly-in-string
          title: this.renderTitle(index, thoiGianDiemDanh),
          dataIndex: `buoi[${index}]`,
          align: 'center',
          width: '100px',
          render: val =>
            ['Đi', <Tag color="yellow">Vắng phép</Tag>, <Tag color="red">Vắng k phép</Tag>][val],
        });
      });
    }
    // Tạo data ưng với mỗi cột
    let hocSinhDaXuLyDuLieu = (sinhVien || []).map((item, index) => {
      const objBuoiDiemDanh = {};
      (mangDiemDanh[item.maSv] || []).map(({ trangThai: val }, i) => {
        objBuoiDiemDanh[`buoi[${i}]`] = val;
      });

      return {
        ...item,
        ...objBuoiDiemDanh,
      };
    });
    hocSinhDaXuLyDuLieu = hocSinhDaXuLyDuLieu.filter(({ hoTen }) =>
      includes(hoTen, this.state.searchText),
    );
    return (
      <Spin spinning={false}>
        <Card
          bordered
          title={
            <Breadcrumb separator=">">
              <Breadcrumb.Item
                onClick={() => router.push('/DanhSachLop')}
                onMouseOver={evt => (evt.target.style.cursor = 'pointer')}
              >
                <a>
                  <Typography.Text underline>Danh sách lớp</Typography.Text>
                </a>
              </Breadcrumb.Item>
              <Breadcrumb.Item>{tenLopHoc || 'Chưa cập nhật'}</Breadcrumb.Item>
            </Breadcrumb>
          }
        >
          <Card
            bordered
            title={
              <div>
                <span onClick={this.goBack}>
                  <a>
                    <Icon type="arrow-left" style={{ fontSize: 18 }} title="Trở về" />
                  </a>
                </span>
                <Divider type="vertical" />
                <span style={{ fontSize: 16, textTransform: 'capitalize', fontWeight: 550 }}>
                  {' '}
                  Thông tin lớp
                </span>
              </div>
            }
          >
            <Spin spinning={loading}>
              <InputSearch
                handleSearch={this.handleSearch}
                placeholder="Tìm kiếm theo tên sinh viên"
              />
              <Table
                bordered
                dataSource={hocSinhDaXuLyDuLieu}
                columns={columns}
                scroll={{ x: 900 }}
                pagination={{
                  showSizeChanger: true,
                  pageSizeOptions: ['10', '25', '50', '100'],
                  position: 'bottom',
                  defaultPageSize: 25,
                }}
              />
            </Spin>
          </Card>
        </Card>
      </Spin>
    );
  }
}

export default DanhSachTaiLieu;
