/* eslint-disable react/destructuring-assignment */
import React from 'react';
import Form from './Form';
import TableBase from '@/components/Table/Table';

class Manager extends React.Component {
  render() {
    const columns = [
      {
        title: 'STT',
        dataIndex: 'index',
        align: 'center',
        width: '150px',
      },
      {
        title: 'Tên đơn vị',
        dataIndex: 'donVi',
        align: 'center',
        width: '300px',
      },
      {
        title: 'Loại đơn vị',
        dataIndex: 'loai',
        align: 'center',
        width: '300px',
      },
    ];

    const actions = record => [];

    return (
      <TableBase
        model="khoa"
        columns={columns}
        scroll={{ x: 1000, y: 600 }}
        cond={{}}
        actions={actions}
        Form={Form}
        firstAtt="donVi"
        title="Quản lý đơn vị"
        hasEdit
        hasDelete
      />
    );
  }
}

export default Manager;
