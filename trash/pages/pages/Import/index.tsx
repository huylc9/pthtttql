/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { connect } from 'dva';
import { Collapse, Card, Button, Divider, } from 'antd';
import UploadFile from './components/UploadForImport';
import { IBase } from '@/utils/base';

const { Panel } = Collapse;

const url = [
  'https://api.sotayliencap.com/data/Mau_Thoi_Khoa_Bieu.xlsx',
]

type Import_Props = {
  notification: IBase
}

@connect(({ notification }) => ({
  notification,
}))
class ThongBaoManager extends React.Component<Import_Props> {

  download = (link) => {
    window.open(link, '_blank');
  }

  onUpload = (data) => {
  }

  render() {
    return (
      <Card bordered title={<div className="cardTitle">Import dữ liệu và các biểu mẫu</div>}>
        <Collapse defaultActiveKey={['1']}>
          <Panel header="Tải các biểu mẫu" key="1">
            <Button onClick={() => this.download(url[0])}> Mẫu thời khóa biểu </Button>
            <Divider type='vertical' />
            <Button onClick={() => this.download(url[1])}> Mẫu danh sách học sinh </Button>

          </Panel>
          <Panel header="Import dữ liệu" key="2">
            <UploadFile title='Import thời khóa biểu' onChange={this.onUpload} />
          </Panel>
        </Collapse>
      </Card>
    );
  }
}

export default ThongBaoManager;
