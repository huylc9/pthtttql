/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { connect } from 'dva';
import { Select, Upload } from 'antd';
import uuidv1 from 'uuid/v1';
import { formatMessage } from 'umi/locale';
import _ from 'lodash';
import Form from './Form';
import TableBase from '@/components/Table/Table';
import { renderParagraph } from '@/utils/table';

let key = uuidv1();

@connect(({ loading, lopgv, login }) => ({
  loading: loading.models.lopgv,
  lopgv,
  login,
}))
class QuanLyTaiLieu extends React.Component {

  state = {
    mon: undefined,
  };

  componentDidMount() {
    this.props.dispatch({
      type: 'lopgv/get',
      payload: {
        page: 1,
        limit: 10000,
        cond: {}
      }
    });
  }

  onSelectMon = _id => {
    key = uuidv1();
    this.setState({
      mon: _id,
    });
  };

  render() {
    const {
      lopgv: { danhSachLopGv: danhSach },
      login: { _id },
    } = this.props;
    const columns = [
      {
        title: 'STT',
        dataIndex: 'index',
        align: 'center',
        width: '150px',
      },
      {
        title: 'Tên tài liệu',
        dataIndex: 'tenTaiLieu',
        align: 'center',
        width: '300px',
      },
      {
        title: 'Môn học',
        dataIndex: 'monHoc',
        align: 'center',
        width: '300px',
        render: item => _.get(item, 'tenMonHoc', ''),
      },
      {
        title: 'Tài liệu đính kèm',
        dataIndex: 'taiLieuDinhKem',
        align: 'center',
        width: '300px',
        render: url => {
          return (
            <div
              style={{
                wordWrap: 'break-word',
                'word-break': 'break-all',
                'max-width': '300px',
              }}
            >
              <Upload
                fileList={[
                  {
                    uid: url,
                    name: url
                      .split('/')
                      .pop()
                      .substring(26),
                    status: 'done',
                    url,
                    key: uuidv1(),
                  },
                ]}
                disabled
              />
            </div>
          );
        }
      },
      {
        title: 'Mô tả',
        dataIndex: 'moTa',
        align: 'center',
        // width: '300px',
        render: val => renderParagraph(val, 3)
      },

    ];

    const setMonHoc = new Set();
    const mapMonHoc = {};
    danhSach.map(({ monHoc: { _id: id, tenMonHoc } }) => {
      setMonHoc.add(id);
      mapMonHoc[id] = tenMonHoc;
    });

    const dataSource = Array.from(setMonHoc);
    const actions = record => [];

    return (
      <TableBase
        key={key}
        model="tailieu"
        columns={columns}
        scroll={{ x: 1600, y: 600 }}
        cond={{ monHoc: this.state.mon }}
        actions={actions}
        Form={Form}
        firstAtt="tenTaiLieu"
        title={
          <Select
            value={this.state.mon}
            placeholder="Hãy chọn môn học"
            onChange={this.onSelectMon}
            style={{ width: 200 }}
            allowClear
          >
            {dataSource.map((id, index) => (
              <Select.Option key={index} value={id}>
                {mapMonHoc[id]}
              </Select.Option>
            ))}
          </Select>
        }
        hasEdit
        hasDelete
        hasCreate
        hasSearch
      />
    );
  }
}

export default QuanLyTaiLieu;
