/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Form, Input, Card, Row, Col, Button, DatePicker, Select } from 'antd';
import _ from 'lodash';
import { connect } from 'dva';
import moment from 'moment';
import { Format, chuanHoa, includes, getNameFile, getRecordValue } from '@/utils/utils';
import rules from '@/utils/rules';
import Hint from '@/components/Hint/Hint';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import { renderSelect, renderGroup, formItemLayout, tailFormItemLayout } from '@/utils/form';
import UploadOne from '@/components/Upload/UploadOne';

const { TextArea } = Input;

@connect(({ tailieu, lop, login, thoikhoabieu }) => ({
  tailieu,
  lop,
  login,
  thoikhoabieu,
}))
class GiaoVienForm extends React.Component {
  componentDidMount() {
    this.props.dispatch({
      type: 'monhoc/getLopGv',
      payload: {
        page: 1,
        limit: 1000,
        cond: {},
      },
    });
  }

  render() {
    const {
      tailieu,
      lop: { danhSachLopGv: danhSach },
      login: { _id: idNguoiDang },
      cond,
    } = this.props;
    const { record, edit } = tailieu;
    const setMonHoc = new Set();
    const mapMonHoc = {};
    danhSach.map(({ monHoc: { _id: id, tenMonHoc } }) => {
      setMonHoc.add(id);
      mapMonHoc[id] = tenMonHoc;
    });

    const dataSource = Array.from(setMonHoc);

    const { getFieldDecorator } = this.props.form;
    const handleSubmit = e => {
      e.preventDefault();
      this.props.form.validateFieldsAndScroll((err, values) => {
        if (err) return;
        values.taiLieuDinhKem = _.get(
          values,
          'taiLieuDinhKem.fileList[0].originFileObj',
          undefined
        );
        if (edit) {
          this.props.dispatch({
            type: 'tailieu/upd',
            payload: {
              ...values,
              _id: _.get(record, '_id', -1),
            },
          });
        } else {
          this.props.dispatch({
            type: 'tailieu/add',
            payload: {
              ...values,
              idNguoiDang,
            },
          });
        }
      });
    };
    return (
      <div className="box">
        <Card
          bordered
          title={<div className="cardTitle">{!edit ? 'Thêm tài liệu mới' : 'Sửa tài liệu'}</div>}
        >
          <GridContent>
            <Row>
              <Col span={18}>
                <Form {...formItemLayout} onSubmit={handleSubmit}>
                  <Form.Item label="Môn học">
                    {getFieldDecorator('monHoc', {
                      rules: [...rules.required],
                      initialValue: getRecordValue(tailieu, cond, 'monHoc', ''),
                    })(
                      <Select
                        showSearch
                        filterOption={(value, option) => includes(option.props.children, value)}
                      >
                        {dataSource.map((id, index) => (
                          <Select.Option key={index} value={id}>
                            {mapMonHoc[id]}
                          </Select.Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>
                  <Form.Item label="Tên tài liệu">
                    {getFieldDecorator('tenTaiLieu', {
                      initialValue: edit ? _.get(record, 'tenTaiLieu', '') : '',
                      rules: [...rules.text, ...rules.required, ...rules.length(100)],
                    })(<Input.TextArea rows={2} />)}
                  </Form.Item>
                  <Form.Item label="Mô tả">
                    {getFieldDecorator('moTa', {
                      initialValue: edit ? _.get(record, 'moTa', '') : '',
                      rules: [...rules.text, ...rules.length(200)],
                    })(<TextArea rows={5} />)}
                  </Form.Item>

                  <Form.Item label="File tài liệu">
                    {getFieldDecorator('taiLieuDinhKem', {
                      initialValue:
                        edit && record.taiLieuDinhKem && record.taiLieuDinhKem.length > 0
                          ? {
                              fileList: [
                                {
                                  uid: 'zzz',
                                  name: getNameFile(record.taiLieuDinhKem),
                                  url: record.taiLieuDinhKem,
                                  status: 'done',
                                },
                              ],
                            }
                          : { fileList: [] },
                      rules: [...rules.fileRequired],
                    })(<UploadOne />)}
                  </Form.Item>

                  <Form.Item {...tailFormItemLayout}>
                    <Button.Group>
                      <Button type="primary" icon="plus" htmlType="submit">
                        {edit ? 'Cập nhật' : 'Thêm mới'}
                      </Button>
                    </Button.Group>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedQuanTriBaiVietForm = Form.create({ name: 'GiaoVienForm' })(GiaoVienForm);

export default WrappedQuanTriBaiVietForm;
