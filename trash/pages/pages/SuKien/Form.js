/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-indent */
/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Form, Input, Card, Row, Col, Button, DatePicker, Radio, Spin, Select, InputNumber } from 'antd';
import _ from 'lodash';
import { connect } from 'dva';
import moment from 'moment';
import { Format, isValue } from '@/utils/utils';
import rules from '@/utils/rules';
import Hint from '@/components/Hint/Hint';
import UploadAvatar from '@/components/Upload/Upload';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import { formItemLayout, tailFormItemLayout } from '@/utils/const';

const data = {};
data.trangThai = ['Đang làm việc', 'Chuyển trường', 'Nghỉ hưu'];
data.toChuyenMon = ['Toán', 'Ngữ văn'];
data.gioiTinh = ['Nam', 'Nữ'];
data.kieuLapLai = ['Một lần', 'Hằng ngày', 'Hằng tuần', 'Hằng tháng', 'Hằng năm'];
data.kieuSua = ['Từ sự kiện này đến khi kết thúc', 'Tất cả sự kiện'];

const renderGroup = dataIndex => (
  <Radio.Group>
    {data[dataIndex].map((item, index) => (
      <Radio value={index}>{item}</Radio>
    ))}
  </Radio.Group>
);

const renderSelect = dataIndex => (
  <Select>
    {data[dataIndex].map((item, index) => (
      <Select.Option key={index} value={index.toString()}>
        {item}
      </Select.Option>
    ))}
  </Select>
);
@connect(({ sukien, loading }) => ({
  sukien,
  loading: loading.models.sukien
}))
class SuKienForm extends React.Component {
  state = {
    timeStart: '',
    timeEnd: '',
  }

  componentDidMount() {
    const { sukien: { edit, record, info } } = this.props;
    this.setState({
      timeStart: edit ? moment(_.get(record, 'ngayBatDau', moment())) : moment(_.get(info, 'start', moment())),
      timeEnd: edit ? moment(_.get(record, 'ngayKetThuc', '')) : moment(_.get(info, 'end', moment().add(1, 'hours')))
    })
  }

  endDate = () => {
    const { getFieldsValue } = this.props.form;
    let { kieuLapLai, soLan, ngayBatDau } = getFieldsValue();
    if (!kieuLapLai || !soLan || !ngayBatDau) return '';
    kieuLapLai = parseInt(kieuLapLai);
    const dx = [[0, 'days'], [1, 'days'], [7, 'days'], [1, 'months'], [1, 'years']];
    const add = dx[kieuLapLai];
    return `     Tới ${moment(ngayBatDau).add(add[0] * soLan, add[1]).format('DD/MM/YYYY hh:mm')}`;
  }

  onChange = (field, value) => {
    this.setState({
      [field]: value
    })
  }

  onStartChange = value => {
    this.onChange('timeStart', value);
  }

  onEndChange = value => {
    this.onChange('timeEnd', value);
  }

  render() {
    const { sukien, loading } = this.props;
    const { timeStart, timeEnd } = this.state;
    const { record, edit, info } = sukien;
    const { getFieldDecorator, getFieldValue } = this.props.form;
    const handleSubmit = e => {
      e.preventDefault();
      this.props.form.validateFieldsAndScroll((err, values) => {
        if (err) return;

        // Mặc định là lặp 1 lần, nếu khác thì giảm giá trị đi 1
        // để khớp với backend
        values.kieuLapLai = parseInt(values.kieuLapLai);
        if (!values.kieuLapLai) {
          values.kieuLapLai = undefined;
          values.soLan = undefined;
        }

        // Sự kiện ko lặp lại
        if (edit && !values.kieuLapLai) values.kieuSua = 1;

        else values.kieuLapLai -= 1;
        if (edit) {
          this.props.dispatch({
            type: 'sukien/upd',
            payload: {
              ...values,
              _id: _.get(record, '_id', -1),
            },
          });
        } else {
          this.props.dispatch({
            type: 'sukien/add',
            payload: {
              ...values,
            },
          });
        }
      });
    };
    let kieuLapLai = getFieldValue('kieuLapLai');
    if (typeof kieuLapLai === 'string') kieuLapLai = parseInt(kieuLapLai);
    return (
      <div className="box">
        <Card
          bordered
          title={<div className="cardTitle">{!edit ? 'Thêm sự kiện mới' : 'Sửa sự kiện'}</div>}
        >
          <GridContent>
            <Row>
              <Col span={18}>
                <Spin spinning={loading}>
                  <Form {...formItemLayout} onSubmit={handleSubmit}>
                    <Form.Item label="Tên sự kiện">
                      {getFieldDecorator('tenSuKien', {
                        initialValue: edit ? _.get(record, 'tenSuKien', '') : '',
                        rules: [...rules.text, ...rules.required],
                      })(<Input />)}
                    </Form.Item>
                    <Form.Item label="Thời gian bắt đầu">
                      {getFieldDecorator('ngayBatDau', {
                        initialValue: edit ? moment(_.get(record, 'ngayBatDau', moment())) : moment(_.get(info, 'start', moment())),
                        rules: [
                          {
                            validator: (_, value, callback) => {
                              if (moment(value).isAfter(moment(timeEnd).subtract(1, 'seconds'))) callback('');
                              callback();
                            },
                            message: 'Không được sớm hơn thời điểm kết thúc'
                          }
                        ],
                      })(<DatePicker onChange={this.onStartChange} showTime={{ format: 'HH:mm' }} format="DD/MM/YYYY HH:mm" />)}
                    </Form.Item>
                    <Form.Item label="Thời gian kết thúc">
                      {getFieldDecorator('ngayKetThuc', {
                        initialValue: edit ? moment(_.get(record, 'ngayKetThuc', '')) :
                          moment(_.get(info, 'end', moment().add(1, 'hours'))),
                        rules: [
                          {
                            validator: (_, value, callback) => {
                              if (moment(value).isBefore(moment(timeStart).subtract(1, 'seconds'))) callback('');
                              callback();
                            },
                            message: 'Không được muộn hơn thời điểm bắt đầu'
                          }
                        ],
                      })(<DatePicker onChange={this.onEndChange} showTime={{ format: 'HH:mm' }} format="DD/MM/YYYY HH:mm" />)}
                    </Form.Item>
                    <Form.Item label="Địa điểm">
                      {getFieldDecorator('diaDiem', {
                        initialValue: edit ? _.get(record, 'diaDiem', '') : '',
                        rules: [...rules.text],
                      })(<Input />)}
                    </Form.Item>
                    <Form.Item label="Ghi chú">
                      {getFieldDecorator('ghiChu', {
                        initialValue: edit ? _.get(record, 'ghiChu', '') : '',
                        rules: [...rules.text],
                      })(<Input />)}
                    </Form.Item>

                    {
                      !edit &&
                      <Form.Item label="Lặp lại">
                        {getFieldDecorator('kieuLapLai', {
                          initialValue: edit ? _.get(record, 'kieuLapLai', '0') : '0',
                        })(renderSelect('kieuLapLai'))}
                      </Form.Item>
                    }

                    {
                      !edit && (kieuLapLai > 0) &&
                      <Form.Item label="Số lần">
                        {getFieldDecorator('soLan', {
                          initialValue: edit ? _.get(record, 'soLan', 2) : 2,
                          rules: [...rules.number(100), ...rules.inputNumber]
                        })(<InputNumber min={2} max={100} />)}
                        <span>{`${this.endDate()}`}</span>
                      </Form.Item>
                    }

                    {
                      edit && isValue(record.kieuLapLai) &&
                      <Form.Item label="Cách sửa">
                        {getFieldDecorator('kieuSua', {
                          initialValue: '1',
                        })(renderSelect('kieuSua'))}
                      </Form.Item>
                    }


                    <Form.Item {...tailFormItemLayout}>
                      <Button.Group>
                        <Button type="primary" icon="plus" htmlType="submit">
                          {edit ? 'Cập nhật' : 'Thêm mới'}
                        </Button>
                      </Button.Group>
                    </Form.Item>
                  </Form>
                </Spin>
              </Col>
            </Row>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedQuanTriBaiVietForm = Form.create({ name: 'SuKienForm' })(SuKienForm);

export default WrappedQuanTriBaiVietForm;
