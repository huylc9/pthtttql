/* eslint-disable react/sort-comp */
import React from 'react';
import { Card, Row, Col, Button, Modal, Typography, Menu, Popconfirm, Dropdown, Spin } from 'antd';
import {
  Calendar,
  momentLocalizer,
  Views,
} from 'react-big-calendar'
import moment from 'moment';
import { connect } from 'dva';
import uuidv1 from 'uuid/v1';
import FormDrawer from '@/components/Drawer/FormDrawer';
import Form from './Form';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import { toHexa, isValue } from '@/utils/utils';

const messages = {
  allDay: 'Cả ngày',
  previous: 'Trước',
  next: 'Sau',
  today: 'Hôm nay',
  month: 'Tháng',
  week: 'Tuần',
  day: 'Ngày',
  agenda: 'Chung',
  date: 'Ngày',
  time: 'Thời gian',
  event: 'Sự kiện',
  showMore: total => `+ Xem thêm (${total})`,
};

const kieuLapLai = ['Một lần', 'Hằng ngày', 'Hằng tuần', 'Hằng tháng', 'Hằng năm'];

const renderTime = ({ start, end }) => {
  const Start = moment(start);
  const End = moment(end);
  if (Start.format('DD/MM/YYYY') === End.format('DD/MM/YYYY'))
    return `${Start.format('DD/MM/YYYY HH:mm')}-${End.format('HH:mm')}`;
  return `${Start.format('DD/MM/YYYY HH:mm')}-${End.format('DD/MM/YYYY HH:mm')}`;
}

let Nhi = uuidv1();
@connect(({ loading, sukien }) => ({
  sukien,
  loading: loading.models.sukien
}))


class SuKien extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      record: {}
    }
  }

  componentDidMount() {
    this.props.dispatch({
      type: 'sukien/get',
      payload: {
        ngayBatDau: moment().subtract(1, 'years').toISOString(),
        ngayKetThuc: moment().add(1, 'years').toISOString(),
      }
    });
  }



  handleSelect = ({ start, end }) => {
    this.handleCreate({ start, end });
  }

  handleOk = () => {
    this.setState({ visible: false })
  }

  handleCreate = (info) => {
    Nhi = uuidv1();
    console.log(info, "new")
    this.props.dispatch({
      type: 'sukien/changeState',
      payload: {
        showDrawer: true,
        edit: false,
        record: {},
        info
      }
    })
  }

  handleEdit = (record) => {
    Nhi = uuidv1();
    this.setState({ visible: false });
    this.props.dispatch({
      type: 'sukien/changeState',
      payload: {
        showDrawer: true,
        edit: true,
        record
      }
    })
  }

  handleShow = record => {
    this.setState({
      visible: true,
      record
    })
  }

  handleDelete = (record, kieuXoa) => {
    this.props.dispatch({
      type: 'sukien/del',
      payload: {
        _id: record._id,
        kieuXoa
      }
    })
    this.handleOk();
  }


  eventPropGetter = (event) => ({
    style: { backgroundColor: toHexa(event.maSuKien) }
  })

  render() {
    const { sukien, loading } = this.props;
    const localizer = momentLocalizer(moment)
    let { danhSach } = sukien;
    const { record, visible } = this.state;
    danhSach = danhSach.map(event => ({
      ...event,
      start: moment(event.ngayBatDau).toDate(),
      end: moment(event.ngayKetThuc).toDate(),
      title: event.tenSuKien,
      desc: event.ghiChu + event.diaDiem,
    }));

    const menu = (
      <Menu>
        <Menu.Item>
          <Button icon='swap-left' onClick={() => this.handleDelete(record, 0)}>
            Từ sự kiện này đến khi kết thúc
          </Button>
        </Menu.Item>
        <Menu.Item>
          <Button icon='minus' onClick={() => this.handleDelete(record, 1)}>
            Tất cả sự kiện loại này
          </Button>
        </Menu.Item>
      </Menu>
    );

    return (
      <div className="box">
        <Card bordered title={<Button type='primary' onClick={this.handleCreate}> Thêm mới </Button>}>
          <Spin spinning={loading}>
            <Row>
              <Col xs={24}>
                <Calendar
                  localizer={localizer}
                  selectable
                  events={danhSach}
                  defaultView={Views.WEEK}
                  scrollToTime={new Date(1970, 1, 1, 6)}
                  defaultDate={new Date()}
                  onSelectEvent={event => this.handleShow(event)}
                  onSelectSlot={this.handleSelect}
                  messages={messages}
                  views={['month', 'week', 'day']}
                  style={{ height: 700 }}
                  min={moment('0600', 'HHmm').toDate()}
                  max={moment('2000', 'HHmm').toDate()}
                  eventPropGetter={this.eventPropGetter}
                  popup
                />
              </Col>
            </Row>
          </Spin>
        </Card>
        <Modal
          key={Nhi + 1}
          title={record.title || ''}
          visible={visible}
          onOk={this.handleOk}
          onCancel={this.handleOk}
          footer={[<Button type="primary" onClick={this.handleOk}>OK</Button>,
          <Button icon='edit' type="primary" onClick={() => this.handleEdit(record)}>Sửa</Button>,
          isValue(record.kieuLapLai) ?
            <Dropdown type='danger' overlay={menu} placement="bottomLeft">
              <Button>Xóa</Button>
            </Dropdown>
            :
            <Popconfirm title="Bạn có chắc muốn xóa?" onConfirm={() => this.handleDelete(record, 1)}>
              <Button type="danger" icon="delete"> Xóa</Button>
            </Popconfirm>
          ]}
        >
          <Typography.Paragraph>
            {`Thời gian: ${renderTime(record)}`}
          </Typography.Paragraph>
          {
            isValue(record.kieuLapLai) &&
            <Typography.Paragraph>
              {`Lặp lại: ${kieuLapLai[record.kieuLapLai + 1]}`}
            </Typography.Paragraph>
          }
          {record.diaDiem &&
            <Typography.Paragraph>
              {`Địa điểm: ${record.diaDiem}`}
            </Typography.Paragraph>}
          {record.ghiChu &&
            <Typography.Paragraph>
              {`Ghi chú: ${record.ghiChu}`}
            </Typography.Paragraph>}
        </Modal>
        <FormDrawer name="sukien">
          <Form key={Nhi} Nhi={sukien.record._id || Nhi} />
        </FormDrawer>
      </div>
    );
  }
}
export default SuKien;