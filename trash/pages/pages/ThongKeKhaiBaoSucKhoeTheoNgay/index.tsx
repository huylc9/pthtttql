import {
  Avatar,
  Button,
  Card,
  Col,
  DatePicker,
  Descriptions,
  Modal,
  Row,
  Switch,
  Typography,
} from 'antd';
import { connect } from 'dva';
import moment from 'moment';
import React from 'react';
import TableBase from '@/components/Table/Table.tsx';
import { IBase } from '@/utils/base';
import { IColumn } from '@/utils/interfaces';
import { isValue } from '@/utils/utils';
import { styles } from './index.less';
const { RangePicker } = DatePicker;

interface Props {
  suckhoetheongay: IBase;
  loading: boolean;
  dispatch: Function;
}

@connect(({ suckhoetheongay, loading }) => ({
  suckhoetheongay,
  loading: loading.models.suckhoetheongay,
}))
class SucKhoeTheoNgay extends React.Component<Props> {
  public state = {};

  public componentDidMount() {}

  public onChangeKhaiBao = checked => {
    this.props.dispatch({
      type: 'suckhoetheongay/capNhatKhaiBao',
      payload: {
        khaiBao: checked,
      },
    });
  };

  public onChange = (date, dateString) => {
    let a = moment(dateString[0]).format('DD-MM-YYYY');
    let b = moment(dateString[1]).format('DD-MM-YYYY');

    a = a.split('-');
    b = b.split('-');
    const newDateA = `${a[1]},${a[0]},${a[2]}`;
    const newDateB = `${b[1]},${b[0]},${b[2]}`;
    const ngayBatDau = new Date(newDateA).getTime();
    const ngayKetThuc = new Date(newDateB).getTime();
    this.props.dispatch({
      type: 'suckhoetheongay/changeState',
      payload: {
        ngayBatDau,
        ngayKetThuc,
      },
    });
    this.props.dispatch({
      type: 'suckhoetheongay/getDB',
      payload: {
        ngayBatDau,
        ngayKetThuc,
        page: 1,
        limit: 10,
      },
    });
  };

  public handleView = record => {
    const data = {
      gioiTinh: ['Nam', 'Nữ'],
      vaiTro: ['Sinh viên', 'Giảng viên'],
    };

    Modal.info({
      title: (
        <Typography.Title style={{ marginTop: -6 }} level={3}>
          Thông tin sinh viên
        </Typography.Title>
      ),
      width: '100%',

      content: (
        <Card bordered={false} className="ql-editor" bodyStyle={{ minHeight: 400 }}>
          <div>
            <Row gutter={16}>
              <Col sm={24} lg={6} xl={6}>
                <Row>
                  <Col xs={24}>
                    <div style={{ textAlign: 'center' }}>
                      <Avatar
                        shape="square"
                        size={150}
                        src={_.get(
                          record,
                          'anhDaiDien',
                          'https://i.pinimg.com/236x/40/8a/c5/408ac5a5102e42678de2827089cf177b.jpg'
                        )}

                        //
                      />
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col xs={24}>
                    <div style={{ textAlign: 'center' }}>
                      <Typography.Title level={4}>Ảnh Hồ Sơ</Typography.Title>
                    </div>
                  </Col>
                </Row>
              </Col>
              <Col sm={24} lg={18}>
                <Descriptions
                  title=""
                  bordered
                  column={{ xs: 1, sm: 1, md: 2, lg: 2, xl: 2, xxl: 2 }}
                >
                  <Descriptions.Item span={2} label="Họ và tên">
                    <h3>{_.get(record, 'hoTen', 'Chưa cập nhật')}</h3>
                  </Descriptions.Item>
                  <Descriptions.Item span={1} label="Mã SV">
                    <h3>{_.get(record, 'maSv', 'Chưa cập nhật')}</h3>
                  </Descriptions.Item>
                  <Descriptions.Item span={1} label="Lớp">
                    <h3>{_.get(record, 'lop', 'Chưa cập nhật')}</h3>
                  </Descriptions.Item>
                  <Descriptions.Item span={2} label="Mã Lớp">
                    <h3>{_.get(record, 'maLop', 'Chưa cập nhật')}</h3>
                  </Descriptions.Item>
                  <Descriptions.Item span={2} label="Mã Khoa">
                    <h3>{_.get(record, 'maKhoa', 'Chưa cập nhật')}</h3>
                  </Descriptions.Item>
                  <Descriptions.Item span={2} label="Ngành">
                    <h3>{_.get(record, 'maNganh', 'Chưa cập nhật')}</h3>
                  </Descriptions.Item>
                  <Descriptions.Item span={1} label="Ngày sinh">
                    <h3>
                      {moment(_.get(record, 'ngaySinh', 'Chưa cập nhật')).format('DD/MM/YYYY')}
                    </h3>
                  </Descriptions.Item>
                  <Descriptions.Item span={1} label="Giới tính">
                    <h3>
                      {isValue(record.gioiTinh) ? data.gioiTinh[record.gioiTinh] : 'Chưa cập nhật'}
                    </h3>
                  </Descriptions.Item>
                  <Descriptions.Item span={2} label="Số Căn Cước Công Dân">
                    <h3>{_.get(record, 'soCMND', 'Chưa cập nhật')}</h3>
                  </Descriptions.Item>
                  <Descriptions.Item span={2} label="Nơi ở hiện tại">
                    <h3>{_.get(record, 'diaChiHienNay', 'Chưa cập nhật')}</h3>
                  </Descriptions.Item>
                  <Descriptions.Item span={1} label="Số điện thoại">
                    <h3>{_.get(record, 'soDienThoai', 'Chưa cập nhật')}</h3>
                  </Descriptions.Item>
                  <Descriptions.Item span={1} label="Email">
                    <h3>{_.get(record, 'email', 'Chưa cập nhật')}</h3>
                  </Descriptions.Item>
                  <Descriptions.Item span={1} label="Vai trò">
                    <h3>{isValue(record.vaiTro) ? data.vaiTro[record.vaiTro] : 'Chưa cập nhật'}</h3>
                  </Descriptions.Item>
                  <Descriptions.Item span={1} label="Trạng thái">
                    <h3>{isValue(record.activated) ? 'Đã kích hoạt' : 'Chưa kích hoạt'}</h3>
                  </Descriptions.Item>
                </Descriptions>
              </Col>
            </Row>
          </div>
        </Card>
      ),
    });
  };

  public render() {
    const renderLast = (value, record) => (
      <React.Fragment>
        <Button
          type="primary"
          shape="circle"
          icon="eye"
          onClick={() => this.handleView(record)}
          title="Xem trước"
        />
      </React.Fragment>
    );

    const columns: IColumn[] = [
      {
        title: 'STT',
        dataIndex: 'index',
        align: 'center',
        width: '80px',
      },
      {
        title: 'Mã khoa',
        dataIndex: 'maKhoa',
        align: 'center',
        width: '150px',
      },
      {
        title: 'Họ đệm',
        dataIndex: 'hoDem',
        align: 'center',
        width: '150px',
      },
      {
        title: 'Tên',
        dataIndex: 'ten',
        align: 'center',
        width: '150px',
      },
      {
        title: 'Họ tên',
        dataIndex: 'hoTen',
        align: 'center',
        width: '150px',
      },
      {
        title: 'Mã sinh viên',
        dataIndex: 'maSv',
        align: 'center',
        width: '150px',
      },
      {
        title: 'Username',
        dataIndex: 'username',
        align: 'center',
        width: '150px',
      },
      {
        title: 'Vai trò',
        dataIndex: 'vaiTro',
        align: 'center',
        width: '150px',
        search: 'filter',
      },
      {
        title: 'Ngày sinh',
        dataIndex: 'ngaySinh',
        align: 'center',
        width: 110,
        render: val => (
          <span title={moment(val).format('DD/MM/YYYY HH:mm:ss')}>
            {moment(val).format('DD/MM/YYYY')}
          </span>
        ),
      },
      {
        title: 'Giới tính',
        dataIndex: 'gioiTinh',
        align: 'center',
        search: 'filter',
        width: '150px',
      },
      {
        title: 'Số điện thoại',
        dataIndex: 'soDienThoai',
        align: 'center',
        width: '150px',
      },
      {
        title: 'Địa chỉ hiện nay',
        dataIndex: 'diaChiHienNay',
        align: 'center',
        // width: '150px',
      },
      {
        title: 'email',
        dataIndex: 'email',
        align: 'center',
        // width: '150px',
      },

      {
        title: 'Thao tác',
        align: 'center',
        render: (value, record) => renderLast(value, record),
        fixed: 'right',
        width: 110,
      },
    ];

    const { suckhoetheongay } = this.props;
    const { ngayBatDau, ngayKetThuc } = suckhoetheongay;
    return (
      <div>
        <TableBase
          model={this.props.suckhoetheongay}
          modelName="suckhoetheongay"
          loading={this.props.loading}
          dispatch={this.props.dispatch}
          columns={columns}
          cond={{}}
          title="Danh sách khai báo sức khỏe theo ngày"
          tableProp={{
            scroll: { x: 2000 },
          }}
        >
          <div>
            <RangePicker
              onChange={this.onChange}
              value={[moment(new Date(ngayBatDau)), moment(new Date(ngayKetThuc))]}
            />
          </div>
          <div style={{ marginTop: 12 }}>
            <Switch
              checkedChildren="Đã khai báo"
              unCheckedChildren="Chưa khai báo"
              onChange={this.onChangeKhaiBao}
              defaultChecked
            />
          </div>
        </TableBase>
      </div>
    );
  }
}

export default SucKhoeTheoNgay;
