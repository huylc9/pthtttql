import base from '@/utils/base';
import modelExtend from 'dva-model-extend';
import Services from '@/pages/KhaoSatNew/service';
import notificationAlert from '@/components/Notification';
import { formatMessage } from 'umi/locale';
import _ from 'lodash';

export default modelExtend(base(Services), {
  namespace: 'khaosatnew',
  effects: {
    *upd({ payload }, { call, put }) {
      yield call(Services.upd, payload);
      notificationAlert('success', formatMessage({ id: 'SUA_THANH_CONG' }));
      yield put({ type: 'get' });
      yield put({ type: 'changeState', payload: { showDrawer: false } });
    },
    *getId({ payload, onComplete }, { call, put, select }) { // result
      const response = yield call(Services.getId, payload);
      const record = yield select(state => state.khaosatnew.record);
      record.ketQua = _.get(response, 'data.data', []);
      yield put({ type: 'changeState', payload: { record } });
      if (onComplete) onComplete();
    },
  },
});
