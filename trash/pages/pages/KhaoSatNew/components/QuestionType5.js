import React from 'react';
import _ from 'lodash';
import { getRecordValue } from '@/utils/utils';
import rules from '@/utils/rules';
import { formItemLayout, tailFormItemLayout, handleValuesChange, handleCommonSubmit, getSubmitText, renderGroup, renderSelect } from '@/utils/form';
import {
  Form,
  Input,
  Card,
  Row,
  Col,
  Button,
  Select,
  InputNumber,
  DatePicker,
  Typography,
  List,
  Checkbox,
  Icon,
  Radio,
  Tooltip,
  Spin
} from 'antd';
import EditableItem from '@/components/EditableItem';
import styles from '../style.css';
//  gom cau hoi va cac cau tra loi
//
//
//

export default class Question extends React.Component {
  constructor(props) {
    super(props);
    const { value, onChange } = props;
    this.state = {
      cauHoi: value.cauHoi || "",
      hang: value.hang || [""],
      cot: value.cot || [""]
    }
  }

  triggerChange = changedValue => {
    if (this.props.onChange) {
      this.props.onChange({
        ...this.state,
        ...changedValue,
      });
    }
  };

  onQuestionChange = e => {
    const cauHoi = e.target.value;
    this.setState({ cauHoi })
    this.triggerChange({
      cauHoi,
    });
  };

  onRowChange = (text, index) => {
    const { hang } = this.state;
    hang[index] = text;
    this.setState({ hang })
    this.triggerChange({
      hang,
    });
  };

  onColumnChange = (text, index) => {
    const { cot } = this.state;
    cot[index] = text;
    this.setState({ cot })
    this.triggerChange({
      cot,
    });
  };

  removeRow = (index) => {
    let { hang } = this.state;
    hang.splice(index, 1);
    this.setState({ hang });
    this.triggerChange({
      hang,
    });
  }

  removeColumn = (index) => {
    let { cot } = this.state;
    cot.splice(index, 1);
    this.setState({ cot });
    this.triggerCcote({
      hang,
    });
  }

  addRow = () => {
    let { hang } = this.state;
    hang.push("");
    this.setState({ hang });
    this.triggerChange({
      hang,
    });
  }

  addColumn = () => {
    let { cot } = this.state;
    cot.push("");
    this.setState({ cot });
    this.triggerChange({
      cot,
    });
  }


  render() {
    const { cauHoi, hang, cot } = this.state;

    return (
      <React.Fragment>
        <Form.Item className={styles.formItem} label="Mô tả">
          <Input.TextArea
            value={cauHoi}
            placeholder="Nội dung mô tả" style={{ width: '100%' }}
            onChange={this.onQuestionChange}
          />
        </Form.Item>
        <Form.Item {...tailFormItemLayout} style={{ marginTop: 24 }}>
          <Button type="primary" style={{ marginRight: 20 }} onClick={this.props.removeQuestion}>
            <Icon type="delete" /> Xóa câu hỏi
          </Button>
        </Form.Item>
      </React.Fragment>
    )
  }

}