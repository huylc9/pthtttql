import React from 'react';
import _ from 'lodash';
import { getRecordValue } from '@/utils/utils';
import rules from '@/utils/rules';
import { formItemLayout, tailFormItemLayout, handleValuesChange, handleCommonSubmit, getSubmitText, renderGroup, renderSelect } from '@/utils/form';
import {
  Form,
  Input,
  Card,
  Row,
  Col,
  Button,
  Select,
  InputNumber,
  DatePicker,
  Typography,
  List,
  Checkbox,
  Icon,
  Radio,
  Tooltip,
  Spin
} from 'antd';
import EditableItem from '@/components/EditableItem';
import styles from '../style.css';
//  gom cau hoi va cac cau tra loi
//
//
//

export default class Question extends React.Component {
  constructor(props) {
    super(props);
    const { value, onChange } = props;
    this.state = {
      cauHoi: value.cauHoi || "",
      cauTraLoi: value.cauTraLoi || [{ luaChon: "", isKhac: false }]
    }
  }

  triggerChange = changedValue => {
    // gui len state -> props.value => changedValue
    // neu co props.value thi se bo qua state
    const { cauHoi, cauTraLoi } = this.state;
    if (this.props.onChange) {
      this.props.onChange({
        cauHoi,
        cauTraLoi,
        ...this.props.value,
        ...changedValue,
      });
    }
  };

  onQuestionChange = e => {
    const cauHoi = e.target.value;
    this.setState({ cauHoi })
    this.triggerChange({
      cauHoi,
    });
  };

  onAnswerChange = (text, index) => {
    const { cauTraLoi } = this.state;
    cauTraLoi[index].luaChon = text;
    this.setState({ cauTraLoi })
    this.triggerChange({
      cauTraLoi,
    });
  };

  remove = (index) => {
    let { cauTraLoi } = this.state;
    cauTraLoi.splice(index, 1);
    this.setState({ cauTraLoi });
    this.triggerChange({
      cauTraLoi,
    });
  }

  hasKhac = () => {
    let { cauTraLoi } = this.state;
    if (!cauTraLoi.length) return;
    return (!!cauTraLoi[cauTraLoi.length - 1].isKhac);
  }

  addAnswer = (isKhac) => {
    let { cauTraLoi } = this.state;
    console.log(cauTraLoi, cauTraLoi.length);
    if (!this.hasKhac()) { // chua co cau tra loi khac
      cauTraLoi.push({
        isKhac,
        luaChon: isKhac ? "Đáp án khác" : ""
      })
    }
    else {
      cauTraLoi.splice(cauTraLoi.length - 1, 0, {
        isKhac,
        luaChon: ""
      });
    }
    this.setState({ cauTraLoi });
    this.triggerChange({
      cauTraLoi,
    });
    console.log(cauTraLoi, cauTraLoi.length);
  }


  render() {
    const { cauHoi, cauTraLoi } = this.state;

    return (
      <React.Fragment>
        <Form.Item className={styles.formItem} label="Câu hỏi">
          <Input.TextArea
            value={cauHoi}
            placeholder="Nội dung câu hỏi" style={{ width: '100%' }}
            onChange={this.onQuestionChange}
          />
        </Form.Item>
        {
          cauTraLoi.map(({ luaChon, isKhac }, index) => (
            <React.Fragment>
              {
                !isKhac ? (
                  <Form.Item className={styles.formItem} label={"Đáp án " + (index + 1)} >
                    <Input
                      value={luaChon}
                      placeholder="Nội dung câu trả lời" style={{ width: '90%', marginRight: 5 }}
                      onChange={e => this.onAnswerChange(e.target.value, index)}
                    />
                    {cauTraLoi.length > 1 && (
                      <Icon
                        className="dynamic-delete-button"
                        type="minus-circle-o"
                        onClick={() => this.remove(index)}
                        title="Xóa câu trả lời"
                        style={{ fontSize: 24 }}
                      />
                    )}
                  </Form.Item>
                ) : (
                    <Form.Item className={styles.formItem} colon={false} label={<EditableItem value={luaChon} onChange={(value) => this.onAnswerChange(value, index)} />} >
                      <Input
                        disabled
                        placeholder=""
                        style={{ width: '90%', marginRight: 5 }}
                      />
                      {cauTraLoi.length > 1 && (
                        <Icon
                          className="dynamic-delete-button"
                          type="minus-circle-o"
                          onClick={() => this.remove(index)}
                          title="Xóa câu trả lời"
                          style={{ fontSize: 24 }}
                        />
                      )}
                    </Form.Item>
                  )
              }

            </React.Fragment>
          ))
        }
        <Form.Item {...tailFormItemLayout} style={{ marginTop: 24 }}>
          <Button type="primary" style={{ marginRight: 20 }} onClick={() => this.addAnswer(false)}>
            <Icon type="plus" /> Đáp án
              </Button>
          {!this.hasKhac() && <Button type="primary" style={{ marginRight: 20 }} onClick={() => this.addAnswer(true)}>
            <Icon type="plus" /> Đáp án khác
              </Button>}
          <Button type="primary" style={{ marginRight: 20 }} onClick={this.props.removeQuestion}>
            <Icon type="delete" /> Xóa câu hỏi
          </Button>
        </Form.Item>
      </React.Fragment>
    )
  }

}