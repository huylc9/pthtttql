import React from 'react';
import _ from 'lodash';
import { getRecordValue } from '@/utils/utils';
import rules from '@/utils/rules';
import { formItemLayout, tailFormItemLayout, handleValuesChange, handleCommonSubmit, getSubmitText, renderGroup, renderSelect } from '@/utils/form';
import {
  Form,
  Input,
  Card,
  Row,
  Col,
  Button,
  Select,
  InputNumber,
  DatePicker,
  Typography,
  List,
  Checkbox,
  Icon,
  Radio,
  Tooltip,
  Spin
} from 'antd';
import EditableItem from '@/components/EditableItem';
import styles from '../style.css';
//  gom cau hoi va cac cau tra loi
//
//
//

export default class Question extends React.Component {
  constructor(props) {
    super(props);
    const { value, onChange } = props;
    this.state = {
      cauHoi: value.cauHoi || "",
    }
  }

  triggerChange = changedValue => {
    // gui len state -> props.value => changedValue
    // neu co props.value thi se bo qua state
    const { cauHoi } = this.state;
    if (this.props.onChange) {
      this.props.onChange({
        cauHoi,
        ...changedValue,
      });
    }
  };

  onQuestionChange = e => {
    const cauHoi = e.target.value;
    this.setState({ cauHoi })
    this.triggerChange({
      cauHoi,
    });
  };


  render() {
    const { cauHoi } = this.state;
    return (
      <React.Fragment>
        <Form.Item className={styles.formItem} label="Câu hỏi">
          <Input.TextArea
            value={cauHoi}
            placeholder="Nội dung câu hỏi" style={{ width: '100%' }}
            onChange={this.onQuestionChange}
          />
        </Form.Item>
        <Form.Item {...tailFormItemLayout} style={{ marginTop: 24 }}>
          <Button type="primary" style={{ marginRight: 20 }} onClick={this.props.removeQuestion}>
            <Icon type="delete" /> Xóa câu hỏi
          </Button>
        </Form.Item>
      </React.Fragment>
    )
  }

}