import React from 'react';
import _ from 'lodash';
import { getRecordValue } from '@/utils/utils';
import rules from '@/utils/rules';
import { formItemLayout, tailFormItemLayout, handleValuesChange, handleCommonSubmit, getSubmitText, renderGroup, renderSelect } from '@/utils/form';
import {
  Form,
  Input,
  Card,
  Row,
  Col,
  Button,
  Select,
  InputNumber,
  DatePicker,
  Typography,
  List,
  Checkbox,
  Icon,
  Radio,
  Tooltip,
  Spin
} from 'antd';
import EditableItem from '@/components/EditableItem';
import styles from '../style.css';
//  gom cau hoi va cac cau tra loi
//
//
//

export default class Question extends React.Component {
  constructor(props) {
    super(props);
    const { value, onChange } = props;
    this.state = {
      cauHoi: value.cauHoi || "",
      cauTraLoi: value.cauTraLoi,
    }
  }

  triggerChange = changedValue => {
    if (this.props.onChange) {
      this.props.onChange({
        ...this.state,
        ...changedValue,
      });
    }
  };

  onQuestionChange = e => {
    const cauHoi = e.target.value;
    this.setState({ cauHoi })
    this.triggerChange({
      cauHoi,
    });
  };

  onMinChange = (min) => {
    const { max } = this.state;
    let cauTraLoi = [];
    for (let i = min; i <= max; i++) {
      cauTraLoi.push(i);
    }
    this.setState({
      min, cauTraLoi
    })
  }

  onMaxChange = (max) => {
    const { min } = this.state;
    let cauTraLoi = [];
    for (let i = min; i <= max; i++) {
      cauTraLoi.push(i);
    }
    this.setState({
      max, cauTraLoi
    })
  }

  render() {
    const { cauHoi, min, max } = this.state;

    return (
      <React.Fragment>
        <Form.Item className={styles.formItem} label="Câu hỏi">
          <Input.TextArea
            value={cauHoi}
            placeholder="Nội dung câu hỏi" style={{ width: '100%' }}
            onChange={this.onQuestionChange}
          />
        </Form.Item>
        <Form.Item className={styles.formItem} label="Phạm vi">
          Từ
          <Select value={min} style={{ width: 60 }} onChange={val => this.onMinhange(val)}>
            <Select.Option value={0}>0</Select.Option>
            <Select.Option value={1}>1</Select.Option>
          </Select>
          Đến
          <Select value={max} style={{ width: 60 }} onChange={val => this.onMaxChange(val)}>
            {[2, 3, 4, 5, 6, 7, 8, 9, 10].map(val => (<Select.Option value={val}>{val}</Select.Option>))}
          </Select>
        </Form.Item>
        <Form.Item {...tailFormItemLayout} style={{ marginTop: 24 }}>
          <Button type="primary" style={{ marginRight: 20 }} onClick={this.props.removeQuestion}>
            <Icon type="delete" /> Xóa câu hỏi
          </Button>
        </Form.Item>
      </React.Fragment>
    )
  }

}