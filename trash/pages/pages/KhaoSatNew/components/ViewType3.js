import React from 'react';
import _ from 'lodash';
import { getRecordValue } from '@/utils/utils';
import rules from '@/utils/rules';
import { formItemLayout, tailFormItemLayout, handleValuesChange, handleCommonSubmit, getSubmitText, renderGroup, renderSelect } from '@/utils/form';
import {
    Form,
    Input,
    Card,
    Row,
    Col,
    Button,
    Select,
    InputNumber,
    DatePicker,
    Typography,
    List,
    Checkbox,
    Icon,
    Radio,
    Tooltip,
    Spin,
    Tag
} from 'antd';
import EditableItem from '@/components/EditableItem';
import styles from '../style.css';
import { Pie, yuan } from 'ant-design-pro/lib/Charts';

//  gom cau hoi va cac cau tra loi
//
//
//

export default class Question extends React.Component {
    render() {
        const { soNguoiTraLoi, cauHoi } = this.props.value; // cauTraLoi : {cauTraLoi: string, soNguoiChon:number}[]

        return (
            <React.Fragment>
                <Tag color="green">{cauHoi}</Tag>
                <div >
                    {"Trả lời: " + soNguoiTraLoi}
                </div>
            </React.Fragment>
        )
    }

}