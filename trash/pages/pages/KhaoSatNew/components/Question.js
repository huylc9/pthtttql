import React from 'react';
import _ from 'lodash';
import { getRecordValue } from '@/utils/utils';
import rules from '@/utils/rules';
import { formItemLayout, tailFormItemLayout, handleValuesChange, handleCommonSubmit, getSubmitText, renderGroup, renderSelect } from '@/utils/form';
import {
  Form,
  Input,
  Card,
  Row,
  Col,
  Button,
  Select,
  InputNumber,
  DatePicker,
  Typography,
  List,
  Checkbox,
  Icon,
  Radio,
  Tooltip,
  Spin
} from 'antd';

import { connect } from 'dva';
import moment from 'moment';
import UploadOne from '@/components/Upload/UploadOne';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
// import LineChart from './LineChart';
import QuestionType0 from './QuestionType0';
import QuestionType1 from './QuestionType1';
import QuestionType2 from './QuestionType2';
import QuestionType3 from './QuestionType3';
import QuestionType4 from './QuestionType4';
import QuestionType5 from './QuestionType5';

import styles from '../style.css';

//  gom loai cau hoi va cau hoi
//
//
//

export default class Question extends React.Component {
  constructor(props) {
    super(props);
    const { value, onChange } = props;
    const { loai, isRequired, ...childValue } = value;
    this.state = {
      loai: loai || 0,
      isRequired: isRequired || true,
      childValue // chua thong tin cau hoi: 
      // ex: {cauHoi: "1+1=?", cauTraLoi: ["2", "3", "4", "5"]}
    }
  }

  triggerChange = changedValue => {
    const { loai, isRequired, childValue } = this.state;
    if (this.props.onChange) {
      this.props.onChange({
        loai, isRequired,
        ...childValue,
        ...changedValue,
      });
    }
  };

  onTypeChange = loai => {
    this.setState({ loai })
    this.triggerChange({
      loai,
    });
  };

  onRequiredChange = isRequired => {
    this.setState({ isRequired })
    this.triggerChange({
      isRequired,
    });
  };


  onChildValueChange = childValue => {
    this.setState({ childValue })
    this.triggerChange({
      ...childValue
    });
  };

  render() {
    const { loai, childValue, isRequired } = this.state;
    const { value, onChange, removeQuestion, mirrorQuestion, up, down } = this.props;
    const common = {
      removeQuestion,
      mirrorQuestion,
      up,
      down,
      value: childValue,
      onChange: this.onChildValueChange,
    }
    const child = [
      <QuestionType0 {...common} />,
      <QuestionType1 {...common} />,
      <QuestionType2 {...common} />,
      <QuestionType3 {...common} />,
      <QuestionType4 {...common} />,
      <QuestionType5 {...common} />,

    ];
    return (
      <div className={styles.question}>
        <Form.Item label="Loại nội dung">
          <Select value={value.loai} style={{ width: '60%', marginRight: 20 }} onChange={this.onTypeChange}>
            <Select.Option value={0}>
              Chọn một đáp án
          </Select.Option>
            <Select.Option value={1}>
              Chọn nhiều đáp án
          </Select.Option>
            <Select.Option value={2}>
              Đánh giá
          </Select.Option>
            <Select.Option value={3}>
              Câu trả lời text
          </Select.Option>
            <Select.Option value={4}>
              Nhiều câu hỏi dạng bảng
          </Select.Option>
            <Select.Option value={5}>
              Mô tả
          </Select.Option>
          </Select>
          Bắt buộc: <Checkbox checked={isRequired} onChange={e => this.onRequiredChange(e.target.checked)} />
        </Form.Item>
        {
          child[loai]
        }
      </div>
    )
  }

}