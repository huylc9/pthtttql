import React from 'react';
import _ from 'lodash';
import { getRecordValue } from '@/utils/utils';
import rules from '@/utils/rules';
import { formItemLayout, tailFormItemLayout, handleValuesChange, handleCommonSubmit, getSubmitText, renderGroup, renderSelect } from '@/utils/form';
import {
    Form,
    Input,
    Card,
    Row,
    Col,
    Button,
    Select,
    InputNumber,
    DatePicker,
    Typography,
    List,
    Checkbox,
    Icon,
    Radio,
    Tooltip,
    Spin
} from 'antd';

import { connect } from 'dva';
import moment from 'moment';
import UploadOne from '@/components/Upload/UploadOne';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
// import LineChart from './LineChart';
import ViewType0 from './ViewType0';
import ViewType1 from './ViewType1';
import ViewType5 from './ViewType5';
import ViewType3 from './ViewType3';
import ViewType4 from './ViewType4';

import styles from '../style.css';

//  gom loai cau hoi va cau hoi
//
//
//

export default class Question extends React.Component {
    render() {
        const { value } = this.props;
        const { loai } = value;
        const child = [
            <ViewType0 value={value} />,
            <ViewType1 value={value} />,
            <ViewType0 value={value} />,
            <ViewType3 value={value} />,
            <ViewType4 value={value} />,
            <ViewType5 value={value} />,
        ];
        return (
            <div className={styles.question}>
                {
                    child[loai]
                }
            </div>
        )
    }

}