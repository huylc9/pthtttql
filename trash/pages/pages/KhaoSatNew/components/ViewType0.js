import React from 'react';
import _ from 'lodash';
import { getRecordValue } from '@/utils/utils';
import rules from '@/utils/rules';
import { formItemLayout, tailFormItemLayout, handleValuesChange, handleCommonSubmit, getSubmitText, renderGroup, renderSelect } from '@/utils/form';
import {
  Form,
  Input,
  Card,
  Row,
  Col,
  Button,
  Select,
  InputNumber,
  DatePicker,
  Typography,
  List,
  Checkbox,
  Icon,
  Radio,
  Tooltip,
  Spin,
  Tag
} from 'antd';
import EditableItem from '@/components/EditableItem';
import styles from '../style.css';
import { Pie, yuan } from 'ant-design-pro/lib/Charts';
//  gom cau hoi va cac cau tra loi
//
//
//

export default class Question extends React.Component {
  render() {
    const { cauHoi, cauTraLoi, tongHop } = this.props.value;
    const map = {};
    let posKhac = 0;
    cauTraLoi.map(({ luaChon, isKhac }, index) => {
      if (isKhac) posKhac = index; // vi tri cau khac
    });

    // khoi tao du lieu
    let data = cauTraLoi.map(({ luaChon, isKhac }) => ({
      x: luaChon,
      y: 0
    }));

    tongHop.map(({ cauTraLoi: text, soNguoiChon }) => {
      cauTraLoi.map(({ luaChon, isKhac }, index) => {
        if (text === luaChon) data[index].y += soNguoiChon;
      })
    });
    return (
      <React.Fragment>
        <Tag color="green">{cauHoi}</Tag>
        <Pie
          hasLegend
          title={cauHoi}
          subTitle={"Tổng số"}
          total={() => (
            <span
              dangerouslySetInnerHTML={{
                __html: data.reduce((pre, now) => now.y + pre, 0),
              }}
            />
          )}
          data={data}
          valueFormat={val => <span dangerouslySetInnerHTML={{ __html: val }} />}
          height={294}
        />
      </React.Fragment>
    )
  }

}