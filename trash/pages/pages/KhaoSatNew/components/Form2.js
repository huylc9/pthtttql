import React from 'react';
import _ from 'lodash';
import { getRecordValue } from '@/utils/utils';
import rules from '@/utils/rules';
import { formItemLayout, tailFormItemLayout, handleValuesChange, handleCommonSubmit, getSubmitText, renderGroup, renderSelect } from '@/utils/form';
import {
  Form,
  Input,
  Card,
  Row,
  Col,
  Button,
  Select,
  InputNumber,
  DatePicker,
  Typography,
  List,
  Checkbox,
  Icon,
  Radio,
  Tooltip,
  Spin
} from 'antd';

import { connect } from 'dva';
import moment from 'moment';
import UploadOne from '@/components/Upload/UploadOne';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
// import LineChart from './LineChart';
import Question from './Question';
import styles from '../style.css';
import View from './View';

const Process = (arr) => arr.map(({ loai, cauHoi, cauTraLoi, hang, cot, isRequired }) => ({
  loai, cauHoi, cauTraLoi, hang, cot, isRequired
}));

class KhaoSat extends React.Component {

  constructor(props) {
    super(props);
    const { model: { edit, record } } = this.props;
    this.state = {
      danhSachCauHoi: edit ? Process(record.noiDungKhaoSat) : []
    }
  }

  onQuestionValueChange = (value, index) => {
    let { danhSachCauHoi } = this.state;
    danhSachCauHoi[index] = value;
    this.setState({ danhSachCauHoi });
  }

  addQuestion = () => {
    let { danhSachCauHoi } = this.state;
    danhSachCauHoi.push({
      loai: 0,
      childValue: {
        cauHoi: '',
        cauTraLoi: [{
          isKhac: false,
          noiDung: ''
        }]
      }
    });
    this.setState({ danhSachCauHoi });
  }

  removeQuestion = (index) => {
    let { danhSachCauHoi } = this.state;
    danhSachCauHoi.splice(index, 1);
    this.setState({ danhSachCauHoi });
  }

  mirrowQuestion = (index) => {
    let { danhSachCauHoi } = this.state;
    danhSachCauHoi.splice(index + 1, 0, danhSachCauHoi[index]);
    this.setState({ danhSachCauHoi });
  }

  up = (index) => {
    if (index == 0) return;
    let { danhSachCauHoi } = this.state;
    const element = danhSachCauHoi.splice(index, 1);
    danhSachCauHoi.splice(index - 1, 0, element);
    this.setState({ danhSachCauHoi });
  }

  down = (index) => {
    let { danhSachCauHoi } = this.state;
    if (index == danhSachCauHoi.length - 1) return;
    const element = danhSachCauHoi.splice(index, 1);
    danhSachCauHoi.splice(index, 0, element);
    this.setState({ danhSachCauHoi });
  }

  render() {
    const { danhSachCauHoi } = this.state;
    const { model: { record, edit }, loading } = this.props;
    const handleSubmit = e => {
      e.preventDefault();
      this.props.form.validateFieldsAndScroll((err, values) => {
        if (err) return;
        const { thoiGian } = values;
        values.thoiGian = undefined;
        values.ngayBatDau = thoiGian[0];
        values.ngayKetThuc = thoiGian[1];
        if (edit) {
          this.props.dispatch({
            type: 'khaosatnew/upd',
            payload: {
              _id: record._id,
              ...values,
              noiDungKhaoSat: danhSachCauHoi
            }
          })
        }
        else {
          this.props.dispatch({
            type: 'khaosatnew/add',
            payload: {
              ...values,
              noiDungKhaoSat: danhSachCauHoi
            }
          })
        }
      });
    };
    const { form: { getFieldDecorator }, model } = this.props;
    const view = record && record.view;
    let ketQua;
    if (view) {
      ketQua = record.ketQua;
    }
    return (
      <div className="box">
        <Card
          bordered
          title={<div className="cardTitle">{['Thêm khảo sát mới', 'Sửa khảo sát', 'Kết quả khảo sát: ' + record.tieuDe][+edit + !!view]}</div>}
        >
          <GridContent>
            <Row>
              <Col span={24}>
                <Form className={styles.form} size="small" {...formItemLayout} onSubmit={handleSubmit}>
                  {/* {view && <Typography.Text strong style={{ fontSize: 20 }}>{`Số người tham gia khảo sát: ${_.get(record, 'nguoiTraLoi.length', 0)}`} </Typography.Text>} */}
                  <Form.Item label="Tiêu đề">
                    {getFieldDecorator('tieuDe', {
                      initialValue: `${edit ? _.get(record, 'tieuDe', '') : ''}`,
                    })(<Input.TextArea disabled={view} rows={2} />)}
                  </Form.Item>
                  <Form.Item label="Mô tả">
                    {getFieldDecorator('moTa', {
                      initialValue: `${edit ? _.get(record, 'moTa', '') : ''}`,
                    })(<Input.TextArea disabled={view} rows={2} />)}
                  </Form.Item>
                  <Form.Item label="Thời gian khảo sát">
                    {getFieldDecorator('thoiGian', {
                      initialValue: edit ? [moment(record.ngayBatDau), moment(record.ngayKetThuc)] : [moment(), moment().add(1, 'day')],
                    })(<DatePicker.RangePicker disabled={view} format="DD/MM/YYYY HH:mm" showTime={{ format: 'DD/MM/YYYY HH:mm' }} />)}
                  </Form.Item>
                  <br />
                  {
                    !view ? (
                      danhSachCauHoi.map((value, index) => (
                        <Question
                          value={value}
                          onChange={(value) => this.onQuestionValueChange(value, index)}
                          removeQuestion={() => this.removeQuestion(index)}
                          mirrorQuestion={this.mirrowQuestion}
                          up={this.up}
                          down={this.down}
                        />
                      )
                      )) : (
                        ketQua.map(value => <View value={value} />)

                      )
                  }

                  <Form.Item {...tailFormItemLayout}>
                    <Button
                      type="primary"
                      onClick={() => this.addQuestion()}
                      style={{ width: '60%' }}
                    >
                      <Icon type="plus" /> Thêm câu hỏi
                        </Button>
                  </Form.Item>
                  <Form.Item {...tailFormItemLayout}>
                    <Button.Group>
                      <Spin spinning={loading}>
                        <Button type="primary" icon="plus" htmlType="submit">
                          {edit ? 'Cập nhật' : 'Thêm mới'}
                        </Button>
                      </Spin>
                    </Button.Group>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedForm = Form.create({ name: 'KhaoSat' })(KhaoSat);

export default WrappedForm;