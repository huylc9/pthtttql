import Service from '@/services/BaseService';
import axios from '@/utils/axios.js';
import { ip3 } from '@/services/ip';

class ExtendedService extends Service {
    getId = async ({ _id }) => axios.get(`${ip3}/${this.url}/result/${_id}`)
}

export default new ExtendedService({ name: 'khaosatnew', formData: false, url: "khaosat-v2" });
