/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-indent */
import React from 'react';
import {
    Card,
    Avatar,
    Typography,
    Tooltip
} from 'antd';
import moment from 'moment';
import _ from 'lodash';

const { Title } = Typography;

interface Props {
    thongTinChiTiet: object
}

class ModalNews extends React.Component<Props> {

    render() {
        const thongTinChiTiet = this.props.thongTinChiTiet || {};
        return (
            <div >

                <Card
                    bordered={false}
                    title={
                        <Tooltip placement="bottom" title={thongTinChiTiet.tieuDe}>
                        <Title
                            level={3}
                            // style={{ position: 'absolute' }}
                            ellipsis={true}
                            onMouse
                        >{thongTinChiTiet.tieuDe}
                        </Title>
                      </Tooltip>
                        }
                    className="ql-editor"

                    actions={[<p>{moment(thongTinChiTiet.ngayDang).format('DD/MM/YYYY')}</p>]}
                    bodyStyle={{ minHeight: 400 }}
                >
                    <Card.Meta
                        avatar={
                            thongTinChiTiet.anhDaiDien != undefined ? (
                                <Avatar src={_.get(thongTinChiTiet, 'anhDaiDien', null)} size="large" />
                            ) : (
                                    <Avatar icon="user" size="large" />
                                )
                        }
                        description={thongTinChiTiet.moTa}
                    />
                    {thongTinChiTiet.noiDung !== undefined ? (
                        <p style={{ marginTop: 20 }} dangerouslySetInnerHTML={{ __html: thongTinChiTiet.noiDung }} />
                    ) : null}
                </Card>
            </div>
        )
    }
}
export default ModalNews

