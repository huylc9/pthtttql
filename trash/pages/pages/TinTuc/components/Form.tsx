/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Form, Input, Card, Row, Col, Button, DatePicker, Select, Spin, Divider, Modal, Typography, Avatar } from 'antd';
import _ from 'lodash';
import { getRecordValue, includes, chuanHoa } from '@/utils/utils';
import rules from '@/utils/rules';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import { formItemLayout, tailFormItemLayout, handleValuesChange, handleCommonSubmit, getSubmitText } from '@/utils/form';
import { connect } from 'dva';
import { IBase } from '@/utils/base';
import Upload from '@/components/Upload/Upload';
import TextEditor from '@/components/TextEditor/TextEditor';
import moment from 'moment';

type TinTuc_Props = {
  model: { edit, record },
  cond: object,
  loading: boolean,
  dispatch: Function,
  form: { validateFieldsAndScroll, getFieldDecorator },
  login: IBase,
  loaitintuc: IBase,
}

@connect(({ loaitintuc, login }) => ({
  loaitintuc,
  login,
}))

class TinTuc extends React.Component<TinTuc_Props> {
  componentDidMount() {
    this.props.dispatch({
      type: 'loaitintuc/get',
      payload: {
        page: 1,
        limit: 1000,
        cond: {},
      },
    });
  }

  handleSubmit = e => {
    const { form, model: { edit, record }, cond, login } = this.props;
    e.preventDefault();
    form.validateFieldsAndScroll((err, values) => {
      if (err) return;
      values = {
        nguoiDang: login._id,
        ...values,
      }
      values.anhDaiDien = _.get(values, 'anhDaiDien.fileList[0].originFileObj', undefined);
      values.noiDung = values.noiDung.text;
      handleCommonSubmit(values, this.props);
    });
  };

  handleView = () => {
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        Modal.info({
          title: 'Xem trước bài viết',
          width: 1000,
          content: (
            <Card
              bordered={false}
              title={<Typography.Title level={3}>{values.tieuDe}</Typography.Title>}
              className="ql-editor"
              actions={[<p>{moment(values.ngayDang).format('DD/MM/YYYY')}</p>]}
              bodyStyle={{ minHeight: 400 }}
            >
              <Card.Meta
                avatar={
                  values.anhDaiDien != undefined ? (
                    <Avatar
                      src={
                        _.get(values, 'anhDaiDienTin.fileList[0].thumbUrl', null) ||
                        _.get(values, 'anhDaiDienTin[0].url', null)
                      }
                      size="large"
                    />
                  ) : (
                      <Avatar icon="user" size="large" />
                    )
                }
                description={values.moTa}
              />
              {values.noiDung !== undefined ? (
                <p
                  style={{ marginTop: 20 }}
                  dangerouslySetInnerHTML={{ __html: _.get(values, 'noiDung.text', '') }}
                />
              ) : null}
            </Card>
          ),
        });
      }
    });
  };

  render() {
    const { model, form: { getFieldDecorator }, cond, loading, login, loaitintuc: { danhSach } } = this.props;
    return (
      <div className="box">
        <Card
          bordered
          title={
            <div className="cardTitle">{!model.edit ? `Thêm mới` : `Chỉnh sửa`}</div>
          }
        >
          <GridContent>
            <Row>
              <Col span={12}>
                <Spin spinning={loading}>
                  <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                    <Form.Item label="Tiêu đề">
                      {getFieldDecorator('tieuDe', {
                        initialValue: getRecordValue(model, cond, 'tieuDe', ''),
                        rules: [...rules.required, ...rules.text],
                      })(<Input />)}
                    </Form.Item>
                    <Form.Item label="Loại tin tức">
                      {getFieldDecorator('loaiTinTuc', {
                        initialValue: !!getRecordValue(model, cond, 'loaiTinTuc') ? !!getRecordValue(model, cond, 'loaiTinTuc')._id : undefined,
                        rules: [...rules.required],
                      })(
                        <Select
                          showSearch
                          filterOption={(inputValue, option) => includes(inputValue, option.chuDe)}
                        >
                          {danhSach.map((item, index) => (
                            <Select.Option key={index} value={item._id}>
                              {item.chuDe}
                            </Select.Option>
                          ))}
                        </Select>
                      )}
                    </Form.Item>
                    <Form.Item label="Mô tả">
                      {getFieldDecorator('moTa', {
                        initialValue: getRecordValue(model, cond, 'moTa', ''),
                        rules: [...rules.required],
                      })(<Input.TextArea rows={3} />)}
                    </Form.Item>
                    <Form.Item label="Ảnh đại diện">
                      {getFieldDecorator('anhDaiDien', {
                        initialValue:
                          model.edit && model.record.anhDaiDien && model.record.anhDaiDien.length > 0
                            ? {
                              fileList: [
                                {
                                  uid: 'zzz',
                                  name: 'Ảnh',
                                  url: model.record.anhDaiDien,
                                  status: 'done',
                                },
                              ],
                            }
                            : { fileList: [] },
                        rules: [],
                      })(<Upload />)}
                    </Form.Item>
                    <Form.Item label={<span>Nội dung</span>}>
                      {getFieldDecorator('noiDung', {
                        rules: [
                          {
                            validator: (_, value, callback) => {
                              const { text } = value;
                              if (!text || !text.length || !text[0] || !chuanHoa(text).length)
                                callback('');
                              callback();
                            },
                            message: 'Hãy nhập nội dung',
                          },
                        ],
                        initialValue: model.edit ? { text: model.record.noiDung } : { text: '' },
                      })(<TextEditor />)}
                    </Form.Item>
                    <Form.Item label="Ngày đăng">
                      {getFieldDecorator('ngayDang', {
                        initialValue: model.edit ? moment(model.record.ngayDang) : moment(),
                        rules: model.edit ? [] : [...rules.sauHomNay],
                      })(<DatePicker format="DD/MM/YYYY" />)}
                    </Form.Item>
                    <Form.Item {...tailFormItemLayout}>
                      <Button.Group>
                        <Button type="default" icon="eye" onClick={this.handleView}>
                          Xem trước
                        </Button>
                        <Divider type="vertical" />
                        <Button type="primary" icon="plus" htmlType="submit">
                          {getSubmitText(model)}
                        </Button>
                      </Button.Group>
                    </Form.Item>
                  </Form>
                </Spin>
              </Col>
            </Row>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedForm = Form.create({ name: 'TinTuc', onValuesChange: handleValuesChange })(TinTuc);

export default WrappedForm;
