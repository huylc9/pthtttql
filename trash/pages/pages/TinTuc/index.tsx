import React from 'react';
import { connect } from 'dva';
import uuidv1 from 'uuid/v1';
import { Button, Divider, Popconfirm, Typography, Modal } from 'antd';
import { IColumn } from '@/utils/interfaces';
import Form from './components/Form';
import TableBase from '@/components/Table/Table.tsx';
import { IBase } from '@/utils/base';
import moment from 'moment';
import TagPopover from '@/components/TagPopover/TagPopover';
import ModalNews from './components/ModalNews';
import { renderParagraph } from '@/utils/table';

type Props = {
  tintuc: IBase,
  loading: boolean,
  dispatch: Function,
  modal: boolean,
  thongTinChiTiet: object,
}

@connect(({ tintuc, loading }) => ({
  tintuc,
  modal: tintuc.modal,
  thongTinChiTiet: tintuc.thongTinChiTiet,
  loading: loading.models.tintuc,
}))
class TinTuc extends React.Component<Props> {
  state = {
  };

  componentDidMount() {
  }

  handleView = record => {
    this.props.dispatch({
      type: 'tintuc/changeState',
      payload: {
        modal: true,
        thongTinChiTiet: record,
      },
    });
  }

  handleEdit = record => {
    this.props.dispatch({
      type: 'tintuc/changeState',
      payload: {
        showDrawer: true,
        edit: true,
        record,
        key: uuidv1(),
        isTouched: false,
      },
    });
  };

  handleDel = _id => {
    this.props.dispatch({
      type: 'tintuc/del',
      payload: {
        _id,
        key: uuidv1(),
      },
    });
  };

  public handleOk = e => {
    this.props.dispatch({
      type: 'tintuc/changeState',
      payload: {
        modal: false,
      },
    });
  };

  public handleCancel = e => {
    this.props.dispatch({
      type: 'tintuc/changeState',
      payload: {
        modal: false,
      },
    });
  };

  render() {
    const { modal, thongTinChiTiet } = this.props;
    const actions = [
      {
        node: (
          <Button
            style={{ backgroundColor: '#096dd9', color: '#fff' }}
            type="primary"
            shape="circle"
            icon="eye"
            title="Xem Thông Tin"
          />
        ),
        click: 'handleView'
      }]

    const functions = {
      handleView: this.handleView,
    };

    const renderLast = (value, record) => (
      <TagPopover
        record={record}
        handleEdit={this.handleEdit}
        handleDel={this.handleDel}
        actions={actions}
        functions={functions}
      />
    )

    const columns: IColumn[] = [
      {
        title: 'STT',
        dataIndex: 'index',
        align: 'center',
        width: '80px'
      },
      {
        title: 'Tiêu đề',
        dataIndex: 'tieuDe',
        align: 'center',
        // width: 200,
        render: val => renderParagraph(val, 3),
      },
      {
        title: 'Loại tin tức',
        dataIndex: 'loaiTinTuc',
        align: 'center',
        // width: 200,
        render: loaiTinTuc => loaiTinTuc && loaiTinTuc.chuDe || ''
      },
      {
        title: 'Ngày tạo',
        dataIndex: 'createdAt',
        align: 'center',
        width: '150px',
        render: value => moment(value).format('DD/MM/YYYY'),
        search: 'sort',
      },
      {
        title: 'Ngày đăng',
        dataIndex: 'ngayDang',
        width: 200,
        align: 'center',
        render: value => moment(value).format('DD/MM/YYYY'),
      },
      {
        title: 'Thao tác',
        align: 'center',
        render: (value, record) => renderLast(value, record),
        fixed: 'right',
        width: 110,
      }
    ];

    return (
      <React.Fragment>
        <Modal
          visible={modal}
          onOk={this.handleOk}
          width='80vw'
          closable='true'
          onCancel={this.handleCancel}
          footer={[
            <Button
              type="primary"
              key="back"
              onClick={this.handleOk}
            >
              OK
            </Button>
          ]}
        >
          <ModalNews thongTinChiTiet={thongTinChiTiet} />,
        </Modal>
        <TableBase
          model={this.props.tintuc}
          modelName="tintuc"
          loading={this.props.loading}
          dispatch={this.props.dispatch}
          columns={columns}
          cond={{}}
          Form={Form}
          title={'Quản lý danh sách bài viết'}
          hasCreate
          tableProp={{
            scroll: { x: 1000 }
          }}
        />
      </React.Fragment>
    );
  }
}

export default TinTuc;
