import base from '@/utils/base';
import modelExtend from 'dva-model-extend';
import Services from '@/pages/TinTuc/service';

export default modelExtend(base(Services), {
    namespace: 'tintuc1',
    state: {
        modal: false,
        thongTinChiTiet: {},
    }
})
