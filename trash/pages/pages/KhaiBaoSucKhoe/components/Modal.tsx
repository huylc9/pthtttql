import React from 'react';
import { connect } from 'dva';
import { IBase } from '@/utils/base';
import { Modal, Card, Table } from 'antd';
import _ from 'lodash';
import data from '@/utils/data';
import moment from 'moment';
type Props = {
  khaibaosuckhoe: IBase;
  loading: boolean;
  dispatch: Function;
};

@connect(({ khaibaosuckhoe, loading }) => ({
  khaibaosuckhoe,
  loading: loading.models.khaibaosuckhoe,
}))
class ViewModal extends React.Component<Props> {
  componentDidMount() { }

  handleOk = () => {
    this.props.dispatch({
      type: 'khaibaosuckhoe/changeState',
      payload: {
        visible: false,
      },
    });
  };

  handleCancel = () => {
    this.props.dispatch({
      type: 'khaibaosuckhoe/changeState',
      payload: {
        visible: false,
      },
    });
  };

  onChange = pagination => {
    const { current: page, pageSize: limit } = pagination;
    const {
      record: { maSv },
    } = this.props.khaibaosuckhoe;
    this.props.dispatch({
      type: 'khaibaosuckhoe/getByMSV',
      payload: {
        maSv,
        page,
        limit,
      },
    });
  };

  render() {
    const { khaibaosuckhoe, loading } = this.props;
    let {
      danhSachGui,
      pagingModal: { page, limit },
      visible,
      totalModal,
    } = khaibaosuckhoe;
    danhSachGui = danhSachGui.map((item: any, index: any) => ({
      ...item,
      index: index + 1 + (page - 1) * limit,
    }));

    const columns = [
      {
        title: 'STT',
        dataIndex: 'index',
        align: 'center',
        width: '150px',
      },
      {
        title: 'Thời gian khai báo',
        dataIndex: 'thoiGianKhaiBao',
        align: 'center',
        width: 110,
        render: val => (
          <span title={moment(val).format('DD/MM/YYYY HH:mm:ss')}>
            {moment(val).format('DD/MM/YYYY')}
          </span>
        ),
        search: 'sort',
      },
      {
        title: 'Trạng Thái',
        dataIndex: 'trangThai',
        align: 'center',
        width: '150px',
        render: arr =>
          Array.isArray(arr) && arr.map((value: any) => data.trangThaiSucKhoe[value]).join(),
      },
      {
        title: 'Đã Tiếp Xúc',
        dataIndex: 'tiepXuc',
        align: 'center',
        width: '150px',
        render: arr => Array.isArray(arr) && arr.map((value: any) => data.tiepXuc[value]).join(),
      },

      {
        title: 'Các nơi đã đi qua',
        dataIndex: 'cacNoiDaDiQua',
        align: 'center',
        width: '150px',
      },
      {
        title: 'Sốt',
        dataIndex: 'sot',
        align: 'center',
        width: '150px',
      },
    ];
    return (
      <Modal
        title="Danh sách khai báo sức khỏe chi tiết"
        visible={visible}
        destroyOnClose
        onOk={this.handleOk}
        onCancel={this.handleCancel}
        width="80%"
      >
        <Card bordered={false} bodyStyle={{ minHeight: 400 }}>
          <Table
            style={{ marginBottom: 10 }}
            loading={loading}
            bordered
            onChange={this.onChange}
            columns={columns}
            dataSource={danhSachGui}
            pagination={{
              position: 'top',
              total: totalModal,
              showSizeChanger: true,
              pageSizeOptions: ['10', '20', '30', '40'],
            }}
          />
        </Card>
      </Modal>
    );
  }
}

export default ViewModal;
