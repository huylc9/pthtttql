import { Button, Tag } from 'antd';
import { connect } from 'dva';
import moment from 'moment';
import React from 'react';
import TableBase from '@/components/Table/Table.tsx';
import { IBase } from '@/utils/base';
import data from '@/utils/data';
import { IColumn } from '@/utils/interfaces';
import ThongKeTheoNgay from '../ThongKeKhaiBaoSucKhoeTheoNgay';
import Form from './components/Form';
import ViewModal from './components/Modal';

interface Props {
  khaibaosuckhoe: IBase;
  loading: boolean;
  dispatch: Function;
}

@connect(({ khaibaosuckhoe, loading }) => ({
  khaibaosuckhoe,
  loading: loading.models.khaibaosuckhoe,
}))
class KhaiBaoSucKhoe extends React.Component<Props> {
  public state = {};

  public handleView = (record: any) => {
    this.props.dispatch({
      type: 'khaibaosuckhoe/changeState',
      payload: {
        record,
        visible: true,
      },
    });
    this.props.dispatch({
      type: 'khaibaosuckhoe/getByMSV',
      payload: {
        maSv: record.maSv,
        page: 1,
        limit: 10,
      },
    });
  };

  public render() {
    const renderLast = (value: any, record: any) => (
      <React.Fragment>
        <Button
          type="primary"
          shape="circle"
          icon="eye"
          onClick={() => this.handleView(record)}
          title="Xem trước"
        />
      </React.Fragment>
    );

    const columns: IColumn[] = [
      {
        title: 'STT',
        dataIndex: 'index',
        align: 'center',
        width: '150px',
      },
      {
        title: 'Họ và Tên',
        dataIndex: 'nguoiDung',
        align: 'center',
        width: '150px',
        render: item => _.get(item, 'hoTen', ''),
      },
      {
        title: 'Mã sinh viên',
        dataIndex: 'maSv',
        align: 'center',
        width: '150px',
      },
      {
        title: 'Thời gian khai báo',
        dataIndex: 'thoiGianKhaiBao',
        align: 'center',
        width: 120,
        render: val => (
          <span title={moment(val).format('DD/MM/YYYY HH:mm:ss')}>
            {moment(val).format('DD/MM/YYYY')}
          </span>
        ),
        search: 'sort',
      },
      {
        title: 'Trạng Thái',
        dataIndex: 'trangThai',
        align: 'center',
        width: '150px',
        render: arr =>
          Array.isArray(arr) && arr.map((value: any) => data.trangThaiSucKhoe[value]).join(),
      },
      {
        title: 'Đã Tiếp Xúc',
        dataIndex: 'tiepXuc',
        align: 'center',
        //width: '150px',
        render: arr => Array.isArray(arr) && arr.map((value: any) => data.tiepXuc[value]).join(),
      },

      {
        title: 'Sốt',
        dataIndex: 'sot',
        align: 'center',
        width: '80px',
        search: 'filter',
      },

      {
        title: 'An toàn',
        dataIndex: 'anToan',
        align: 'center',
        width: '150px',
        render: val =>
          val ? <Tag color="blue">An Toàn</Tag> : <Tag color="red">Không an toàn</Tag>,
      },
      {
        title: 'Các nơi đã đi qua',
        dataIndex: 'cacNoiDaDiQua',
        align: 'center',
        width: '150px',
      },
      {
        title: 'Thao tác',
        align: 'center',
        render: (value, record) => renderLast(value, record),
        fixed: 'right',
        width: 140,
      },
    ];

    return (
      <div>
        <TableBase
          model={this.props.khaibaosuckhoe}
          modelName="khaibaosuckhoe"
          loading={this.props.loading}
          dispatch={this.props.dispatch}
          columns={columns}
          cond={{}}
          Form={Form}
          title="Thông tin khai báo sức khỏe"
          tableProp={{
            scroll: { x: 1500 },
          }}
        ></TableBase>
        <ViewModal />
        <ThongKeTheoNgay />
      </div>
    );
  }
}

export default KhaiBaoSucKhoe;
