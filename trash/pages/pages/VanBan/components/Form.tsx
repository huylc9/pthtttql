/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Form, Input, Card, Row, Col, Button } from 'antd';
import _ from 'lodash';
import { connect } from 'dva';
import rules from '@/utils/rules';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import { formItemLayout, tailFormItemLayout } from '@/utils/form';
import UploadOne from '@/components/Upload/UploadOne';
import { IBase } from '@/utils/base';

const { TextArea } = Input;
type Form_Props = {
  vanban: IBase;
  loading: boolean;
  dispatch: Function;
  form: { validateFieldsAndScroll: any; getFieldDecorator: any };
};

@connect(({ vanban, loading }) => ({
  vanban,
  loading: loading.models.vanban,
}))
class TaiLieuVanBanForm extends React.Component<Form_Props> {
  handleSubmit = (e: any) => {
    const { vanban, idVanBan } = this.props;
    const { record, edit } = vanban;
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (err) return;
      values.taiLieu = _.get(values, 'taiLieu.fileList[0].originFileObj', undefined);
      if (edit) {
        this.props.dispatch({
          type: 'vanban/updateTaiLieu',
          payload: {
            ...values,
            idVanBan,
            idTaiLieu: _.get(record, '_id', -1),
          },
        });
      } else {
        this.props.dispatch({
          type: 'vanban/addTaiLieu',
          payload: {
            ...values,
            idVanBan,
          },
        });
      }

      this.props.dispatch({
        type: `vanban/changeState`,
        payload: {
          showDrawer: false,
          edit: false,
          record: {},
        },
      });
    });
  };

  render() {
    const { vanban } = this.props;
    const { getFieldDecorator } = this.props.form;
    const { record, edit } = vanban;
    return (
      <div className="box">
        <Card bordered title={<div className="cardTitle">{!edit ? 'Thêm mới' : 'Sửa'}</div>}>
          <GridContent>
            <Row>
              <Col span={18}>
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                  <Form.Item label="Tên tài liệu">
                    {getFieldDecorator('ten', {
                      initialValue: edit ? _.get(record, 'ten', '') : '',
                      rules: [...rules.required, ...rules.text],
                    })(<Input.TextArea rows={2} />)}
                  </Form.Item>
                  {!edit && (
                    <Form.Item label="Mô tả">
                      {getFieldDecorator('moTa', {
                        initialValue: edit ? _.get(record, 'moTa', '') : '',
                        rules: [...rules.required, ...rules.text],
                      })(<TextArea rows={7} />)}
                    </Form.Item>
                  )}
                  <Form.Item label="Tài liệu">
                    {getFieldDecorator('taiLieu', {
                      initialValue:
                        edit && record.taiLieu && record.taiLieu.length > 0
                          ? {
                              fileList: [
                                {
                                  uid: 'zzz',
                                  name: record.ten,
                                  url: record.taiLieu,
                                  status: 'done',
                                },
                              ],
                            }
                          : { fileList: [] },
                      rules: [
                        {
                          validator: (_, value, callback) => {
                            if (value.fileList.length === 0) callback('');
                            callback();
                          },
                          message: 'Không có file được tải lên',
                        },
                        ...rules.required,
                      ],
                    })(<UploadOne />)}
                  </Form.Item>
                  <Form.Item {...tailFormItemLayout}>
                    <Button.Group>
                      <Button type="primary" icon="plus" htmlType="submit">
                        {edit ? 'Cập nhật' : 'Thêm mới'}
                      </Button>
                    </Button.Group>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedQuanTriBaiVietForm = Form.create({ name: 'TaiLieuVanBanForm' })(TaiLieuVanBanForm);

export default WrappedQuanTriBaiVietForm;
