/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Row, Col, Button, Card, Table, Input, Icon, Popover, Divider, Popconfirm } from 'antd';
import { connect } from 'dva';
import uuidv1 from 'uuid/v1';
import _ from 'lodash';
import PropTypes, { ReactNodeLike } from 'prop-types';
import FormDrawer from '@/components/Drawer/FormDrawer';
import { toRegex, isValue } from '@/utils/utils';
import InputSearch from '@/components/InputSearch/InputSearch';
import data from '@/utils/data';
import { IBase } from '@/utils/base';

const Nhi = uuidv1();

type CustomTable_Props = {
  vanban: IBase;
  model: string;
  cond: object;
  columns: Array<object>;
  Form: ReactNodeLike;
  firstAtt: string;
  title: string;
  actions: Array<object>;
  otherProps: object;
  hasDelete: boolean;
  hasEdit: boolean;
  hasCreate: boolean;
  hasSearch: boolean;
  loading: boolean;
  dispatch: Function;
  idVanBan: string;
  scroll?: object;
  form: { validateFieldsAndScroll; getFieldDecorator };
};

@connect(({ vanban, loading }) => ({
  vanban,
  loading: loading.models.vanban,
}))
class CustomTable extends React.Component<CustomTable_Props> {
  componentDidMount() {
    const { model, idVanBan } = this.props;
    // console.log('model, cond', model, cond);
    this.props.dispatch({
      type: `${model}/getById`,
      id: idVanBan,
    });
  }

  changeState = filters => {
    const { vanban } = this.props;
    const { filterInfo } = vanban;
    this.props.dispatch({
      type: `vanban/changeState`,
      payload: {
        filterInfo: {
          ...filterInfo,
          ...filters,
        },
      },
    });
  };

  handleDel = _id => {
    this.props.dispatch({
      type: `vanban/delTaiLieu`,
      payload: {
        idTaiLieu: _id,
        idVanBan: this.props.idVanBan,
      },
    });
  };

  handleThem = () => {
    this.props.dispatch({
      type: `vanban/changeState`,
      payload: {
        showDrawer: true,
        edit: false,
        record: {},
      },
    });
  };

  handleEdit = record => {
    this.props.dispatch({
      type: `vanban/changeState`,
      payload: {
        showDrawer: true,
        edit: true,
        record,
      },
    });
  };

  render() {
    const {
      loading,
      vanban,
      title,
      Form,
      columns,
      cond,

      actions,
      hasDelete,
      hasEdit,
      hasCreate,
      hasSearch,
      scroll,
      idVanBan,
      ...otherProps
    } = this.props;
    let { danhSach, total, paging, vanBanDangChon } = vanban;
    danhSach = vanBanDangChon.taiLieu || [];
    const { page, limit } = paging;
    danhSach = danhSach.map((item, index) => ({
      ...item,
      index: index + 1 + (page - 1) * limit,
    }));
    total = danhSach.length;

    // các actions ở cuối
    const list = (text, record) => (
      <React.Fragment>
        {actions && actions(record).map(x => x)}
        {hasEdit && (
          <React.Fragment>
            <Button
              type="primary"
              shape="circle"
              icon="edit"
              onClick={() => this.handleEdit(record)}
              title="Chỉnh sửa"
            />
            <Divider type="vertical" />
          </React.Fragment>
        )}
        {hasDelete && (
          <Popconfirm title="Bạn có chắc muốn xóa?" onConfirm={() => this.handleDel(record._id)}>
            <Button type="danger" shape="circle" icon="delete" title="Xóa" />
          </Popconfirm>
        )}
      </React.Fragment>
    );

    const last = {
      // phần tử cuối của columns
      title: 'Tùy chọn',
      width: '120px',
      align: 'center',
      fixed: 'right',
      render: (text, record) => {
        const numOfActions = parseInt((actions(record).length + 1) / 2) + !!hasEdit + !!hasCreate;
        return numOfActions >= 3 ? (
          <Popover
            content={list(text, record)}
            title="Thao tác"
            style={{ display: 'flex', justifyContent: 'center' }}
          >
            <Button type="primary">
              <Icon type="edit" />
            </Button>
          </Popover>
        ) : (
            list(text, record)
          );
      },
    };

    if (actions) columns.push(last);
    return (
      <div>
        <Card bordered>
          <Row>
            <Col xs={24}>
              {hasCreate && (
                <Button
                  style={{ marginRight: '20%' }}
                  type="primary"
                  // shape="round"
                  icon="plus"
                  // disabled
                  onClick={() => this.handleThem()}
                >
                  Thêm mới
                </Button>
              )}

              <h3 style={{ display: 'inline-block', margin: '0 10px 10px 50px', float: 'right' }}>
                Tổng số :
                <Input
                  style={{ width: '70px', fontWeight: 700, fontSize: 18 }}
                  value={total}
                  readOnly
                />
              </h3>

              <Table
                style={{ marginTop: 10 }}
                loading={loading}
                bordered
                scroll={scroll}
                columns={columns}
                dataSource={danhSach}
                onChange={this.onChange}
                pagination={{
                  current: page,
                  position: 'bottom',
                  total,
                  showSizeChanger: true,
                  pageSizeOptions: ['10', '25', '50', '100'],
                }}
                {...otherProps}
              />
            </Col>
          </Row>
        </Card>
        <FormDrawer name="vanban">
          <Form cond={cond} key={vanban.record._id || Nhi} idVanBan={idVanBan} />
        </FormDrawer>
      </div>
    );
  }
}

export default CustomTable;
