/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Form, Input, Card, Row, Col, Button, Drawer, Spin } from 'antd';
import _ from 'lodash';
import { connect } from 'dva';
import rules from '@/utils/rules';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import {
  formItemLayout,
  tailFormItemLayout,
  getSubmitText,
  handleValuesChange,
} from '@/utils/form';
import { IBase } from '@/utils/base';

type VanBan_Form = {
  model: { edit; record; isTouched };
  loading: boolean;
  dispatch: Function;
  form: { validateFieldsAndScroll; getFieldDecorator };
};

class VanBanForm extends React.Component<VanBan_Form> {
  handleSubmit = e => {
    const { model } = this.props;
    const { record, edit, isTouched } = model;
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (err) return;
      // values.taiLieu = _.get(values, 'taiLieu.fileList[0].originFileObj', undefined);
      if (edit) {
        if (isTouched) {
          this.props.dispatch({
            type: 'vanban/upd',
            payload: {
              ...values,
              _id: _.get(record, '_id', -1),
            },
          });
        } else {
          this.props.dispatch({
            type: 'vanban/changeState',
            payload: {
              showDrawer: false,
            },
          });
        }
      } else {
        this.props.dispatch({
          type: 'vanban/add',
          payload: {
            ...values,
          },
        });
      }
    });
  };

  onClose = () => {
    this.props.dispatch({
      type: `vanban/changeState`,
      payload: {
        showDrawer: false,
      },
    });
  };

  render() {
    const { loading, model } = this.props;
    const { getFieldDecorator } = this.props.form;
    const { edit, record } = model;
    return (
      <div>
        <Card bordered title={<div className="cardTitle">{!edit ? 'Thêm mới' : 'Sửa'}</div>}>
          <GridContent>
            <Row>
              <Col span={18}>
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                  <Form.Item label="Tên thư mục">
                    {getFieldDecorator('tenThuMuc', {
                      initialValue: edit ? _.get(record, 'tenThuMuc', '') : '',
                      rules: [...rules.required, ...rules.text],
                    })(<Input />)}
                  </Form.Item>

                  <Form.Item label="Mô tả">
                    {getFieldDecorator('moTaThuMuc', {
                      initialValue: edit ? _.get(record, 'moTaThuMuc', '') : '',
                      // rules: [...rules.required],
                    })(<Input.TextArea rows={7} />)}
                  </Form.Item>

                  <Form.Item {...tailFormItemLayout}>
                    <Button.Group>
                      <Spin spinning={loading}>
                        <Button type="primary" icon="plus" htmlType="submit">
                          {getSubmitText(model)}
                        </Button>
                      </Spin>
                    </Button.Group>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedQuanTriBaiVietForm = Form.create({
  name: 'vanban',
  onValuesChange: handleValuesChange,
})(VanBanForm);

export default WrappedQuanTriBaiVietForm;
