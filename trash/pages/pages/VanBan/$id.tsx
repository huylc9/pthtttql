/* eslint-disable react/destructuring-assignment */
import { Breadcrumb, Card, Divider, Icon, Spin, Typography, Upload } from 'antd';
import { connect } from 'dva';
import _ from 'lodash';
import React, { Component } from 'react';
import { router } from 'umi';
import uuidv1 from 'uuid/v1';
import { IBase } from '@/utils/base';
import CustomTable from './components/CustomTable.tsx';
import Form from './components/Form';


const key = uuidv1();

interface id_Props {
  vanban: IBase;
  loading: boolean;
  dispatch: Function;
}

@connect(({ loading, vanban }) => ({
  loading: loading.models.vanban,
  vanban,
}))
class ThongTinVanBan extends Component<id_Props> {
  public componentDidMount() {
    const id = _.get(this, 'props.match.params.id', undefined);
    if (id) {
      this.props.dispatch({
        type: 'vanban/getById',
        id,
      });
    }
    this.goBack = this.goBack.bind(this);
  }

  // eslint-disable-next-line class-methods-use-this
  public goBack() {
    router.push('/admin/vanban');
  }

  public render() {
    const { loading } = this.props;
    const columns = [
      {
        title: 'STT',
        dataIndex: 'index',
        align: 'center',
        width: '100px',
      },
      {
        title: 'Tên văn bản',
        dataIndex: 'ten',
        align: 'center',
        // width: '300px',
      },
      {
        title: 'Mô tả',
        dataIndex: 'ten',
        align: 'center',
        // width: '300px',
      },
      {
        title: 'Văn bản đính kèm',
        dataIndex: 'taiLieu',
        align: 'center',
        width: '300px',
        render: _url => {
          const url = _url || undefined;
          // console.log('url', url);
          return (
            <div
              style={{
                wordWrap: 'break-word',
                'word-break': 'break-all',
                'max-width': '300px',
              }}
            >
              <Upload
                fileList={
                  url
                    ? [
                      {
                        uid: url,
                        name:
                          url &&
                          url
                            .toString()
                            .split('/')
                            .pop()
                            .substring(26),
                        status: 'done',
                        url,
                        key: uuidv1(),
                      },
                    ]
                    : []
                }
                dizzsabled
              />
            </div>
          );
        },
      },
    ];
    const idVanBan = _.get(this, 'props.match.params.id', undefined);

    return (
      <Spin spinning={false}>
        <Card
          bordered
          title={
            <Breadcrumb separator=">">
              <Breadcrumb.Item
                onClick={() => router.push('/admin/vanban')}
                onMouseOver={evt => (evt.target.style.cursor = 'pointer')}
              >
                <a>
                  <Typography.Text underline>Danh sách văn bản</Typography.Text>
                </a>
              </Breadcrumb.Item>
              <Breadcrumb.Item>{this.props.vanban.vanBanDangChon.tenThuMuc}</Breadcrumb.Item>
            </Breadcrumb>
          }
        >
          <Card
            bordered
            title={
              <div>
                <span onClick={this.goBack}>
                  <a>
                    <Icon type="arrow-left" style={{ fontSize: 18 }} title="Trở về" />
                  </a>
                </span>
                <Divider type="vertical" />
                <span style={{ fontSize: 16, textTransform: 'capitalize', fontWeight: 550 }}>
                  Thông tin văn bản
                </span>
              </div>
            }
          >
            <Spin spinning={loading}>
              <CustomTable
                key={key}
                model="vanban"
                idVanBan={idVanBan}
                columns={columns}
                scroll={{ x: 1300, y: 600 }}
                cond={{}}
                actions={record => []}
                Form={Form}
                title="Danh sách văn bản"
                hasEdit
                hasDelete
                hasCreate
                hasSearch
              />
            </Spin>
          </Card>
        </Card>
      </Spin>
    );
  }
}

export default ThongTinVanBan;
