/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Form, Input, Card, Row, Col, Button, DatePicker, Select, Spin } from 'antd';
import _ from 'lodash';
import { getRecordValue, includes } from '@/utils/utils';
import rules from '@/utils/rules';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import {
  formItemLayout,
  tailFormItemLayout,
  handleValuesChange,
  handleCommonSubmit,
  getSubmitText,
} from '@/utils/form';
import { connect } from 'dva';
import { IBase } from '@/utils/base';

type MonHoc_Props = {
  model: { edit; record };
  khoa: IBase;
  cond: object;
  loading: boolean;
  dispatch: Function;
  form: { validateFieldsAndScroll; getFieldDecorator };
};

@connect(({ khoa }) => ({
  khoa,
}))
class MonHoc extends React.Component<MonHoc_Props> {
  componentDidMount() {}

  handleSubmit = e => {
    const {
      form,
      model: { edit, record },
      cond,
    } = this.props;
    e.preventDefault();
    form.validateFieldsAndScroll((err, values) => {
      if (err) return;
      handleCommonSubmit(values, this.props);
    });
  };

  render() {
    const {
      model,
      form: { getFieldDecorator },
      cond,
      loading,
      khoa: { danhSach },
    } = this.props;
    return (
      <div className="box">
        <Card
          bordered
          title={<div className="cardTitle">{!model.edit ? 'Thêm mới' : 'Chỉnh sửa'}</div>}
        >
          <GridContent>
            <Row>
              <Col span={18}>
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                  <Form.Item label="Tên môn học">
                    {getFieldDecorator('tenMonHoc', {
                      initialValue: getRecordValue(model, cond, 'tenMonHoc', ''),
                      rules: [...rules.text, ...rules.required],
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Mã môn học">
                    {getFieldDecorator('maMonHoc', {
                      initialValue: getRecordValue(model, cond, 'maMonHoc', ''),
                      rules: [...rules.required],
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Số tín chỉ">
                    {getFieldDecorator('soTinChi', {
                      initialValue: getRecordValue(model, cond, 'soTinChi', 0),
                      rules: [...rules.number(100), ...rules.required],
                    })(<Input />)}
                  </Form.Item>
                  <Spin spinning={loading}>
                    <Form.Item {...tailFormItemLayout}>
                      <Button.Group>
                        <Button type="primary" icon="plus" htmlType="submit">
                          {getSubmitText(model)}
                        </Button>
                      </Button.Group>
                    </Form.Item>
                  </Spin>
                </Form>
              </Col>
            </Row>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedForm = Form.create({ name: 'MonHoc', onValuesChange: handleValuesChange })(MonHoc);

export default WrappedForm;
