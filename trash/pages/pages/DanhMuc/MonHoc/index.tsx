import React from 'react';
import { connect } from 'dva';
import uuidv1 from 'uuid/v1';
import { Button, Divider, Popconfirm } from 'antd';
import { IColumn } from '@/utils/interfaces';
import Form from './components/Form';
import TableBase from '@/components/Table/Table.tsx';
import { IBase } from '@/utils/base';
import SelectKhoa from '@/components/SelectKhoa';

type Props = {
  monhoc: IBase,
  khoa: IBase,
  loading: boolean,
  dispatch: Function,
}

@connect(({ monhoc, khoa, loading }) => ({
  monhoc,
  khoa,
  loading: loading.models.monhoc,
}))
class MonHoc extends React.Component<Props> {
  state = {
    khoa: undefined
  };

  componentDidMount() {
  }

  onSelectKhoa = _id => {
    this.setState({
      khoa: _id,
    });
  };

  handleEdit = record => {
    this.props.dispatch({
      type: 'monhoc/changeState',
      payload: {
        showDrawer: true,
        edit: true,
        record,
        key: uuidv1(),
        isTouched: false,
      },
    });
  };

  handleDel = _id => {
    this.props.dispatch({
      type: 'monhoc/del',
      payload: {
        _id,
        key: uuidv1(),
      },
    });
  };

  render() {
    const { khoa: { danhSach } } = this.props;
    const renderLast = (value, record) => (
      <React.Fragment>
        <Button
          type="primary"
          shape="circle"
          icon="edit"
          onClick={() => this.handleEdit(record)}
          title="Chỉnh sửa"
        />

        <Divider type="vertical" />

        <Popconfirm title="Bạn có chắc muốn xóa?" onConfirm={() => this.handleDel(record._id)}>
          <Button type="danger" shape="circle" icon="delete" title="Xóa" />
        </Popconfirm>

      </React.Fragment>
    )

    const columns: IColumn[] = [
      {
        title: 'STT',
        dataIndex: 'index',
        align: 'center',
        width: '80px',
      },
      {
        title: 'Tên môn học',
        dataIndex: 'tenMonHoc',
        align: 'center',
        width: '300px',
      },
      {
        title: 'Mã môn học',
        dataIndex: 'maMonHoc',
        align: 'center',
        // width: '120px',
      },
      {
        title: 'Số tín chỉ',
        dataIndex: 'soTinChi',
        align: 'center',
        // width: '120px',
      },
      {
        title: 'Thao tác',
        align: 'center',
        render: (value, record) => renderLast(value, record),
        fixed: 'right',
        width: 120,
      }
    ];

    return (
      <TableBase
        key={this.state.khoa}
        model={this.props.monhoc}
        modelName="monhoc"
        loading={this.props.loading}
        dispatch={this.props.dispatch}
        columns={columns}
        cond={{ idKhoa: this.state.khoa }}
        Form={Form}
        title={SelectKhoa(danhSach, this.onSelectKhoa, this.state.khoa)}
        hasCreate
        tableProp={{
          scroll: { x: 1000, y: 600 }
        }}
      />
    );
  }
}

export default MonHoc;
