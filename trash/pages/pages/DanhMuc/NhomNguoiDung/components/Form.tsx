/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Form, Input, Card, Row, Col, Button, DatePicker, Select, Spin, Transfer } from 'antd';
import _ from 'lodash';
import { getRecordValue, toRegex } from '@/utils/utils';
import rules from '@/utils/rules';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import { formItemLayout, tailFormItemLayout, handleValuesChange, handleCommonSubmit, getSubmitText } from '@/utils/form';
import { connect } from 'dva';
import { IBase } from '@/utils/base';

type NhomNguoiDung_Props = {
  model: { edit, record, isTouched },
  sinhvien: IBase,
  cond: object,
  loading: boolean,
  dispatch: Function,
  form: { validateFieldsAndScroll, getFieldDecorator },
}

type NhomNguoiDung_States = {
  targetKeys: Array<string>,
}

let data = {};

@connect(({ sinhvien }) => ({
  sinhvien
}))

class NhomNguoiDung extends React.Component<NhomNguoiDung_Props, NhomNguoiDung_States> {
  state = {
    targetKeys: []
  }

  componentDidMount() {
    this.props.dispatch({
      type: 'sinhvien/get',
      payload: {
        page: 1,
        limit: 20,
        cond: { vaiTro: 0 }
      }
    })
    const { model: { record: { danhSachNguoiDung } } } = this.props;
    if (danhSachNguoiDung) {
      this.setState({
        targetKeys: danhSachNguoiDung.filter(item => !!item.idUser).map(item => _.get(item, 'idUser._id', undefined))
      })
      danhSachNguoiDung.filter(item => !!item.idUser).map(item => this.addToData(_.get(item, 'idUser._id', ''), _.get(item, 'idUser.hoTen', '')));
    }
  }

  debounced = _.debounce((dir, val) => { this.handleSearch(dir, val) }, 500);

  handleSearch = (dir, hoTen) => {
    if (dir === 'right') return;
    this.props.dispatch({
      type: 'sinhvien/get',
      payload: {
        page: 1,
        limit: 20,
        cond: {
          vaiTro: 0,
          hoTen: toRegex(hoTen)
        }
      }
    })
  }

  handleChange = (targetKeys) => {
    this.props.model.isTouched = true;
    this.setState({ targetKeys });
  };

  renderItem = ({ hoTen }) => ({
    label: hoTen
  })

  addToData = (_id, hoTen) => {
    if (data[_id]) return;
    data[_id] = hoTen;
  }

  addToDanhSach = (dataSource, key) => {
    let flag = false;
    dataSource.map(item => {
      if (item.key === key) flag = true;
    })
    if (flag) return;
    dataSource.push({
      key,
      hoTen: data[key]
    })
  }

  handleSubmit = e => {
    const { form, model: { edit, record }, cond } = this.props;
    e.preventDefault();
    form.validateFieldsAndScroll((err, values) => {
      if (err) return;
      const { targetKeys: keys } = this.state;
      values.danhSachNguoiDung = keys;
      handleCommonSubmit(values, this.props);
    });
  };


  render() {
    const { model, form: { getFieldDecorator }, cond, loading, sinhvien: { danhSach } } = this.props;

    const dataSource = danhSach.map(item => ({
      ...item,
      key: item._id
    }));
    danhSach.filter(item => !!item).map(item => this.addToData(item._id, item.hoTen));
    const { targetKeys } = this.state;
    targetKeys.map(key => this.addToDanhSach(dataSource, key));
    return (
      <div className="box">
        <Card
          bordered
          title={
            <div className="cardTitle">{!model.edit ? `Thêm mới` : `Chỉnh sửa`}</div>
          }
        >
          <GridContent>
            <Row>
              <Col span={18}>
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                  <Form.Item label='Tên nhóm người dùng'>
                    {getFieldDecorator('tenNhom', {
                      initialValue: getRecordValue(model, cond, 'tenNhom', ''),
                      rules: [...rules.text, ...rules.required]
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label='Danh sách thành viên' />
                  <Transfer
                    showSearch
                    dataSource={dataSource}
                    targetKeys={targetKeys}
                    render={this.renderItem}
                    onChange={this.handleChange}
                    onSearch={(dir, val) => { this.debounced(dir, val) }}
                    listStyle={{
                      width: '45%',
                      height: 300,
                    }}
                    filterOption={() => true}
                  />
                  <Spin spinning={loading}>
                    <Form.Item {...tailFormItemLayout}>
                      <Button.Group>
                        <Button type="primary" icon="plus" htmlType="submit">
                          {getSubmitText(model)}
                        </Button>
                      </Button.Group>
                    </Form.Item>
                  </Spin>
                </Form>
              </Col>
            </Row>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedForm = Form.create({ name: 'NhomNguoiDung', onValuesChange: handleValuesChange })(NhomNguoiDung);

export default WrappedForm;
