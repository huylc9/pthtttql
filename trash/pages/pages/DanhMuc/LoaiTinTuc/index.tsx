import React from 'react';
import { connect } from 'dva';
import uuidv1 from 'uuid/v1';
import { Button, Divider, Popconfirm } from 'antd';
import { IColumn } from '@/utils/interfaces';
import Form from './components/Form';
import TableBase from '@/components/Table/Table.tsx';
import { IBase } from '@/utils/base';

type Props = {
  loaitintuc: IBase,
  loading: boolean,
  dispatch: Function,
}

@connect(({ loaitintuc, loading }) => ({
  loaitintuc,
  loading: loading.models.loaitintuc,
}))
class LoaiTinTuc extends React.Component<Props> {
  state = {
  };

  componentDidMount() {
  }

  handleEdit = record => {
    this.props.dispatch({
      type: 'loaitintuc/changeState',
      payload: {
        showDrawer: true,
        edit: true,
        record,
        key: uuidv1(),
        isTouched: false,
      },
    });
  };

  handleDel = _id => {
    this.props.dispatch({
      type: 'loaitintuc/del',
      payload: {
        _id,
        key: uuidv1(),
      },
    });
  };

  render() {
    const renderLast = (value, record) => (
      <React.Fragment>
        <Button
          type="primary"
          shape="circle"
          icon="edit"
          onClick={() => this.handleEdit(record)}
          title="Chỉnh sửa"
        />

        <Divider type="vertical" />

        <Popconfirm title="Bạn có chắc muốn xóa?" onConfirm={() => this.handleDel(record._id)}>
          <Button type="danger" shape="circle" icon="delete" title="Xóa" />
        </Popconfirm>

      </React.Fragment>
    )

    const columns: IColumn[] = [
      {
        title: 'STT',
        dataIndex: 'index',
        align: 'center',
        width: '80px',
      },
      {
        title: 'Tên loại tin tức',
        dataIndex: 'chuDe',
        align: 'center'
      },
      {
        title: 'Thao tác',
        align: 'center',
        render: (value, record) => renderLast(value, record),
        fixed: 'right',
        width: 120,
      }
    ];

    return (
      <TableBase
        model={this.props.loaitintuc}
        modelName="loaitintuc"
        loading={this.props.loading}
        dispatch={this.props.dispatch}
        columns={columns}
        cond={{}}
        Form={Form}
        title={'Quản lý loại tin tức'}
        hasCreate
        tableProp={{
          scroll: { x: 800, y: 600 }
        }}
      />
    );
  }
}

export default LoaiTinTuc;
