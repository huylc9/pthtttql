import Service from '@/services/BaseService';
import axios from '@/utils/axios.js';
import { ip } from '@/services/ip';
import { isValue, trim } from '@/utils/utils';

class ExtendedService extends Service {
  upd1 = async ({ _id }) => axios.put(`${ip}/${this.url}/admin/accept/${_id}`);

  upd2 = async payload => {
    const form = new FormData();
    const { _id } = payload;
    payload._id = undefined;
    Object.keys(payload).map(key => {
      if (isValue(payload[key])) {
        if (Array.isArray(payload[key])) {
          for (let i = 0; i < payload[key].length; i += 1) {
            form.append(key, payload[key][i]);
          }
          return;
        }
        form.set(key, trim(payload[key]));
      }
    });
    return axios.put(`${ip}/${this.url}/admin/process/${_id}`, form);
  };

  get = async payload => axios.get(`${ip}/${this.url}`, { params: payload });
}

export default new ExtendedService({ name: 'mot-cua-bang-diem', formData: false });
