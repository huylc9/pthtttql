import base from '@/utils/base';
import modelExtend from 'dva-model-extend';
import Services from '@/pages/MotCua/BangDiem/service';
import _ from 'lodash';
import notificationAlert from '@/components/Notification';

export default modelExtend(base(Services), {
  namespace: 'bangdiem',
  state: {
    danhSach1: [],
    danhSach2: [],
  },
  effects: {
    *baseGet({ payload }, { call, put, select }) {
      let currentPayload = { ...payload };
      if (!payload) {
        const modelName = Services.name;
        currentPayload = yield select(state => state[modelName].paging);
      }
      const arr = currentPayload.arr || 'danhSach';
      currentPayload.arr = undefined;
      const response = yield call(Services.get, currentPayload);
      const state = {
        paging: currentPayload,
        total: _.get(response, 'data.total', 0),
      }
      state[arr] = _.get(response, 'data.data', []);
      yield put({
        type: 'changeState',
        payload: state,
      });
    },
    *get({ payload }, { put }) {
      yield put({
        type: 'baseGet',
        payload: {
          arr: 'danhSach',
          ...payload
        }
      })
    },
    *get1({ payload }, { put }) {
      yield put({
        type: 'baseGet',
        payload: {
          arr: 'danhSach1',
          ...payload
        }
      })
    },
    *get2({ payload }, { put }) {
      yield put({
        type: 'baseGet',
        payload: {
          arr: 'danhSach2',
          ...payload
        }
      })
    },
    *accept({ payload }, { put, call }) {
      yield call(Services.upd1, payload);
      notificationAlert('success', 'Tiếp nhận thành công');
      yield put({ type: 'get' });
      yield put({ type: 'get1' });
    },
    *process({ payload }, { put, call }) {
      yield call(Services.upd2, payload);
      notificationAlert('success', 'Xử lý thành công');
      yield put({ type: 'get1' });
      yield put({ type: 'get2' });
    }
  }
})
