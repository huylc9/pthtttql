import React from 'react';
import { connect } from 'dva';
import Table1 from './components/Table1';
import Table2 from './components/Table2';
import Table3 from './components/Table3';
import { Tabs } from 'antd';

@connect(({ bangdiem1, bangdiem2, bangdiem3 }) => ({
  total1: bangdiem1.total,
  total2: bangdiem2.total,
  total3: bangdiem3.total,
}))
class BangDiem extends React.Component<Props> {
  state = {
    selectedKeys: '1'
  };

  componentDidMount() {
    this.props.dispatch({
      type: 'bangdiem2/get'
    })
    this.props.dispatch({
      type: 'bangdiem3/get'
    })
  }

  render() {
    const { total1, total2, total3 } = this.props;
    const title1 = 'Chưa tiếp nhận ' + '(' + total1 + ')';
    const title2 = 'Đang xử lý ' + '(' + total2 + ')';
    const title3 = 'Đã xử lý ' + '(' + total3 + ')';

    return (
      <>
        <Tabs type="card">
          <Tabs.TabPane tab={title1} key="1">
            <Table1 />
          </Tabs.TabPane>
          <Tabs.TabPane tab={title2} key="2">
            <Table2 />
          </Tabs.TabPane>
          <Tabs.TabPane tab={title3} key="3">
            <Table3 />
          </Tabs.TabPane>
        </Tabs>
      </>
    );
  }
}

export default BangDiem;
