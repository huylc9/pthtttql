import { Descriptions, Badge } from 'antd';
import React from 'react';
import moment from 'moment';
import { dataObj } from '@/utils/data';


export default class View extends React.Component {

    render() {
        const {record} = this.props;
        const {trangThai, tepDinhKem, loaiBangDiem, namHoc, hocKy, 
            diaChiNhan, maSv, createdAt, updatedAt, sinhVien} = record;
        const bangDiem = dataObj[loaiBangDiem] + (loaiBangDiem >= 1 ? `, năm học ${namHoc}` : '') + (loaiBangDiem === 2 ? `, kỳ học ${hocKy}` : '');
        return (
            <Descriptions title="Thông tin chi tiết" bordered column={{xs: 3, lg: 6}}>
              <Descriptions.Item label="Loại bảng điểm" span={3}>{bangDiem}</Descriptions.Item>
              <Descriptions.Item label="Địa chỉ nhận" span={3}>{diaChiNhan}</Descriptions.Item>
              <Descriptions.Item label="Mã sinh viên">{maSv}</Descriptions.Item>
            </Descriptions>
        )
    },
}