import React from 'react';
import { IColumn } from '@/utils/interfaces';
import moment from 'moment';
import data, { dataObj } from '@/utils/data';
import { renderParagraph } from '@/utils/table';
import TableBase from '@/components/Table/Table.tsx';
import { connect } from 'dva';

@connect(({ bangdiem1, loading }) => ({
  bangdiem1,
  loading: loading.models.bangdiem,
}))
class BangDiem extends React.Component {

  handleAccept = record => {
    this.props.dispatch({
      type: 'bangdiem1/upd',
      payload: {
        _id: record._id,
        trangThai: 'DA_TIEP_NHAN'
      },
    });
  };

  render() {
    const renderLast = (value, record) => (
      <React.Fragment>
        <a onClick={() => this.handleAccept(record)}>Duyệt</a>

      </React.Fragment>
    )

    const columns: IColumn[] = [
      {
        title: 'STT',
        dataIndex: 'index',
        align: 'center',
        width: '150px',
      },
      {
        title: 'Thời gian tạo',
        dataIndex: 'createdAt',
        align: 'center',
        width: 150,
        render: val => <span title={moment(val).format('DD/MM/YYYY HH:mm:ss')}>{moment(val).fromNow()}</span>,
        search: 'sort',
      },
      {
        title: 'Mã sinh viên',
        dataIndex: 'maSv',
        align: 'center',
        width: '150px',
        search: 'search',
      },
      {
        title: 'Loại bảng điểm',
        dataIndex: 'loaiBangDiem',
        align: 'center',
        width: '300px',
        render: (val, record) => {
          const { loaiBangDiem, namHoc, hocKy } = record;
          return data.loaiBangDiem[loaiBangDiem] + (loaiBangDiem >= 1 ? `, năm học ${namHoc}` : '') + (loaiBangDiem === 2 ? `, kỳ học ${hocKy}` : '');
        }
      },
      {
        title: 'Địa chỉ nhận',
        dataIndex: 'diaChiNhan',
        align: 'center',
        // width: '250px',
        search: 'search',
        render: val => renderParagraph(val, 3),
      },
      {
        title: 'Thao tác',
        align: 'center',
        render: (value, record) => renderLast(value, record),
        fixed: 'right',
        width: 110,
      }
    ];

    return (
      <>
        <TableBase
          model={this.props.bangdiem1}
          modelName="bangdiem1"
          loading={this.props.loading}
          dispatch={this.props.dispatch}
          columns={columns}
          cond={{}}
          title={'Chưa tiếp nhận'}
          tableProp={{
            scroll: { x: 1000 }
          }}
        />
      </>
    );
  }
}

export default BangDiem;
