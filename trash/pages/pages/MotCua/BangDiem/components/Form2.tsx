/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Form, Input, Card, Row, Col, Button, DatePicker, Select, Spin } from 'antd';
import _ from 'lodash';
import { getRecordValue, renderFileList } from '@/utils/utils';
import rules from '@/utils/rules';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import UploadMulti from '@/components/Upload/UploadMultiFile';
import { formItemLayout, tailFormItemLayout, handleValuesChange, handleCommonSubmit, getSubmitText } from '@/utils/form';

type BangDiem_Props = {
  model: { edit, record },
  cond: object,
  loading: boolean,
  dispatch: Function,
  form: { validateFieldsAndScroll, getFieldDecorator },
}

class BangDiem extends React.Component<BangDiem_Props> {
  componentDidMount() {
  }

  handleSubmit = e => {
    const { form, model: { record, callback } } = this.props;
    e.preventDefault();
    form.validateFieldsAndScroll((err, values) => {
      if (err) return;
      if (callback) callback(record, values);
    });
  };

  render() {
    const { model: { record, isTouched }, form: { getFieldDecorator }, loading } = this.props;
    return (
      <div className="box">
        <Card
          bordered
          title={
            <div className="cardTitle">{record.title}</div>
          }
        >
          <GridContent>
            <Row>
              <Col span={18}>
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                  <Form.Item label="Ghi chú">
                    {getFieldDecorator('ghiChu', {
                      rules: [...rules.required],
                    })(
                      <Input.TextArea rows={4} />
                    )}
                  </Form.Item>
                  <Form.Item label="Tệp đính kèm">
                    {getFieldDecorator('tepDinhKem', {
                    })(
                      <UploadMulti
                        otherProps={{
                          accept: 'image/*,application/pdf',
                          multiple: true,
                        }}
                      />
                    )}
                  </Form.Item>
                  <Spin spinning={!!loading}>
                    <Form.Item {...tailFormItemLayout}>
                      <Button.Group>
                        <Button type="primary" icon="plus" htmlType="submit">
                          Gửi
                        </Button>
                      </Button.Group>
                    </Form.Item>
                  </Spin>
                </Form>
              </Col>
            </Row>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedForm = Form.create({ name: 'BangDiem', onValuesChange: handleValuesChange })(BangDiem);

export default WrappedForm;
