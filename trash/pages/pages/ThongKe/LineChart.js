import React from 'react';
import { Chart, Geom, Axis, Tooltip, Legend } from 'bizcharts';
import { connect } from 'dva';
import moment from 'moment';

class Curved extends React.Component {
  render() {
    const data = [
      { genre: 'Đông Hưng', sold: 275 },
      { genre: 'Hưng Hà', sold: 115 },
      { genre: 'Kiến Xương', sold: 120 },
      { genre: 'Tiền Hải', sold: 350 },
      { genre: 'Other', sold: 150 },
    ];

    const cols = {
      sold: { alias: 'Số lượng thân nhân' },
      genre: { alias: 'Huyện' },
    };

    return (
      <div>
        <Chart forceFit data={data} scale={cols}>
          <Axis name="genre" title />
          <Axis name="sold" title />
          <Legend position="top" dy={-20} />
          <Tooltip />
          <Geom type="interval" position="genre*sold" color="genre" />
        </Chart>
      </div>
    );
  }
}

export default Curved;
