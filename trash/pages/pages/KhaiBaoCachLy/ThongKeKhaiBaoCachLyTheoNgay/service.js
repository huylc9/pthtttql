import Service from '@/services/BaseService';
import { ip } from '@/services/ip';
import axios from '@/utils/axios.js';

class ExtendedService extends Service {
  getDB = async payload => {
    const { ngayBatDau, ngayKetThuc, khaiBao } = payload;
    const urlKhaiBao = khaiBao ? 'da-khai-bao' : 'chua-khai-bao';
    return axios.get(`${ip}/khai-bao-cach-ly/${urlKhaiBao}/from/${ngayBatDau}/to/${ngayKetThuc}`, {
      params: payload,
    });
  };
}

export default new ExtendedService({
  name: 'cachlytheongay',
  formData: false,
  url: 'khai-bao-cach-ly',
});
