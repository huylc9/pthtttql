import React from 'react';
import { connect } from 'dva';
import { IBase } from '@/utils/base';
import { Modal, Card, Table, Tag } from 'antd';
import _ from 'lodash';
import moment from 'moment';

type Props = {
  khaibaocachly: IBase;
  loading: boolean;
  dispatch: Function;
};

@connect(({ khaibaocachly, loading }) => ({
  khaibaocachly,
  loading: loading.models.khaibaocachly,
}))
class ViewModal extends React.Component<Props> {
  componentDidMount() { }

  handleOk = () => {
    this.props.dispatch({
      type: 'khaibaocachly/changeState',
      payload: {
        visible: false,
      },
    });
  };

  handleCancel = () => {
    this.props.dispatch({
      type: 'khaibaocachly/changeState',
      payload: {
        visible: false,
      },
    });
  };

  onChange = (pagination: any) => {
    const { current: page, pageSize: limit } = pagination;
    const {
      record: { maSv },
    } = this.props.khaibaocachly;
    this.props.dispatch({
      type: 'khaibaocachly/getByMSV',
      payload: {
        maSv,
        page,
        limit,
      },
    });
  };

  render() {
    const { khaibaocachly, loading } = this.props;

    let {
      danhSachGui,
      pagingModal: { page, limit },
      visible,
      totalModal,
    } = khaibaocachly;

    danhSachGui = danhSachGui.map((item, index) => ({
      ...item,
      index: index + 1 + (page - 1) * limit,
    }));

    const columns = [
      {
        title: 'STT',
        dataIndex: 'index',
        align: 'center',
        width: '90px',
      },
      {
        title: 'Thời gian khai báo',
        dataIndex: 'thoiGianKhaiBao',
        align: 'center',
        width: 150,
        render: val => (
          <span title={moment(val).format('DD/MM/YYYY HH:mm:ss')}>
            {moment(val).format('DD/MM/YYYY')}
          </span>
        ),
        search: 'sort',
      },
      {
        title: 'Trạng thái cách ly',
        dataIndex: 'trangThaiCachLy',
        align: 'center',
        width: '150px',
        render: val => (val ? <Tag color="red">True</Tag> : <Tag color="blue">False</Tag>),
      },
      {
        title: 'Ngày cách ly',
        dataIndex: 'ngayCachLy',
        align: 'center',
        width: '150px',
        render: val => (
          <span title={moment(val).format('DD/MM/YYYY HH:mm:ss')}>
            {moment(val).format('DD/MM/YYYY')}
          </span>
        ),
      },

      {
        title: 'Số ngày',
        dataIndex: 'soNgay',
        align: 'center',
        width: '100px',
      },
      {
        title: 'Hình thức cách ly',
        dataIndex: 'hinhThucCachLy',
        align: 'center',
        width: '150px',
        render: val => (val ? 'Tập Trung' : 'Tại Nhà'),
      },
      {
        title: 'Ghi chú',
        dataIndex: 'ghiChu',
        align: 'center',
        width: '150px',
      },
    ];
    return (
      <Modal
        title="Danh sách khai báo cách ly chi tiết"
        visible={visible}
        destroyOnClose
        onOk={this.handleOk}
        onCancel={this.handleCancel}
        width="80%"
      >
        <Card bordered={false} bodyStyle={{ minHeight: 400 }}>
          <Table
            style={{ marginBottom: 10 }}
            loading={loading}
            bordered
            onChange={this.onChange}
            columns={columns}
            dataSource={danhSachGui}
            pagination={{
              position: 'top',
              total: totalModal,
              showSizeChanger: true,
              pageSizeOptions: ['10', '20', '30', '40'],
            }}
          />
        </Card>
      </Modal>
    );
  }
}

export default ViewModal;
