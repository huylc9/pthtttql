import { Button, Divider, Popconfirm, Tag } from 'antd';
import { connect } from 'dva';
import moment from 'moment';
import React from 'react';
import uuidv1 from 'uuid/v1';
import { IColumn } from '@/utils/interfaces';
import { IBase } from '@/utils/base';
import TableBase from '@/components/Table/Table.tsx';
import Form from './components/Form';
import ViewModal from './components/Modal';
import ThongKeKhaiBaoCachLyTheoNgay from './ThongKeKhaiBaoCachLyTheoNgay';

interface Props {
  khaibaocachly: IBase;
  loading: boolean;
  dispatch: Function;
}

@connect(({ khaibaocachly, loading }) => ({
  khaibaocachly,
  loading: loading.models.khaibaocachly,
}))
class KhaiBaoCachLy extends React.Component<Props> {
  public state = {};

  public componentDidMount() { }

  public handleView = (record: any) => {
    this.props.dispatch({
      type: 'khaibaocachly/changeState',
      payload: {
        record,
        visible: true,
      },
    });
    this.props.dispatch({
      type: 'khaibaocachly/getByMSV',
      payload: {
        maSv: record.maSv,
        page: 1,
        limit: 10,
      },
    });
  };

  public render() {
    const renderLast = (value, record) => (
      <React.Fragment>
        <Button
          type="primary"
          shape="circle"
          icon="eye"
          onClick={() => this.handleView(record)}
          title="Xem trước"
        />
      </React.Fragment>
    );

    const columns: IColumn[] = [
      {
        title: 'STT',
        dataIndex: 'index',
        align: 'center',
        width: '80px',
      },
      {
        title: 'Họ và Tên',
        dataIndex: 'nguoiKhaiBao',
        align: 'center',
        width: '150px',
        render: item => _.get(item, 'hoTen', ''),
      },
      {
        title: 'Số điện thoại',
        dataIndex: 'nguoiKhaiBao',
        align: 'center',
        width: '150px',
        render: item => _.get(item, 'soDienThoai', ''),
      },

      {
        title: 'Mã sinh viên',
        dataIndex: 'maSv',
        align: 'center',
        width: '100px',
      },
      {
        title: 'Trạng thái cách ly',
        dataIndex: 'trangThaiCachLy',
        align: 'center',
        width: '200px',
        render: val =>
          (val ? (
            <Tag color="red">Đang trong thời gian cách ly</Tag>
          ) : (
              <Tag color="blue">Không phải cách ly</Tag>
            )),
      },
      {
        title: 'Ngày cách ly',
        dataIndex: 'ngayCachLy',
        align: 'center',
        width: '150px',
        render: val => (
          <span title={moment(val).format('DD/MM/YYYY HH:mm:ss')}>
            {moment(val).format('DD/MM/YYYY')}
          </span>
        ),
        search: 'sort',
      },
      {
        title: 'Số ngày',
        dataIndex: 'soNgay',
        align: 'center',
        width: '80px',
      },
      {
        title: 'Hình thức cách ly',
        dataIndex: 'hinhThucCachLy',
        align: 'center',
        width: '150px',
        render: val => (val ? 'Tập Trung' : 'Tại Nhà'),
      },
      {
        title: 'Thời gian khai báo',
        dataIndex: 'createdAt',
        align: 'center',
        width: 180,
        render: val => (
          <span title={moment(val).format('DD/MM/YYYY HH:mm:ss')}>
            {moment(val).format('DD/MM/YYYY')}
          </span>
        ),
        search: 'sort',
      },
      {
        title: 'Ghi chú',
        dataIndex: 'ghiChu',
        align: 'center',
        // width: '150px',
      },
      {
        title: 'Thao tác',
        align: 'center',
        render: (value, record) => renderLast(value, record),
        fixed: 'right',
        width: 110,
      },
    ];

    return (
      <div>
        <TableBase
          model={this.props.khaibaocachly}
          modelName="khaibaocachly"
          loading={this.props.loading}
          dispatch={this.props.dispatch}
          columns={columns}
          cond={{}}
          Form={Form}
          title="Thông tin khai báo cách ly"
          tableProp={{
            scroll: { x: 1500 },
          }}
        />
        <ViewModal />
        <ThongKeKhaiBaoCachLyTheoNgay />
      </div>
    );
  }
}

export default KhaiBaoCachLy;
