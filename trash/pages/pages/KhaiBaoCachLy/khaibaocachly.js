import _ from 'lodash';
import axios from '@/utils/axios.js';
import { ip } from '@/services/ip';
import Service from '@/services/BaseService';

class ExtendedService extends Service {
  get = async payload => axios.get(`${ip}/${this.url}/all/latest`, { params: payload });

  getByMSV = async payload => {
    const { maSv, page, limit } = payload;
    return axios.get(`${ip}/${this.url}/msv/${maSv}`, { params: { page, limit } });
  };
}

export default new ExtendedService({
  name: 'khaibaocachly',
  formData: false,
  url: 'khai-bao-cach-ly',
});
