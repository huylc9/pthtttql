import React from 'react';
import { connect } from 'dva';
import uuidv1 from 'uuid/v1';
import { Button, Divider, Popconfirm, Row, Col, Card } from 'antd';
import Form from './components/Form';
import { IBase } from '@/utils/base';
import TreeView from './components/TreeView';
import FormDrawer from '@/components/Drawer/FormDrawer';

type Props = {
  cocautochuc: IBase,
  loading: boolean,
  dispatch: Function,
}

@connect(({ cocautochuc, loading }) => ({
  cocautochuc,
  loading: loading.models.cocautochuc,
}))
class CoCauToChuc extends React.Component<Props> {
  state = {
  };

  componentDidMount() {
    this.props.dispatch({
      type: 'cocautochuc/get',
      payload: {
        page: 1,
        limit: 1000,
        cond: {},
        sort: 'createdAt',
        order: 1,
      }
    })
  }

  handleThem = () => {
    this.props.dispatch({
      type: 'cocautochuc/changeState',
      payload: {
        showDrawer: true,
        edit: false,
        record: {},
        key: uuidv1(),
        isTouched: false,
      },
    });
  };

  render() {
    return (
      <React.Fragment>
        <Card title='Cơ cấu tổ chức' bordered>
          <Row>
            <Col xs={24}>
              <Button
                style={{ marginBottom: 20 }}
                type="primary"
                icon="plus"
                onClick={() => this.handleThem()}
              >
                Thêm mới
              </Button>
            </Col>
            <Col xs={24} lg={18}>
              <TreeView model={this.props.cocautochuc} dispatch={this.props.dispatch} />
            </Col>
            <FormDrawer name='cocautochuc'>
              <Form
                model={this.props.cocautochuc}
                modelName="cocautochuc"
                loading={this.props.loading}
                dispatch={this.props.dispatch}
              />
            </FormDrawer>
          </Row>
        </Card>
      </React.Fragment>
    );
  }
}

export default CoCauToChuc;
