import _ from 'lodash';
import uuidv1 from 'uuid/v1';

let data = [
  {
    tenDonVi: 'User 1',
    isRoot: true,
    _id: 'abc',
  },
  {
    tenDonVi: 'User 2',
    isRoot: false,
    idCha: 'abc',
    _id: 'abcd'
  },
  {
    tenDonVi: 'User 3',
    isRoot: false,
    idCha: 'abcd',
    _id: 'abcde'
  },
  {
    tenDonVi: 'User 4',
    isRoot: false,
    idCha: 'abcd',
    _id: 'abcvca'
  },
]

export default {
  // GET
  'GET /api/cocautochuc': (req, res) => {
    res.status(200).send({
      data: _.reverse(_.clone(data)),
      total: data.length,
    });
  },
  'DELETE /api/cocautochuc/:_id': (req, res) => {
    const { _id } = req.params;
    data = data.filter(item => item._id !== _id);
    res.status(200).send({
      message: 'OK'
    });
  },
  'POST /api/cocautochuc': (req, res) => {
    data.push({
      ...req.body,
      _id: uuidv1()
    })
    res.status(200).send({
      message: 'OK'
    });
  },
  'PUT /api/cocautochuc/:_id': (req, res) => {
    const { _id } = req.params;
    data = data.map(item => {
      if (item._id !== _id) { return item; }
      return {
        ...item,
        ...req.body,
      }
    })
    res.status(200).send({
      message: 'OK'
    });
  },
}