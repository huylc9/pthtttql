/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Form, Input, Card, Row, Col, Button, DatePicker, Select, Spin, Radio } from 'antd';
import _ from 'lodash';
import { getRecordValue, includes } from '@/utils/utils';
import rules from '@/utils/rules';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import { formItemLayout, tailFormItemLayout, handleValuesChange, handleCommonSubmit, getSubmitText } from '@/utils/form';
import { IBase } from '@/utils/base';
import TextEditor from '@/components/TextEditor/TextEditor.js';

type CoCauToChuc_Props = {
  model: IBase,
  cond: object,
  loading: boolean,
  dispatch: Function,
  form: { validateFieldsAndScroll, getFieldDecorator },
}

class CoCauToChuc extends React.Component<CoCauToChuc_Props> {
  state = {
    isRoot: false
  }

  componentDidMount() {
    const { model: { record, edit } } = this.props;
    const isRoot = edit ? !_.get(record, 'idCapTren', false) : false;
    this.setState({ isRoot });
  }

  onChange = e => {
    this.setState({
      isRoot: e.target.value
    })
  }

  handleSubmit = e => {
    const { form } = this.props;
    e.preventDefault();
    form.validateFieldsAndScroll((err, values) => {
      if (err) return;
      values.moTa = values.moTa.text;
      handleCommonSubmit(values, this.props);
    });
  };

  handleDelete = () => {
    const { model: { edit, record } } = this.props;
    if (!edit) return;
    this.props.dispatch({
      type: 'cocautochuc/del',
      payload: {
        _id: record._id
      }
    });
  }

  render() {
    const { model, form: { getFieldDecorator }, cond, loading } = this.props;
    return (
      <div className="box">
        <Card
          bordered
          title={
            <div className="cardTitle">{!model.edit ? `Thêm mới` : `Chỉnh sửa`}</div>
          }
        >
          <GridContent>
            <Row>
              <Col span={18}>
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                  <Form.Item label="Tên đơn vị">
                    {getFieldDecorator('tenDonVi', {
                      initialValue: getRecordValue(model, cond, 'tenDonVi', ''),
                      rules: [...rules.text, ...rules.required, ...rules.length(100)],
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Có cấp trên hay không?">
                    {getFieldDecorator('isRoot', {
                      initialValue: model.edit ? !getRecordValue(model, cond, 'idCapTren', false) : false,
                    })(
                      <Radio.Group onChange={this.onChange} disabled={model.edit}>
                        <Radio value={false}>Có đơn vị cấp trên</Radio>
                        <Radio value={true}>Không có đơn vị cấp trên</Radio>
                      </Radio.Group>)}
                  </Form.Item>

                  {
                    !this.state.isRoot && (
                      <Form.Item label="Đơn vị cấp trên">
                        {getFieldDecorator('idCapTren', {
                          initialValue: getRecordValue(model, cond, 'idCapTren', undefined),
                          rules: [...rules.required],
                        })(
                          <Select
                            disabled={model.edit}
                            showSearch
                            filterOption={(value, option) => includes(option.props.children, value)}
                          >
                            {model.danhSach.map((item, index) => (
                              <Select.Option key={item._id} value={item._id}>
                                {item.tenDonVi}
                              </Select.Option>
                            ))}
                          </Select>
                        )}
                      </Form.Item>
                    )
                  }

                  <Form.Item label="Mô tả">
                    {getFieldDecorator('moTa', {
                      initialValue: { text: getRecordValue(model, cond, 'moTa', '') },
                      // rules: [...rules.textEditor],
                    })(<TextEditor />)}
                  </Form.Item>

                  <Spin spinning={loading}>
                    <Form.Item {...tailFormItemLayout}>
                      <Button.Group>
                        <Button type="primary" htmlType="submit">
                          {getSubmitText(model)}
                        </Button>
                        {model.edit && <Button type="danger" icon="delete" onClick={this.handleDelete} style={{ marginLeft: 50 }}>
                          Xóa
                        </Button>}
                      </Button.Group>
                    </Form.Item>
                  </Spin>
                </Form>
              </Col>
            </Row>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedForm = Form.create({ name: 'CoCauToChuc', onValuesChange: handleValuesChange })(CoCauToChuc);

export default WrappedForm;
