import React from 'react';
import { Tree, Input, Row, Col, Spin } from 'antd';
import { connect } from 'dva';
import { Scrollbars } from 'react-custom-scrollbars';
import _ from 'lodash';
import { Format } from '@/utils/utils';

const { TreeNode } = Tree;
const { Search } = Input;


class SearchTree extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchValue: '',
      expandedKeys: [],
      first: true
    };
  }

  onExpand = expandedKeys => {
    this.setState({
      expandedKeys,
    });
  };

  onChange = e => {
    const { model: { danhSach } } = this.props;
    const { searchValue, expandedKeys } = this.state;
    danhSach.map(item => {
      if (Format(item.tenDonVi).includes(Format(searchValue))) {

        if (!expandedKeys.includes(item._id)) expandedKeys.push(item._id);
      }
    });
    this.setState({
      searchValue: e.target.value,
      expandedKeys
    });
  };

  handleSelect = (selectedKey, e) => {
    const { expandedKeys } = this.state;
    this.props.dispatch({
      type: 'cocautochuc/changeState',
      payload: {
        showDrawer: true,
        edit: true,
        record: _.get(e, 'node.props.record'),
        isTouched: false,
      }
    })
  };

  render() {
    const { searchValue, expandedKeys, first } = this.state;
    const { model: { danhSach } } = this.props;
    const TreeData = [
    ];
    const roots = [];

    const child = {}; // danh sach con: child[0] = [1,2,3,4]

    // Đưa các con vào cha
    danhSach.forEach((item, index) => {
      if (!item.idCapTren) return;
      const indexPar = _.findIndex(danhSach, { _id: _.get(item, 'idCapTren') });
      if (indexPar === -1) {
        item.idCapTren = undefined;
        return;
      }
      if (!child[indexPar]) child[indexPar] = [];
      child[indexPar].push(index);
    });

    // Đưa thông tin các đon vị vào cây
    danhSach.map((item, index) => {
      if (!item.idCapTren) roots.push(item._id);
      TreeData.push({
        tenDonVi: item.tenDonVi,
        _id: item._id,
        record: item,
        isRoot: !item.idCapTren,
        idCapTren: item.idCapTren
      })
    });

    if ((!expandedKeys || !expandedKeys.length) && (first || danhSach.length)) {
      this.setState({ expandedKeys: roots, first: false });
    }

    const duyet = i => {
      if (!TreeData[i]) return;
      const { tenDonVi, _id, record, isRoot } = TreeData[i];
      if (!child[i] || child[i].length === 0) return <TreeNode title={tenDonVi} key={_id} record={record} />;

      return (
        <TreeNode title={tenDonVi} key={_id} autoExpandParent={isRoot} record={record}>
          {child[i].map(val => duyet(val))}
        </TreeNode>
      );
    };

    return (
      <div>
        <Spin spinning={false}>
          <div>
            <Scrollbars
              style={{ height: '90vh', border: '1px solid #d9d9d9', borderRadius: 4, marginBottom: 8 }}
            >
              <Input placeholder='Tìm kiếm' onChange={this.onChange} />
              <Tree
                style={{ margin: '12px 24px' }}
                showLine
                expandedKeys={expandedKeys}
                onExpand={this.onExpand}
                defaultExpandedKeys={this.state.expandedKeys}
                defaultSelectedKeys={this.state.expandedKeys}
                filterTreeNode={node =>
                  searchValue.length > 0 && Format(node.props.title).includes(Format(searchValue))
                }
                onSelect={this.handleSelect}
              >
                {
                  TreeData.map((item, i) => {
                    if (item.isRoot) return duyet(i)
                  }).filter(item => !!item)
                }
              </Tree>
            </Scrollbars>
          </div>
        </Spin>
      </div>
    );
  }
}

export default SearchTree;
