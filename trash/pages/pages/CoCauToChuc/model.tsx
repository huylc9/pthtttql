import base from '@/utils/base';
import modelExtend from 'dva-model-extend';
import Services from '@/pages/CoCauToChuc/service';
import { formatMessage } from 'umi/locale';
import notificationAlert from '@/components/Notification';

export default modelExtend(base(Services), {
    namespace: 'cocautochuc',
    effects: {
        *del({ payload }, { call, put, select }) {
            let { paging: { page, limit, cond }, total } = yield select(state => state[Services.name]);
            let currentPage = (total % limit == 1 && page == Math.ceil(total / limit) && page > 1) ? page - 1 : page;
            yield call(Services.del, payload);
            notificationAlert('success', formatMessage({ id: 'XOA_THANH_CONG' }));
            yield put({ type: 'get', payload: { page: currentPage, limit, cond } });
            yield put({ type: 'changeState', payload: { showDrawer: false } });
        },
    }
})
