import React from 'react';
import { connect } from 'dva';
import uuidv1 from 'uuid/v1';
import { Button, Divider, Popconfirm, Modal, Card, Row, Col, Avatar, Descriptions, Typography } from 'antd';
import { IColumn } from '@/utils/interfaces';
import Form from './components/Form';
import { formItemLayout, tailFormItemLayout, handleValuesChange, handleCommonSubmit, getSubmitText, renderGroup, renderSelect } from '@/utils/form';
import _ from 'lodash';
import TableBase from '@/components/Table/Table.tsx';
import { IBase } from '@/utils/base';
import moment from 'moment';

type Props = {
  khaosat: IBase,
  loading: boolean,
  dispatch: Function,
}

@connect(({ khaosat, loading }) => ({
  khaosat,
  loading: loading.models.khaosat,
}))
class KhaoSat extends React.Component<Props> {

  state = {
  };

  componentDidMount() {
  }

  handleEdit = record => {
    this.props.dispatch({
      type: 'khaosat/changeState',
      payload: {
        showDrawer: true,
        edit: true,
        view: false,
        record,
        key: uuidv1(),
        isTouched: false,
        done: false,
      },
    });
  };

  handleDel = _id => {
    this.props.dispatch({
      type: 'khaosat/del',
      payload: {
        _id,
        key: uuidv1(),
      },
    });
  };

  handleView = record => {
    record.view = true;
    this.props.dispatch({
      type: 'khaosat/changeState',
      payload: {
        showDrawer: true,
        edit: true,
        record,
        key: uuidv1(),
        isTouched: false,
        done: false,
      }
    })
  };


  render() {
    const renderLast = (value, record) => {
      const canEdit = moment(record.ngayBatDau).isAfter(moment());
      return (
        <React.Fragment>
          {!canEdit && (
            <React.Fragment>
              <Button
                type="primary"
                shape="circle"
                icon="eye"
                onClick={() => this.handleView(record)}
                title="Xem thống kê"
                style={{ backgroundColor: 'green' }}
              />
              <Divider type="vertical" />
            </React.Fragment>
          )}

          {canEdit && (
            <React.Fragment>
              <Button
                type="primary"
                shape="circle"
                icon="edit"
                onClick={() => this.handleEdit(record)}
                title="Chỉnh sửa"
              />
              <Divider type="vertical" />
            </React.Fragment>
          )}
          <Popconfirm title="Bạn có chắc muốn xóa?" onConfirm={() => this.handleDel(record._id)}>
            <Button type="danger" shape="circle" icon="delete" title="Xóa" />
          </Popconfirm>
        </React.Fragment>
      )
    }

    const columns: IColumn[] = [
      {
        title: 'STT',
        dataIndex: 'index',
        width: '15%',
        align: 'center',
      },
      {
        title: 'Tiêu để',
        dataIndex: 'tieuDe',
        width: '15%',
        align: 'center',
        render: text => text,
      },
      {
        title: 'Thời gian làm',
        dataIndex: 'ngayBatDau',
        width: '15%',
        align: 'center',
        render: (text, record) =>
          `${moment(record.ngayBatDau).format('DD/MM/YYYY HH:mm')} - ${moment(
            record.ngayKetThuc
          ).format('DD/MM/YYYY HH:mm')}`,
      },
      {
        title: 'Số lượng câu hỏi',
        dataIndex: 'noiDungKhaoSat',
        width: '15%',
        align: 'center',
        render: (val) => val ? val.length : 0,
      },

      {
        title: 'Số người tham gia',
        dataIndex: 'nguoiTraLoi',
        // width: '15%',
        align: 'center',
        render: (val) => val ? val.length : 0,
      },
      {
        title: 'Sửa/Xóa',
        key: 'Sua/Xoa',
        width: '15%',
        align: 'center',
        render: renderLast,
      },
    ];

    return (
      <TableBase
        model={this.props.khaosat}
        modelName="khaosat"
        loading={this.props.loading}
        dispatch={this.props.dispatch}
        columns={columns}
        cond={{}}
        Form={Form}
        title={'Danh sách các cuộc khảo sát'}
        hasCreate
        tableProp={{
          scroll: { x: 1200 }
        }}
      />
    );
  }
}

export default KhaoSat;
