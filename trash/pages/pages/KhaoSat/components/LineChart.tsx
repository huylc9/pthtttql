import React from 'react';
import { Chart, Geom, Axis, Tooltip, View } from 'bizcharts';
import _ from 'lodash';

type Props_Type = {
    data: Array<Object>,
}

class LineChart extends React.Component<Props_Type>{

    getTick = max => {
        let boi = 1;
        while (max / boi >= 10) {
            boi *= 10;
        }
        const x = max / boi;
        return Math.round((x > 4 ? 2 : (x > 2 ? 1 : 0.5)) * boi);
    }

    render() {
        const { data } = this.props;
        let max = _.max(data.map(item => item.soLuong));
        const tickInterval = this.getTick(max);
        const cols = {
            soLuong: { alias: "Số lượng", tickInterval },
            cauTraLoi: { alias: 'Câu trả lời' }
        }
        return (
            <div style={{ padding: 10, width: 'calc(100% - 20px)' }}>
                <Chart height={400} forceFit data={data} scale={cols} style={{ width: "100%" }}>
                    <Tooltip />
                    <Geom type="interval" position="cauTraLoi*soLuong" color="cauTraLoi" select />
                    <View data={data}>
                        <Axis name="soLuong" title />
                        <Geom type="interval" position="label*soLuong" color="label" select />
                    </View>
                </Chart>
            </div>
        )
    }
}

export default LineChart;