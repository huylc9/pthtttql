/* eslint-disable react/destructuring-assignment */
import React from 'react';
import _ from 'lodash';
import { getRecordValue } from '@/utils/utils';
import rules from '@/utils/rules';
import { formItemLayout, tailFormItemLayout, handleValuesChange, handleCommonSubmit, getSubmitText, renderGroup, renderSelect } from '@/utils/form';
import {
  Form,
  Input,
  Card,
  Row,
  Col,
  Button,
  Select,
  InputNumber,
  DatePicker,
  Typography,
  List,
  Checkbox,
  Icon,
  Radio,
  Tooltip,
  Spin
} from 'antd';

import { connect } from 'dva';
import moment from 'moment';
import UploadOne from '@/components/Upload/UploadOne';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import LineChart from './LineChart';

let id = 0;
let idQuestion = 0;
const arr = []; // danh sách câu hỏi
const type = [];

type KhaoSat_Props = {
  model: { edit, record, done },
  cond: object,
  loading: boolean,
  dispatch: Function,
  form: WrappedFormUtils,
}

class KhaoSat extends React.Component<KhaoSat_Props> {
  Process = record => {
    // setup prop Form : arr, keys, questionId, listId, diemCong, diemTru
    const { done } = this.props.model;
    if (done) return;
    const { getFieldDecorator, setFieldsValue } = this.props.form;
    getFieldDecorator('questionId', { initialValue: [] });
    const keys = []; // Thứ tự các câu hỏi
    const questionId = []; // vị trí câu hỏi trong arr
    const listId = []; // danh sách các id của câu trả lời
    const { noiDungKhaoSat } = record;
    id = 0;
    idQuestion = 0;
    noiDungKhaoSat.forEach((item, k) => {
      this.addQuestion(item.loai);
      keys.push(k);
      listId[k] = [];
      type[k] = item.loai;
      questionId[k] = id;
      arr[id] = item.cauHoi;
      id++;
      item.cauTraLoi.forEach(cau => {
        arr[id] = cau;
        listId[k].push(id);
        id++;
      });
    });
    setFieldsValue({
      keys,
      questionId,
      listId,
      type,
    });
    this.props.dispatch({
      type: 'khaosat/changeState',
      payload: {
        done: true,
        arr,
      },
    });
  };

  addQuestion = (loai: string, add = true) => { // them keys, questionId.push, listId[questionId] = [], type[idQuestion] = loai
    const { form } = this.props;
    const { getFieldDecorator } = form;
    const keys = form.getFieldValue('keys');
    const questionId = form.getFieldValue('questionId');
    const listId = form.getFieldValue('listId');
    idQuestion++;
    questionId[idQuestion] = id++;
    listId[idQuestion] = [];
    type[idQuestion] = loai;
    getFieldDecorator(`type[${idQuestion}]`, { initialValue: loai });
    form.setFieldsValue({
      keys: keys.concat(idQuestion),
      questionId,
      listId,
      type
    });
    if (add) this.addAnswer(idQuestion);
    return idQuestion;
  };

  removeQuestion = k => {
    const { form } = this.props;
    const keys = form.getFieldValue('keys');
    form.setFieldsValue({
      keys: keys.filter(key => key !== k),
    });
  };

  addAnswer = k => {
    // add answer to question k
    const { form } = this.props;
    const listId = form.getFieldValue(`listId`);
    listId[k] = listId[k].concat(id++);
    form.setFieldsValue({
      listId,
    });
    return id;
  };

  addContent = (idAnswer, text) => { // set answer text
    const { form: { getFieldValue, setFieldsValue, getFieldDecorator } } = this.props;
    let arr = getFieldValue(`arr`);
    getFieldDecorator(`arr[${idAnswer}]`, { initialValue: text });
    arr[idAnswer] = text;
    setFieldsValue({ arr });
  }

  remove = (k, answerId) => {
    // remove answer with specific id of question k
    const { form } = this.props;
    const listId = form.getFieldValue(`listId`);
    if (listId[k].length === 0) return;
    listId[k] = listId[k].filter(key => key !== answerId);
    form.setFieldsValue({
      listId,
    });
  };

  disabledEndDate = endValue => {
    const { form } = this.props;
    const startValue = form.getFieldValue('thoiGianBatDau');
    return moment(startValue).isAfter(endValue);
  };

  RenderQuestion = (k, i) => {
    const { form: { getFieldDecorator, getFieldValue }, model: { edit, record } } = this.props;
    const view = record.view;
    const questionId = getFieldValue(`questionId`)[k];
    let listId = getFieldValue(`listId`)[k]; // danh sach id cau tra loi
    const loai = getFieldValue(`type[${k}]`) || type[k] || '0';
    const hasAnswer = parseInt(loai) < 2;
    if (view) {
      listId = [listId.map((answerId, index) => {
        const label = `Đáp án ${index + 1}`;
        const cauTraLoi = _.get(arr, `[${answerId}].noiDung`, '');
        const soLuong = _.get(arr, `[${answerId}].nguoiChon.length`, 0);
        return {
          label,
          cauTraLoi,
          soLuong: soLuong
        }
      })]
    }
    return (
      <List
        key={`${i}zzz`}
        header={
          <div>
            {!view && <Form.Item label="Loại câu hỏi" style={{ margin: 5 }}>
              {getFieldDecorator(`type[${k}]`, {
                initialValue: edit ? loai.toString() : type[k] || '0',
              })(renderSelect('loaiCauHoi'))}
            </Form.Item>}
            <Form.Item label="Câu hỏi" style={{ margin: 5 }}>
              {getFieldDecorator(`arr[${questionId}]`, {
                rules: [
                  {
                    required: true,
                    message: 'Hãy nhập câu hỏi',
                  },
                ],
                initialValue: edit ? _.get(arr, `[${questionId}]`, '') : '',
              })(<Input.TextArea disabled={view} placeholder="Nội dung câu hỏi" style={{ width: '100%' }} />)}
            </Form.Item>
          </div>
        }
        style={{ border: '2px solid black', margin: 5 }}
        dataSource={listId}
        renderItem={(answerId, j) => hasAnswer ? (
          <List.Item key={j} style={{ padding: 0 }}>
            {!view &&
              <Form.Item
                style={{ width: '100%', margin: 0, padding: 0 }}
                label={`Đáp án ${j + 1}`}
              >
                {getFieldDecorator(`arr[${answerId}]`, {
                  rules: [
                    {
                      validator: (_, value, callback) => {
                        if (!value || !value.length) callback('');
                        callback();
                      },
                      message: 'Hãy nhập câu trả lời',
                    },
                  ],
                  initialValue: edit ? _.get(arr, `[${answerId}]`, '') : '',
                })(<Input disabled={view} style={{ width: '65%', marginRight: 10, fontSize: 16, fontWeight: 500 }} />)}
                {listId.length > 1 && !view && (
                  <Icon
                    className="dynamic-delete-button"
                    type="minus-circle-o"
                    onClick={() => this.remove(k, answerId)}
                    title="Xóa câu trả lời"
                    style={{ fontSize: 24 }}
                  />
                )}
                {view && <Typography.Text>{`Số lượng: ${_.get(arr, `[${answerId}].nguoiChon.length`, 0)}`}</Typography.Text>}

              </Form.Item>
            }
            {
              view && <LineChart data={answerId} />
            }
          </List.Item>
        ) : <div />}



      >
        {!view && <Form.Item {...tailFormItemLayout} style={{ marginTop: 24 }}>
          {hasAnswer && <Button type="primary" style={{ marginRight: 30 }} onClick={() => this.addAnswer(k)}>
            <Icon type="plus" /> Thêm câu trả lời
                </Button>}
          <Button style={{ marginRight: 5 }} type="primary" onClick={() => this.dupplicateQuestion(k)}>
            <Icon type="plus" /> Sao chép
          </Button>
          <Button style={{ marginRight: 5 }} type="danger" onClick={() => this.removeQuestion(k)}>
            <Icon type="minus" /> Xóa câu hỏi
          </Button>
        </Form.Item>}
      </List>
    )
  }

  dupplicateQuestion = (k) => {
    const { form: { getFieldValue, setFieldsValue } } = this.props;
    const listId = getFieldValue(`listId`); // danh sach id cau tra loi
    const loai = getFieldValue(`type[${k}]`);
    const arr = getFieldValue(`arr`);
    const curQues = this.addQuestion(loai, false);
    const questionId = getFieldValue(`questionId`);
    this.addContent(questionId[curQues], arr[questionId[k]]);
    listId[k].map(key => { // tung cau hoi
      let curId = this.addAnswer(curQues) - 1;
      this.addContent(curId, arr[key]);
    });
  }


  render() {
    const { model, loading } = this.props;
    let { record, edit } = model;
    const view = record.view;
    const { getFieldDecorator, getFieldValue, getFieldsValue } = this.props.form;
    getFieldDecorator('keys', { initialValue: [] });
    getFieldDecorator('questionId', { initialValue: [] });
    getFieldDecorator('listId', { initialValue: [] });
    getFieldDecorator('type', { initialValue: [] });
    const keys = getFieldValue('keys');
    if (!keys) return <div />;
    if (edit) this.Process(record);
    const noiDungKhaoSat = keys.map((k, i) => this.RenderQuestion(k, i));
    const handleSubmit = e => {
      e.preventDefault();
      if (!!view) {
        this.props.dispatch({
          type: 'khaosat/changeState',
          payload: {
            showDrawer: false,
          },
        });
        return;
      }
      this.props.form.validateFieldsAndScroll((err, values) => {
        if (err) return;

        values.noiDungKhaoSat = values.keys.map(k => {
          const questionId = getFieldValue('questionId')[k];
          let listId = getFieldValue('listId')[k]; // danh sach id cau tra loi
          const arr = getFieldValue('arr');
          const loai = getFieldValue(`type[${k}]`) || type[k];
          if (parseInt(loai) > 1) listId = [];
          return {
            cauHoi: arr[questionId],
            loai: parseInt(loai),
            cauTraLoi: listId.map(answerId => ({
              luaChon: arr[answerId],
            })),
          };
        });
        values.ngayBatDau = values.thoiGian[0];
        values.ngayKetThuc = values.thoiGian[1];
        if (edit) {
          this.props.dispatch({
            type: 'khaosat/upd',
            payload: {
              ...record,
              ...values,
            },
          });
        } else {
          this.props.dispatch({
            type: 'khaosat/add',
            payload: {
              ...values,
            },
          });
        }
        this.props.dispatch({
          type: 'khaosat/changeState',
          payload: {
            showDrawer: false,
          },
        });
      });
    };

    if (edit) {
      record.thoiGian = [record.ngayBatDau, record.ngayKetThuc];
      record.loai = `${record.loai}`;
    }
    console.log(getFieldsValue());
    return (
      <div className="box">
        <Card
          bordered
          title={<div className="cardTitle">{['Thêm khảo sát mới', 'Sửa khảo sát', 'Kết quả khảo sát: ' + record.tieuDe][+edit + !!view]}</div>}
        >
          <GridContent>
            <Row>
              <Col span={24}>
                <Form {...formItemLayout} onSubmit={handleSubmit}>
                  {view && <Typography.Text strong style={{ fontSize: 20 }}>{`Số người tham gia khảo sát: ${_.get(record, 'nguoiTraLoi.length', 0)}`} </Typography.Text>}
                  {!view && <Form.Item label="Tiêu đề">
                    {getFieldDecorator('tieuDe', {
                      initialValue: `${edit ? _.get(record, 'tieuDe', '') : ''}`,
                    })(<Input.TextArea disabled={view} rows={2} />)}
                  </Form.Item>}
                  {!view && <Form.Item label="Thời gian khảo sát">
                    {getFieldDecorator('thoiGian', {
                      initialValue: edit ? [moment(record.ngayBatDau), moment(record.ngayKetThuc)] : [moment(), moment().add(1, 'day')],
                    })(<DatePicker.RangePicker disabled={view} format="DD/MM/YYYY HH:mm" showTime={{ format: 'DD/MM/YYYY HH:mm' }} />)}
                  </Form.Item>}
                  <br />
                  {noiDungKhaoSat}

                  {!view && <Form.Item {...tailFormItemLayout}>
                    <Button
                      type="primary"
                      onClick={() => this.addQuestion('0')}
                      style={{ width: '60%' }}
                    >
                      <Icon type="plus" /> Thêm câu hỏi
                    </Button>
                  </Form.Item>}
                  <Form.Item {...tailFormItemLayout}>
                    <Button.Group>
                      <Spin spinning={loading}>
                        <Button type="primary" icon="plus" htmlType="submit">
                          {[getSubmitText(model), getSubmitText(model), 'Quay lại'][!!edit + !!view]}
                        </Button>
                      </Spin>
                    </Button.Group>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedForm = Form.create({ name: 'KhaoSat', onValuesChange: handleValuesChange })(KhaoSat);

export default WrappedForm;
