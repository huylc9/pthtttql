import { ChartCard } from 'ant-design-pro/lib/Charts';
import { Card, Col, Divider, Row } from 'antd';
import { connect } from 'dva';
import _ from 'lodash';
import React from 'react';
import { IBase } from '@/utils/base';
import {
  Chart1,
  Chart10,
  Chart2,
  Chart3,
  Chart4,
  Chart5,
  Chart8,
  Chart9,
  Stacked,
  Table2,
  Table3,
} from './components/Charts';

interface Props {
  dashboard: IBase;
  loading: boolean;
  dispatch: Function;
}

@connect(({ dashboard, loading }) => ({
  dashboard,
  loading: loading.models.dashboard,
}))
class DashBoard extends React.Component<Props> {
  public state = {};

  public componentDidMount() {
    this.props.dispatch({
      type: 'dashboard/getNguoiDung',
      payload: {},
    });

    this.props.dispatch({
      type: 'dashboard/getThongBao',
      payload: {},
    });

    this.props.dispatch({
      type: 'dashboard/getPhanHoi',
      payload: {},
    });

    this.props.dispatch({
      type: 'dashboard/getGiangVien',
      payload: {
        page: 1,
        limit: 10,
      },
    });

    this.props.dispatch({
      type: 'dashboard/getSinhVien',
      payload: {},
    });

    this.props.dispatch({
      type: 'dashboard/getTaiKhoanKhach',
      payload: {},
    });

    this.props.dispatch({
      type: 'dashboard/getDanhSachKhaiBaoSucKhoe',
      payload: {},
    });

    this.props.dispatch({
      type: 'dashboard/getKhaiBaoSucKhoeTheoNgay',
      payload: {
        days: 30,
      },
    });

    this.props.dispatch({
      type: 'dashboard/getDanhSachKhaiBaoCachLy',
      payload: {},
    });
  }

  public render() {
    const {
      dashboard: {
        phanHoi,
        nguoiDung,
        taiKhoanKhach,
        thongBao,
        giangVienTheoKhoa,
        sinhVienTheoKhoa,
        danhSach1,
      },
    } = this.props;
    const Gv = giangVienTheoKhoa;
    const Sv = sinhVienTheoKhoa;
    const tong = Gv.map((item, index) => {
      const d = _.find(Sv, { makhoa: item.makhoa });
      return {
        ...item,
        sinhVien: d ? d.total : 0,
        giangVien: item.total,
        index: index + 1,
      };
    });

    return (
      <>
        <h1 style={{ fontWeight: 'bold' }}>THÔNG TIN CHUNG</h1>
        <Row style={{ marginLeft: '-12px', marginRight: '-12px', marginTop: '-12px' }}>
          <Col sm={12} lg={12} xl={6} style={{ marginTop: '24px' }}>
            <Chart1 data={nguoiDung} loading={this.props.loading} dispatch={this.props.dispatch} />
          </Col>
          <Col sm={12} lg={12} xl={6} style={{ marginTop: '24px' }}>
            <Chart2
              data={taiKhoanKhach}
              loading={this.props.loading}
              dispatch={this.props.dispatch}
            />
          </Col>
          <Col sm={12} lg={12} xl={6} style={{ marginTop: '24px' }}>
            <Chart3 data={thongBao} loading={this.props.loading} dispatch={this.props.dispatch} />
          </Col>
          <Col sm={12} lg={12} xl={6} style={{ marginTop: '24px' }}>
            <Chart4 data={phanHoi} loading={this.props.loading} dispatch={this.props.dispatch} />
          </Col>
        </Row>
        <Row style={{ marginLeft: '-12px', marginRight: '-12px', marginTop: '24px' }}>
          <ChartCard style={{ marginLeft: '12px', marginRight: '12px' }} bordered={false}>
            <Row style={{ marginBottom: 24 }}>
              <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                <p
                  style={{
                    fontSize: '18px',
                    marginTop: -15,
                    textAlign: 'center',
                    fontWeight: 'bold',
                  }}
                >
                  Thống Kê Giảng Viên Theo Khoa
                </p>
                <Divider />
                <Chart5
                  data={giangVienTheoKhoa}
                  loading={this.props.loading}
                  dispatch={this.props.dispatch}
                />
              </Col>
              <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                <p
                  style={{
                    fontSize: '18px',
                    marginTop: -15,
                    textAlign: 'center',
                    fontWeight: 'bold',
                  }}
                >
                  Thống Kê Sinh Viên Theo Khoa
                </p>
                <Divider />
                <Chart5
                  data={sinhVienTheoKhoa}
                  loading={this.props.loading}
                  dispatch={this.props.dispatch}
                />
              </Col>
            </Row>
            <Table2 data={tong} />
          </ChartCard>
        </Row>
        <Row style={{ marginRight: -12, marginLeft: -12 }}>
          <Col xs={24} sm={24} md={24} lg={16} xl={16} style={{ marginTop: 24 }}>
            <div style={{ marginRight: 12, marginLeft: 12, height: '100%' }}>
              <Card
                bordered={false}
                title={<div className="cardTitle">Thống Kê Khai Báo Sức Khỏe</div>}
              >
                <Stacked
                  model={danhSach1}
                  loading={this.props.loading}
                  dispatch={this.props.dispatch}
                />
              </Card>
            </div>
          </Col>
          <Col xs={24} sm={24} md={24} lg={8} xl={8}>
            <div style={{ marginTop: 24 }}>
              <Chart10
                model={danhSach1}
                loading={this.props.loading}
                dispatch={this.props.dispatch}
              />
            </div>
            <div style={{ marginTop: 24 }}>
              <Chart8
                model={danhSach1}
                loading={this.props.loading}
                dispatch={this.props.dispatch}
              />
            </div>
            <div style={{ marginTop: 24 }}>
              <Chart9
                model={danhSach1}
                loading={this.props.loading}
                dispatch={this.props.dispatch}
              />
            </div>
          </Col>
        </Row>
        <div style={{ marginTop: 24 }}>
          <Card bordered={false}>
            <Table3 />
          </Card>
        </div>
      </>
    );
  }
}

export default DashBoard;
