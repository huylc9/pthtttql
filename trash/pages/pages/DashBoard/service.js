import Service from '@/services/BaseService';
import { ip } from '@/services/ip';
import axios from '@/utils/axios';

class ExtendedService extends Service {
  getNguoiDung = async payload => axios.get(`${ip}/${this.url}/nguoi-dung`, { params: payload });

  getGiangVien = async payload =>
    axios.get(`${ip}/${this.url}/giang-vien-theo-khoa`, { params: payload });

  getThongBao = async payload => axios.get(`${ip}/${this.url}/thong-bao`, { params: payload });

  getPhanHoi = async payload => axios.get(`${ip}/${this.url}/phan-hoi`, { params: payload });

  getSinhVien = async payload =>
    axios.get(`${ip}/${this.url}/sinh-vien-theo-khoa`, { params: payload });

  getTaiKhoanKhach = async payload =>
    axios.get(`${ip}/${this.url}/tai-khoan-khach`, { params: payload });

  getKhaiBaoSucKhoeTheoNgay = async payload =>
    axios.get(`${ip}/${this.url}/khai-bao-suc-khoe/${payload.days}`, { params: payload });

  getKhaiBaoCachLyTheoNgay = async payload =>
    axios.get(`${ip}/${this.url}/khai-bao-cach-ly/${payload.days}`, { params: payload });

  getKhaiBaoSucKhoe = async payload =>
    axios.get(
      `${ip}/${this.url}/khai-bao-suc-khoe/from/${payload.fromTimestamp}/to/${payload.toTimestamp}`,
      { params: payload },
    );

  getKhaiBaoCachLy = async payload =>
    axios.get(
      `${ip}/${this.url}/khai-bao-cach-ly/from/${payload.fromTimestamp}/to/${payload.toTimestamp}`,
      { params: payload },
    );

  getDanhSachKhaiBaoSucKhoe = async payload =>
    axios.get(
      `${ip}/khai-bao-suc-khoe`,
      { params: payload },
    );

  getDanhSachKhaiBaoCachLy = async payload =>
    axios.get(
      `${ip}/khai-bao-cach-ly`,
      { params: payload },
    );
}

export default new ExtendedService({ name: 'dashboard', formData: false, url: 'dashboard/thong-ke' });
