import { Button, Divider, Popconfirm, Tag, Table, Input, Icon } from 'antd';
import { connect } from 'dva';
import moment from 'moment';
import React from 'react';
import uuidv1 from 'uuid/v1';
import { IColumn } from '@/utils/interfaces';
import { IBase } from '@/utils/base';
import { SmileTwoTone, FrownTwoTone, SearchOutline } from '@ant-design/icons';
import TableBase from '@/components/Table/Table.tsx';
import Highlighter from 'react-highlight-words';
import _ from 'lodash';
import ViewModal from './modal';
import Form from '../Form';

interface Props {
    khaibaocachly: IBase;
    loading: boolean;
    dispatch: Function;
}

@connect(({ khaibaocachly, loading }) => ({
    khaibaocachly,
    loading: loading.models.khaibaocachly,
}))
class KhaiBaoCachLy extends React.Component<Props> {
    public state = {
    };

    public componentDidMount() { }

    public handleView = (record: any) => {
        const { model } = this.props;
        this.props.dispatch({
            type: 'khaibaocachly/changeState',
            payload: {
                record,
                visible: true,
            },
        });
        this.props.dispatch({
            type: 'khaibaocachly/getByMSV',
            payload: {
                maSv: record.maSv,
                page: 1,
                limit: 10,
            },
        });
    };

    public render() {
        const columns: IColumn[] = [
            {
                title: 'STT',
                dataIndex: 'index',
                align: 'center',
            },

            {
                title: 'Họ và Tên',
                dataIndex: 'nguoiKhaiBao',
                align: 'center',
                width: '300px',
                render: d => _.get(d, '[0].hoTen', ''),
            },
            {
                title: 'Mã sinh viên',
                dataIndex: 'maSv',
                align: 'center',
                search: 'search',
            },
            {
                title: 'Trạng thái',
                dataIndex: 'trangThaiCachLy',
                align: 'center',
                // search: 'filter',
                render: val => (
                    (val ? <Tag color="red">Đang Cách Ly</Tag> : <Tag color="green">Không Cách Ly</Tag>)
                 ),
            },
            {
                title: 'Thời gian khai báo',
                dataIndex: 'thoiGianKhaiBao',
                align: 'center',
                render: val => (
                    <span title={moment(val).format('DD/MM/YYYY HH:mm:ss')}>
                        {moment(val).format('DD/MM/YYYY')}
                    </span>
                ),
                search: 'sort',
            },


        ];

        return (
            <div >
                <TableBase
                    model={this.props.khaibaocachly}
                    modelName="khaibaocachly"
                    loading={this.props.loading}
                    dispatch={this.props.dispatch}
                    columns={columns}
                    cond={{}}
                    Form={Form}
                    title="Thông tin khai báo cách ly"
                    tableProp={{
                        onRow: record => ({
                            onClick: () => { this.handleView(record) },
                            style: { cursor: 'pointer' },
                        }),
                        scroll: { x: 600 },
                    }}

                />
                <ViewModal
                    model={this.props.khaibaocachly}
                    modelName="khaibaocachly"
                    loading={this.props.loading}
                    dispatch={this.props.dispatch}
                />
            </div>
        );
    }
}

export default KhaiBaoCachLy;
