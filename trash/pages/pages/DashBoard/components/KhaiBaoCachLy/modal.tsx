import React from 'react';
import { connect } from 'dva';
import { IBase } from '@/utils/base';
import { Modal, Card, Table, Tag, Row, Col, Icon } from 'antd';
import _ from 'lodash';
import data from '@/utils/data';
import moment from 'moment';
import { SmileTwoTone, FrownTwoTone } from '@ant-design/icons';


class ViewModal extends React.Component<Props> {
  componentDidMount() { }

  handleOk = () => {
    const { modelName, cond } = this.props;
    this.props.dispatch({
      type: `${modelName}/changeState`,
      payload: {
        visible: false,
      },
    });
  };

  handleCancel = () => {
    const { modelName, cond } = this.props;
    this.props.dispatch({
      type: `${modelName}/changeState`,
      payload: {
        visible: false,
      },
    });
  };

  onChange = pagination => {
    const { modelName, cond, model } = this.props;
    const { current: page, pageSize: limit } = pagination;
    const {
      record: { maSv },
    } = model;
    this.props.dispatch({
      type: `${modelName}/getByMSV`,
      payload: {
        maSv,
        page,
        limit,
      },
    });
  };

  render() {
    const { loading, model } = this.props;
    console.log('prop', model);
    let {
      danhSachGui,
      pagingModal: { page, limit },
      visible,
      totalModal,
      record,
    } = model;
    console.log('nodelee', this.props);
    danhSachGui = danhSachGui.map((item: any, index: any) => ({
      ...item,
      index: index + 1 + (page - 1) * limit,
    }));
    const hoten = _.get(record, 'nguoiKhaiBao[0].hoTen', 'Chưa cập nhật')
    const masv = _.get(record, 'nguoiKhaiBao[0].maSv', 'Chưa cập nhật')
    const columns = [
      {
        title: 'STT',
        dataIndex: 'index',
        align: 'center',
        width: 60,
      },
      {
        title: 'Thời gian khai báo',
        dataIndex: 'thoiGianKhaiBao',
        align: 'center',
        width: 150,
        render: val => (
          <span title={moment(val).format('DD/MM/YYYY HH:mm:ss')}>
            {moment(val).format('DD/MM/YYYY')}
          </span>
        ),
        search: 'sort',
      },
      {
        title: 'Trạng thái cách ly',
        dataIndex: 'trangThaiCachLy',
        align: 'center',
        width: '150px',
        render: val => (
          (val ? <Tag color="red">Đang Cách Ly</Tag> : <Tag color="green">Không Cách Ly</Tag>)
        ),
      },
      {
        title: 'Ngày cách ly',
        dataIndex: 'ngayCachLy',
        align: 'center',
        width: '150px',
        render: val => (
          <span title={moment(val).format('DD/MM/YYYY HH:mm:ss')}>
            {moment(val).format('DD/MM/YYYY')}
          </span>
        ),
      },

      {
        title: 'Số ngày',
        dataIndex: 'soNgay',
        align: 'center',
        width: '100px',
      },
      {
        title: 'Hình thức cách ly',
        dataIndex: 'hinhThucCachLy',
        align: 'center',
        width: '150px',
        render: val => (<div style={{ fontSize: 10 }}>{val ? <Tag color="green">Tại Nhà</Tag> : <Tag color="red"> Tập Trung</Tag>}</div>),
      },
      {
        title: 'Ghi chú',
        dataIndex: 'ghiChu',
        align: 'center',
        width: '150px',
      },

    ];
    return (
      <Modal
        title="Danh sách khai báo cách ly chi tiết"
        visible={visible}
        destroyOnClose
        onOk={this.handleOk}
        onCancel={this.handleCancel}
        width="80%"
        footer={null}
      >
        <Card bordered={false} >
          <Row gutter={24}>
            <Col span={12}><h3>{hoten}</h3></Col>
            <Col span={12} style={{ textAlign: 'right' }}><Tag color="green" style={{ fontSize: 14 }}>Mã SV: {masv}</Tag></Col>
          </Row>
          <Table

            style={{ marginBottom: 10 }}
            loading={loading}
            bordered
            onChange={this.onChange}
            columns={columns}
            dataSource={danhSachGui}
            pagination={{
              total: totalModal,
              showSizeChanger: true,
              pageSizeOptions: ['10', '20', '30', '40'],
            }}
            scroll={{ y: 400 }}
          />
        </Card>
      </Modal>
    );
  }
}

export default ViewModal;
