/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Form, Input, Card, Row, Col, Button, DatePicker, Select, Checkbox, Spin } from 'antd';
import _ from 'lodash';
import { getRecordValue } from '@/utils/utils';
import rules from '@/utils/rules';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import {
  formItemLayout,
  tailFormItemLayout,
  handleValuesChange,
  handleCommonSubmit,
  getSubmitText,
} from '@/utils/form';

type KhaiBaoSucKhoe_Props = {
  model: { edit; record };
  cond: object;
  loading: boolean;
  dispatch: Function;
  form: { validateFieldsAndScroll; getFieldDecorator };
};

class KhaiBaoSucKhoe extends React.Component<KhaiBaoSucKhoe_Props> {
  componentDidMount() { }

  state = {
    disable: true,
    checked: false,
  };

  toggleDisable = checked => {
    if (checked) {
      this.setState({ disable: false });
    } else {
      this.setState({ disable: true });
    }
  };

  handleSubmit = e => {
    const {
      form,
      model: { edit, record },
      cond,
    } = this.props;
    e.preventDefault();
    form.validateFieldsAndScroll((err, values) => {
      if (err) return;

      if (values.trangThai) {
        values.trangThai = values.trangThai.map((val: any) => Number(val));
      } else {
        values.trangThai = [];
      }
      if (values.tiepXuc) {
        values.tiepXuc = values.tiepXuc.map((val: any) => Number(val));
      } else {
        values.tiepXuc = [];
      }
      if (values.sot == '') {
        values.sot = null;
      } else {
        values.sot = Number(values.sot);
      }
      if (
        values.trangThai == [] &&
        values.tiepXuc == [] &&
        values.sot == null &&
        values.noiDaDiQua == ''
      ) {
        values.anToan = true;
      } else {
        values.anToan = false;
      }
      console.log('values :', values);
      handleCommonSubmit(values, this.props);
    });
  };

  render() {
    const {
      model,
      form: { getFieldDecorator },
      cond,
      loading,
    } = this.props;

    return (
      <div className="box">
        <Card
          bordered
          title={<div className="cardTitle">{!model.edit ? 'Thêm mới' : 'Chỉnh sửa'}</div>}
        >
          <GridContent>
            <Row>
              <Col span={18}>
                <Form onSubmit={this.handleSubmit}>
                  <Spin spinning={loading}>
                    <Form.Item>
                      <Form.Item label="Triệu chứng">
                        {getFieldDecorator('trangThai', {
                          initialValue: getRecordValue(model, cond, 'trangThai', ''),
                        })(
                          <Checkbox.Group style={{ width: '100%' }}>
                            <Row>
                              <Row>
                                <Col xs={24} md={8}>
                                  <Checkbox
                                    onClick={this.toggleDisable}
                                    checked={this.state.checked}
                                    value="0"
                                  >
                                    Sốt
                                  </Checkbox>
                                </Col>
                                <Col xs={24} md={8}>
                                  <Checkbox value="1">Ho</Checkbox>
                                </Col>
                                <Col xs={24} md={8}>
                                  <Checkbox value="2">Khó thở</Checkbox>
                                </Col>
                              </Row>

                              <Row>
                                <Col xs={24} md={8}>
                                  <Checkbox value="3">Viêm Phổi</Checkbox>
                                </Col>
                                <Col xs={24} md={8}>
                                  <Checkbox value="4">Đau họng</Checkbox>
                                </Col>
                                <Col xs={24} md={8}>
                                  <Checkbox value="5">Mệt mỏi</Checkbox>
                                </Col>
                              </Row>
                              <Row>
                                <Col xs={24}>
                                  <Checkbox value="6">Khác</Checkbox>
                                </Col>
                              </Row>
                            </Row>
                          </Checkbox.Group>,
                        )}
                      </Form.Item>
                      <Form.Item label="Nếu sốt, hãy nhập nhiệt độ">
                        {getFieldDecorator('sot', {
                          initialValue: getRecordValue(model, cond, 'sot', ''),
                          rules: [...rules.number(45, 20)],
                        })(<Input suffix="độ C" disabled={this.state.disable} />)}
                      </Form.Item>
                      <Form.Item label="Tiếp xúc với đối tượng">
                        {getFieldDecorator('tiepXuc', {
                          initialValue: getRecordValue(model, cond, 'tiepXuc', ''),
                        })(
                          <Checkbox.Group style={{ width: '100%' }}>
                            <Row>
                              <Col>
                                <Checkbox value="0">Người bệnh hoặc nghi nhiễm</Checkbox>
                              </Col>
                              <Col>
                                <Checkbox value="1">Người từ nước có bệnh</Checkbox>
                              </Col>
                              <Col>
                                <Checkbox value="2">Nguười có biểu hiện</Checkbox>
                              </Col>
                            </Row>
                          </Checkbox.Group>,
                        )}
                      </Form.Item>
                      <Form.Item label="Những nơi đã đi qua" style={{ width: '100%' }}>
                        {getFieldDecorator('cacNoiDaDiQua', {
                          initialValue: getRecordValue(model, cond, 'cacNoiDaDiQua', ''),
                        })(<Input style={{ width: '100%' }} />)}
                      </Form.Item>
                      <Button.Group>
                        <Button type="primary" icon="plus" htmlType="submit">
                          {getSubmitText(model)}
                        </Button>
                      </Button.Group>
                    </Form.Item>
                  </Spin>
                </Form>
              </Col>
            </Row>
          </GridContent>
        </Card>
      </div>
    );
  }
}

const WrappedForm = Form.create({ name: 'KhaiBaoSucKhoe', onValuesChange: handleValuesChange })(KhaiBaoSucKhoe);

export default WrappedForm;
