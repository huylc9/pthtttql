import React from 'react';
import { connect } from 'dva';
import { IBase } from '@/utils/base';
import { Modal, Card, Table, Tag, Row, Col } from 'antd';
import _ from 'lodash';
import data from '@/utils/data';
import moment from 'moment';
import { SmileTwoTone, FrownTwoTone } from '@ant-design/icons';


class ViewModal extends React.Component<Props> {
    componentDidMount() { }

    handleOk = () => {
        const { modelName, cond } = this.props;
        this.props.dispatch({
            type: `${modelName}/changeState`,
            payload: {
                visible: false,
            },
        });
    };

    handleCancel = () => {
        const { modelName, cond } = this.props;
        this.props.dispatch({
            type: `${modelName}/changeState`,
            payload: {
                visible: false,
            },
        });
    };

    onChange = pagination => {
        const { modelName, cond, model } = this.props;
        const { current: page, pageSize: limit } = pagination;
        const {
            record: { maSv },
        } = this.props.model;
        this.props.dispatch({
            type: `${modelName}/getByMSV`,
            payload: {
                maSv,
                page,
                limit,
            },
        });
    };

    render() {
        const { loading, model } = this.props;
        console.log('prop', model);
        let {
            danhSachGui,
            pagingModal: { page, limit },
            visible,
            totalModal,
            record,
        } = model;
        console.log('nodelee', this.props);
        danhSachGui = danhSachGui.map((item: any, index: any) => ({
            ...item,
            index: index + 1 + (page - 1) * limit,
        }));
        const hoten = _.get(record, 'nguoiDung.hoTen', 'Chưa cập nhật')
        const masv = _.get(record, 'nguoiDung.maSv', 'Chưa cập nhật')
        const columns = [
            {
                title: 'STT',
                dataIndex: 'index',
                align: 'center',
                width: 50,
            },
            {
                title: 'Thời gian khai báo',
                dataIndex: 'thoiGianKhaiBao',
                align: 'center',
                width: 110,
                render: val => (
                    <span title={moment(val).format('DD/MM/YYYY HH:mm:ss')}>
                        {moment(val).format('DD/MM/YYYY')}
                    </span>
                ),
                search: 'sort',
            },
            {
                title: 'Trạng Thái',
                dataIndex: 'trangThai',
                align: 'center',
                width: '150px',
                render: arr =>
                Array.isArray(arr) && arr.map((value: any) => data.trangThaiSucKhoe[value]).join(),
            },
            {
                title: 'Đã Tiếp Xúc',
                dataIndex: 'tiepXuc',
                align: 'center',
                width: '150px',
                render: arr => Array.isArray(arr) && arr.map((value: any) => data.tiepXuc[value]).join(),
            },

            {
                title: 'Các nơi đã đi qua',
                dataIndex: 'cacNoiDaDiQua',
                align: 'center',
                width: '150px',
            },
            {
                title: 'Sốt',
                dataIndex: 'sot',
                align: 'center',
                width: '100px',
            },

        ];
        return (
            <Modal
                title="Danh sách những ngày khai báo sức khỏe"
                visible={visible}
                destroyOnClose
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                width="80%"
                footer={null}
            >
                <Card bordered={false} >
                    <Row gutter={24}>
                        <Col span={12}><h3>{hoten}</h3></Col>
                        <Col span={12} style={{ textAlign: 'right' }}><Tag color="green" style={{ fontSize: 14 }}>Mã SV: {masv}</Tag></Col>
                    </Row>
                    <Table

                        style={{ marginBottom: 10}}
                        loading={loading}
                        bordered
                        onChange={this.onChange}
                        columns={columns}
                        dataSource={danhSachGui}
                        pagination={{
                            total: totalModal,
                            showSizeChanger: true,
                            pageSizeOptions: ['10', '20', '30', '40'],
                        }}
                        scroll={{ y: 400, x: 500 }}
                    />
                </Card>
            </Modal>
        );
    }
}

export default ViewModal;
