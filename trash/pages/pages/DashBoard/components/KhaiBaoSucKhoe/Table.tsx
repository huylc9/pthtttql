import { Button, Tag, Input, Button } from 'antd';
import { connect } from 'dva';
import moment from 'moment';
import React from 'react';
import TableBase from '@/components/Table/Table.tsx';
import { IBase } from '@/utils/base';
import data from '@/utils/data';
import { IColumn } from '@/utils/interfaces';
import { SmileTwoTone, FrownTwoTone, SearchOutline } from '@ant-design/icons';
import Highlighter from 'react-highlight-words';
import ViewModal from './modal';
import Form from '../Form';

interface Props {
    khaibaosuckhoe: IBase;
    loading: boolean;
    dispatch: Function;
}

@connect(({ khaibaosuckhoe, loading }) => ({
    khaibaosuckhoe,
    loading: loading.models.khaibaosuckhoe,
}))
class KhaiBaoSucKhoe extends React.Component<Props> {
    public state = {
        searchText: '',
        searchedColumn: '',
    };

    public componentDidMount() { }

    public handleView = (record: any) => {
        const { modelName, model } = this.props;
        this.props.dispatch({
            type: 'khaibaosuckhoe/changeState',
            payload: {
                record,
                visible: true,
            },
        });
        this.props.dispatch({
            type: 'khaibaosuckhoe/getByMSV',
            payload: {
                maSv: record.maSv,
                page: 1,
                limit: 10,
            },
        });
    };

    public render() {
        const columns: IColumn[] = [
            {
                title: 'STT',
                dataIndex: 'index',
                align: 'center',
            },
            {
                title: 'Họ và Tên',
                dataIndex: 'nguoiDung',
                align: 'center',
                render: nguoiDung => _.get(nguoiDung, 'hoTen', ''),
            },
            {
                title: 'Mã sinh viên',
                dataIndex: 'maSv',
                align: 'center',
                search: 'search',
            },

            {
                title: 'Trạng Thái',
                dataIndex: 'anToan',
                align: 'center',
                render: val => (
                    val ? <Tag color="green">An Toàn</Tag> : <Tag color="red"> Không An Toàn</Tag>
                ),
            },
            {
                title: 'Thời gian khai báo',
                dataIndex: 'thoiGianKhaiBao',
                align: 'center',
                render: val => (
                    <span title={moment(val).format('DD/MM/YYYY HH:mm:ss')}>
                        {moment(val).format('DD/MM/YYYY')}
                    </span>
                ),
                search: 'sort',
            },
        ];

        return (
            <div>
                <TableBase
                    model={this.props.khaibaosuckhoe}
                    modelName="khaibaosuckhoe"
                    loading={this.props.loading}
                    dispatch={this.props.dispatch}
                    columns={columns}
                    cond={{}}
                    Form={Form}
                    title="Thông tin khai báo sức khỏe"
                    tableProp={{
                        onRow: record => ({
                            onClick: () => { this.handleView(record) },
                            style: { cursor: 'pointer' },
                        }),
                        scroll: { x: 600 },
                    }}

                >
                </TableBase>
                <ViewModal
                    model={this.props.khaibaosuckhoe}
                    modelName="khaibaosuckhoe"
                    loading={this.props.loading}
                    dispatch={this.props.dispatch}
                />
            </div>
        );
    }
}

export default KhaiBaoSucKhoe;
