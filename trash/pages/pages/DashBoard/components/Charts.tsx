import React from 'react';
import {
  G2,
  Chart,
  Geom,
  Axis,
  Tooltip,
  Coord,
  Label,
  Legend,
  View,
  Guide,
  Shape,
  Facet,
  Util,
} from 'bizcharts';
import DataSet from '@antv/data-set';
import moment from 'moment';
import _ from 'lodash';
import { connect } from 'dva';
import uuidv1 from 'uuid/v1';
import { Tag, Table, List, Badge, Row, Col, Menu, Spin } from 'antd';
import { ChartCard, MiniArea, MiniProgress, Pie, yuan, MiniBar } from 'ant-design-pro/lib/Charts';
import Trend from 'ant-design-pro/lib/Trend';
import NumberInfo from 'ant-design-pro/lib/NumberInfo';
import numeral from 'numeral';
import { enquireScreen } from 'enquire-js';
import TableBase from '@/components/Table/Table.tsx';
import styles from './style.css';
import KhaiBaoSucKhoe from './KhaiBaoSucKhoe/Table';
import KhaiBaoCachLy from './KhaiBaoCachLy/Table';

export class Chart1 extends React.Component {
  componentDidMount() {}

  render() {
    const danhSachNguoiDung = this.props.data;
    const tong = danhSachNguoiDung?.tongGiangVien + danhSachNguoiDung?.tongSinhVien;
    return (
      <>
        <Spin spinning={this.props.loading}>
          <ChartCard
            title="Tổng Số Tài Khoản"
            total={tong || '0'}
            className={styles.dau}
            bordered={false}
            contentHeight={78}
          >
            <Badge color="red" text={`Số Giảng Viên:  ${danhSachNguoiDung?.tongGiangVien}`} /> <br />
            <Badge color="blue" text={`Số Sinh Viên:  ${danhSachNguoiDung?.tongSinhVien}`} />
          </ChartCard>
        </Spin>
      </>
    );
  }
}

export class Chart2 extends React.Component {
  componentDidMount() {}

  render() {
    const taiKhoanKhach = this.props.data;
    const tong = _.get(taiKhoanKhach, 'total', '...');
    return (
      <>
        <Spin spinning={this.props.loading}>
          <ChartCard
            title="Số Tài Khoản Khách"
            total={tong}
            className={styles.dau}
            bordered={false}
            contentHeight={78}
          >
            <Badge color="red" text="Đang cập nhật" /> <br />
            <Badge color="red" text="Đang cập nhật" /> <br />
            <Badge color="blue" text="Đang cập nhật" />
          </ChartCard>
        </Spin>
      </>
    );
  }
}

export class Chart3 extends React.Component {
  componentDidMount() { }

  render() {
    const danhSachData = this.props.data;
    const tong = danhSachData?.thongBaoHeThong + danhSachData?.thongBaoQuanTri + danhSachData?.thongBaoGiangVien
    const danhSachThongBao = [
      {
        x: `${'TB Hệ Thống' + ':  '}${danhSachData?.thongBaoHeThong || '0'}`,
        y: 'blue',
      },
      {
        x: `${'TB Quản Trị' + ':  '}${danhSachData?.thongBaoQuanTri || '0'}`,
        y: 'red',
      },
      {
        x: `${'TB Giảng Viên' + ':  '}${danhSachData?.thongBaoGiangVien || '0'}`,
        y: 'green',
      },
    ];
    return (
      <>
        <Spin spinning={this.props.loading}>
            <ChartCard
              title="Số Lượng Thông Báo"
              className={styles.cuoi}
              bordered={false}
              contentHeight={78}
            total={tong || '0'}
            >
              {
                danhSachThongBao.map(index => (
                  <div>
                    <Badge color={index.y} text = {index.x}/>
                  </div>
                ))
              }
            </ChartCard>
          </Spin>
      </>
    );
  }
}

export class Chart4 extends React.Component {
  componentDidMount() { }

  render() {
    const danhSachData = this.props.data;
    const khoa_hoidap_da = danhSachData?.KHOA?.HOI_DAP?.DA_TRA_LOI;
    const khoa_hoidap_chua = danhSachData?.KHOA?.HOI_DAP?.CHUA_TRA_LOI;
    const khoa_kythuat_da = danhSachData?.KHOA?.KY_THUAT?.DA_TRA_LOI;
    const khoa_kythuat_chua = danhSachData?.KHOA?.KY_THUAT?.CHUA_TRA_LOI;
    const truong_hoidap_da = danhSachData?.TRUONG?.HOI_DAP?.DA_TRA_LOI;
    const truong_hoidap_chua = danhSachData?.TRUONG?.HOI_DAP?.CHUA_TRA_LOI;
    const truong_kythuat_da = danhSachData?.TRUONG?.KY_THUAT?.DA_TRA_LOI;
    const truong_kythuat_chua = danhSachData?.TRUONG?.KY_THUAT?.CHUA_TRA_LOI;
    const datraloi = khoa_hoidap_da + khoa_kythuat_da + truong_hoidap_da + truong_kythuat_da;
    const chuatraloi = khoa_hoidap_chua + khoa_kythuat_chua + truong_hoidap_chua + truong_kythuat_chua;
    const tong = khoa_hoidap_chua + khoa_hoidap_da + khoa_kythuat_chua + khoa_kythuat_da + truong_hoidap_chua + truong_hoidap_da + truong_kythuat_chua + truong_kythuat_da;
    return (
      <>
        <Spin spinning={this.props.loading}>
          <ChartCard
            title="Số Lượng Phản Hồi"
            className={styles.cuoi2}
            bordered={false}
            total={tong || '0'}
            contentHeight={78}
          >
            <Badge color="green" text={`Đã Trả Lời:  ${datraloi}`} /> <br />
            <Badge color="red" text={`Chưa Trả Lời:  ${chuatraloi}`} />
          </ChartCard>
        </Spin>
      </>
    );
  }
}

export class Chart5 extends React.Component {
  componentDidMount() {}

  render() {
    const danhSachData = this.props.data || '[]';
    const datada = danhSachData.map(ecec => {
      const tmp = {};
      tmp.x = ecec.tenKhoa;
      tmp.y = ecec.total;
      return tmp;
    })
    console.log('ssss', datada);
    return (
      <Spin spinning={this.props.loading}>
        <Pie
          total={datada.reduce((pre, now) => now.y + pre, 0)}
          subTitle="Tổng"
          data={datada}
          height={250}
        />
      </Spin>
    );
  }
}


// export class Chart7 extends React.Component {

//   componentDidMount() {}

//   render() {
//     const danhSachKhaiBaoSucKhoe = this.props.model || '[]';
//     const dakhaibao = danhSachKhaiBaoSucKhoe.map(d => d.anToan + d.khongAnToan);
//     const danhSachData = danhSachKhaiBaoSucKhoe.map(d => {
//       const tmp = {};
//       tmp.y = d.anToan + d.khongAnToan;
//       tmp.x = moment(d.ngay).format('DD/MM/YYYY');
//       return tmp;
//     });
//     danhSachData.reverse();
//     return (
//       <>
//         <ChartCard
//           title="Đã Khai Báo Hôm Nay"
//           total={dakhaibao[0]}
//           contentHeight={46}
//           className={styles.dau}
//           bordered={false}
//         >
//           <MiniArea line height={45} data={danhSachData} />
//         </ChartCard>
//       </>
//     );
//   }
// }

export class Chart8 extends React.Component {
  componentDidMount() {}

  render() {
    const danhSachKhaiBaoSucKhoe = this.props.model || '[]';
    const binhthuong = danhSachKhaiBaoSucKhoe.map(d => d.anToan);
    const danhSachData = danhSachKhaiBaoSucKhoe.map(d => {
      const tmp = {};
      tmp.y = d.anToan;
      tmp.x = moment(d.ngay).format('DD/MM/YYYY');
      return tmp;
    });
    danhSachData.reverse();
    return (
      <>
        <ChartCard
          title="Số tài khoản an toàn"
          total={binhthuong[0]}
          className={styles.dau}
          bordered={false}
        >
          <MiniBar height={46} data={danhSachData} color="#3578E5" />
        </ChartCard>
      </>
    );
  }
}

export class Chart9 extends React.Component {
  componentDidMount() {}

  render() {
    const danhSachKhaiBaoSucKhoe = this.props.model || '[]';
    const khongbinhthuong = danhSachKhaiBaoSucKhoe.map(d => d.khongAnToan);
    const danhSachData = danhSachKhaiBaoSucKhoe.map(d => {
      const tmp = {};
      tmp.y = d.khongAnToan;
      tmp.x = moment(d.ngay).format('DD/MM/YYYY');
      return tmp;
    });
    console.log('daaaa', danhSachData);
    danhSachData.reverse();
    return (
      <>
        <ChartCard
          title="Số tài khoản không an toàn"
          total={khongbinhthuong[0]}
          className={styles.dau}
          bordered={false}
        >
          <MiniBar height={46} data={danhSachData} color="#cc0d00" />
        </ChartCard>
      </>
    );
  }
}
export class Chart10 extends React.Component {
  componentDidMount() { }

  render() {
    const danhSachKhaiBaoSucKhoe = this.props.model || '[]';
    const tong = danhSachKhaiBaoSucKhoe.map(d => d.khongAnToan + d.anToan);
    const danhSachData = danhSachKhaiBaoSucKhoe.map(d => {
      const tmp = {};
      tmp.y = d.khongAnToan + d.anToan;
      tmp.x = moment(d.ngay).format('DD/MM/YYYY');
      return tmp;
    });
    console.log('daaaa', danhSachData);
    danhSachData.reverse();
    return (
      <>
        <ChartCard
          title="Số tài khoản đã khai báo"
          total={tong[0]}
          className={styles.dau}
          bordered={false}
        >
          <MiniArea height={46} data={danhSachData} />
        </ChartCard>
      </>
    );
  }
}

export class Stacked extends React.Component {
  componentDidMount() {}

  render() {
    let danhSachKhaiBaoSucKhoe1 = this.props.model || '[]';
    danhSachKhaiBaoSucKhoe1 = danhSachKhaiBaoSucKhoe1.map(d => {
      // them truong tong
      const dakhaibao = d.anToan + d.khongAnToan;
      const ngay = moment(d.ngay).format('DD/MM');
      return {
        ...d,
        dakhaibao,
        ngay,
      };
    });
    let danhSachKhaiBaoSucKhoe = danhSachKhaiBaoSucKhoe1.filter(
      (value, index, array) => index < 7,
    );

    danhSachKhaiBaoSucKhoe.reverse();
    danhSachKhaiBaoSucKhoe = danhSachKhaiBaoSucKhoe.map(item => ({
      ...item,
      'Không an toàn': item.khongAnToan,
      'An toàn': item.anToan,
    }))
    const ds = new DataSet();
    const dv = ds.createView().source(danhSachKhaiBaoSucKhoe);

    dv.transform({
      // truc y
      type: 'fold',
      fields: ['Không an toàn', 'An toàn'],
      key: 'key',
      value: 'value',
      retains: ['ngay', 'dakhaibao'],
    });
    const colorMap = {
      'An toàn': '#3578E5',
      'Không an toàn': '#cc0d00',
    };
    return (
      <div>
        <Chart height={392} data={dv} forceFit style={{ marginLeft: -50 }}>
          <Legend />
          <Coord />
          <Axis name="ngay" />
          <Tooltip />
          <Geom
            type="intervalStack"
            position="ngay*value" // ddinh dang x,y
            color={[
              'key',
              function(key) {
                return colorMap[key];
              },
            ]}
          >
            {/* <Geom type="line" position="ngay*dakhaibao" size={2} />
            <Geom
              type="point"
              position="ngay*dakhaibao"
              size={4}
              shape="circle"
              style={{
                stroke: '#fff',
                lineWidth: 1,
              }}
            /> */}
          </Geom>
        </Chart>
      </div>
    );
  }
}

const columns = [
  {
    title: 'STT',
    dataIndex: 'index',
    align: 'center',
    width: 60,
  },
  {
    title: 'Tên Khoa',
    dataIndex: 'tenKhoa',
    key: 'tenKhoa',
    align: 'center',
    render: text => <a>{text}</a>,
  },
  {
    title: 'Mã Khoa',
    dataIndex: 'makhoa',
    align: 'center',
    render: makhoa => (
    <Tag color = "green">{makhoa}</Tag>
    ),
  },
  {
    title: 'Số Giảng Viên',
    key: 'giangVien',
    align: 'center',
    dataIndex: 'giangVien',
  },
  {
    title: 'Số Sinh Viên',
    key: 'sinhVien',
    dataIndex: 'sinhVien',
    align: 'center',
  },

];

export class Table2 extends React.Component {
  componentDidMount() {

  }

  render() {
    const data1 = this.props.data || '[]';

    return (
      <Table columns={columns} dataSource={data1} scroll={{ y: 240, x: 600 }} bordered = {false}/>
    )
  }
}

export class Table3 extends React.Component {
  state = {
    current: '0',
  };

  componentDidMount() {

  }

  handleClick = (e, days) => {
    console.log('click ', e);
    this.setState({
      current: e.key,
    });
  };

  render() {
    const { current } = this.state;
    const tmp = key => {
      if (key === '0') {
        return (

             <KhaiBaoSucKhoe />
        );
      }
      if (key === '1') {
        return (
           <KhaiBaoCachLy />

        );
      }
    }

    return (
      <>
        <Menu onClick={this.handleClick} selectedKeys={[this.state.current]} mode="horizontal" style={{ marginTop: -17 }}>
          <Menu.Item key="0">
            Khai Báo Sức Khỏe
                  </Menu.Item>
          <Menu.Item key="1">
            Khai Báo Cách Ly
                  </Menu.Item>

        </Menu>
        {
          tmp(current)
        }
      </>
    );
  }
}
