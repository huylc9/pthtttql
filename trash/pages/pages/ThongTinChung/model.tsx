import base from '@/utils/base';
import modelExtend from 'dva-model-extend';
import Services from '@/pages/ThongTinChung/service';

export default modelExtend(base(Services), {
    namespace: 'thongtinchung',
})
