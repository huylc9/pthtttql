import React from 'react';
import { connect } from 'dva';
import uuidv1 from 'uuid/v1';
import { Button, Divider, Popconfirm, Card } from 'antd';
import Form from './components/Form';
import { IBase } from '@/utils/base';
import _ from 'lodash';
import FormDrawer from '@/components/Drawer/FormDrawer';

type Props = {
  baivietchung: IBase,
  loading: boolean,
  dispatch: Function,
}
const cond = { danhMuc: 1 };

@connect(({ baivietchung, loading }) => ({
  baivietchung,
  loading: loading.models.baivietchung,
}))
class BaiVietChung extends React.Component<Props> {
  state = {
  };

  componentDidMount() {
    this.props.dispatch({
      type: 'baivietchung/get',
      payload: {
        cond,
        page: 1,
        limit: 10,
      }
    })
  }

  handleEdit = () => {
    const { baivietchung: { danhSach } } = this.props;
    this.props.dispatch({
      type: 'baivietchung/changeState',
      payload: {
        showDrawer: true,
        edit: true,
        record: _.get(danhSach, '[0]', undefined),
        key: uuidv1(),
        isTouched: false,
      },
    });
  };

  render() {
    const { baivietchung: { danhSach } } = this.props;
    return (
      <div className="box">
        <Card
          bordered={true}
          title={
            <div>
              <div className="cardTitle" style={{ float: 'left' }}>Giới thiệu</div>
              <Button
                type="primary"
                icon="edit"
                onClick={this.handleEdit}
                title="Chỉnh sửa"
                style={{ float: 'right' }}
              >Chỉnh sửa</Button>
            </div>
          }
        >
          {_.get(danhSach, '[0].noiDung', undefined) ? (
            <p style={{ marginTop: 20 }} dangerouslySetInnerHTML={{ __html: `${danhSach[0].noiDung}` }} />
          ) : null}
        </Card>
        <FormDrawer drawerProps={{ width: '70%' }} name={`baivietchung`}>
          <Form
            model={this.props.baivietchung}
            modelName={'baivietchung'}
            cond={cond}
            dispatch={this.props.dispatch}
            loading={this.props.loading}
          />
        </FormDrawer>
      </div>
    );
  }
}

export default BaiVietChung;
