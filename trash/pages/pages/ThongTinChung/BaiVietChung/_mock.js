import _ from 'lodash';
import uuidv1 from 'uuid/v1';

let data = [
  {
    noiDung: 'ABC',
    _id: 'abc'
  },
]

export default {
  // GET
  'GET /api/baivietchung': (req, res) => {
    res.status(200).send({
      data: _.reverse(_.clone(data)),
      total: data.length,
    });
  },
  'DELETE /api/baivietchung/:_id': (req, res) => {
    const { _id } = req.params;
    data = data.filter(item => item._id !== _id);
    res.status(200).send({
      message: 'OK'
    });
  },
  'POST /api/baivietchung': (req, res) => {
    data.push({
      ...req.body,
      _id: uuidv1()
    })
    res.status(200).send({
      message: 'OK'
    });
  },
  'PUT /api/baivietchung/:_id': (req, res) => {
    const { _id } = req.params;
    data = data.map(item => {
      if (item._id !== _id) { return item; }
      return {
        ...item,
        ...req.body,
      }
    })
    res.status(200).send({
      message: 'OK'
    });
  },
}