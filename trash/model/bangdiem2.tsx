import modelExtend from 'dva-model-extend';
import Services from '@/pages/MotCua/BangDiem/service';
import base from '../utils/base';
import notificationAlert from '@/components/Notification';
import { formatMessage } from 'umi/locale';
import _ from 'lodash';

export default modelExtend(base(Services), {
    namespace: 'bangdiem2',
    state: {
    },
    effects: {
        *get({ payload }, { call, put, select }) {
            let currentPayload = { ...payload };
            if (!payload) {
                currentPayload = yield select(state => state.bangdiem2.paging);
            }
            currentPayload.cond = {
                ...currentPayload.cond,
                trangThai: 'DA_TIEP_NHAN',
            }
            const response = yield call(Services.get, currentPayload);
            yield put({
                type: 'changeState',
                payload: {
                    danhSach: _.get(response, 'data.data', []),
                    paging: currentPayload,
                    total: _.get(response, 'data.total', 0),
                },
            });
        },
        *upd({ payload }, { call, put }) {
            yield call(Services.upd2, payload);
            notificationAlert('success', formatMessage({ id: 'SUA_THANH_CONG' }));
            yield put({ type: 'get' });
            yield put({ type: 'bangdiem3/get' });
        },
    },
});
