import modelExtend from 'dva-model-extend';
import Services from '@/services/loaitintuc';
import base from '../utils/base';

export default modelExtend(base(Services), {
  namespace: 'loaitintuc',
});
