import modelExtend from 'dva-model-extend';
import Services from '@/services/giangvien';
import base from '../utils/base';
import notificationAlert from '@/components/Notification';
import { resetPwd } from '@/services/sinhvien';

export default modelExtend(base(Services), {
  namespace: 'giangvien',
  state: {

    modal: false,
    thongTinChiTiet: {},
  },
  effects: {
    *resetPwd({ idUser }, { call, put }) {
      const response = yield call(resetPwd, idUser);
      notificationAlert('success', 'Tạo lại mật khẩu thành công');
      yield put({
        type: 'get',
      });
    },
    * forceActive({ payload }, { call, put }) {
      yield call(Services.forceActive, payload);
      notificationAlert('success', 'Kích hoạt tài khoản thành công');
      yield put({
        type: 'get',
      });
    },
  },
});
