import base from '@/utils/base';
import modelExtend from 'dva-model-extend';
import Services from '@/pages/KhaoSat/service';
import _ from 'lodash';

export default modelExtend(base(Services), {
  namespace: 'khaosat',
  effects: {
    *getResult({ payload }, { call, put, select }) {
      const response = yield call(Services.getId, payload);
      yield put({
        type: 'changeState',
        payload: {
          record: _.get(response, 'data.data', []),
        },
      });
    },
  }
})
