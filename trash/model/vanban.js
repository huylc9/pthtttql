import modelExtend from 'dva-model-extend';
import _ from 'lodash';
import { formatMessage } from 'umi/locale';
import Services from '@/services/vanban';
import notificationAlert from '@/components/Notification';
import base from '../utils/base';

export default modelExtend(base(Services), {
  namespace: 'vanban',
  state: {
    filterInfo: {},
    vanBanDangChon: {},
  },
  effects: {
    *add({ payload }, { call, put }) {
      yield call(Services.add, payload);
      notificationAlert('success', formatMessage({ id: 'THEM_THANH_CONG' }));
      yield put({ type: 'get' });
      yield put({ type: 'changeState', payload: { showDrawer: false } });
    },
    *upd({ payload }, { call, put }) {
      yield call(Services.upd, payload);
      notificationAlert('success', formatMessage({ id: 'SUA_THANH_CONG' }));
      yield put({ type: 'get' });
      yield put({ type: 'changeState', payload: { showDrawer: false } });
    },
    *getById({ id }, { call, put, select }) {
      if (!id) {
        id = yield select(state => state.vanban.vanBanDangChon._id);
      }
      const response = yield call(Services.getById, id);
      yield put({
        type: 'changeState',
        payload: {
          vanBanDangChon: _.get(response, 'data.data', []),
        },
      });
    },
    *addTaiLieu({ payload }, { call, put }) {
      const response = yield call(Services.addTaiLieu, payload);
      yield put({
        type: 'getById',
      });
      notificationAlert('success', formatMessage({ id: 'THEM_THANH_CONG' }));
    },
    *updateTaiLieu({ payload }, { call, put }) {
      const response = yield call(Services.updateTaiLieu, payload);
      yield put({
        type: 'getById',
      });
      notificationAlert('success', formatMessage({ id: 'SUA_THANH_CONG' }));
    },
    *delTaiLieu({ payload }, { call, put }) {
      const response = yield call(Services.delTaiLieu, payload);
      yield put({
        type: 'getById',
      });
      notificationAlert('success', formatMessage({ id: 'XOA_THANH_CONG' }));
    },
  },
});
