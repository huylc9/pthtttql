import modelExtend from 'dva-model-extend';

import _ from 'lodash';
import Services from '@/services/khaibaocachly';

import base from '../utils/base';

export default modelExtend(base(Services), {
  state: {
    // Su dung cho modal
    visible: false,
    danhSachGui: [],
    totalModal: 0,
    pagingModal: {
      page: 1,
      limit: 10,
    },
  },
  namespace: 'khaibaocachly',
  effects: {
    *getByMSV({ payload }, { call, put }) {
      const { page, limit } = payload;
      const response = yield call(Services.getByMSV, payload);

      yield put({
        type: 'changeState',
        payload: {
          danhSachGui: _.get(response, 'data.data', []),
          pagingModal: {
            page,
            limit,
          },
          totalModal: _.get(response, 'data.total', 0),
        },
      });
    },
  },
});
