import modelExtend from 'dva-model-extend';
import _ from 'lodash';
import { formatMessage } from 'umi/locale';
import Services from '@/services/sinhvien';
import notificationAlert from '@/components/Notification';
import { resetPwd } from '@/services/sinhvien';
import base from '../utils/base';

export default modelExtend(base(Services), {
  namespace: 'sinhvien',
  state: {
    thongTinVeSinhVienDangDuocChonDeGuiThongBao: {},
    modal: false,
    thongTinChiTiet: {},
  },
  effects: {
    *upd({ payload, onComplete }, { call, put }) {
      yield call(Services.upd, payload);
      notificationAlert('success', formatMessage({ id: 'SUA_THANH_CONG' }));
      yield put({ type: 'get' });
      yield put({ type: 'changeState', payload: { showDrawer: false } });
      if (!!onComplete) onComplete();
    },
    *resetPwd({ idUser }, { call, put }) {
      yield call(resetPwd, idUser);
      notificationAlert('success', 'Tạo lại mật khẩu thành công');
      yield put({
        type: 'get',
      });
    },
    *changePassword({ payload }, { call, put }) {
      yield call(Services.changePassword, payload);
      notificationAlert('success', 'Thay đổi mật khẩu thành công');
    },
    *forceActive({ payload }, { call, put }) {
      console.log(payload, "payloadforce")
      yield call(Services.forceActive, payload);
      notificationAlert('success', 'Kích hoạt tài khoản thành công');
      yield put({
        type: 'get',
      });
    },
    *getByMaSinhVien({ maSinhVien, onComplete }, { call, put }) {
      const response = yield call(Services.getByMaSinhVien, maSinhVien);
      const thongTinVeSinhVienDangDuocChonDeGuiThongBao = _.get(response, 'data.data', {});
      yield put({
        type: 'changeState',
        payload: {
          thongTinVeSinhVienDangDuocChonDeGuiThongBao,
        },
      });
      onComplete({ success: true, data: thongTinVeSinhVienDangDuocChonDeGuiThongBao });
    },
    *guiThongBaoTheoMaSinhVien({ payload }, { call, put }) {
      const response = yield call(Services.guiThongBaoTheoMaSinhVien, payload);
      notificationAlert('success', 'Gửi thông báo thành công');
      yield put({
        type: 'get',
      });
    },
  },
});
