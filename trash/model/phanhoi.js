import modelExtend from 'dva-model-extend';
import Services from '@/services/phanhoi';
import base from '../utils/base';

export default modelExtend(base(Services), {
    namespace: 'phanhoi',
})
