import modelExtend from 'dva-model-extend';
import _ from 'lodash';
import notificationAlert from '@/components/Notification';
import Services from '@/services/chedochinhsach';
import base from '../utils/base';
// export interface ICheDoChinhSach {
//   _id: string;
//   nguoiHuong: string;
//   nguoiCoCong: string;
//   cheDo: {
//     hangThang: {
//       soSo: string;
//       soTien: number;
//     }
//     BHYT: {
//       trangThai: string;
//       doiTuong: string;
//       namHuong: number;
//     };
//     dungCuChinhHinh: {
//       tenDungCu: string;
//       namCap: number;
//       nienHan: string;
//     };
//     cheDoTieuDuong: {
//       trangThai: string;
//       namHuongGanNhat: number;
//     };
//     troCapGiaoDucDaoTao: {
//       trangThai: string;
//       truongDangHoc: string;
//       thoiGianKhoaHoc: number;
//       mucHoTro: string;
//       namDangHoc: string;
//     }
//   };
//   ghiChu: {
//     hoSoCheDoChuaDuocHuong: {
//       trangThai: string;
//       lyDo: string;
//       noiLuu: string;
//     };
//     lechThongTin: {
//       noiDung: string;
//       dinhChinh: string;
//     }
//   }
// }

export default modelExtend(base(Services), {
  namespace: 'chedochinhsach',
  state: {
    thannhan: {},
    nguoicocong: {},
    modal: false,
  },
  effects: {
    *getByNguoiHuong({ idNguoiHuong }, { call, put }) {
      const response = yield call(Services.getByNguoiHuong, idNguoiHuong);
      yield put({
        type: 'changeState',
        payload: {
          thannhan: _.get(response, 'data.data', {}),
        },
      });
    },
  },
});
