import modelExtend from 'dva-model-extend';
import Services from '@/services/sukien';
import base from '../utils/base';

export default modelExtend(base(Services), {
  namespace: 'sukien',
  effects: {
    // *get({ payload }, { call, put, select }) {
    //   const danhSach = [
    //     {
    //       'ngayBatDau': '2020-01-07T07:00:00.013Z',
    //       'ngayKetThuc': '2020-01-07T09:00:00.235Z',
    //       'tenSuKien': 'Ăn chiều',
    //       '_id': '5e1409dd1e7446003d8094b4',
    //       'maSuKien': '5e1409dd1e7446003d8094b3',
    //       'createdAt': '2020-01-07T04:32:29.277Z',
    //       'updatedAt': '2020-01-07T04:34:47.826Z',
    //       '__v': 0,
    //       'diaDiem': '',
    //       'ghiChu': ''
    //     },
    //     {
    //       'ngayBatDau': '2020-01-07T04:00:00.177Z',
    //       'ngayKetThuc': '2020-01-07T06:00:00.177Z',
    //       'tenSuKien': 'Kỉ niệm thành lập trường',
    //       '_id': '5e1401121e7446003d8094b0',
    //       'maSuKien': '5e1401121e7446003d8094af',
    //       'createdAt': '2020-01-07T03:54:58.503Z',
    //       'updatedAt': '2020-01-07T04:33:18.722Z',
    //       '__v': 0,
    //       'diaDiem': '',
    //       'ghiChu': ''
    //     },
    //     {
    //       'ngayBatDau': '2020-01-06T03:23:22.364Z',
    //       'ngayKetThuc': '2020-01-06T06:22:00.217Z',
    //       'tenSuKien': 'Ngày hội đọc sách',
    //       '_id': '5e13f9a81e7446003d8094a9',
    //       'diaDiem': 'Sân trường',
    //       'maSuKien': '5e13f9a81e7446003d8094a8',
    //       'createdAt': '2020-01-07T03:23:20.830Z',
    //       'updatedAt': '2020-01-07T03:24:04.172Z',
    //       '__v': 0,
    //       'ghiChu': ''
    //     },
    //     {
    //       'ngayBatDau': '2020-01-05T06:05:00.973Z',
    //       'ngayKetThuc': '2020-01-05T07:00:02.740Z',
    //       'tenSuKien': 'thi văn nghệ',
    //       '_id': '5e12be541e7446003d80944d',
    //       'diaDiem': 'nhà hát lớn',
    //       'ghiChu': 'mọi người nhớ mặc đồng phục',
    //       'maSuKien': '5e12be541e7446003d80944c',
    //       'createdAt': '2020-01-06T04:57:56.796Z',
    //       'updatedAt': '2020-01-06T04:57:56.796Z',
    //       '__v': 0
    //     },
    //     {
    //       'ngayBatDau': '2020-01-09T04:31:38.330Z',
    //       'ngayKetThuc': '2020-01-01T05:33:36.642Z',
    //       'tenSuKien': 'họp giao ban',
    //       '_id': '5e12b8b01e7446003d809434',
    //       'maSuKien': '5e12b8b01e7446003d809433',
    //       'createdAt': '2020-01-06T04:33:52.209Z',
    //       'updatedAt': '2020-01-06T04:33:52.209Z',
    //       '__v': 0
    //     },
    //     {
    //       'ngayBatDau': '2020-01-02T04:25:26.357Z',
    //       'ngayKetThuc': '2019-12-01T05:25:52.231Z',
    //       'tenSuKien': 'họp giao ban',
    //       '_id': '5e12b6d91e7446003d80942f',
    //       'diaDiem': 'hội trường A2',
    //       'maSuKien': '5e12b6d91e7446003d80942e',
    //       'createdAt': '2020-01-06T04:26:01.598Z',
    //       'updatedAt': '2020-01-06T04:28:59.086Z',
    //       '__v': 0,
    //       'ghiChu': ''
    //     },
    //     {
    //       'ngayBatDau': '2020-01-31T06:00:00.000Z',
    //       'ngayKetThuc': '2020-01-31T08:00:00.000Z',
    //       'tenSuKien': 'Tổ chức hoạt động ngoại khóa',
    //       '_id': '5e0b1ea36e5c88003daca491',
    //       'diaDiem': 'Nhà văn hóa',
    //       'ghiChu': '',
    //       'maSuKien': '5e0b1ea36e5c88003daca48f',
    //       'kieuLapLai': 2,
    //       '__v': 0,
    //       'createdAt': '2019-12-31T10:10:43.694Z',
    //       'updatedAt': '2019-12-31T10:10:43.694Z'
    //     },
    //     {
    //       'ngayBatDau': '2020-01-14T00:00:00.000Z',
    //       'ngayKetThuc': '2020-01-14T02:00:00.000Z',
    //       'tenSuKien': 'Sinh hoạt chuyên môn',
    //       '_id': '5e0b1e816e5c88003daca47f',
    //       'diaDiem': 'Hội trường',
    //       'ghiChu': '',
    //       'maSuKien': '5e0b1e816e5c88003daca47c',
    //       'kieuLapLai': 1,
    //       '__v': 0,
    //       'createdAt': '2019-12-31T10:10:09.922Z',
    //       'updatedAt': '2019-12-31T10:10:09.922Z'
    //     },
    //     {
    //       'ngayBatDau': '2020-01-21T00:00:00.000Z',
    //       'ngayKetThuc': '2020-01-21T02:00:00.000Z',
    //       'tenSuKien': 'Sinh hoạt chuyên môn',
    //       '_id': '5e0b1e816e5c88003daca480',
    //       'diaDiem': 'Hội trường',
    //       'ghiChu': '',
    //       'maSuKien': '5e0b1e816e5c88003daca47c',
    //       'kieuLapLai': 1,
    //       '__v': 0,
    //       'createdAt': '2019-12-31T10:10:09.922Z',
    //       'updatedAt': '2019-12-31T10:10:09.922Z'
    //     },
    //     {
    //       'ngayBatDau': '2020-01-28T00:00:00.000Z',
    //       'ngayKetThuc': '2020-01-28T02:00:00.000Z',
    //       'tenSuKien': 'Sinh hoạt chuyên môn',
    //       '_id': '5e0b1e816e5c88003daca481',
    //       'diaDiem': 'Hội trường',
    //       'ghiChu': '',
    //       'maSuKien': '5e0b1e816e5c88003daca47c',
    //       'kieuLapLai': 1,
    //       '__v': 0,
    //       'createdAt': '2019-12-31T10:10:09.922Z',
    //       'updatedAt': '2019-12-31T10:10:09.922Z'
    //     },
    //     {
    //       'ngayBatDau': '2020-01-07T00:00:00.000Z',
    //       'ngayKetThuc': '2020-01-07T02:00:00.000Z',
    //       'tenSuKien': 'Sinh hoạt chuyên môn',
    //       '_id': '5e0b1e816e5c88003daca47e',
    //       'diaDiem': 'Hội trường',
    //       'ghiChu': '',
    //       'maSuKien': '5e0b1e816e5c88003daca47c',
    //       'kieuLapLai': 1,
    //       '__v': 0,
    //       'createdAt': '2019-12-31T10:10:09.921Z',
    //       'updatedAt': '2019-12-31T10:10:09.921Z'
    //     },
    //     {
    //       'ngayBatDau': '2020-02-01T00:00:00.000Z',
    //       'ngayKetThuc': '2020-02-01T02:00:00.000Z',
    //       'tenSuKien': 'Sinh hoạt chuyên môn',
    //       '_id': '5e085ac2a4c598003ef69c32',
    //       'diaDiem': 'Hội trường',
    //       'ghiChu': '',
    //       'maSuKien': '5e085ac2a4c598003ef69c2c',
    //       'kieuLapLai': 1,
    //       '__v': 0,
    //       'createdAt': '2019-12-29T07:50:26.032Z',
    //       'updatedAt': '2019-12-29T07:50:26.032Z'
    //     },
    //     {
    //       'ngayBatDau': '2020-01-04T00:00:00.000Z',
    //       'ngayKetThuc': '2020-01-04T02:00:00.000Z',
    //       'tenSuKien': 'Sinh hoạt chuyên môn',
    //       '_id': '5e085ac2a4c598003ef69c2e',
    //       'diaDiem': 'Hội trường',
    //       'ghiChu': '',
    //       'maSuKien': '5e085ac2a4c598003ef69c2c',
    //       'kieuLapLai': 1,
    //       '__v': 0,
    //       'createdAt': '2019-12-29T07:50:26.031Z',
    //       'updatedAt': '2019-12-29T07:50:26.031Z'
    //     },
    //     {
    //       'ngayBatDau': '2020-01-11T00:00:00.000Z',
    //       'ngayKetThuc': '2020-01-11T02:00:00.000Z',
    //       'tenSuKien': 'Sinh hoạt chuyên môn',
    //       '_id': '5e085ac2a4c598003ef69c2f',
    //       'diaDiem': 'Hội trường',
    //       'ghiChu': '',
    //       'maSuKien': '5e085ac2a4c598003ef69c2c',
    //       'kieuLapLai': 1,
    //       '__v': 0,
    //       'createdAt': '2019-12-29T07:50:26.031Z',
    //       'updatedAt': '2019-12-29T07:50:26.031Z'
    //     },
    //     {
    //       'ngayBatDau': '2020-01-18T00:00:00.000Z',
    //       'ngayKetThuc': '2020-01-18T02:00:00.000Z',
    //       'tenSuKien': 'Sinh hoạt chuyên môn',
    //       '_id': '5e085ac2a4c598003ef69c30',
    //       'diaDiem': 'Hội trường',
    //       'ghiChu': '',
    //       'maSuKien': '5e085ac2a4c598003ef69c2c',
    //       'kieuLapLai': 1,
    //       '__v': 0,
    //       'createdAt': '2019-12-29T07:50:26.031Z',
    //       'updatedAt': '2019-12-29T07:50:26.031Z'
    //     },
    //     {
    //       'ngayBatDau': '2020-01-25T00:00:00.000Z',
    //       'ngayKetThuc': '2020-01-25T02:00:00.000Z',
    //       'tenSuKien': 'Sinh hoạt chuyên môn',
    //       '_id': '5e085ac2a4c598003ef69c31',
    //       'diaDiem': 'Hội trường',
    //       'ghiChu': '',
    //       'maSuKien': '5e085ac2a4c598003ef69c2c',
    //       'kieuLapLai': 1,
    //       '__v': 0,
    //       'createdAt': '2019-12-29T07:50:26.031Z',
    //       'updatedAt': '2019-12-29T07:50:26.031Z'
    //     },
    //     {
    //       'ngayBatDau': '2020-01-26T06:28:04.268Z',
    //       'ngayKetThuc': '2020-01-26T07:28:04.269Z',
    //       'tenSuKien': 'Họp Ban giám hiệu',
    //       '_id': '5e08477da4c598003ef69c21',
    //       'diaDiem': 'Hội trường',
    //       'ghiChu': '',
    //       'maSuKien': '5e08477da4c598003ef69c1c',
    //       'kieuLapLai': 1,
    //       '__v': 0,
    //       'createdAt': '2019-12-29T06:28:13.377Z',
    //       'updatedAt': '2019-12-29T06:28:13.377Z'
    //     },
    //     {
    //       'ngayBatDau': '2020-01-05T06:28:04.268Z',
    //       'ngayKetThuc': '2020-01-05T07:28:04.269Z',
    //       'tenSuKien': 'Họp Ban giám hiệu',
    //       '_id': '5e08477da4c598003ef69c1e',
    //       'diaDiem': 'Hội trường',
    //       'ghiChu': '',
    //       'maSuKien': '5e08477da4c598003ef69c1c',
    //       'kieuLapLai': 1,
    //       '__v': 0,
    //       'createdAt': '2019-12-29T06:28:13.376Z',
    //       'updatedAt': '2019-12-29T06:28:13.376Z'
    //     },
    //     {
    //       'ngayBatDau': '2020-01-12T06:28:04.268Z',
    //       'ngayKetThuc': '2020-01-12T07:28:04.269Z',
    //       'tenSuKien': 'Họp Ban giám hiệu',
    //       '_id': '5e08477da4c598003ef69c1f',
    //       'diaDiem': 'Hội trường',
    //       'ghiChu': '',
    //       'maSuKien': '5e08477da4c598003ef69c1c',
    //       'kieuLapLai': 1,
    //       '__v': 0,
    //       'createdAt': '2019-12-29T06:28:13.376Z',
    //       'updatedAt': '2019-12-29T06:28:13.376Z'
    //     },
    //     {
    //       'ngayBatDau': '2020-01-19T06:28:04.268Z',
    //       'ngayKetThuc': '2020-01-19T07:28:04.269Z',
    //       'tenSuKien': 'Họp Ban giám hiệu',
    //       '_id': '5e08477da4c598003ef69c20',
    //       'diaDiem': 'Hội trường',
    //       'ghiChu': '',
    //       'maSuKien': '5e08477da4c598003ef69c1c',
    //       'kieuLapLai': 1,
    //       '__v': 0,
    //       'createdAt': '2019-12-29T06:28:13.376Z',
    //       'updatedAt': '2019-12-29T06:28:13.376Z'
    //     }
    //   ];
    //   yield put({
    //     type: 'changeState',
    //     payload: {
    //       danhSach
    //     },
    //   });
    // },
  }
})
