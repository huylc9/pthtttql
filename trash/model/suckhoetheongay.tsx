import modelExtend from 'dva-model-extend';
import _ from 'lodash';
import Services from '@/pages/ThongKeKhaiBaoSucKhoeTheoNgay/service';
import base from '@/utils/base';

export default modelExtend(base(Services), {
  state: {
    paging: {
      page: 1,
      limit: 10,
      cond: {},
    },
    ngayBatDau: new Date().getTime() - 604800000,
    ngayKetThuc: new Date().getTime(),
    khaiBao: true,
  },
  namespace: 'suckhoetheongay',

  effects: {
    // trong payload KHÔNG có ngaybdau và kthuc
    *get({ payload }, { call, put, select }) {
      let currentPayload = { ...payload };
      if (!payload) {
        const modelName = Services.name;
        currentPayload = yield select(state => state[modelName].paging);
      }

      const ngayBatDau = yield select(state => state[Services.name].ngayBatDau);
      const ngayKetThuc = yield select(state => state[Services.name].ngayKetThuc);
      const khaiBao = yield select(state => state[Services.name].khaiBao);
      const response = yield call(Services.getDB, { ...currentPayload, ngayBatDau, ngayKetThuc, khaiBao });
      yield put({
        type: 'changeState',
        payload: {
          danhSach: _.get(response, 'data.data', []),
          paging: currentPayload,
          total: _.get(response, 'data.total', 0),
        },
      });
    },
    *capNhatKhaiBao({ payload }, { call, put, select }) {
      const { khaiBao } = payload;
      yield put.resolve({
        type: 'changeState',
        payload: {
          khaiBao,
        },
      });
      yield put({
        type: 'getDB',
        payload: {
          khaiBao,
          page: 1,
          limit: 10,
        },
      });
    },
    // trong payload có ngaybdau và kthuc
    *getDB({ payload }, { call, put, select }) {
      const currrentState = yield select(state => state[Services.name]);
      const ngayBatDau = _.get(payload, 'ngayBatDau', currrentState.ngayBatDau);
      const ngayKetThuc = _.get(payload, 'ngayKetThuc', currrentState.ngayKetThuc);
      const page = _.get(payload, 'page', _.get(currrentState, 'paging.page', 1));
      const limit = _.get(payload, 'limit', _.get(currrentState, 'paging.limit', 10));

      const { khaiBao } = currrentState;

      const response = yield call(Services.getDB, { ngayBatDau, ngayKetThuc, page, limit, khaiBao });

      yield put({
        type: 'changeState',
        payload: {
          danhSach: _.get(response, 'data.data', []),
          paging: {
            page, limit,
          },
          total: _.get(response, 'data.total', 0),
        },
      });
    },
  },
});
