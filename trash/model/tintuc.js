import modelExtend from 'dva-model-extend';
import Services from '@/services/tintuc';
import base from '../utils/base';

export default modelExtend(base(Services), {
    namespace: 'tintuc',
    state: {
        modal: false,
        thongTinChiTiet: {},
    }
})
