import modelExtend from 'dva-model-extend';
import Services from '@/pages/MotCua/BangDiem/service';
import base from '../utils/base';
import notificationAlert from '@/components/Notification';
import _ from 'lodash';

export default modelExtend(base(Services), {
    namespace: 'bangdiem3',
    state: {
    },
    effects: {
        *get({ payload }, { call, put, select }) {
            let currentPayload = { ...payload };
            if (!payload) {
                currentPayload = yield select(state => state.bangdiem3.paging);
            }
            currentPayload.cond = {
                ...currentPayload.cond,
                trangThai: 'DA_XU_LY',
            }
            const response = yield call(Services.get, currentPayload);
            yield put({
                type: 'changeState',
                payload: {
                    danhSach: _.get(response, 'data.data', []),
                    paging: currentPayload,
                    total: _.get(response, 'data.total', 0),
                },
            });
        },
    },
});
