import modelExtend from 'dva-model-extend';
import Services from '@/services/khoa';
import base from '../utils/base';

export default modelExtend(base(Services), {
  namespace: 'khoa',
})
