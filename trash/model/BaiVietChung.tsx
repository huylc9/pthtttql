import modelExtend from 'dva-model-extend';
import _ from 'lodash';
import Services from '@/pages/ThongTinChung/BaiVietChung/service';
import base from '../utils/base';

export default modelExtend(base(Services), {
    namespace: 'baivietchung',
})