import modelExtend from 'dva-model-extend';
import _ from 'lodash';
import Services from '@/services/lop';
import base from '../utils/base';
import { getAuthority } from '@/utils/authority';

export default modelExtend(base(Services), {
    namespace: 'lop',
    state: {
        thongTinDiemDanh: [],
        lopDeChon: [],
        danhSachLopGv: [],
        visible: false,
    },
    effects: {
        *getLopGv({ payload }, { call, put }) {
            const response = yield call(Services.getLopGv, payload);
            yield put({
                type: 'changeState',
                payload: {
                    danhSachLopGv: _.get(response, 'data.data', []),
                    total: _.get(response, 'data.total', 0),
                },
            });
        },
        *getById({ payload }, { call, put }) {
            const response = yield call(Services.getById, payload);
            yield put({
                type: 'changeState',
                payload: {
                    record: _.get(response, 'data.data', []),

                },
            });
        },
        *getDiemDanh({ payload }, { call, put }) {
            const response = yield call(Services.getDiemDanh, payload);
            yield put({
                type: 'changeState',
                payload: {
                    thongTinDiemDanh: _.get(response, 'data.data', []),
                },
            });
        },
        *getLopDeChon({ payload }, { call, put }) {
            const response = yield call(Services.getLopDeChon, payload);
            yield put({
                type: 'changeState',
                payload: {
                    lopDeChon: _.get(response, 'data.data', []),
                },
            });
        },
    }
})
