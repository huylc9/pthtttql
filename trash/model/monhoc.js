import modelExtend from 'dva-model-extend';
import Services from '@/services/monhoc';
import base from '../utils/base';

export default modelExtend(base(Services), {
    namespace: 'monhoc',
})
