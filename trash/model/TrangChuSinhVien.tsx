import { lichHoc, diemDanh, tinTuc } from '@/pages/TrangChu/TrangChuSinhVien/services';
import _ from 'lodash';

export default {
  state: {
    lichHoc: [],
    diemDanh: [],
    tinTuc: [],
  },
  namespace: 'dashboardgvsv',
  effects: {
    *lichHoc({ payload }, { call, put }) {
      const response = yield call(lichHoc);
      yield put({
        type: 'changeState',
        payload: {
          lichHoc: _.get(response, 'data.data', []),
        },
      });
    },
    *diemDanh({ payload }, { call, put }) {
      const response = yield call(diemDanh);
      yield put({
        type: 'changeState',
        payload: {
          diemDanh: _.get(response, 'data.data', []),
        },
      });
    },
    *tinTuc({ payload }, { call, put }) {
      const response = yield call(tinTuc);
      yield put({
        type: 'changeState',
        payload: {
          tinTuc: _.get(response, 'data.data', []),
        },
      });
    },
  },
  reducers: {
    changeState(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },
};
