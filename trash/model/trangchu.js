import modelExtend from 'dva-model-extend';
import _ from 'lodash';
import { formatMessage } from 'umi/locale';
import Services from '@/services/trangchu';
import notificationAlert from '@/components/Notification';

import base from '../utils/base';

export default modelExtend(base(Services), {
  namespace: 'trangchu',
  state: {
    gvKhoa: [],
    slLop: 0,
    slKhaiBaoSucKhoe: [],
    slKhaiBaoCachLy: [],
  },
  effects: {
    *getGVTheoKhoa({ payload }, { call, put, select }) {
      const response = yield call(Services.getGVTheoKhoa);
      yield put({
        type: 'changeState',
        payload: {
          gvKhoa: _.get(response, 'data.data', []),
        },
      });
    },
    *getSLLop({ payload }, { call, put, select }) {
      const response = yield call(Services.getSLLop);
      // console.log(response, "gvKhoa");
      yield put({
        type: 'changeState',
        payload: {
          slLop: _.get(response, 'data.data', []),
        },
      });
    },
    *getSLKhaiBaoSucKhoe({ payload }, { call, put, select }) {
      const response = yield call(Services.getSLKhaiBaoSucKhoe, payload);
      yield put({
        type: 'changeState',
        payload: {
          slKhaiBaoSucKhoe: _.get(response, 'data.data', []),
        },
      });
    },

    *getSLKhaiBaoCachLy({ payload }, { call, put, select }) {
      const response = yield call(Services.getSLKhaiBaoCachLy, payload);
      yield put({
        type: 'changeState',
        payload: {
          slKhaiBaoCachLy: _.get(response, 'data.data', []),
        },
      });
    },
  },
});
