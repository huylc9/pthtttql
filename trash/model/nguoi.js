import modelExtend from 'dva-model-extend';
import notificationAlert from '@/components/Notification';
import Services from '@/services/nguoi';
import base from '../utils/base';

// export interface INguoiCoCong {
//   _id: string;
//   hoTen: string;
//   soHoSo: string;
//   LoaiHoSo: string;
//   diaBan: string;
//   namSinh: number;
//   gioiTinh: number;
//   CMND: {
//     so: string,
//     ngayCap: string,
//     noiCap: string,
//   };
//   nguyenQuan: string;
//   hoKhauThuongTru: string;
//   doiTuong: number;
// }

export default modelExtend(base(Services), {
  namespace: 'nguoi',
});
