import modelExtend from 'dva-model-extend';
import _ from 'lodash';
import Services from '@/services/thoikhoabieu';
import base from '../utils/base';
import notificationAlert from '@/components/Notification';
import { formatMessage } from 'umi/locale';

export default modelExtend(base(Services), {
    namespace: 'thoikhoabieu',
    state: {
        visible: false, // dung cho xem thong tin
        visibleDiemDanh: false, // dung cho diem danh
        thongTinDiemDanh: []
    },
    effects: {
        *get({ payload }, { call, put, select }) {
            const response = yield call(Services.get);
            yield put({
                type: 'changeState',
                payload: {
                    danhSach: _.get(response, 'data.data', []),
                },
            });
        },
        *getLop({ payload }, { call, put }) {
            // Sử dụng để điểm danh
            const response = yield call(Services.getLop, payload);
            yield put({
                type: 'changeState',
                payload: {
                    thongTinDiemDanh: response.data.data,
                }
            })
        },
        *putDiemDanh({ payload }, { call, put }) {
            yield call(Services.putDiemDanh, payload);
            notificationAlert('success', formatMessage({ id: 'DIEM_DANH_THANH_CONG' }));
            yield put({
                type: 'changeState',
                payload: {
                    visibleDiemDanh: false
                },
            })
        }
    }
})
