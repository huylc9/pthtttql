import modelExtend from 'dva-model-extend';
import _ from 'lodash';
import base from '@/utils/base';
import Services from '@/pages/DashBoard/service';

export default modelExtend(base(Services), {
  namespace: 'dashboard',
  state: {
    danhSach1: [],
    nguoiDung: {},
    danhSachSucKhoe: [],
    danhSachCachLy: [],
    thongBao: {},
    phanHoi: {},
    giangVienTheoKhoa: [],
    sinhVienTheoKhoa: [],
  },
  effects: {
    *getNguoiDung({ payload }, { call, put, select }) {
      const response = yield call(Services.getNguoiDung, payload);
      yield put({
        type: 'changeState',
        payload: {
          nguoiDung: _.get(response, 'data.data', []),
        },
      });
    },
    *getGiangVien({ payload }, { call, put, select }) {
      const response = yield call(Services.getGiangVien, payload);
      yield put({
        type: 'changeState',
        payload: {
          giangVienTheoKhoa: _.get(response, 'data.data', []),

        },
      });
    },
    *getTaiKhoanKhach({ payload }, { call, put, select }) {
      const response = yield call(Services.getGiangVien, payload);
      yield put({
        type: 'changeState',
        payload: {
          taiKhoanKhach: _.get(response, 'data.data', []),

        },
      });
    },
    *getSinhVien({ payload }, { call, put, select }) {
      const response = yield call(Services.getSinhVien, payload);
      yield put({
        type: 'changeState',
        payload: {
          sinhVienTheoKhoa: _.get(response, 'data.data', []),
        },
      });
    },
    *getThongBao({ payload }, { call, put, select }) {
      const response = yield call(Services.getThongBao, payload);
      yield put({
        type: 'changeState',
        payload: {
          thongBao: _.get(response, 'data.data', []),
        },
      });
    },
    *getPhanHoi({ payload }, { call, put, select }) {
      const response = yield call(Services.getPhanHoi, payload);
      yield put({
        type: 'changeState',
        payload: {
          phanHoi: _.get(response, 'data.data', []),
        },
      });
    },

    *getKhaiBaoSucKhoeTheoNgay({ payload }, { call, put, select }) {
      const response = yield call(Services.getKhaiBaoSucKhoeTheoNgay, payload);
      yield put({
        type: 'changeState',
        payload: {
          danhSach1: _.get(response, 'data.data', []),
        },
      });
    },
    *getKhaiBaoCachLyTheoNgay({ payload }, { call, put, select }) {
      const response = yield call(Services.getKhaiBaoCachLyTheoNgay, payload);
      yield put({
        type: 'changeState',
        payload: {
          danhSach2: _.get(response, 'data.data', []),
        },
      });
    },
    *getKhaiBaoSucKhoe({ payload }, { call, put, select }) {
      const response = yield call(Services.getPhanHoi, payload);
      yield put({
        type: 'changeState',
        payload: {
          danhSach3: _.get(response, 'data.data', []),
        },
      });
    },
    *getKhaiBaoCachLy({ payload }, { call, put, select }) {
      const response = yield call(Services.getPhanHoi, payload);
      yield put({
        type: 'changeState',
        payload: {
          danhSach4: _.get(response, 'data.data', []),
        },
      });
    },
    *getDanhSachKhaiBaoSucKhoe({ payload }, { call, put, select }) {
      const response = yield call(Services.getDanhSachKhaiBaoSucKhoe, payload);
      yield put({
        type: 'changeState',
        payload: {
          danhSachSucKhoe: _.get(response, 'data.data', []),
        },
      });
    },
    *getDanhSachKhaiBaoCachLly({ payload }, { call, put, select }) {
      const response = yield call(Services.getDanhSachKhaiBaoSucKhoe, payload);
      yield put({
        type: 'changeState',
        payload: {
          danhSachCachLy: _.get(response, 'data.data', []),
        },
      });
    },
  },
});
